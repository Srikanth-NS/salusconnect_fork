#!/usr/bin/python

import os, sys

from subprocess import Popen
from datetime import datetime

def executeShellCommand(cmd): 
	p = Popen(cmd, shell=True)
	status = p.wait()

	if status != 0:
		print "Command returned %(status)d: %(command)s" %\
				{'status': status, 'command': cmd}
		sys.exit(1)

def scpToServer(target, address, location, pem, isFolder):
	options = "-q "
	if isFolder:
		options = "-rq "

	cmd = "scp "  + options

	if (pem != False):
		cmd = cmd + "-i " + pem + " "
	

	cmd = cmd + target + " " + address + ":" + location;

	executeShellCommand(cmd)

	print("Copying " + target + " to sever " + address)


if len(sys.argv) < 3:
	print "\n Please Provide Both Arguments. \n\n Execute file as ./tools/deployAssets.py <username@serverAddress> <basePathOnServer> \n"
	sys.exit(1)

if len(sys.argv) > 3 :
	pemKey = sys.argv[3]
else:
	pemKey = False

basePath = sys.argv[2]
serverAddress = sys.argv[1]

scpToServer("web/output/js", serverAddress, basePath, pemKey, True) 
scpToServer("web/output/css", serverAddress, basePath, pemKey, True)
scpToServer("web/output/translations", serverAddress, basePath, pemKey, True)
scpToServer("web/output/images", serverAddress, basePath, pemKey, True)
scpToServer("web/output/fonts", serverAddress, basePath, pemKey, True)
scpToServer("web/output/consumer.html", serverAddress, basePath, pemKey, False)
scpToServer("web/output/favicon.ico", serverAddress, basePath, pemKey, False)

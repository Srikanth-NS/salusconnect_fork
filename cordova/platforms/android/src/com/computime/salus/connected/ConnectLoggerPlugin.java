package com.computime.salus.connected;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConnectLoggerPlugin extends CordovaPlugin {

    private File currentLogFile;
    private String currentLogDate;
    private String simpleDateFormat = "yyyy-MM-dd";
    /**
     * Overwritten cordova methods
     **/

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        this.initLogs();
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if(action.equals("writeToLog")) {
            writeToLog(args);
            return true;
        } else if (action.equals("sendErrorLogs")){
            emailLogToSupport();
            return true;
        }

        return false;
    }
    /**
     * plug in methods
     **/

    private void initLogs () {
        String dateString = currentLogDate = new SimpleDateFormat(simpleDateFormat).format(new Date());
        File appDirectory = new File( Environment.getExternalStorageDirectory() + "/salusConnect" );
        File logDirectory = new File( appDirectory + "/log" );
        File logFile = new File( logDirectory, "salusConnectErrorLog_" + dateString + ".txt" );

        // create app folder
        if ( !appDirectory.exists() ) {
            appDirectory.mkdir();
        }

        // create log folder
        if ( !logDirectory.exists() ) {
            logDirectory.mkdir();
        }

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.currentLogFile = logFile;
    }

    private void emailLogToSupport () {
        Context context = this.cordova.getActivity().getApplicationContext();

        // send email using an intent
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Android Crash Report");
        Uri uri = Uri.fromFile(new File(currentLogFile.getPath()));
        intent.putExtra(android.content.Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            this.cordova.startActivityForResult(this, Intent.createChooser(intent, "Send mail..."), 0);
        } catch (Exception e) {
            Toast.makeText(context, "Error starting email client, please try again later.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    private void writeToLog (JSONArray args) {
        try {
            String dateString = new SimpleDateFormat(simpleDateFormat).format(new Date());

            if (args.length() >= 2) {
                if (!dateString.equals(currentLogDate)) {
                    //if it's a new day, delete old log file
                    if (currentLogFile != null) {
                        currentLogFile.delete();
                    }
                    this.initLogs();
                }

                String msgType = args.getString(0);
                String msgBody = args.getJSONObject(1).getString("0");

                FileWriter filew = new FileWriter(currentLogFile.getAbsolutePath(), true);
                BufferedWriter writer = new BufferedWriter(filew);

                writer.write("Type - " + msgType + ": " + msgBody);
                writer.newLine();
                writer.flush();
                writer.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

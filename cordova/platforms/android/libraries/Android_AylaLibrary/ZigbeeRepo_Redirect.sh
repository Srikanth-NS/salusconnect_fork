#! /bin/bash

# Establish soft link files under library dir, redirect the whole Android_AylaZigbeeLibrary repo.

# Change here to align your local ZigbeeRepo dir.
export ZigbeeRepo_Path=~/Desktop/Android_AylaZigbeeLibrary

# Relative Path here so DO NOT move this .sh file somewhere else.
export zigbee_dst=./lib/src/com/aylanetworks/aaml/zigbee
export zigbee_test_dst=./test/aAMLUnitTest/src/com/aylanetworks/AMLUnitTest/zigbeeTest

rm -rf $zigbee_dst
ln -s $ZigbeeRepo_Path/zigbee $zigbee_dst      

rm -rf $zigbee_test_dst
ln -s $ZigbeeRepo_Path/zigbeeTest $zigbee_test_dst




//
//  AylaDeviceGateway.java
//  Android_AylaLibrary
//
//  Created by Dan Myers on 7/28/14.
//  Copyright (c) 2014 AylaNetworks. All rights reserved.
//

package com.aylanetworks.aaml;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

//------------------------------------- AylaGateway --------------------------
class AylaDeviceGatewayContainer {
	@Expose
	AylaDeviceGateway gateway;
}

public class AylaDeviceGateway extends AylaDevice  {

	// gateway properties retrievable from the service
	@Deprecated
	public String gatewayType;		// @deprecated
	@Expose
	public AylaDeviceNode node;		// current node;
	@Expose
	public AylaDeviceNode[] nodes;	// nodes associated with this gateway

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");

		result.append(this.getClass().getName() + " Object {" + NEW_LINE);
		result.append(" productName: " + productName + NEW_LINE);
		result.append(" model: " + model + NEW_LINE);
		result.append(" dsn: " + dsn + NEW_LINE );
		result.append(" oemModel: " + oemModel + NEW_LINE);
		result.append(" connectedAt: " + connectedAt + NEW_LINE);
		result.append(" mac: " + mac + NEW_LINE);
		result.append(" lanIp: " + lanIp + NEW_LINE);
		result.append(" templateId: " + templateId + NEW_LINE);
		result.append(" registrationType: " + registrationType + NEW_LINE);
		result.append(" setupToken: " + setupToken + NEW_LINE );
		result.append(" registrationToken: " + registrationToken + NEW_LINE);
		result.append("}");
		System.out.println(result);
		return result.toString();
	}

	// Node registration pass-through methods
	public AylaRestService openRegistrationJoinWindow(Handler mHandle, Map<String, String> callParams) {
		return AylaRegistration.openRegistrationWindow(mHandle, this, callParams);
	}

	public AylaRestService getRegistrationCandidates(Handler mHandle, Map<String, String> callParams) {
		return AylaRegistration.getCandidates(mHandle, this);
	}

	public AylaRestService registerCandidate(Handler mHandle, AylaDeviceNode node) {
		return AylaRegistration.registerCandidate(mHandle, node);
	}

	public AylaRestService closeRegistrationJoinWindow(Handler mHandle, Map<String, String> callParams) {
		return AylaRegistration.closeRegistrationWindow(mHandle, this);
	}


	// ---------------------- Helper Methods --------------------------------------------------

	/*
	 * TODO: this assume as long as zigbee package is available,
	 * the gateway must be a zigbee gateway, as we only have zigbee 
	 * gateway at the moment. As generic gateway support comes in
	 * the assumption does not stand. Need to rework on this.
	 * */
	public boolean isZigbeeGateway() {
		ClassLoader loader = AylaCommProxy.class.getClassLoader();
		Class zigbeeClass = null;
		try {
			zigbeeClass = loader.loadClass(AylaCommProxy.kAylaCommProxyZigbeeGW);
		} catch (Exception e) {
		}

		return zigbeeClass != null && zigbeeClass.isInstance(this);
	}

	// --------------------- Node pass-through methods -----------------------------------------
	/**
	 * These class methods get one or more gateway nodes from the Ayla Cloud Service. If the application has been LAN Mode enabled,
	 * the nodes are read from cache, rather than the Ayla field service.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param AylaDevice is the owner gateway device of the nodes
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService getNodes(Map<String, String> callParams) {
		return AylaDeviceNode.getNodes(this, callParams);
	}
	public AylaRestService getNodes(Handler mHandle, Map<String, String> callParams) {
		return AylaDeviceNode.getNodes(mHandle, this, callParams);
	}
	public AylaRestService getNodes(Handler mHandle, Map<String, String> callParams, Boolean delayExecution) {
		return AylaDeviceNode.getNodes(mHandle, this, callParams, delayExecution);
	}

	public AylaRestService getNodeProperties(Map<String, String> callParams, AylaDeviceNode requestNode) {
		return getNodeProperties(null, callParams, requestNode,  false);
	}
	public AylaRestService getNodeProperties(Handler mHandle, Map<String, String> callParams, AylaDeviceNode requestNode) {
		return getNodeProperties(mHandle, callParams, requestNode,  false);
	}
	public AylaRestService getNodeProperties(Handler mHandle, Map<String, String> callParams, AylaDeviceNode requestNode,  Boolean delayExecution) {
		AylaDeviceNode node = this.findNode(requestNode.dsn)!= null? this.findNode(requestNode.dsn) : requestNode;
		//TODO: add a check if node is null
		AylaRestService rs = AylaDeviceNode.getProperties(mHandle, node, callParams, delayExecution);
		return rs;
	}
	
	public AylaRestService createNodeDatapoint(AylaDatapoint datapoint, AylaDeviceNode requestNode, AylaProperty requestProperty) {
		return createNodeDatapoint(null, datapoint, requestNode, requestProperty, false);
	}
	public AylaRestService createNodeDatapoint(Handler mHandle, AylaDatapoint datapoint, AylaDeviceNode requestNode, AylaProperty requestProperty) {
		return createNodeDatapoint(mHandle, datapoint, requestNode, requestProperty, false);
	}
	public AylaRestService createNodeDatapoint(Handler mHandle, AylaDatapoint datapoint, AylaDeviceNode requestNode, AylaProperty requestProperty, Boolean delayExecution) {
		AylaDeviceNode node = this.findNode(requestNode.dsn)!= null? this.findNode(requestNode.dsn) : requestNode;
		AylaProperty property = node.findProperty(requestProperty.name)!=null? node.findProperty(requestProperty.name): requestProperty;
		//TODO: add a check if node or property is null
		AylaRestService rs = property.createDatapoint(mHandle, datapoint, delayExecution);
		return rs;
	}
	
	public AylaRestService getNodeDatapointsByActivity(Map<String, String> callParams, AylaDeviceNode requestNode, AylaProperty requestProperty) {
		return getNodeDatapointsByActivity(null, callParams, requestNode, requestProperty, false);
	}
	public AylaRestService getNodeDatapointsByActivity(Handler mHandle, Map<String, String> callParams, AylaDeviceNode requestNode, AylaProperty requestProperty) {
		return getNodeDatapointsByActivity(mHandle, callParams, requestNode, requestProperty, false);
	}
	public AylaRestService getNodeDatapointsByActivity(Handler mHandle, Map<String, String> callParams, AylaDeviceNode requestNode, AylaProperty requestProperty, Boolean delayExecution) {
		AylaDeviceNode node = this.findNode(requestNode.dsn)!= null? this.findNode(requestNode.dsn) : requestNode;
		AylaProperty property = node.findProperty(requestProperty.name)!=null? node.findProperty(requestProperty.name): requestProperty;
		//TODO: add a check if node or property is null
		AylaRestService rs = property.getDatapointsByActivity(mHandle, callParams, delayExecution);
		return rs;
	}
	
	//TODO: inner non-static class object has a reference on the outer class object, need to split it apart. 
	public class AylaConnectionStatus {
		@Expose
		public String dsn;
		@Expose
		public String mac;
		@Expose
		public String status;
	}

	public AylaRestService getNodesConnectionStatus(Handler mHandle) {
		return getNodesConnectionStatus(mHandle, null);
	}
	@SuppressLint("HandlerLeak")	// OK
	public AylaRestService getNodesConnectionStatus(Handler mHandle, Map<String, String> callParams) {
		
		final AylaRestService rsConnectionStatus = new AylaRestService(mHandle, "getNodesConnectionStatus", AylaRestService.GET_NODES_CONNECTION_STATUS);

		try {
			AylaDeviceNode.getNodes( new Handler() {
				public void handleMessage(Message msg) {
					String jsonResults = (String)msg.obj;
					if (msg.what == AylaNetworks.AML_ERROR_OK) {
						int count = 0;
						AylaDeviceNode[] nodes = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode[].class);
						AylaConnectionStatus[] connectionStatus = new AylaConnectionStatus[nodes.length];
						for (AylaDeviceNode node : nodes) {
							if ( !TextUtils.isEmpty(node.dsn) &&
								 !TextUtils.isEmpty(node.connectionStatus) )
							{
								connectionStatus[count] = new AylaConnectionStatus();
								connectionStatus[count].dsn = node.dsn;
								connectionStatus[count].mac = node.mac;
								connectionStatus[count].status = node.connectionStatus;
								count++;
							} else  {
								saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "AylaDeviceGateway", "DSN", node.dsn,
										  "connectionStatus", node.connectionStatus, "getNodesConnectionStatus");
							}
						}
						String jsonConnectionStatus = AylaSystemUtils.gson.toJson(connectionStatus, AylaConnectionStatus[].class);
						
						saveToLog("%s, %s, %s:%s, %s", "I", "AylaGateway", "count", count, "getNodesConnectionStatus");
						returnToMainActivity(rsConnectionStatus, jsonConnectionStatus, 200 , 0, false);
					} else {
						saveToLog("%s, %s, %s:%d, %s, %s", "E", "AylaDeviceGateway", "error", msg.arg1, msg.obj, "getNodesConnectionStatus");
						returnToMainActivity(rsConnectionStatus, jsonResults, msg.arg1, AylaRestService.GET_REGISTERED_NODES, false);
					}
				}		
			}, this, callParams, false);
		} catch (Exception ex) {
			ex.printStackTrace();
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaDeviceGateway", "error", ex.getLocalizedMessage(), "getNodesConnectionStatus");
			returnToMainActivity(rsConnectionStatus, "error", AML_GENERAL_EXCEPTION , AylaRestService.GET_REGISTERED_NODES, false);
		}
	
		return rsConnectionStatus;
	}

	//TODO: update nodes list from input Device. Now it only handles lan mode buffers
	protected void updateNodesFromDeviceList(AylaDevice[] devices, Boolean saveToCache) {
		HashMap<String, AylaDeviceNode> nodeMap = new HashMap<String, AylaDeviceNode>();
		for(AylaDevice device : devices) {
			if(device instanceof AylaDeviceNode) {
				AylaDeviceNode node = (AylaDeviceNode)device;
				if (TextUtils.equals(node.gatewayDsn, this.dsn)) {
					nodeMap.put(node.dsn, node);
				} 
			}
		}
		this.updateNodes(nodeMap, saveToCache);
	}
	
	protected void updateNodes(HashMap<String, AylaDeviceNode> nodeMap, Boolean saveToCache) {
		AylaDeviceNode[] nodes = nodeMap.values().toArray(new AylaDeviceNode[nodeMap.size()]);
		if(this.nodes == null) {
			this.nodes = nodes;
		}
	
		if(lanModeState != lanMode.DISABLED) {
			AylaDeviceNodeContainer[] nodeContainers = new AylaDeviceNodeContainer[nodes.length];
			for (int i=0; i<nodes.length; i++) {
				AylaDeviceNodeContainer container = new AylaDeviceNodeContainer();
				container.device = nodes[i];
				nodeContainers[i] = container;
			}
			String jsonNodeContainers = AylaSystemUtils.gson.toJson(nodeContainers, AylaDeviceNodeContainer[].class);
			AylaDeviceNode.lanModeEnable(nodes, jsonNodeContainers, this.dsn);	
		}
	}
	
	//lan mode methods
	@Override
	protected Integer lanModeWillSendEntity(AylaLanCommandEntity entity) {
		return 200;
	}
	
	@Override
	protected Integer updateProperty(String propertyName, String value) {
		property =	this.findProperty(propertyName);
		int status = 200;
		if (property != null) {
			property.value = value;
			//property.dataUpdatedAt = gmtFmt.format(new Date());
			property.updateDatapointFromProperty();
			property.lanModeEnable();
		}
		else {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", propertyName, "PropertyNotFound", "updateProperty");
			status = 404; // 404 Not Found
		}
		return status;
	}

	@Override
	protected Integer lanModeUpdateProperty(String propertyName, String value, Boolean toBeNotified) {
		int propertyUpdateStatus = this.updateProperty(propertyName, value);
		switch (propertyUpdateStatus) {
        case 404:
            return propertyUpdateStatus;
        default:
            break;
		}
		
		if(!TextUtils.isEmpty(propertyName)
				&& AylaCommProxy.isGatewayAttributeProperty(propertyName) ){
			
			String[] propertyNames = AylaCommProxy.updateNodeWithGWAttr(this, propertyName, value, toBeNotified);
			if (propertyNames == null) {
				return 400;
			}
		}
		
		AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "statusFromDevice", "updateReceived", "lanModeUpdateProperty");
		if(toBeNotified) {
			String response = "{\"type\":\"" + AML_NOTIFY_TYPE_PROPERTY + "\",\"dsn\":\"" + this.dsn +
					  "\",\"names\":[\"" + propertyName + "\"]}";
			AylaNotify.returnToMainActivity(null, response, propertyUpdateStatus, 0, true); // request originated from device
		}
		return 200;
	}
	
	@Override
	protected Integer lanModeUpdateProperty(String cmdIdStr, int status, String propertyName, String value, AylaRestService rs, Integer leftCount) {
		int propertyUpdateStatus = this.updateProperty(propertyName, value);
		
		if(!TextUtils.isEmpty(propertyName) 
				&& AylaCommProxy.isGatewayAttributeProperty(propertyName)) {
			AylaCommProxy.updateNodeWithGWAttr(this, propertyName, value, cmdIdStr!=null? false: true);
		}
		
		int updateStatus = propertyUpdateStatus;
		if (rs != null) {
			if (rs.RequestType == AylaRestService.GET_PROPERTIES_LANMODE) {
				if(status < 200 || status > 299 || propertyName == null){
					//TODO: do we really need to clear send queue here?
					AylaLanMode.clearSendQueue();
					AylaLanMode.clearCommandsOutstanding();
					updateStatus = 400;
					leftCount = 0;
				}
				else {
					updateStatus = 200;
				}
				if (leftCount == 0) { // wait for all to return
					
					AylaDevice endpoint = this.lanModeEdptFromDsn(rs.info);	
					
					if(endpoint != null && endpoint.properties != null) {
						String jsonProperties = AylaSystemUtils.gson.toJson(endpoint.properties,AylaProperty[].class);
						AylaProperty.returnToMainActivity(rs, jsonProperties, updateStatus, 0, false);
						saveToLog("%s, %s, %s:%s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "method", "lanModeUpdateProperty", "updateStatus", updateStatus, "count", leftCount, "GET_PROPERTIES_LANMODE");
					}
					else {
						saveToLog("%s, %s, %s:%s, %s:%s, %s:%s, %s", "E", "AylaLanMode", "method", "lanModeUpdateProperty", "invalid device info", rs.info!=null?rs.info:null, "count", leftCount, "GET_PROPERTIES_LANMODE");
					}
				}
				
			} else if (rs.RequestType == AylaRestService.GET_DATAPOINT_LANMODE) {
				saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "method", "GET_DATAPOINT_LANMODE_", "updateStatus", updateStatus, "Response_datapoint");
				if(updateStatus < 300) { //TODO: Now do not bother app to handle all failure updates. We can discuss 
					String jsonDatapoint = "[" + AylaSystemUtils.gson.toJson(property.datapoint,AylaDatapoint.class) + "]";
					AylaDatapoint.returnToMainActivity(rs, jsonDatapoint, updateStatus, 0);
				}
			} else {
				saveToLog("%s, %s, %s:%d, %s", "E", "AylaLanMode", "requestType_NotFound", rs.RequestType, "Response_datapoint");
				updateStatus = 404;
			}
		} else {
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "RestService", "NotFound", "Response_datapoint"); 
			updateStatus = 404;
		}

		return updateStatus;
	}	
	
	@Override
	protected AylaDevice lanModeEdptFromDsn(String dsn) {
		AylaDevice device = null;
		if(TextUtils.equals(this.dsn, dsn)) {
			device = this;
		}
		else {
			device = this.findNode(dsn);
		}
		return device;
	}
	
	
	//TODO: Add more to be changed values
	protected void updateNode(AylaDeviceNode node, AylaDeviceNode newNode) {
		
		node.connectionStatus = newNode.connectionStatus;
		node.swVersion = newNode.swVersion;
	}
	
	
	public AylaDeviceNode findNode(String dsn) {
		AylaDeviceNode node = null;
		if(this.nodes == null ||
			this.nodes.length == 0) {
			//no nodes found
		}
		else {
			for(AylaDeviceNode aNode :nodes) {
				if(TextUtils.equals(aNode.dsn, dsn)) {
					node = aNode;
					break;
				}
			}
		}
		return node;
	}
	
	public AylaDeviceNode findNodeWithMacAddress(String mac) {
		AylaDeviceNode node = null;
		
		if(TextUtils.isEmpty(mac)) {
			return node;
		}
		
		if(this.nodes != null &&
			this.nodes.length != 0) {
			for(AylaDeviceNode aNode :nodes) {
				if(TextUtils.equals(mac, aNode.mac)) {
					node = aNode;
					break;
				}
			}
		}
		return node;
	}
}






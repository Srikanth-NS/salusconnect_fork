//
//  AylaDeviceNode.java
//  Android_AylaLibrary
//
//  Created by Dan Myers on 7/28/14.
//  Copyright (c) 2014 AylaNetworks. All rights reserved.
//

package com.aylanetworks.aaml;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.aylanetworks.aaml.AylaLanMode.lanModeSession;
import com.aylanetworks.aaml.enums.CommandEntityBaseType;
import com.google.gson.annotations.Expose;

import android.os.Handler;
import android.text.TextUtils;


//------------------------------------- AylaGateway --------------------------
class AylaDeviceNodeContainer {
	@Expose
	AylaDeviceNode device;
}

public class AylaDeviceNode extends AylaDevice  {

	// node properties retrievable from the service
	@Deprecated
	public String nodeType;					// @Deprecated
	@Expose
	public String nodeDsn;					// The DSN for this node
	@Expose
	public String action;					//  The action last action attempted
	@Expose
	public String status;					// The status of the last action attempted
	@Expose
	public String errorCode;				// The error code of the last action attempted
	@Expose
	public String ackedAt;					// When the latest action was acknowledged  by an owner gateway
	@Expose
	public String gatewayDsn;				// Owner gateway DSN
	@Expose
	public String address;					// Node address

	@Expose
	public AylaDeviceGateway gateway;		// The current gateway that owns this node
	
	// derived property
	@Expose
	public AylaDeviceGateway[] gateways;	// The gateways that owns this node for developer usage
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");

		result.append(this.getClass().getName() + " Object {" + NEW_LINE);
		result.append(" productName: " + productName + NEW_LINE);
		result.append(" model: " + model + NEW_LINE);
		result.append(" dsn: " + dsn + NEW_LINE );
		result.append(" oemModel: " + oemModel + NEW_LINE);
		result.append(" connectedAt: " + connectedAt + NEW_LINE);
		result.append(" mac: " + mac + NEW_LINE);
		result.append(" lanIp: " + lanIp + NEW_LINE);
		result.append(" templateId: " + templateId + NEW_LINE);
		result.append(" registrationType: " + registrationType + NEW_LINE);
		result.append(" setupToken: " + setupToken + NEW_LINE );
		result.append(" registrationToken: " + registrationToken + NEW_LINE);
		result.append(" nodeType: " + nodeType + NEW_LINE);
		result.append(" nodeDsn: " + nodeDsn + NEW_LINE);
		result.append(" action: " + action + NEW_LINE);
		result.append(" status: " + status + NEW_LINE);
		result.append(" errorCode: " + errorCode + NEW_LINE);
		result.append(" ackedAt: " + ackedAt + NEW_LINE);
		result.append(" gatewayDsn: " + gatewayDsn + NEW_LINE);
		result.append("}");
		return result.toString();
	}

	public static String kAylaNodeParamIdentifyValue = "value";	
	public static String kAylaNodeParamIdentifyTime = "time";	
	public static String kAylaNodeParamIdentifyOn = "On";	
	public static String kAylaNodeParamIdentifyOff = "Off";
	public static String kAylaNodeParamIdentifyResult = "Result";
	
	// --------------------- Properties pass-through methods -----------------------------------------
	/**
	 * Gets all properties summary objects associated with the device from Ayla device Service. Use getProperties when ordering is not important.
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	//@override
		public AylaRestService getProperties() {	// sync, get all properties
			return AylaDeviceNode.getProperties(null, this, null, true);
		}
		public AylaRestService getProperties(Map<String, String> callParams) {	// sync, get some properties
			return AylaDeviceNode.getProperties(null, this, callParams, true);
		}
		public AylaRestService getProperties(Handler mHandle) {	// async, get all properties
			AylaRestService rs = AylaDeviceNode.getProperties(mHandle, this, null, false); 
			return rs;
		}
		public AylaRestService getProperties(Handler mHandle, Map<String, String> callParams) {	// async get some properties async
			AylaRestService rs = AylaDeviceNode.getProperties(mHandle, this, callParams, false); 
			return rs;
		}
		public AylaRestService getProperties(Handler mHandle, Boolean delayExecution) {	// async get all properties
			AylaRestService rs = AylaDeviceNode.getProperties(mHandle, this, null, delayExecution); 
			return rs;
		}
		public AylaRestService getProperties(Handler mHandle, Map<String, String> callParams, Boolean delayExecution) {	// async get some properties
			AylaRestService rs = AylaDeviceNode.getProperties(mHandle, this, callParams, delayExecution); 
			return rs;
		}
		
		public static AylaRestService getProperties(Handler mHandle, AylaDeviceNode device, Map<String, String> callParams, Boolean delayExecution) {	// async get some properties
			
			ArrayList<String> nPropertyNames = new ArrayList<String>();
			ArrayList<String> readFromCachePropertyNames = new ArrayList<String>();
			Boolean hasListChanges = true;
			
			AylaDeviceNode lanNode = null;
			if(device.properties == null) {
				device.initPropertiesFromCache();
			} 
			
			initPropertiesOwner(device);
			
			if(device.isLanModeActive()) {
				lanNode = (AylaDeviceNode)AylaLanMode.device.lanModeEdptFromDsn(device.dsn);
				//TODO: What if lanNode is null?
				if ( lanNode.properties==null || lanNode.properties.length == 0) {
					lanNode.mergeNewProperties(device.properties);
					
					/*
					 * TODO: fix the hasListChanges logic.  
					 * false is default value, it always works but add unnecessary workload sometimes.  
					 * 
					 * To put it simply, for a set of properties, if there is anyone not get true from 
					 * the isLanModeEnabledProperty(pn), then this is true.
					 * */
					hasListChanges = false;
				}
				
				if (lanNode.properties != null) {
					String namesWithSpace = callParams!=null? callParams.get("names"): null;
					String[] propertyNames = null;
					if(namesWithSpace != null) {
						propertyNames = namesWithSpace.split(" ");
						for(String propertyName : propertyNames) {
							if(lanNode.findProperty(propertyName) == null){
								hasListChanges = false;
								break;
							}
							if(lanNode.isLanModeEnabledProperty(propertyName)){
								nPropertyNames.add(propertyName);
							}
							else {
								readFromCachePropertyNames.add(propertyName);
							}
						}
					}
					else {
						for(AylaProperty property : lanNode.properties) {
							if(lanNode.isLanModeEnabledProperty(property.name)){
								nPropertyNames.add(property.name);
							}
							else {
								readFromCachePropertyNames.add(property.name);
							}
						}
					}
				}				
			}
			else {
				hasListChanges = false;
			}
			     
			if(hasListChanges) {
				//lan mode is active, and could be fetched through lan mode
				String[] names = nPropertyNames.toArray(new String[nPropertyNames.size()]);
				Boolean isFirst = true;
				String namesInString = "";
				for(String propertyName : names) {
					if(isFirst) {
						namesInString = propertyName;
						isFirst = false;
					}
					else {
						namesInString += " ";
						namesInString += propertyName;
					}
				}
				if(callParams == null) {callParams = new HashMap<String, String>();}
				callParams.put("names", namesInString);
			}
			
			//TODO: in lan mode, only return lan mode enabled properties. Need change
			AylaRestService rs = AylaProperty.getProperties(mHandle, device, callParams, delayExecution); 
			return rs;
		}
		
		// TODO: Is this particularly for Node?
		private static void initPropertiesOwner(AylaDevice device) {
			if (device == null || device.properties == null)
				return;
			
			for (int i=0; i<device.properties.length; i++)
				device.properties[i].owner = device.dsn;
		}
		
	public void initPropertiesFromCache() {
		String jsonProperties = AylaCache.get(AML_CACHE_PROPERTY, this.dsn);
		this.properties = AylaSystemUtils.gson.fromJson(jsonProperties, AylaProperty[].class);		
	}
		
	/**
	 * These class methods get one or more gateway nodes from the Ayla Cloud Service. If the application has been LAN Mode enabled,
	 * the nodes are read from cache, rather than the Ayla field service.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param AylaDevice is the owner gateway device of the nodes
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public static AylaRestService getNodes(AylaDevice device, Map<String, String> callParams) {
		return getNodes(null, device, callParams, true);
	}
	public static AylaRestService getNodes(Handler mHandle, AylaDevice device, Map<String, String> callParams) {
		return getNodes(mHandle, device, callParams, false);
	}
	public static AylaRestService getNodes(Handler mHandle, AylaDevice device, Map<String, String> callParams, Boolean delayExecution) {
		Number devKey = device.getKey().intValue();
		AylaRestService rs = null;
		String savedJsonNodeContainers = "";

		// read the nodes from storage, returns "" if no cached values
		savedJsonNodeContainers = AylaCache.get(AML_CACHE_NODE, device.dsn);
		

		// get nodes from the service
		if ( AylaReachability.isCloudServiceAvailable()) {
			if (AylaSystemUtils.slowConnection == AylaNetworks.YES) {
				if(!TextUtils.isEmpty(savedJsonNodeContainers)) {
					// Use cached values
					try {
						String jsonNodes = stripContainers(savedJsonNodeContainers, null);
						rs = new AylaRestService(mHandle, "GetNodesStorageLanMode", AylaRestService.GET_NODES_LANMODE);
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaNodes", "fromStorage", "true", "getNodesStorage");
						returnToMainActivity(rs, jsonNodes, 203, 0, delayExecution);
					} catch (Exception e) {
						AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaNodes", "exception", e.getCause(), "getNodesStorage_lanMode");
						e.printStackTrace();
					}			
				} else {
					// query field service
					// String url = "http://ads-dev.aylanetworks.com/apiv1/devices/<key>/nodes.json";
					String url = String.format("%s%s%s%s", AylaSystemUtils.deviceServiceBaseURL(), "devices/", devKey, "/nodes.json");
					rs = new AylaRestService(mHandle, url, AylaRestService.GET_REGISTERED_NODES, device.dsn);
					String delayedStr = (delayExecution == true) ? "true" : "false";
					saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaNodes", "url", url, "delayedExecution", delayedStr, "getNodesService");
					if (delayExecution == false) {
						rs.execute(); 
					}
				}
			} else {
				// query field service
				// String url = "http://ads-dev.aylanetworks.com/apiv1/devices/<key>/nodes.json";
				String url = String.format("%s%s%s%s", AylaSystemUtils.deviceServiceBaseURL(), "devices/", devKey, "/nodes.json");
				rs = new AylaRestService(mHandle, url, AylaRestService.GET_REGISTERED_NODES, device.dsn);
				String delayedStr = (delayExecution == true) ? "true" : "false";
				saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaNodes", "url", url, "delayedExecution", delayedStr, "getNodesService");
				if (delayExecution == false) {
					rs.execute(); 
				}
			}
		}

		// service is not reachable
		else 
			if (AylaReachability.isWiFiConnected(null) && !TextUtils.isEmpty(savedJsonNodeContainers))
			{
				// use cached values
				try {
					String jsonNodes = stripContainers(savedJsonNodeContainers, null);

					rs = new AylaRestService(mHandle, "GetNodesStorageLanMode", AylaRestService.GET_NODES_LANMODE);
					saveToLog("%s, %s, %s:%s, %s", "I", "AylaNodes", "fromStorage", "true", "getNodesStorage");
					returnToMainActivity(rs, jsonNodes, 203, 0, delayExecution);
				} catch (Exception e) {
					AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaNodes", "exception", e.getCause(), "getNodesStorage_lanMode");
					e.printStackTrace();
				}
			}

			// no nodes
			else { 
				if (AylaCache.cacheEnabled(AML_CACHE_NODE) == true && !TextUtils.isEmpty(savedJsonNodeContainers)) {
					// use cached values
					try {
						String jsonNodes = stripContainers(savedJsonNodeContainers, null);

						rs = new AylaRestService(mHandle, "GetNodesStorageLanMode", AylaRestService.GET_NODES_LANMODE);
						saveToLog("%s, %s, %s:%s, %s", "I", "AylaNodes", "fromStorage", "true", "getNodesStorage");
						returnToMainActivity(rs, jsonNodes, 203, 0, delayExecution);
					} catch (Exception e) {
						AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaNodes", "exception", e.getCause(), "getNodesStorage_lanMode");
						e.printStackTrace();
					}
				} else {
					rs = new AylaRestService(mHandle, "GetNodesStorageLanMode", AylaRestService.GET_NODES_LANMODE);
					saveToLog("%s, %s, %s:%s, %s", "I", "AylaNodes", "nodes", "null", "getNodes_notFound");
					returnToMainActivity(rs, null, 404, 0, delayExecution);
				}
			}
		return rs;
	}
	
	public static String stripContainers(String jsonNodeContainers, AylaRestService rs) throws Exception {
		int count = 0;
		String jsonNodes = "";

		try {
			AylaDeviceNodeContainer[] deviceContainers = AylaSystemUtils.gson.fromJson(jsonNodeContainers, AylaDeviceNodeContainer[].class);
			AylaDeviceNode[] nodes = new AylaDeviceNode[deviceContainers.length];
			for (AylaDeviceNodeContainer deviceContainer : deviceContainers) {
				nodes[count] = (AylaDeviceNode)deviceContainer.device;
				count++;
			}

			jsonNodes = AylaSystemUtils.gson.toJson(nodes,AylaDeviceNode[].class);
			
			//Use rs to check if this is from buffer or from cloud. If from buffer, skip udpate 
			if (rs != null && count > 0) {				
				AylaDeviceNode.lanModeEnable(nodes, jsonNodeContainers, rs.info);
			}
			AylaSystemUtils.saveToLog("%s %s %s:%s %s", "I", "Nodes", "count", count, "stripContainers");

			return jsonNodes;

		} catch (Exception e) {
			if (jsonNodeContainers == null) {
				jsonNodeContainers = "null";
			}
			AylaSystemUtils.saveToLog("%s %s %s:%s %s:%s %s", "E", "Nodes", "count", count, "jsonNodeContainers", jsonNodeContainers, "stripContainers");
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * Used to identify a node by blinking a light, making a sound, vibrating, etc
	 * 
	 * callParams
	 *   {"value":"On","time":"55"}
	 *       The key "value" may have a corresponding value of "On" or "Off"
	 *       The key "time" has a corresponding value from 0 to 255 in seconds, passed as a string
	 *   {"value":"Results"}
	 *   	The key "value" has a corresponding value of "Results"
	 *       
	 * Return
	 * 		AylaRestService for this call
	 * 
	 * Results
	 *     {"id":"12345"} for the "On" and "Off" option, or 
	 *     {"id":"on_0x123456789abc","status":"success"} for the "Result" option
	 *     
	 * Errors
	 * 		401 - Unauthorized
	 * 		404 - Node not found
	 * 		405 - Not supported for this node
	 */
	public AylaRestService identify(Map<String, String> callParams) {
		return identify(null, callParams);
	}
	public AylaRestService identify(Handler mHandle, Map<String, String> callParams) {
		// String url = "http://user.aylanetworks.com/users.json";
		String url = null;
		AylaRestService rs = null;
		String jsonUserValues = null;
		JSONObject userValues = new JSONObject();
		JSONObject errors = new JSONObject();
		String paramValue;

		try {
			if (callParams != null) {
				// test validity of mandatory objects
				paramValue = (String)callParams.get(kAylaNodeParamIdentifyValue);
				if (paramValue != null) {
					userValues.put(kAylaNodeParamIdentifyValue, paramValue);
					
					if (TextUtils.equals(paramValue, kAylaNodeParamIdentifyOn)) {
						paramValue = (String)callParams.get(kAylaNodeParamIdentifyTime);
						if (paramValue != null) {
							Integer timeInSeconds = Integer.parseInt(paramValue);
							if (timeInSeconds >= 0 && timeInSeconds <= 255) {
								userValues.put(kAylaNodeParamIdentifyTime, timeInSeconds);
							} else {
								errors.put(kAylaNodeParamIdentifyTime, "out of range");
							}
						} else {
							errors.put(kAylaNodeParamIdentifyTime, "can't be empty");
						}
					}
				} else {
					errors.put(kAylaNodeParamIdentifyValue, "can't be blank");
				}
					
			} else {
				errors.put(kAylaNodeParamIdentifyValue, "mandatory parameter missing");
			}

			// return if errors in required fields
			if(errors.length() != 0) {
				rs = new AylaRestService(mHandle, ERR_URL, AylaRestService.IDENTIFY_NODE);
				saveToLog("%s, %s, %s:%s, %s", "E", "AylaNodes", ERR_URL, errors.toString(), "identifyNode");
				returnToMainActivity(rs, errors.toString(), AML_USER_INVALID_PARAMETERS, 0, false);
				return rs;
			}

			// String url = "http://ads-dev.aylanetworks.com/apiv1/devices/<key>/identify.json";
			Number devKey = this.getKey().intValue();
			url = String.format("%s%s%s%s", AylaSystemUtils.deviceServiceBaseURL(), "devices/", devKey, "/identify.json");
			rs = new AylaRestService(mHandle, url, AylaRestService.IDENTIFY_NODE, this.dsn);
			jsonUserValues = userValues.toString();
			saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaNodes", "url", url, "userValues", jsonUserValues, "identifyNode");

			rs.setEntity(jsonUserValues);	
			if (mHandle != null) {
				rs.execute(); 
			}

			return rs;

		} catch (Exception e) {
			rs = new AylaRestService(mHandle, ERR_URL, AylaRestService.IDENTIFY_NODE);
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaNode", "exception", e.getCause(), "identifyNode");
			returnToMainActivity(rs, errors.toString(), AML_GENERAL_EXCEPTION, 0, false);
			return rs;
		}
	}

	// ----------------------------- Lan Mode Enable --------------------------------
	protected static void lanModeEnable(AylaDeviceNode[] nodes, String jsonNodeContainers, String gatewayDsn) {
		
		HashMap<String, AylaDeviceNode> nodeMap = new HashMap<String, AylaDeviceNode>();
		for(AylaDeviceNode device : nodes) {
			if(TextUtils.equals(device.gatewayDsn, gatewayDsn)) {
				nodeMap.put(device.dsn, device);
			} 
		}
		
		//Now only update current lan mode enabled device
		AylaDeviceGateway lanDevice = null; 
		if (AylaLanMode.device instanceof AylaDeviceGateway) {
			lanDevice = (AylaDeviceGateway)AylaLanMode.device;
		}
		if(lanDevice != null &&
			TextUtils.equals(lanDevice.dsn, gatewayDsn)) {
			
			if(lanDevice.nodes == null) {
				lanDevice.nodes = nodes;
			}
			else {	
				HashMap<String, AylaDeviceNode> lanNodeMap = new HashMap<String, AylaDeviceNode>();
				
				//current captured nodes
				for(AylaDeviceNode node : lanDevice.nodes) {
					if(nodeMap.containsKey(node.dsn)) {
						AylaDeviceNode nNode = nodeMap.get(node.dsn);
						lanDevice.updateNode(node, nNode);
						lanNodeMap.put(node.dsn, node);
					}
					else {
						//TODO: test only
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "AylaLanMode", node.dsn, "removedFromLanList", "updateProperty");
					}
				}
				
				//new nodes
				for(AylaDeviceNode node : nodes) {
					if(!lanNodeMap.containsKey(node.dsn)) {
						lanNodeMap.put(node.dsn, node);						
					}	
				}
				lanDevice.nodes = lanNodeMap.values().toArray(new AylaDeviceNode[lanNodeMap.size()]);
			}
			
			AylaCache.save(AML_CACHE_NODE, gatewayDsn, jsonNodeContainers);  
		}
	}

	@Override
	public boolean isLanModeActive() {
		if( AylaReachability.isDeviceLanModeAvailable() &&
			AylaLanMode.device != null &&
			TextUtils.equals(AylaLanMode.device.dsn, this.gatewayDsn) &&
			AylaLanMode.device.properties != null &&
			this.properties != null &&
			AylaLanMode.sessionState == lanModeSession.UP) {
			return true;
		}
		return false;
	}
	
	@Override
	public Integer lanModeUpdateProperty(String propertyName, String value, Boolean toBeNotified) {
		int propertyUpdateStatus = this.updateProperty(propertyName, value);
		AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "AylaLanMode", "updateReceived", propertyUpdateStatus, "Node.lanModeUpdateProperty");

		switch (propertyUpdateStatus) {
        case 404:
            return propertyUpdateStatus;
        default:
            break;
		}
		
		if(toBeNotified) {
			String response = "{\"type\":\"" + AML_NOTIFY_TYPE_NODE + "\",\"dsn\":\"" + this.dsn +
					"\",\"statusCode\":" + 200 + ",\"names\":[\"" + propertyName + "\"]}";
			AylaNotify.returnToMainActivity(null, response, propertyUpdateStatus, 0, true); // request originated from device
		}
		return 200;
	}
	
	@Override
	protected String lanModeToDeviceUpdate(AylaRestService rs, AylaProperty property, String value, int cmdId) {
		
		AylaDevice gateway = AylaLanMode.device;
		if(gateway == null || !(gateway instanceof AylaDeviceGateway)) {
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaDeviceNode", "gateway", "isInvalid", "lanModeToDeviceUpdate");
			return null;
		}
		String command = AylaCommProxy.zCmdToNode(this, property, value, cmdId+"");
		return command;
	}
	
	@Override	
	protected String lanModeToDeviceCmd(AylaRestService rs, String type, String uri, Object obj) {
		String cmd = "";
		if(TextUtils.equals(type, "GET")) {
			if(TextUtils.equals(uri, "datapoint.json")) {
				//GET property/datapoint.json
				AylaProperty property = (AylaProperty)obj;
				
				int cmdId = AylaLanMode.nextCommandOutstandingId(); // match up async response with request
				Integer cmdIdInteger = Integer.valueOf(cmdId);
				AylaCommand command = AylaCommProxy.getAylaCommand(this, property, null, cmdIdInteger.toString());
				String toDeviceString = null;
				if(command!= null && command.isValid()) {
					try {
						String cmdString = command.toGatewayGetPropertyCmdData();
                        String replyUri = serverPath + "/property/datapoint.json";
						toDeviceString = zCmdToLanModeDevice(type, AylaCommProxy.isZigBeeAvailable()?"attr_read_data":"", cmdString, replyUri, cmdId);
						
					}catch(Exception e) {
						AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaNodes", "exception", e.getCause(), "lanModeToDeviceCmd");
						e.printStackTrace();
					}
					if(toDeviceString != null) {
						AylaLanCommandEntity entity = new AylaLanCommandEntity(toDeviceString, cmdId, CommandEntityBaseType.AYLA_LAN_COMMAND);
						AylaLanMode.sendToLanModeDevice(entity, rs);
					}
						
				}
				else {
					saveToLog("%s, %s, %s:%s, %s", "E", "AylaDeviceNode", "Command", "can't be created", "lanModeToDeviceCmd");
				}
			}
		}
		return cmd;
	}
	
	
	@Override
	protected String lanModePropertyNameFromEdptPropertyName(String name) {
		return AylaCommProxy.getLanModePropertyName(name);
	}
	
	
// ------------------------------- Node Helper Methods ----------------------

	protected boolean isLanModeEnabledProperty(final String propertyName) {
		/*
		 * TODO: put property filtering logic for generic AylaDeviceNode here, existing implementation is for zigbee node. 
		 * 
		 * reference AylaDeviceZigbeeNode.isLanModeEnabledProperty(pn) for zigbee solution. 
		 * */
		if (TextUtils.isEmpty(propertyName)) {
			return false;
		}
		
		String values[] = propertyName.split("_");
		return (values.length == 4);
	}
}
	



//
//  AylaRestService.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 8/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//
package com.aylanetworks.aaml;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.ResultReceiver;

/**
 * 
 * Asynchronous Rest Service using Intent
 *
 */
public class AylaRestService {
	
	public AylaRestService() {
	}

	private ArrayList <AylaParcelableNVPair> params;
	private ArrayList <AylaParcelableNVPair> headers;

	String url;
	Handler mHandler;
	private Context mContext;
	public int RequestType;
	private String entity;
	
	public String jsonResults;
	public int responseCode;
	public int subTaskFailed;
	public String info;
	
	/**
	 * method/request identifiers: must be unique
	 */
	final static int GET_DEVICES = 100;
	final static int GET_DEVICE_DETAIL = 101;
	final static int GET_NEW_DEVICE_CONNECTED = 102;
	final static int GET_REGISTERED_NODES = 103;
	final static int GET_DEVICE_METADATA_BY_KEY = 105;
	final static int GET_DEVICE_METADATA = 106;
	final static int GET_PROPERTIES = 110;
	final static int GET_PROPERTY_DETAIL = 111;
	final static int GET_DATAPOINTS = 120;
	final static int GET_DATAPOINT_BLOB = 121;
	final static int GET_DATAPOINT_BLOB_SAVE_TO_FILE = 123;
	final static int GET_PROPERTY_TRIGGERS = 150;
	final static int GET_DEVICE_NOTIFICATIONS = 153;
	final static int GET_APPLICATION_TRIGGERS = 170;
	final static int GET_APP_NOTIFICATIONS = 173;
	final static int GET_REGISTRATION_CANDIDATE = 180;
	final static int GET_MODULE_REGISTRATION_TOKEN = 181;
	final static int GET_GATEWAY_REGISTRATION_CANDIDATES = 182;
	final static int GET_NEW_DEVICE_STATUS = 200; // module
	final static int GET_NEW_DEVICE_SCAN_RESULTS_FOR_APS = 201;
	final static int GET_MODULE_WIFI_STATUS = 202;
	final static int GET_NEW_DEVICE_PROFILES = 203;
	final static int GET_DEVICE_LANMODE_CONFIG = 220;
	final static int GET_USER_INFO = 230;
	final static int GET_USER_METADATA_BY_KEY = 231;
	final static int GET_USER_METADATA = 232;
	final static int GET_SCHEDULES = 240;
	final static int GET_SCHEDULE = 241;
	final static int GET_SCHEDULE_ACTIONS = 245; 
	final static int GET_SCHEDULE_ACTIONS_BY_NAME = 246;
	final static int GET_TIMEZONE = 247;
	final static int GET_USER_SHARE = 255;
	final static int GET_USER_SHARES = 256;
	final static int GET_USER_RECEIVED_SHARES = 258;
	final static int GET_USER_CONTACT = 260;
	final static int GET_USER_CONTACT_LIST = 261;

	final static int POST_USER_LOGIN = 500;
	final static int POST_USER_LOGOUT = 501;
	final static int POST_USER_SIGNUP = 502;
	final static int POST_USER_RESEND_CONFIRMATION = 503;
	final static int POST_USER_RESET_PASSWORD = 504;
	final static int POST_USER_REFRESH_ACCESS_TOKEN = 505;
	final static int PUT_RESET_PASSWORD_WITH_TOKEN = 506;
	final static int POST_USER_OAUTH_LOGIN = 507;
	final static int POST_USER_OAUTH_AUTHENTICATE_TO_SERVICE = 508;
	final static int CREATE_DATAPOINT = 520;
	final static int CREATE_DATAPOINT_BLOB = 521;
	final static int CREATE_DATAPOINT_BLOB_POST_TO_FILE = 523;
	final static int CREATE_PROPERTY_TRIGGER = 530;
	final static int CREATE_DEVICE_NOTIFICATION = 532;
	final static int CREATE_APPLICATION_TRIGGER = 540;
	final static int CREATE_APP_NOTIFICATION = 542;
	final static int START_NEW_DEVICE_SCAN_FOR_APS = 550;
	final static int REGISTER_DEVICE = 555;
	final static int SET_DEVICE_CONNECT_TO_NETWORK = 560; 			// connect module to service
	final static int POST_LOCAL_REGISTRATION = 570; 				// initiate a session with a new device
	final static int OPEN_REGISTRATION_WINDOW = 575;
	//final static int CREATE_SCHEDULE = 580; 						//temporary, not supported
	final static int CREATE_SCHEDULE_ACTION = 585;
	final static int CREATE_SCHEDULE_ACTIONS = 586;
	final static int CREATE_LOG_IN_SERVICE = 590;
	final static int CREATE_TIMEZONE = 595;
	final static int CREATE_DEVICE_METADATA = 600;
	final static int CREATE_USER_METADATA = 601;
	final static int CREATE_USER_SHARE = 605;
	final static int CREATE_USER_CONTACT = 606;

	final static int PUT_LOCAL_REGISTRATION = 700;
	final static int PUT_NEW_DEVICE_TIME = 701;
	final static int PUT_USER_CHANGE_PASSWORD = 710;
	final static int PUT_USER_CHANGE_INFO = 711;
	final static int PUT_DATAPOINT = 712;
	final static int PUT_USER_SIGNUP_CONFIRMATION = 713;
	final static int UPDATE_PROPERTY_TRIGGER = 714;
	final static int UPDATE_APPLICATION_TRIGGER = 715;
	final static int UPDATE_DEVICE_NOTIFICATION = 716;
	final static int UPDATE_APP_NOTIFICATION = 717; 
	final static int UPDATE_DEVICE = 720;
	final static int PUT_DEVICE_FACTORY_RESET = 722;
	final static int UPDATE_SCHEDULE = 730;
	final static int CLEAR_SCHEDULE = 731;
	final static int UPDATE_SCHEDULE_ACTION = 732;
	final static int UPDATE_TIMEZONE = 735;
	final static int UPDATE_DEVICE_METADATA = 740;
	final static int UPDATE_USER_METADATA = 741;
	final static int UPDATE_USER_SHARE = 745;
	final static int IDENTIFY_NODE = 750;
	final static int UPDATE_USER_CONTACT = 754;

	final static int DELETE = 800;
	final static int DESTROY_PROPERTY_TRIGGER = 810;
	final static int DESTROY_DEVICE_NOTIFICATION = 812;
	final static int DESTROY_APPLICATION_TRIGGER = 815;
	final static int DESTROY_APP_NOTIFICATION = 817;
	final static int UNREGISTER_DEVICE= 830;
	final static int DELETE_DEVICE_WIFI_PROFILE = 835;
	final static int DELETE_USER = 840;
	//final static int DELETE_SCHEDULE = 841; 							// temporary, not supported
	final static int DELETE_SCHEDULE_ACTION = 845;
	final static int DELETE_SCHEDULE_ACTIONS = 846;
	final static int DELETE_DEVICE_METADATA = 850;
	final static int DELETE_USER_METADATA = 851;
	final static int DELETE_USER_SHARE = 855;
	final static int DELETE_USER_CONTACT = 857;
	final static int BLOB_MARK_FETCHED = 858;							// Once marked fetched, one would never get the blob, equals to delete.
	final static int BLOB_MARK_FINISHED = 859;

	final static int REGISTER_NEW_DEVICE = 900; 						// compound call for GET_REGISTRATION_CANDIDATE, GET_MODULE_REGISTRATION_TOKEN, & REGISTER_DEVICE
	final static int CONNECT_TO_NEW_DEVICE = 905; 						// compound call
	final static int CONNECT_NEW_DEVICE_TO_SERVICE = 910; 				// compound call
	final static int CONFIRM_NEW_DEVICE_TO_SERVICE_CONNECTION = 915; 	// Don't send auth_token in the clear if secureSetup is false
	final static int GET_NEW_DEVICE_WIFI_STATUS = 920;
	final static int LOGIN_THROUGH_OAUTH = 930;							// compound call for AML_POST_OAUTH_PROVIDER_URL

	final static int NO_AUTH_TOKEN_ABOVE_THIS_ID = 1500;
	final static int RETURN_HOST_WIFI_STATE = 1500;
	final static int RETURN_HOST_SCAN = 1501; 							// return a wifi scan from the local device: phone/tablet/etc
	final static int RETURN_HOST_NETWORK_CONNECTION = 1502;
	final static int SET_HOST_NETWORK_CONNECTION = 1503;
	final static int DELETE_HOST_NETWORK_CONNECTION = 1504;
	final static int DELETE_HOST_NETWORK_CONNECTIONS = 1505;
	final static int RETURN_HOST_DNS_CHECK = 1506;
	
	// General callbacks for LAN Mode, device & service reachability, and push notifications
	final static int PROPERTY_CHANGE_NOTIFIER = 2000; 					// Don't send auth_token in the clear on id's > 2000
	final static int REACHABILITY = 2001;
	final static int PUSH_NOTIFICATION = 2002;
	
	 
//	final static int LAN_MODE_SESSION_ESTABLISHED = 2003;

	final static int DELETE_NETWORK_PROFILE_LANMODE = 2098;				// 
	final static int SEND_NETWORK_PROFILE_LANMODE = 2099;				// for secure wifi setup. 
	final static int GET_DEVICES_LANMODE = 2100;						// lan mode values. 
	final static int GET_DEVICE_DETAIL_LANMODE = 2101;
	final static int GET_NEW_DEVICE_CONNECTED_LANMODE = 2102;
	final static int GET_NODES_LANMODE = 2103;
	public final static int GET_PROPERTIES_LANMODE = 2110;
	final static int GET_PROPERTY_DETAIL_LANMODE = 2111;
	public final static int GET_DATAPOINT_LANMODE = 2120;
	final static int GET_DATAPOINTS_LANMODE = 2121;
	final static int GET_PROPERTY_TRIGGERS_LANMODE = 2150;
	final static int GET_APPLICATION_TRIGGERS_LANMODE = 2170;
	final static int GET_REGISTRATION_CANDIDATE_LANMODE = 2180;
	final static int GET_MODULE_REGISTRATION_TOKEN_LANMODE = 2181;
	final static int GET_NEW_DEVICE_STATUS_LANMODE = 2200; // module
	final static int GET_NEW_DEVICE_SCAN_RESULTS_FOR_APS_LANMODE = 2201;
	final static int GET_MODULE_WIFI_STATUS_LANMODE = 2202;
	final static int GET_NEW_DEVICE_PROFILES_LANMODE = 2203;
	

	final static int CREATE_DATAPOINT_LANMODE = 2520;
	
	// zigbee request ids
	public final static int CREATE_GROUP_ZIGBEE = 3000;
	public final static int CREATE_BINDING_ZIGBEE = 3005;
	public final static int CREATE_SCENE_ZIGBEE = 3010;
	
	public final static int UPDATE_GROUP_ZIGBEE = 3100;
	public final static int UPDATE_BINDING_ZIGBEE = 3105;
	public final static int UPDATE_SCENE_ZIGBEE = 3110;
	public final static int RECALL_SCENE_ZIGBEE = 3115;
	public final static int TRIGGER_GROUP_ZIGBEE = 3120;
	
	public final static int GET_GROUP_ZIGBEE = 3200;
	public final static int GET_BINDING_ZIGBEE = 3205;
	public final static int GET_SCENE_ZIGBEE = 3210;
	
	public final static int GET_GROUPS_ZIGBEE = 3300;
	public final static int GET_BINDINGS_ZIGBEE = 3305;
	public final static int GET_SCENES_ZIGBEE = 3310;
	
	public final static int DELETE_GROUP_ZIGBEE = 3400;
	public final static int DELETE_BINDING_ZIGBEE = 3405;
	public final static int DELETE_SCENE_ZIGBEE = 3410;
	
	public final static int GROUP_ACK_ZIGBEE = 3500;			// compound call
	public final static int BINDING_ACK_ZIGBEE = 3505;			// compound call
	public final static int SCENE_ACK_ZIGBEE = 3510;			// compound call
	
	public final static int GET_NODES_CONNECTION_STATUS = 3600;	// compound call

	
	
	// end method/request identifiers

	public AylaRestService(Handler mHandler, String url,int requestType) {
		this.mHandler = mHandler;
		this.mContext = AylaNetworks.appContext;
		this.url = url;
		this.RequestType = requestType;
		params = new ArrayList<AylaParcelableNVPair>();
		headers = new ArrayList<AylaParcelableNVPair>();
		// assume JSON
		this.addHeader("Accept", "application/json");
		this.addHeader("Content-Type","application/json");
		this.addHeader("Connection", "Keep-Alive");
		if (url.startsWith("https")) {
			this.addHeader("Authorization", AylaUser.user.getauthHeaderValue());
		}else {
			this.addHeader("Authorization", "none");
		}
	}
	
	public AylaRestService(Handler mHandler, String url,int requestType, String info) {
    	this(mHandler, url, requestType);
		this.info = info;
	}

	public void addParam(String name, String value){
		params.add(new AylaParcelableNVPair(name, value));
	}

	public void addHeader(String name, String value){
		headers.add(new AylaParcelableNVPair(name,value));
	}

	/**
	 * Sets the HTTPEntity. Override all params set for the request.
	 */
	public void setEntity(String entity){
		this.entity = entity;
	}

	/**
	 * createUrlArray - create a string of restful array params for appending to a given array
	 * @param arrayName - array variable name
	 * @param array - array values
	 * @return a string containing restful array parameters 
	 */
	protected static String createUrlArray(String arrayName, String[] array) {
		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(array));
		return createUrlArray(arrayName, arrayList);
	}
	protected static String createUrlArray(String arrayName, ArrayList<String> arrayList) {
		String urlArray = "";
		Boolean first = true;

		String firstFormat = String.format("?%s[]=", arrayName);
		String nextFormat  = String.format("&%s[]=", arrayName);

		for(String element : arrayList) {
			urlArray += (first?firstFormat:nextFormat) + element;
			first = false;
		}

		return urlArray;
	}
	
	/**
	 * Executes the REST request in an IntentService. 
	 * 
	 * The return string is provided in the HandleMessage method of the handler provided by
	 * the constructor. It will be present as the object field of the incoming message. 
	 */
	public Message execute()
	{
		Message message = null;

		ResultReceiver receiver;
		receiver = new ResultReceiver(mHandler){
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) {
				if (resultCode >= 200 && resultCode < 300) {
					mHandler.obtainMessage(AylaNetworks.AML_ERROR_OK,resultCode,0,resultData.getString("result")).sendToTarget();
				} else {
					mHandler.obtainMessage(AylaNetworks.AML_ERROR_FAIL,resultCode,resultData.getInt("subTask"),resultData.getString("result")).sendToTarget();	
				}
			}
		};
		
		if (mContext != null) {
			final Intent intent = new Intent(mContext, AylaExecuteRequest.class);
			intent.putParcelableArrayListExtra("headers", (ArrayList<? extends Parcelable>) headers);
			intent.putExtra("params", params);
			intent.putExtra("url", url);
			intent.putExtra("receiver", receiver);
			intent.putExtra("method", RequestType);
			intent.putExtra("entity", entity);
			
			if (mHandler != null) {
				// asynchronous request returned to mHandler callback
				intent.putExtra("async", true);
				intent.putExtra("result", this.jsonResults);
				intent.putExtra("subTask", this.subTaskFailed);
				intent.putExtra("responseCode", this.responseCode);
				intent.putExtra("info", this.info);
				
				// Start service if not started, send intent params to service
				mContext.startService(intent);	// ==> AylaExecuteRequest.onHandleIntent()
			} else {
				// syncrhonous request
				intent.putExtra("async", false);
				AylaRestService rs = this;
				
				try {
					AylaCallResponse callResponse;
					AylaExecuteRequest aylaExecuteRequest = new AylaExecuteRequest();

					callResponse = aylaExecuteRequest.handleIntent(intent, rs); // call setup in AylaExecuteRequest.handleIntent()
					
					int resultCode = callResponse.getResultCode();
					String resultData = callResponse.getBundle().getString("result");
					int resultSubTask =  callResponse.getBundle().getInt("subTask");
					
					if (callResponse.getResultCode() >= 200 && callResponse.getResultCode() < 300) {
						message =  Message.obtain(null, AylaNetworks.AML_ERROR_OK, resultCode, 0, resultData);
					} else {
						message =  Message.obtain(null, AylaNetworks.AML_ERROR_FAIL, resultCode, resultSubTask, resultData);	
					}
				} catch (Exception e) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaRestService", "status", AylaNetworks.AML_ERROR_FAIL, "execute");
					message =  Message.obtain(null, AylaNetworks.AML_ERROR_FAIL, AylaNetworks.AML_ERROR_FAIL, 0, e.getLocalizedMessage());	
				}
			}
		} else {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaRestService", "mContext", "null", "execute");
		}
		return message;
	}
}

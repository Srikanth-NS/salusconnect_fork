
/*
 * AylaLanCommandEntity.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 02/27/2015
 * Copyright (c) 2015 Ayla Networks. All Rights Reserved.
 * */




package com.aylanetworks.aaml;

import com.aylanetworks.aaml.enums.CommandEntityBaseType;



public class AylaLanCommandEntity {
	public String jsonStr;
	public int cmdId;
	public CommandEntityBaseType baseType;

	
	public AylaLanCommandEntity(){
		jsonStr = "";
		cmdId = 0;
		baseType = CommandEntityBaseType.AYLA_LAN_COMMAND;
	}
	
	public AylaLanCommandEntity(String json, int id, CommandEntityBaseType type) {
		jsonStr = json;
		cmdId = id;
		baseType = type;
	}
}// end of AylaLanCommandentity class   





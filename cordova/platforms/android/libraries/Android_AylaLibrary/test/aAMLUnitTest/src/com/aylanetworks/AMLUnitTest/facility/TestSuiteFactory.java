/*
 * TestSuiteFactory.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.facility;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import android.util.Log;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig.testModule;
import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig.testSuite;
import com.aylanetworks.AMLUnitTest.TestCasePool.AMLBlobUnitTest;
import com.aylanetworks.AMLUnitTest.TestCasePool.AMLContactsUnitTest;
import com.aylanetworks.AMLUnitTest.gatewayTest.AMLGenericGatewayUnitTestCaseServiceOnly;
import com.aylanetworks.AMLUnitTest.interfaces.AMLDefaultUnitTestCase;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;

/**
 * Factory pattern
 * 
 * Factory to produce the necessary stuff
 * */
public class TestSuiteFactory {

	private static final String tag = TestSuiteFactory.class.getSimpleName();

	private static final String AMLZigbeeUnitTestCaseLanMode = "com.aylanetworks.AMLUnitTest.zigbeeTest.AMLZigbeeUnitTestCaseLanMode";
	private static final String AMLZigbeeUnitTestCaseServiceOnly = "com.aylanetworks.AMLUnitTest.zigbeeTest.AMLZigbeeUnitTestCaseServiceOnly";

	private static final String AMLEVBUnitTestCaseLanMode = "com.aylanetworks.AMLUnitTest.evbTest.AMLEVBUnitTestCaseLanMode";
	private static final String AMLEVBUnitTestCaseServiceOnly = "com.aylanetworks.AMLUnitTest.evbTest.AMLEVBUnitTestCaseServiceOnly";

	private static final String AMLEVBSetupRegistrationUnitTestCase = "com.aylanetworks.AMLUnitTest.evbTest.AMLEVBSetupRegistrationUnitTestCase";

	// Singleton producer
	private static TestSuiteFactory mInstance = null;
	// private static ITestResultDisplayable mScreen = null;
	private ITestResultDisplayable mScreen = null;

	public static synchronized TestSuiteFactory getInstance() {
		if (mInstance == null) {
			mInstance = new TestSuiteFactory(null);
		}

		return mInstance;
	}// end of getInstance

	public static synchronized TestSuiteFactory getInstance(
			ITestResultDisplayable screen) {
		if (mInstance == null) {
			mInstance = new TestSuiteFactory(screen);
		}

		return mInstance;
	}

	private TestSuiteFactory(ITestResultDisplayable screen) {
		mScreen = screen;
	}

	/**
	 * @param screen
	 *            null would load DefaultScreen
	 * */
	public void setScreenDisplayer(ITestResultDisplayable screen) {
		mScreen = screen;
	}

	/**
	 * Firstly check testModule, if it is testModule.NOT_APPLICABLE, check testSuite.
	 * 
	 * */
	public IUnitTestable produceTestSuite(final testSuite ts, final testModule tm) {
		
		switch (tm) {
		case AML_CONTACT:
			return new AMLContactsUnitTest(mScreen);
		case AML_BLOB:
			// Not tested this yet.
			return new AMLBlobUnitTest(mScreen);
		default:
			Log.d(tag, "testModule not applicable, check testSuite prameter.");
		}
		
		switch (ts) {
		case GENERIC_GATEWAY_SERVICE_ONLY:
			// Assume they are always in the core repo, if we need to split, rework on this.
			return new AMLGenericGatewayUnitTestCaseServiceOnly(mScreen);
		case GATEWAY_SERVICE_ONLY:
			return getZigbeeTestCase(false);
		case GATEWAY_LAN_MODE_ONLY:
			return getZigbeeTestCase(true);
		case LAN_MODE_ONLY:
			return getEVBTestCase(true);
		case DEVICE_SERVICE_ONLY:
			return getEVBTestCase(false);
		case SETUP_AND_REGISTRATION_ONLY:
		case SETUP_ONLY:
		/* TODO:
		 * Implement SETUP_ONLY, SETUP_AND_REGISTRATION_ONLY respectively, testSuite constructor with param. 
		 */
			return getEVBSetupRegistrationTestCase();
		case GENERIC_GATEWAY_LAN_MODE_ONLY:
		default:
//			Log.e(tag,
//					"SETUP_ONLY, SETUP_AND_REGISTRATION_ONLY, SETUP_REGISTRATION_AND_DEVICE_SERVICE to be implemented.");
			Log.w(tag, "Test Suite not implemented for now.");
			return new AMLDefaultUnitTestCase();
		}
	}// end of produceTestCase

	private IUnitTestable getZigbeeTestCase(final boolean isLanMode) {
		IUnitTestable result = new AMLDefaultUnitTestCase();

		try {
			ClassLoader loader = TestSuiteFactory.class.getClassLoader();
			Class c = null;
			if (isLanMode) {
				c = loader.loadClass(AMLZigbeeUnitTestCaseLanMode);
				// TODO: setup proper tag for mScreen using c
			} else {
				c = loader.loadClass(AMLZigbeeUnitTestCaseServiceOnly);
				// TODO: setup proper tag for mScreen using c
			}

			if (c != null) {
				result = (IUnitTestable) TestSuiteFactory.getInstanceForClass(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (mScreen == null) {
			result.setResultScreen(new DefaultScreen());
		} else {
			result.setResultScreen(mScreen);
		}
		return result;
	}// end of getZigbeeTestCase

	private IUnitTestable getEVBTestCase(final boolean isLanMode) {
		IUnitTestable result = new AMLDefaultUnitTestCase();

		try {
			ClassLoader loader = TestSuiteFactory.class.getClassLoader();
			Class c = null;
			if (isLanMode) {
				c = loader.loadClass(AMLEVBUnitTestCaseLanMode);
				// TODO: setup proper tag for mScreen using c
			} else {
				c = loader.loadClass(AMLEVBUnitTestCaseServiceOnly);
				// TODO: setup proper tag for mScreen using c
			}

			if (c != null) {
				result = (IUnitTestable) TestSuiteFactory.getInstanceForClass(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (mScreen == null) {
			result.setResultScreen(new DefaultScreen());
		} else {
			result.setResultScreen(mScreen);
		}
		return result;
	}// end of getEVBTestCase

	
	// TODO: add some params to enable test cases pending, to allow
	// SETUP_ONLY, SETUP_AND_REGISTRATION, SETUP_AND_REGISTRATION_DEVICE_SERVICE
	private IUnitTestable getEVBSetupRegistrationTestCase() {
		IUnitTestable result = new AMLDefaultUnitTestCase();

		try {
			ClassLoader loader = TestSuiteFactory.class.getClassLoader();
			Class c = null;
			c = loader.loadClass(AMLEVBSetupRegistrationUnitTestCase);
			// TODO: setup proper tag for mScreen using c
			if (c != null) {
				result = (IUnitTestable) TestSuiteFactory.getInstanceForClass(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (mScreen == null) {
			result.setResultScreen(new DefaultScreen());
		} else {
			result.setResultScreen(mScreen);
		}
		return result;
	}// end of getEVBSetupRegistrationTestCase
	
	
	

	private static Object getInstanceForClass(Class clazz) throws Exception {
		/*
		 * If multiple constructors
		 * */
//		Constructor[] cs = clazz.getDeclaredConstructors();
//		Constructor c = null;
//		for (int i=0; i<cs.length; i++) {
//			c = cs[i];
//			if (c.getGenericParameterTypes().length == 0) {
//				break;
//			}
//		}
//		
//		c.setAccessible(true);
//		return c.newInstance();
		
		return clazz.newInstance();
		
	}// end of getClassinstance

	
	
}// end of TestSuiteFactory class












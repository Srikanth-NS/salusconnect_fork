/*
 * AMLDefaultUnitTestCase.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.interfaces;


/**
 * Proxy like class with empty implementation, in case destination class is missing.
 * */
public class AMLDefaultUnitTestCase 
implements IUnitTestable{

	private final String tag = AMLDefaultUnitTestCase.class.getSimpleName();
	private ITestResultDisplayable mResultScreen = null;
	
	private Runnable mEndHandler = null;
	
	@Override
	public void onResume() {
		mResultScreen = new DefaultScreen();
		mResultScreen.displayPassMessage(tag + "setup().");
	}

	@Override
	public void start() {
		if (mResultScreen == null) {
			onResume();
		}
		mResultScreen.displayPassMessage(tag + "start().");
	}

	@Override
	public void onPause() {
		if (mResultScreen == null) {
			onResume();
		}
		mResultScreen.displayPassMessage(tag + "tearDown().");
	}
	
	@Override
	public void onDestroy() {
		mResultScreen = null;
	}

	@Override
	public void setResultScreen(ITestResultDisplayable screen) {
		mResultScreen = screen;
	}

	@Override
	public boolean isRunning() {
		return false;
	}

	@Override
	public void onEnd() {
		mResultScreen.displayPassMessage(tag + "onEnd().");
		if (mEndHandler != null) {
			mEndHandler.run();
		}
	}

	@Override
	public void setEndHandler(Runnable r) {
		mEndHandler = r;
	}
}// end of AMLZigbeeUnitTest class        





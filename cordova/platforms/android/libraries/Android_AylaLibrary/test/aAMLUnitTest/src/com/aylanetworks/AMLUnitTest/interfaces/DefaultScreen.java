/*
 * DefaultScreen.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.interfaces;




import android.text.TextUtils;
import android.util.Log;

public class DefaultScreen implements ITestResultDisplayable {

	private String mTag = DefaultScreen.class.getSimpleName();
	
	@Override
	public void displayProgressInfo(String msg) {
		Log.i(mTag, msg+"");
	}

	@Override
	public void displayFailMessage(String msg) {
		Log.e(mTag, msg+"");
	}

	@Override
	public void displayPassMessage(String msg) {
		Log.d(mTag, msg+"");
	}

	@Override
	public void setTag(String tag) {
		if (!TextUtils.isEmpty(tag)) {
			mTag = tag;
		}
	}

	@Override
	public void init() {
		
	}

}// end of DefaultScreen class      








/*
 * AMLUnitTest.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/05/2015.
 * Copyright (c) 2015 Ayla Networks. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest;


import com.aylanetworks.AMLUnitTest.facility.TestSuiteFactory;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaCache;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaSystemUtils;

import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class AMLUnitTest 
extends Activity 

implements ITestResultDisplayable
, OnClickListener
{
	private static String tag = AMLUnitTest.class.getSimpleName();
	
	TextView tv_test1 = null;
	TextView tv_test2 = null;
	TextView tv_test3 = null;
	
	Button mButton1 = null;
	Button mButton2 = null;
	Button mButton3 = null;
	
	Button mBtnReset = null;
	
	private int mNumIteration;
	
	private IUnitTestable mTestSuite = null;

	
	// TODO: setup an interface in AylaNetworks to define library configuration, encapsulating these configuration variables.
	// AylaSystemUtils should not appear here. 
	private void setUserSettings() {
		
		// must be called once in onCreate() for library initialization
		AylaNetworks.init(this, AMLUnitTestConfig.gblAmlDeviceSsidRegex, AMLUnitTestConfig.appId); 

//		AylaSystemUtils.serviceType = AylaNetworks.AML_STAGING_SERVICE; // use the development service. Default is AML_DEVICE_SERVICE DNS service based djm

		AylaSystemUtils.serviceType = AylaNetworks.AML_DEVELOPMENT_SERVICE; // use the development service. Default is AML_DEVICE_SERVICE DNS service based
		AylaSystemUtils.loggingLevel = AylaNetworks.AML_LOGGING_LEVEL_INFO; // support for debugging. Default is AML_LOGGING_LEVEL_ERROR
		AylaSystemUtils.saveCurrentSettings();
		
		// TODO: Not sure how to hide this, maybe we need to leave it as it is.
		AylaCache.clearAll();
	}// end of setUserSettings       
	

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.testrest);
		tv_test1 = (TextView) findViewById(R.id.t_query1); //Setup Views and Button for response
		tv_test1.setMovementMethod(new ScrollingMovementMethod());
		tv_test2 = (TextView) findViewById(R.id.t_query2); //Setup Views and Button for response
		tv_test2.setMovementMethod(new ScrollingMovementMethod());
		tv_test3 = (TextView) findViewById(R.id.t_query3); //Setup Views and Button for response
		tv_test3.setMovementMethod(new ScrollingMovementMethod());

		//--------------------- Begin Unit test ------------------------
		
		setUserSettings();
		try {
			// TODO: Replace with AylaLogUtils.saveToLog once the library refactor is done. 
			String versionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "D", "aAMLUnitTest", "version", versionName, "onCreate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mTestSuite = TestSuiteFactory.getInstance(this).produceTestSuite(AMLUnitTestConfig.amlTestSuite, AMLUnitTestConfig.amlTestModule);
		
		// kick off the tests by pressing button1
		mButton1 = (Button)findViewById(R.id.button1);
		mButton1.setOnClickListener(this);
		mButton1.setEnabled(true);
		
		mBtnReset = (Button)this.findViewById(R.id.btnRest);
		mBtnReset.setOnClickListener(this);
		mBtnReset.setEnabled(true);
		
		mButton2 = (Button)this.findViewById(R.id.button2);
		mButton2.setOnClickListener(this);
		mButton2.setEnabled(false);
		
		mButton3 = (Button)this.findViewById(R.id.button3);
		mButton3.setOnClickListener(this);
		mButton3.setEnabled(false);
	}// end of onCreate        


	@Override
	protected void onResume() {
		super.onResume();

		initUnitTest();
	}// end of onResume   
	
	
	private void initUnitTest() {
		AylaNetworks.onResume();
		
		if (mTestSuite==null) {
			mTestSuite = TestSuiteFactory.getInstance(this)
					.produceTestSuite(AMLUnitTestConfig.amlTestSuite, AMLUnitTestConfig.amlTestModule);
		}
		
		mNumIteration = 0;
		mButton2.setText(R.string.button2_text);
		
		// kick off the tests automatically
		if (AMLUnitTestConfig.amlRunMode == AMLUnitTestConfig.runMode.AUTO) {
			mTestSuite.onResume();
			mTestSuite.start(); // Kick off the tests
		}
	}// end of initUnitTest       
	

	@Override
	protected void onPause() {
		super.onPause();
		
		AylaNetworks.onPause(false);
		
		if (mTestSuite!=null) {
			mTestSuite.onPause();
		}
	}// end of onPause                           
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if (mTestSuite!=null) {
			mTestSuite.onDestroy();
			mTestSuite = null;
		}
	}// end of onDestroy                

	
	@Override
	public void displayProgressInfo(final String msg) {
		
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				tv_test1.append(msg);
			}// end of run
			
		});
	}// end of displayProgressInfo


	@Override
	public void displayFailMessage(final String msg) {
		
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				tv_test2.append(msg);
			}// end of run
			
		});
	}// end of displayFailMessage


	@Override
	public void displayPassMessage(final String msg) {
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				tv_test3.append(msg);
			}// end of run
			
		});
	}// end of displayPassMessage

	
	@Override
	public void setTag(String t) {
		if (!TextUtils.isEmpty(t)) {
			tag = t;
		}
	}// end of setTag   


	@Override
	public void init() {
		// Assuming UI xml binding is done correctly.
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
//				if (tv_test1 == null) {
//					tv_test1 = (TextView)AMLUnitTest.this.findViewById(R.id.t_query1);
//				}
				tv_test1.setText("");
				
//				if (tv_test2 == null) {
//					tv_test2 = (TextView)AMLUnitTest.this.findViewById(R.id.t_query2);
//				}
				tv_test2.setText("");
				
//				if (tv_test3 == null) {
//					tv_test3 = (TextView)AMLUnitTest.this.findViewById(R.id.t_query3);
//				}
				tv_test3.setText("");
			}// end of run
			
		});
	}// end of init
	
	
	@Override
	public void onClick(View v) {
		if (v == mBtnReset) {
//			onResume();
			initUnitTest();
		}
		if (v == mButton1) {
			runTestSuite();
			return;
		}// end of mButton1 handler
		else if (v == mButton2) {
			return;
		} 
		else if (v == mButton3) {
			return;
		}
	}// end of onClick
	
	
	private void runTestSuite() {
		if ( mTestSuite == null) {
			mTestSuite = TestSuiteFactory.getInstance(this)
					.produceTestSuite(AMLUnitTestConfig.amlTestSuite, AMLUnitTestConfig.amlTestModule);
		}
		if ( mTestSuite.isRunning() ) {
			return;
		}
		if (mNumIteration >= AMLUnitTestConfig.testIterations) {
			return;
		}
		
		mNumIteration++;
		mButton2.setText("Iteration: " + mNumIteration + "\\" + AMLUnitTestConfig.testIterations);
		mTestSuite.setEndHandler(r);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				
				synchronized (mTestSuite) {
					mTestSuite.onResume();
					mTestSuite.start();
				}
				Looper.loop();
			}
		}).start();
	}// end of runTestSuite
	
	
	private final Runnable r = new Runnable(){
		@Override
		public void run() {
			runTestSuite();
		}
	};
		
}// end of AMLUnitTest class       












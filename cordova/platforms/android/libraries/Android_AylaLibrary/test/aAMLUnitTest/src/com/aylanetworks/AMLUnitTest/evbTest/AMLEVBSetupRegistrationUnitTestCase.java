/*
 * AMLEVBSetupRegistrationUnitTestCase.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.evbTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig;
import com.aylanetworks.AMLUnitTest.facility.Test;
import com.aylanetworks.AMLUnitTest.facility.TestSequencer;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaDevice;
import com.aylanetworks.aaml.AylaHost;
import com.aylanetworks.aaml.AylaHostNetworkConnection;
import com.aylanetworks.aaml.AylaHostScanResults;
import com.aylanetworks.aaml.AylaLanMode;
import com.aylanetworks.aaml.AylaModule;
import com.aylanetworks.aaml.AylaModuleScanResults;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaRestService;
import com.aylanetworks.aaml.AylaSetup;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.aylanetworks.aaml.AylaUser;
import com.aylanetworks.aaml.AylaWiFiConnectHistory;
import com.aylanetworks.aaml.AylaWiFiStatus;

@SuppressLint({ "HandlerLeak", "DefaultLocale" })
public class AMLEVBSetupRegistrationUnitTestCase implements IUnitTestable {

	private static final String tag 
		= AMLEVBSetupRegistrationUnitTestCase.class.getSimpleName();
	private ITestResultDisplayable mScreen = null;            
	
	private TestSequencer testSequencer = null;
	private Runnable mEndHandler = null;
	
	// Copy/Paste from AMLUnitTest.java   
	AylaUser gblAmlTestUser = null;
	AylaRestService restService1 = null;
	AylaDevice gblAmlTestDevice = null;
			
	int numberOfTests = 0; // total number of tests
	
	// Copy/Paste ends here
	
	@Override
	public void onResume() {
		if (mScreen == null) {
			mScreen = new DefaultScreen();
		}
		mScreen.init();
//		AylaNetworks.onResume();
		
		initTestSuite();
	}// end of onResume   

	
	@Override
	public void start() {
		if (testSequencer == null) {
			onResume();
		}
		testSequencer.nextTest().execute();
	}// end of start

	
	@Override
	public void onPause() {
		if (testSequencer == null) {
			onResume();
		}
		
	}// end of onPause

	
	@Override
	public void onEnd(){
		if (mEndHandler !=null) {
			mEndHandler.run();
		}
	}
	
	@Override
	public void onDestroy() {
		
		AylaNetworks.onDestroy();
		//TODO: GC work done here.
		testSequencer = null;
	}// end of onDestroy    
	
	
	@Override
	public void setResultScreen(ITestResultDisplayable screen) {
		if (screen == null) {
			mScreen = new DefaultScreen();
		} else {
			mScreen = screen;
		}
	}// end of setResultScreen     
	
	
	@Override
	public void setEndHandler(Runnable r) {
		mEndHandler = r;
	}
	
	
	@Override
	public boolean isRunning() {
		if (testSequencer != null) {
			return testSequencer.isTestSuiteRunning();
		}
		return false;
	}
	
	
	// TODO: Add some params to implement setup/setupEnd/registration/registrationEnd combination.
	// For now, add all test list, comment it out manually when necessary.     
	private void initTestSuite() {
		List<Test> setupAndRegistrationTestList = new ArrayList<Test>(settingsTestList);
		
		setupAndRegistrationTestList.addAll(setupStartTestList);
		setupAndRegistrationTestList.addAll(registrationStartTestList);
   
		setupAndRegistrationTestList.addAll(setupEndTestList);
		setupAndRegistrationTestList.addAll(registrationEndTestList);  
		
		setupAndRegistrationTestList.addAll(endList);
		
		testSequencer = new TestSequencer(setupAndRegistrationTestList);           
		
		//TODO: get Statistics and show in amlTestStatusMsg            
		numberOfTests = testSequencer.numberOfTests();
	}// end of initTestSuite     
	
	
	private List<Test> settingsTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestNothing(); }
			}));
	
	private List<Test> setupStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostScanForNewDevices(); } },
			new Test() { public void execute() { amlTestConnectToNewDevice(); } },
			new Test() { public void execute() { amlTestNewDeviceScanForAPs(); } },
			new Test() { public void execute() { amlTestConnectNewDeviceToService(); } },
			new Test() { public void execute() { amlTestConfirmNewDeviceToServiceConnection(); } }
			));
	
	private List<Test> setupEndTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestDeleteDeviceWifiProfile(); } }
			));
	
	private List<Test> registrationStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState(); } },
			new Test() { public void execute() { amlTestUserLogin(); } },
			new Test() { public void execute() { amlTestRegisterNewDevice(); } }
			));
	
	private List<Test> registrationEndTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestUnregisterDevice(); } },
			new Test() { public void execute() { amlTestUserLogout(); } }
			));
	
	private List<Test> endList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { onEnd(); } }
			));
	
	
	private void amlTestNothing() {
		String passMsg = String.format("%s, %s", "P", "TestNothing");
		amlTestStatusMsg("Pass", "TestNothing", passMsg);
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		testSequencer.nextTest().execute();
	}
	
	
	
	
	/**
	 * This MUST be the first Setup call
	 * Finds all new Ayla devices within wifi distance of the host
	 * Present the scan results array to the user to select one
	 * The goal of Setup is to connect the new device to the Ayla device service
	 */
	private void amlTestReturnHostScanForNewDevices() {
		AylaSetup.returnHostScanForNewDevices(returnHostScanForNewDevices);
	}

	private final Handler returnHostScanForNewDevices = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostScanResults[] scanResults = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostScanResults[].class);

				String ssid = "";
				AylaSetup.newDevice.hostScanResults = null;
				for (AylaHostScanResults scanResult : scanResults) {
					ssid = scanResult.ssid;
					if (ssid.equals(AMLUnitTestConfig.gblAmlModuleSsid)) { // simulate user selection of a new device
						AylaSetup.newDevice.hostScanResults = scanResult;
					}
				}
				if (AylaSetup.newDevice.hostScanResults != null) {
					int lastTask = AylaSetup.lastMethodCompleted;
					String passMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "ssid", AylaSetup.newDevice.hostScanResults.ssid, "lastTask", lastTask, "returnHostScanForNewDevices");
					amlTestStatusMsg("Pass", "AylaSetup.returnHostScanForNewDevices", passMsg);

					testSequencer.nextTest().execute();
//					amlTestConnectToNewDevice();  // next Setup call
				} else {
					String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s", "F", "amlTest", "ssid", "null", "returnHostScanForNewDevices - no device assigned");
					AylaSetup.exit(); // retry this task, or call exit and begin setup again
					amlTestStatusMsg("Fail", "AylaSetup.returnHostScanForNewDevices", errMsg);
				}
			} else {
				String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
				amlTestStatusMsg("Fail", "AylaSetup.returnHostScanForNewDevices", errMsg);
			}
		}		
	};
	
	
	/**
	 * Connect host to new device selected by user
	 */
	private void amlTestConnectToNewDevice() {
		AylaSetup.connectToNewDevice(connectToNewDevice);
	}

	private final Handler connectToNewDevice = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSetup.newDevice = AylaSystemUtils.gson.fromJson(jsonResults,AylaModule.class); // special case parsing for "DSN" in api v1.0
				String dsn = AylaSetup.newDevice.dsn;
				String build = AylaSetup.newDevice.build;
				//String connectedMode = AylaSetup.connectedMode;
				//String deviceService = AylaSetup.newDevice.device_service;
				//long lastConnectMtime = AylaSetup.newDevice.last_connect_mtime;
				//long mtime = AylaSetup.newDevice.mtime;
				//String version = AylaSetup.newDevice.version; // Firmware version
				//String apiVersion = AylaSetup.newDevice.api_version; // HTTP/JSON API version

				String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "dsn", dsn, "build", build, "connectToNewDevice");
				amlTestStatusMsg("Pass", "AylaSetup.connectToNewDevice", passMsg);

				testSequencer.nextTest().execute();
//				amlTestNewDeviceScanForAPs(); // next Setup call
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
				amlTestStatusMsg("Fail", "AylaSetup.connectToNewDevice", errMsg);
			}
		}		
	};
	
	
	private void amlTestNewDeviceScanForAPs() {
		AylaSetup.getNewDeviceScanForAPs(getDeviceScanForAPs);
	}

	private final Handler getDeviceScanForAPs = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaModuleScanResults[] scanResults = AylaSystemUtils.gson.fromJson(jsonResults,  AylaModuleScanResults[].class);

				String ssid = "";
				for (AylaModuleScanResults scanResult : scanResults) {
					ssid = scanResult.ssid;
					if (ssid.equals(AMLUnitTestConfig.gblAmlLanSsid)) { // simulate user selected ssid
						AylaSetup.lanSsid = ssid;
						AylaSetup.lanPassword = AMLUnitTestConfig.gblAmlLanSsidPassword; // simulate user entered password
						AylaSetup.lanSecurityType = scanResult.security; // v1.2.0
						break;
					}
				}
				if (AylaSetup.lanSsid != null) {
					int lastTask = AylaSetup.lastMethodCompleted;
					String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "lanSsid", AylaSetup.lanSsid, "lastTask", lastTask, "getDeviceScanForAPs");
					amlTestStatusMsg("Pass", "AylaSetup.getDeviceScanForAPs", passMsg);

					testSequencer.nextTest().execute();
//					amlTestConnectNewDeviceToService(); // next setup call
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlLanSsid", AMLUnitTestConfig.gblAmlLanSsid, "getDeviceScanForAPs - no device assigned");
					amlTestStatusMsg("Fail", "AylaSetup.getDeviceScanForAPs", errMsg);
					AylaSetup.exit(); // retry this task only, or call exit and begin setup again
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "AylaSetup.getDeviceScanForAPs", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}		
	};
	
	
	private void amlTestConnectNewDeviceToService() {
		AylaSetup.connectNewDeviceToService(connectNewDeviceToService);
	}

	private final Handler connectNewDeviceToService = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			amlTestStatusMsg("Pass", "AylaSetup.connectNewDeviceToService", jsonResults);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				int lastTask = AylaSetup.lastMethodCompleted;
				String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "jsonResults", jsonResults, "lastTask", lastTask, "connectNewDeviceToService");
				amlTestStatusMsg("Pass", "AylaSetup.connectNewDeviceToService", passMsg);

				testSequencer.nextTest().execute();
//				amlTestConfirmNewDeviceToServiceConnection();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "connectNewDeviceToService");
				amlTestStatusMsg("Fail", "AylaSetup.connectNewDeviceToService", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}
	};
	
	
	private void amlTestConfirmNewDeviceToServiceConnection() {
		AylaSetup.confirmNewDeviceToServiceConnection(confirmNewDeviceToServiceConnection);
	}

	private final Handler confirmNewDeviceToServiceConnection = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				//int lastTask = AylaSetup.lastMethodCompleted;
				int retries = AylaSetup.newDeviceToServiceConnectionRetries;
				String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "jsonResults", jsonResults, "retries:", retries, "confirmNewDeviceToServiceConnection");
				amlTestStatusMsg("Pass", "AylaSetup.confirmNewDeviceToServiceConnection", passMsg);
				
				testSequencer.nextTest().execute();	
			} else {
				int responseCode = (int)msg.arg1;
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", responseCode, msg.obj, "confirmNewDeviceToServiceConnection");
				amlTestStatusMsg("Fail", "AylaSetup.confirmNewDeviceToServiceConnection", errMsg);

				if (responseCode != AylaNetworks.AML_WIFI_ERROR) {
					amlTestGetNewDeviceWiFiStatus(); 
					numberOfTests++; // final Setup call if confirmation failed
				} else {
					AylaSetup.exit(); // retry this task, or call exit and begin setup again
				}
			}
		}
	};
	
	
	private void amlTestGetNewDeviceWiFiStatus() {
		AylaSetup.getNewDeviceWiFiStatus(getNewDeviceWiFiStatus);
	}

	private final Handler getNewDeviceWiFiStatus = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaWiFiStatus wifiStatus = AylaSystemUtils.gson.fromJson(jsonResults,AylaWiFiStatus.class);
				//String dns = wifiStatus.dns;
				//String deviceService = wifiStatus.deviceService;
				String mac = wifiStatus.mac;
				//int ant = wifiStatus.ant;
				//int rssi = wifiStatus.rssi; // always zero in AP mode
				//int bars = wifiStatus.bars;

				int count = 0;
				AylaWiFiConnectHistory[] wifiConnectHistories = null;
				if (wifiStatus.connectHistory != null) {
					wifiConnectHistories = new AylaWiFiConnectHistory[wifiStatus.connectHistory.length];
					for (AylaWiFiConnectHistory connectHistory : wifiStatus.connectHistory) {
						wifiConnectHistories[count] = connectHistory; // first error is the most recent
						AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s, %s", "I", "amlTest", "error", wifiConnectHistories[count].error, "Connect Message", wifiConnectHistories[count].msg, "getNewDeviceWiFiStatus");
						count++;
					}
				}
				String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "mac", mac, "wifiConnectHistories", count, "getDeviceWiFiStatus");
				amlTestStatusMsg("Pass", "AylaSetup.getDeviceWiFiStatus", passMsg);

				// ---------------- END OF UNSUCCESSFUL SETUP ----------------------------

			} else {
				int responseCode = (int)msg.arg1;
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", responseCode, msg.obj, "");
				amlTestStatusMsg("Fail", "AylaSetup.getDeviceWiFiStatus", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}		
	};
	
	
	/**
	 * Get WiFi state from host
	 */
	private void amlTestReturnHostWiFiState() {
		AylaHost.returnHostWiFiState(returnHostWiFiState);
	}

	private final Handler returnHostWiFiState = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostNetworkConnection state = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostNetworkConnection.class);
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "state", state.state, "returnHostWiFiState");
				amlTestStatusMsg("Pass", "returnHostWiFiState", passMsg);	

				testSequencer.nextTest().execute();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "returnHostWiFiState", errMsg);
			}
		}		
	};
	
	
	private void amlTestUserLogin() {
		restService1 = AylaUser.login(login, 
				AMLUnitTestConfig.userName, 
				AMLUnitTestConfig.password, 
				AMLUnitTestConfig.appId, 
				AMLUnitTestConfig.appSecret);
	}

	private final Handler login = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {

				AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
				aylaUser = AylaUser.setCurrent(aylaUser);
				gblAmlTestUser = aylaUser;		// save for unit test
				//String accessToken = aylaUser.getAccessToken();
				//String refreshToken = aylaUser.getRefreshToken();
				//int expiresIn = aylaUser.getExpiresIn();

				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "secondsToExpiry", aylaUser.accessTokenSecondsToExpiry(), "login");
				amlTestStatusMsg("Pass", "login", passMsg);

				testSequencer.nextTest().execute();
//				amlTestRegisterNewDevice(); // next test
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login");
				amlTestStatusMsg("Fail", "login", errMsg);
			}
		}		
	};
	
	
	private void amlTestRegisterNewDevice() {
		// retrieved most recently setup device
		AylaDevice newDevice;
		String jsonNewDevice = AylaSetup.get(); // read cached new device info
		AylaSystemUtils.saveToLog("%s, %s, %s:%s.", "D", "AMLUnitTest", "AylaSetup Cached Device", jsonNewDevice);
		if ( !TextUtils.isEmpty(jsonNewDevice) ) {
			newDevice = AylaSystemUtils.gson.fromJson(jsonNewDevice,  AylaDevice.class);
		} else { // Use Same-Lan as default
			newDevice = new AylaDevice();
			newDevice.registrationType = AylaNetworks.AML_REGISTRATION_TYPE_SAME_LAN;
			newDevice.dsn = null;
		}
		newDevice.registerNewDevice(registerNewDevice);
	}

	private final Handler registerNewDevice = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice.class);
//				String productName = gblAmlTestDevice.getProductName();

				mScreen.displayProgressInfo(gblAmlTestDevice.toString());

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "jsonResults", jsonResults, "registerNewDevice");
				amlTestStatusMsg("Pass", "registerNewDevice1", passMsg);

				testSequencer.nextTest().execute();
//				if ( (amlTestSuite == testSuite.SETUP_ONLY) || (amlTestSuite == testSuite.SETUP_AND_REGISTRATION_ONLY) ) {
//					amlTestDeleteDeviceWifiProfile(); // delete the wifi profile from the device
//				} else {
//					amlTestUserLogout(); // leave the device connected and registered
//				}
			} else {
				String subMethodFailure = "";
     			if (msg.arg2 == AylaNetworks.AML_REGISTER_NEW_DEVICE) {
    				subMethodFailure = "getRegisterNewDevice";
    				if (msg.arg1 == AylaNetworks.AML_ERROR_NOT_FOUND) {
    					// Unsupported registration type
    				} else if (msg.arg1 == AylaNetworks.AML_NO_ITEMS_FOUND) {
    					// Setup token not found for AP-Mode registration
    				}
    			} else
    			if (msg.arg2 == AylaNetworks.AML_GET_REGISTRATION_CANDIDATE) {
    				subMethodFailure = "getRegistrationCandidate";
    				// No registration candidates were found
    			} else if (msg.arg2 == AylaNetworks.AML_GET_MODULE_REGISTRATION_TOKEN) {
    				subMethodFailure = "getModuleRegistrationCandidate";
    				// Failed to connect to new device for registration

    			}  else if (msg.arg2 == AylaNetworks.AML_REGISTER_DEVICE) {
    				subMethodFailure = "registerDevice";
    				// Failed to register the new device
    			}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.%s", "F", "amlTest", "error", msg.arg1, msg.obj, "registerNewDevice",  subMethodFailure);
				amlTestStatusMsg("Fail", "registerNewDevice", errMsg);
			}
		}		
	};
	
	
	private void amlTestDeleteDeviceWifiProfile() {
//		String lanIp = AylaSetup.lanIp;
//		String lanIp = "192.168.0.1"; // Station LanIp.
//		String lanIp = "172.16.11.235";
//		String lanSsid = "Ayla2";
//		String lanIp = AylaSetup.lanIp;
//		String lanSsid = AylaSetup.lanSsid;
//		AylaSystemUtils.sleep(1500);     
//		try {
//			Thread.sleep(5000);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		// For unit test, assuming AylaLanMode.device is the right device, which is context based.
//		AylaModule.deleteDeviceWifiProfile(deleteDeviceWifiProfile, lanIp, lanSsid);
//		AylaModule.deleteDeviceWifiProfile(deleteDeviceWifiProfile, AMLUnitTestConfig.gblAmlTestEVBDsn, lanSsid);
		
		String lanSsid = AMLUnitTestConfig.gblAmlLanSsid;         
		String jsonSetupDevice = AylaSetup.get();
		AylaDevice setupDevice = null;
		try {
			setupDevice = AylaSystemUtils.gson.fromJson(jsonSetupDevice, AylaDevice.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if ( setupDevice == null || !TextUtils.equals(setupDevice.dsn, gblAmlTestDevice.dsn) ) {
			AylaSystemUtils.saveToLog("%s, %s, %s.", "D", "amlTestDeleteDeviceWifiProfile", "setupDevice not initiated properly");
		}
		
		gblAmlTestDevice.lanIp = setupDevice.lanIp;
		
		// NOTE: Assuming gblAmlTestDevice is properly initiated in registerNewDevice, should not be isolated from registration test list. 
		AylaModule.deleteDeviceWifiProfile(deleteDeviceWifiProfile, gblAmlTestDevice , lanSsid);
//		AylaDevice device = new AylaDevice();
//		device.dsn = AMLUnitTestConfig.gblAmlTestEVBDsn;
		
//		AylaModule.deleteDeviceWifiProfile(deleteDeviceWifiProfile, AylaLanMode.device, lanSsid);
	}

	private final Handler deleteDeviceWifiProfile = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s"
						, "D", "amlTest", "Lan IP", AylaSetup.lanIp
						, "jsonResults:", jsonResults, "amlDeleteDeviceWifiProfile");
				amlTestStatusMsg("Pass", "amlDeleteDeviceWifiProfile", passMsg);
				
				final Handler handler = new Handler();
			    handler.postDelayed(new Runnable() {	// allow time for new device AP to establish
			        @Override
			        public void run() {
						testSequencer.nextTest().execute();
			        }
			    }, 3000);	// Delay 1.5 seconds        
//				if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_ONLY || 
//						AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_AND_REGISTRATION_ONLY )
//				{
//					/*---------------------- END OF SUCCESSFUL SETUP ONLY UNIT TEST SUITE --------------------------*/
//					final Handler handler = new Handler();
//				    handler.postDelayed(new Runnable() {	// allow time for new device AP to establish
//				        @Override
//				        public void run() {
//							testSequencer.nextTest().execute();
//				        }
//				    }, 5000);	// Delay 5 seconds
//				} else {
//					testSequencer.nextTest().execute();
//				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "amlDeleteDeviceWifiProfile");
				amlTestStatusMsg("Fail", "amlDeleteDeviceWifiProfile", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}
	};
	
	
	
	private void amlTestUnregisterDevice() {
		gblAmlTestDevice.unregisterDevice(unregisterDevice);
	}

	private final Handler unregisterDevice = new Handler() {
		public void handleMessage(Message msg) {
			//String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "unregisterDevice");
				amlTestStatusMsg("Pass", "unregisterDevice", passMsg);

				testSequencer.nextTest().execute();
//				amlTestUserLogout();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "unregisterDevice");
				amlTestStatusMsg("Fail", "unregisterDevice", errMsg);
			}
		}		
	};
	
	
	private void amlTestUserLogout() {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put("access_token", AylaUser.user.getauthHeaderValue());
		restService1 = AylaUser.logout(logout, callParams);
	}

	private final Handler logout = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
				String logout = aylaUser.getLogout();
				gblAmlTestUser = null;		// save for unit test

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "logout", logout, "logout");
				amlTestStatusMsg("Pass", "logout", passMsg);

				//
				// ---------------------- END OF SUCCESSFUL SETUP & REGISTRATION UNIT TEST SUITE --------------------------
				//
				testSequencer.nextTest().execute();
//				if (amlTestSuite == testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE) {
//					amlTestReturnHostWiFiState2(); numberOfTests += 16;// Begin Device Service Unit Test (less Settings tests)
//				}		
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "logout");
				amlTestStatusMsg("Fail", "logout", errMsg);
			}
		}		
	};
	
	
	
	
	// TODO: extract as a common utility function
	private void amlTestStatusMsg(String passFail, String methodName,
			String methodMsg) {
		String testMsg = "";
		if (passFail != null && passFail.contains("Pass")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s", methodName, "passed");

			if (!passFail.contains("Sync")) {
				mScreen.displayFailMessage(testMsg + "\n\n");
			} else {
				Log.i(tag, testMsg);
			}
		} else if (passFail != null && passFail.contains("Fail")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s.", methodName, "failed");
			if (!passFail.contains("Sync")) {
				mScreen.displayPassMessage(testMsg + "\n\n");
			} else {
				Log.e(tag, testMsg);
			}
		} else {
			// TODO: passFail null or no match.
		}
		AylaSystemUtils.saveToLog("%s", testMsg);

	}// end of amlTestStatusMsg


	

}// end of AMLEVBSetupRegistrationUnitTestCase class                         







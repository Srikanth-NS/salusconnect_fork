/*
 * AMLEVBUnitTestCaseLanMode.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */


package com.aylanetworks.AMLUnitTest.evbTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig;
import com.aylanetworks.AMLUnitTest.facility.Test;
import com.aylanetworks.AMLUnitTest.facility.TestSequencer;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaDatapoint;
import com.aylanetworks.aaml.AylaDevice;
import com.aylanetworks.aaml.AylaDeviceGateway;
import com.aylanetworks.aaml.AylaHost;
import com.aylanetworks.aaml.AylaHostNetworkConnection;
import com.aylanetworks.aaml.AylaLanMode;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaNotify;
import com.aylanetworks.aaml.AylaProperty;
import com.aylanetworks.aaml.AylaReachability;
import com.aylanetworks.aaml.AylaRestService;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.aylanetworks.aaml.AylaUser;
import com.aylanetworks.aaml.AylaNetworks.lanMode;

public class AMLEVBUnitTestCaseLanMode implements IUnitTestable {

	private static final String tag 
		= AMLEVBUnitTestCaseLanMode.class.getSimpleName();
	private ITestResultDisplayable mScreen = null;
	
	private TestSequencer testSequencer = null;     
	private Runnable mEndHandler = null;
	
 
	private Message msg = new Message();
	private AylaRestService rsCreateDatapoint = null;
	private AylaUser gblAmlTestUser = null;
	private AylaDevice gblAmlTestDevice = null;
	private AylaDeviceGateway gblAmlTestGateway = null;    
	private AylaProperty gblAmlTestProperty = null;          

	
	private String gblAmlPropertyName = null;
	private String gblAmlBaseType = null;
	private String gblAmlSValue = null;
	private Number gblAmlNValue = null;

	
	private int gblStringLen = 0;	// Size of test string value
	private boolean isWifi = false;
	private boolean isGateway = false;
	private boolean isNode = false;
	private boolean gblAmlTestLanModeEnabled = false;         


	
	@Override
	public void onResume() {
		if (mScreen == null) {
			mScreen = new DefaultScreen();
		}
		mScreen.init();   
		
		AylaSystemUtils.lanModeState = lanMode.ENABLED; // enable lan mode
		AylaLanMode.enable(notifierHandle, reachabilityHandle); // start http server, set notifier and reachability handlers    
		
//		AylaNetworks.onResume();
		
		initTestSuite();
	}// end of onResume                           

	
	@Override
	public void start() {
		if (testSequencer == null) {
			onResume();
		}
		testSequencer.nextTest().execute();
	}// end of start

	
	@Override
	public void onPause() {
		if (testSequencer == null) {
			onResume();
		}
		AylaNetworks.onPause(false);
	}// end of onPause                   

	
	@Override
	public void onDestroy() {
		
		AylaNetworks.onDestroy();
		//TODO: GC work done here.
		testSequencer = null;
	}// end of onDestroy     
	
	

	@Override
	public void onEnd() {
		if (mEndHandler != null) {
			mEndHandler.run();
		}
	}// end of onEnd


	@Override
	public void setEndHandler(Runnable r) {
		mEndHandler = r;
	}
	
	
	@Override
	public void setResultScreen(ITestResultDisplayable screen) {
		if (screen == null) {
			mScreen = new DefaultScreen();
		} else {
			mScreen = screen;
		}
	}// end of setResultScreen        
	
	
	@Override
	public boolean isRunning() {
		if (testSequencer != null) {
			return testSequencer.isTestSuiteRunning();
		}
		return false;
	}
	
	
	private void initTestSuite() {
		List<Test> lanModeOnlyTestList = new ArrayList<Test>(settingsTestList);
		lanModeOnlyTestList.addAll(lanModeTestList);
		lanModeOnlyTestList.addAll(endList);
		
		
		testSequencer = new TestSequencer(lanModeTestList);
	}// end of initTestSuite                     
	
	
	private List<Test> settingsTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestNothing(); }
			}));
	
	private List<Test> lanModeTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			new Test() { public void execute() { amlTestGetDevices(); } },
//			new Test() { public void execute() { amlTestGetDeviceDetail(); } },
			
			//TODO: Some global stuff does not get cleaned up. Need to restart the app to pass this test for now. 
			
			new Test() { public void execute() {
				gblAmlBaseType = "boolean";
				gblAmlPropertyName = "Blue_LED";	// boolean
				amlTestGetProperties();				// Get all properties, filtering out ours
			} },
			
			new Test() { public void execute() {
				gblAmlBaseType = "boolean";
				gblAmlPropertyName = "Blue_LED";
				Map<String, String> callParams = new HashMap<String, String>();
				callParams.put("names", gblAmlPropertyName);

				amlTestGetProperties(callParams);		// Get a single property
			} },
			
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
				gblAmlBaseType = "boolean";
				gblAmlNValue = 1 ;
				gblAmlSValue = "Hello - ";
				// Append a bunch of random characters to make a long string
				Random randGen = new Random();
				while (gblAmlSValue.length() < gblStringLen) {
					gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
				}

				final AylaDatapoint myDatapoint = new AylaDatapoint();
				myDatapoint.sValue(gblAmlSValue);
				myDatapoint.nValue(gblAmlNValue);
				amlTestCreateDatapoint(myDatapoint);
			} },
			new Test() { public void execute() { 
				amlTestGetDatapointsByActivity(2); 
			} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },		// Go to the service this time
			
			new Test() { public void execute() {
				gblAmlPropertyName = "cmd";		// string
				Map<String, String> callParams = new HashMap<String, String>();
				callParams.put("names", gblAmlPropertyName);

				amlTestGetProperties(callParams);		// Get a single property
			} },
			new Test() { public void execute() {
				gblAmlPropertyName = "cmd";		// string
				amlTestGetProperties();
			} },
			new Test() { public void execute() {
				gblAmlBaseType = "string";
				gblAmlNValue = 1 ;
				gblAmlSValue = "Hello - ";
				// Append a bunch of random characters to make a long string
				Random randGen = new Random();
				while (gblAmlSValue.length() < gblStringLen) {
					gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
				}

				final AylaDatapoint myDatapoint = new AylaDatapoint();
				myDatapoint.sValue(gblAmlSValue);
				myDatapoint.nValue(gblAmlNValue);
				amlTestCreateDatapoint(myDatapoint);
			} },
			new Test() { public void execute() { 
				amlTestGetDatapointsByActivity(2); 
			} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } }
			
			));
	
	    private List<Test> endList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { onEnd(); } }
			));
	
	
	// called when there is a change in device or service reachability
		private final Handler reachabilityHandle = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "reachability", jsonResults, "reachability");
				}
			}
		};
	
	
	private final Handler notifierHandle = new Handler() {
		public void handleMessage(Message msg) {
		String jsonResults = (String)msg.obj;
		AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "notifierHandle", jsonResults);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				// TODO: AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY is always true here.      
				if (msg.arg1 == 200 && (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
						|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY)) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "notifier", jsonResults, "notifier");
					AylaNotify notifyMsg = AylaSystemUtils.gson.fromJson(jsonResults, AylaNotify.class);
					
					// Only trigger the next test to for the first "session" message
					if (!gblAmlTestLanModeEnabled && notifyMsg.type.equals("session")) {
						gblAmlTestLanModeEnabled = true;
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
				} else {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "amlTest", "notifier", jsonResults, "notifier");
				}
			} else {
				AylaNotify.notifyAcknowledge();
			}
		}
	};
	
	
	// FIX: Temporarily take out settings tests, since they interfere with
		// cached values (BUG AML-40)
		private void amlTestNothing() {
			String passMsg = String.format("%s, %s", "P", "TestNothing");
			amlTestStatusMsg("Pass", "TestNothing", passMsg);
			testSequencer.nextTest().execute();
		}
		
		
		/**
		 * Get WiFi state from host
		 */
		private void amlTestReturnHostWiFiState2() {
			
			if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
					|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) {
				boolean waitForResults = true;
//				AylaReachability.determineReachability(waitForResults);
				if ( AylaReachability.isCloudServiceAvailable()) {
					if (AylaLanMode.lanModeState != lanMode.RUNNING) {
						AylaLanMode.resume();	// start the Lan Mode service. Only required for iterative testing
					}
					AylaHost.returnHostWiFiState(returnHostWiFiState2);
				} else {
					String failMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device", "not reachable", "returnHostWiFiState2");
					amlTestStatusMsg("Fail", "returnHostWiFiState2.reachability", failMsg);	
				}
			} else {
				AylaHost.returnHostWiFiState(returnHostWiFiState2);
			}
		}

		private final Handler returnHostWiFiState2 = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaHostNetworkConnection state = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostNetworkConnection.class);
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "state", state.state, "returnHostWiFiState2");
					amlTestStatusMsg("Pass", "returnHostWiFiState2", passMsg);	

					testSequencer.nextTest().execute();
//					amlTestUserLogin2();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
					amlTestStatusMsg("Fail", "returnHostWiFiState", errMsg);
				}
			}		
		};
		
		
		private void amlTestUserLogin2() {
			if (AMLUnitTestConfig.gblAmlAsyncMode) {
				AylaUser.login(login2, 
						AMLUnitTestConfig.userName, 
						AMLUnitTestConfig.password, 
						AMLUnitTestConfig.appId, 
						AMLUnitTestConfig.appSecret);
			} else {
				// sync
				msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

				Thread thread = new Thread(new Runnable() {
					public void run()
					{
						AylaRestService rs = AylaUser.login(
								AMLUnitTestConfig.userName, 
								AMLUnitTestConfig.password, 
								AMLUnitTestConfig.appId, 
								AMLUnitTestConfig.appSecret);
						msg =  rs.execute();
					}
				});
				thread.start();
				try {
					thread.join();		// Wait for thread to finish
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				doLogin2(msg);
			}
		}

		private final Handler login2 = new Handler() {
			public void handleMessage(Message msg) {
				doLogin2(msg);
			}		
		};
		
		void doLogin2(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
		
				AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
				aylaUser = AylaUser.setCurrent(aylaUser);
				gblAmlTestUser = aylaUser;		// save for unit test
				
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "secondsToExpiry", aylaUser.accessTokenSecondsToExpiry(), "login2");
				amlTestStatusMsg("Pass", "login2", passMsg);
		
				testSequencer.nextTest().execute();
//				amlTestGetDevices(); // next test
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login2");
				amlTestStatusMsg("Fail", "" +
						"", errMsg);
			}
		}
		
		
		private void amlTestGetDevices() {
			AylaDevice.getDevices(getDevices);
		} 

		private final Handler getDevices = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaDevice[] devices = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice[].class);
					gblAmlTestDevice = null;
					for (AylaDevice device : devices) {
						if ( TextUtils.equals(device.dsn, AMLUnitTestConfig.gblAmlTestEVBDsn) ) {
							
							mScreen.displayProgressInfo(device.toString());
							gblAmlTestLanModeEnabled = false;
							gblAmlTestDevice = device; // assign device under test
							gblAmlTestDevice.lanModeEnable(); //Start a lan mode session with this device
							//String model = device.model;
							//String dsn = device.dsn;
							//String oemMode = device.oemModel;
							//String connectedAt = device.connectedAt;
							//String mac = device.mac;
							//String lanIp = device.lanIp;
							//String token = device.token;
							// check device class type
							isWifi = device.isWifi(); 
							isGateway = device.isGateway(); 
							isNode = device.isNode();
							if (isWifi && 
									(AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.DEVICE_SERVICE_ONLY) 
									&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE) 
									&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) // LAN_MODE_ONLY would need to get devices first.
									|| isGateway 
									&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_SERVICE_ONLY) 
									&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY))
							{
								String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device class & suite", "mismatch", "getDevices - class & suite must match");
								amlTestStatusMsg("Fail", "getDevices", errMsg);
							}

							if (isGateway) {
								gblAmlTestGateway = (AylaDeviceGateway) device;
								//gblAmlTestGateway = (AylaDeviceZigbeeGateway) device;				// if only one kind of gateway for the app
								
							}
						}
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", device.dsn, "amOwner", device.amOwner(), "getDevices");
					}

					if (gblAmlTestDevice != null) { // simulate user selection of a device
						String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", devices.length, "getDevices");
						amlTestStatusMsg("Pass", "getDevices", passMsg);	

						if ( (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) ) {
							synchronized (testSequencer) {
								testSequencer.nextTest().execute();
							}
						} else {
							// notifierHandle only works when a property changes under lanMode, not for the first time lib retrieves devices from cloud. 
//							testSequencer.nextTest().execute();
						}
//						synchronized (testSequencer) {
//							testSequencer.nextTest().execute();
//						}
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getDevices - no device assigned");
						amlTestStatusMsg("Fail", "getDevices", errMsg);
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDevices");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			}		
		};                   
		
		
		
		public void amlTestGetProperties() {
			
			Map<String, String> param = new HashMap<String, String>();
			//Blue_LED is necessary to update the GblTestProperty
			param.put("names", "attr_read_cmd attr_read_data cmd log attr_set_cmd attr_set_result Blue_LED Green_LED decimal_in input"); 
			amlTestGetProperties(param);
			// get all properties in one API in lan_mode would randomly cause property not found(404), which is a constraint from gateway
//			amlTestGetProperties(null);
		}

		private void amlTestGetProperties(final Map<String, String> callParams) {
			// test async interface
			if (AMLUnitTestConfig.gblAmlAsyncMode) {
				gblAmlTestDevice.getProperties(getProperties, callParams); // next test: no delay
			} else {
				// sync
				msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

				// test synchronous interface
				Thread thread = new Thread(new Runnable() {
					public void run()
					{
						AylaRestService rs = gblAmlTestDevice.getProperties(callParams);
						msg = rs.execute();
					}
				});
				thread.start();
				try {
					thread.join();		// Wait for thread to finish
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				doGetProperties(msg);
			}
		}

		private final Handler getProperties = new Handler() {
			public void handleMessage(Message msg) {
				doGetProperties(msg);
			};
		};

		private void doGetProperties(Message msg) {
			String jsonResults = (String)msg.obj;
			AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "doGetProperties", jsonResults);
			AylaSystemUtils.saveToLog("%s, %s, gblAmlPropertyName:%s.", "D", "doGetProperties", gblAmlPropertyName);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				// for unit test purpose 
				AylaNotify.notifyAcknowledge();
				AylaProperty[] ps = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
				gblAmlTestDevice.mergeNewProperties(ps);
				
				if (TextUtils.isEmpty(gblAmlPropertyName)) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.properties.length, "getProperties");
					amlTestStatusMsg("Pass", "getProperties", passMsg);
					
					if ( AylaSystemUtils.lanModeState != lanMode.ENABLED ) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
					return;
				}
				
				gblAmlTestProperty = null;
				for (AylaProperty property : gblAmlTestDevice.properties) {
					String name = property.name();
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getProperties");
					if (TextUtils.equals(name, gblAmlPropertyName)) { // simulate user selection of a property
						gblAmlTestProperty = property; // assign property under test
					}
				}

				
				if (gblAmlTestProperty != null ) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.properties.length, "getProperties");
					amlTestStatusMsg("Pass", "getProperties", passMsg);
					
					if ( AylaSystemUtils.lanModeState != lanMode.ENABLED ) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
					
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getProperties - no property assigned");
					amlTestStatusMsg("Fail", "getProperties", errMsg);
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getProperties");
				amlTestStatusMsg("Fail", "getProperties", errMsg);
			}		
		}
		
		
		// usually not get property detail is not necessary, use get devices instead
		private void amlTestGetPropertyDetail() {
			//Map<String, String> callParams = new HashMap<String, String>();
			//callParams.put("class", "service");
			Map<String, String> callParams = null; // callParams is unused at this time
			gblAmlTestProperty.getPropertyDetail(getPropertyDetail, callParams); // next test: no delay		
		}

		private final Handler getPropertyDetail = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaProperty property = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty.class);

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", property.name(), "getPropertyDetail");
					amlTestStatusMsg("Pass", "getPropertyDetail", passMsg);

					if (AylaSystemUtils.lanModeState != lanMode.ENABLED) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getPropertyDetail");
					amlTestStatusMsg("Fail", "getPropertyDetail", errMsg);
				}		
			}
		};
		
		
		private void amlTestCreateDatapoint(final AylaDatapoint datapoint) {
			if (AMLUnitTestConfig.gblAmlAsyncMode) {
				// asynchronous
				rsCreateDatapoint = gblAmlTestProperty.createDatapoint(createDatapoint, datapoint); // next test: no delay
			} else {
				// sync
				msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;
				
				// use synchronous method calls
				Thread thread = new Thread(new Runnable() {
					public void run()
					{
							AylaRestService rs;
							
							// get all schedules associated with this device
							rs = gblAmlTestProperty.createDatapoint(datapoint); // next test: no delay
							msg = rs.execute();
					}
				});
				thread.start();
				try {
					thread.join();		// Wait for thread to finish
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				doCreateDatapoint(msg);
			}
		}

		private final Handler createDatapoint = new Handler() {
			public void handleMessage(Message msg) {
				doCreateDatapoint(msg);
			}
		};
		
		private void doCreateDatapoint(Message msg) {
			String jsonResults = (String)msg.obj;
			AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "doCreateDatapoint", jsonResults);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestProperty.datapoint = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint.class);
				AylaDatapoint datapoint= gblAmlTestProperty.datapoint;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datapoint.value(), "createDatapoint");
				amlTestStatusMsg("Pass", "createDatapoint", passMsg);
				new CountDownTimer(3000, 1000) {
					@Override
					public void onFinish() {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}

					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createDatapoint");
				amlTestStatusMsg("Fail", "createDatapoint", errMsg);
			}		
		}
		
	
		
		private void amlTestGetDatapointsByActivity(int count) {
			Map<String, String> callParams = new HashMap<String, String>();
			callParams.put("count", Integer.toString(count));
			rsCreateDatapoint = gblAmlTestProperty.getDatapointsByActivity(getDatapointsByActivity, callParams); // next test: no delay
		}

		private final Handler getDatapointsByActivity = new Handler() {
			public void handleMessage(Message msg) {
				boolean pass = false;
				String jsonResults = (String)msg.obj;
				AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "getDatapointByActivity", jsonResults);
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestProperty.datapoints = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint[].class);
					String baseType = gblAmlTestProperty.baseType();
					pass = baseType.equals(gblAmlBaseType);
					if (pass) {					
						pass = false;
						// Print all of the datapoints
						for (AylaDatapoint datapoint : gblAmlTestProperty.datapoints) {
							String sValue = datapoint.sValueFormatted(baseType);
							Number nValue = datapoint.nValueFormatted(baseType);
		
							String nValueStr = nValue.toString();
							AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "nValue", nValueStr, "sValue", sValue, "getDatapointsByActivity");
							//Log.i("datapoint.toString():", datapoint.toString());
						}
						// Check most recent value
						int mostRecent = gblAmlTestProperty.datapoints.length - 1;
						if ( TextUtils.equals(baseType, "integer") || TextUtils.equals(baseType, "boolean")) {
							Number nValue = gblAmlTestProperty.datapoints[mostRecent].nValueFormatted(baseType);
							int nValueInt = nValue.intValue();
							int gblAmlNValueInt = gblAmlNValue.intValue();
							pass = (nValueInt == gblAmlNValueInt);
							if (pass) {
								String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "nValue", nValueInt, "getDatapointsByActivity");
								amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
							} else {
								String errMsg = String.format("%s, %s, %s:%s, %d!=%d, %s", "F", "amlTest", "error", "nValueMismatch", nValueInt, gblAmlNValueInt, "getDatapointsByActivity");
								amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
							}
						} else if ( TextUtils.equals(baseType, "float") || TextUtils.equals(baseType, "decimal")) {
							Number nValue = gblAmlTestProperty.datapoints[mostRecent].nValueFormatted(baseType);
							double nValueDouble = nValue.doubleValue();
							double gblAmlNValueDouble = gblAmlNValue.doubleValue();
							pass = (nValueDouble == gblAmlNValueDouble);
							if (pass) {
								String passMsg = String.format("%s, %s, %s:%f, %s", "P", "amlTest", "nValue", nValueDouble, "getDatapointsByActivity");
								amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
							} else {
								String errMsg = String.format("%s, %s, %s:%s, %f!=%f, %s", "F", "amlTest", "error", "nValueMismatch", nValueDouble, gblAmlNValueDouble, "getDatapointsByActivity");
								amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
							}
						} else if (TextUtils.equals(baseType, "string")) {
							String sValue = gblAmlTestProperty.datapoints[mostRecent].sValueFormatted(baseType);
							pass = TextUtils.equals(sValue, gblAmlSValue);
							if (pass) {
								String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "sValue", sValue, "getDatapointsByActivity");
								amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
							} else {
								String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "sValueMismatch", sValue, gblAmlSValue, "getDatapointsByActivity");
								amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
							}
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %s, %s", "F", "amlTest", "error", "illegalBaseType", baseType, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);												
						}
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "baseTypeMismatch", baseType, gblAmlBaseType, "getDatapointsByActivity");
						amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);					
					}
					if (pass) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
//					amlTestCreatePropertyTrigger();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDatapointsByActivity");
					amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);
				}		
			}
		};
	
	// TODO: extract as a common utility function
	private void amlTestStatusMsg(String passFail, String methodName,
			String methodMsg) {
		String testMsg = "";
		if (passFail != null && passFail.contains("Pass")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s", methodName, "passed");

			if (!passFail.contains("Sync")) {
				mScreen.displayFailMessage(testMsg + "\n\n");
			} else {
				Log.i(tag, testMsg);
			}
		} else if (passFail != null && passFail.contains("Fail")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s.", methodName, "failed");
			if (!passFail.contains("Sync")) {
				mScreen.displayPassMessage(testMsg + "\n\n");
			} else {
				Log.e(tag, testMsg);
			}
		} else {
			// TODO: passFail null or no match.
		}
		AylaSystemUtils.saveToLog("%s", testMsg);

	}// end of amlTestStatusMsg



}// end of AMLEVBUnitTestCaseLanMode class         








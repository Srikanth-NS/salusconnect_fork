"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/ayla/AylaTriggerApp.model"
], function (App, P, AylaConfig, AylaBackedMixin) {
	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.AylaTriggerModel = B.Model.extend({
			defaults: {
				trigger_apps: null,
				property_key: null,
				key: null,
				value: null,
				trigger_type: "compare_absolute",
				compare_type: "==",
				active: true
			},

			initialize: function () {
				if (!this.get("trigger_apps")) {
					this.set("trigger_apps", new Models.AylaTriggerAppCollection());
				}
			},

			create: function () {
				var that = this, url = _.template(AylaConfig.endpoints.aylaTrigger.create)(this.toJSON()),
						payload = this._buildPayload();

				return App.salusConnector.makeAjaxCall(url, payload, "POST", "json").then(function (data) {
					that.set(that.aParse(data.trigger));
				});
			},

			update: function () {
				var that = this, url = _.template(AylaConfig.endpoints.aylaTrigger.update)(this.toJSON()),
						payload = this._buildPayload(true);

				var triggerAppsToDelete = this._getTriggerAppsToDelete();

				return App.salusConnector.makeAjaxCall(url, payload, "PUT", "json").then(function (data) {
					that.set(that.aParse(data.trigger));

					_.map(triggerAppsToDelete, function (triggerApp) {
						return triggerApp.remove();
					});
				});
			},

			persist: function () {
				if (!this.get("key")) {
					return this.create();
				} else {
					return this.update();
				}
			},

			remove: function () {
				var that = this, url = _.template(AylaConfig.endpoints.aylaTrigger.delete)(this.toJSON());

				return App.salusConnector.makeAjaxCall(url, null, "DELETE", "text").then(function () {
					that.destroy();
				});
			},

			_buildPayload: function (isUpdate) {
				var json = this.toJSON(),
					payload = {
						trigger: _.pick(json, ["trigger_type", "compare_type", "value", "active"])
					};

				payload.trigger.trigger_apps = this._getTriggerAppPayload(isUpdate);

				return payload;
			},

			setTriggerValues: function (triggerData) {
				var that = this;

				_.each(triggerData.recipients, function (recipient) {
					var actualRecipient = triggerData.type === "email" ? recipient : recipient.phoneNumber;

					var triggerApp = that.get("trigger_apps").findWhere({
						recipient: actualRecipient,
						name: triggerData.type
					});

					// create new
					if (!triggerApp) {
						var user = App.salusConnector.getSessionUser();
						triggerApp = new App.Models.AylaTriggerAppModel({
							name: triggerData.type,
							recipient: actualRecipient,
							email_subject: App.translate("equipment.oneTouch.emails.subjectFormatted", {ruleName: triggerData.ruleName}),
							username: App.translate("equipment.oneTouch.emails.usernameFormatted", {firstname: user.get("firstname"), lastname: user.get("lastname")})
						});

						if (triggerData.type === "sms") {
							triggerApp.set("countryCode", recipient.countryCode);
						}

						that.get("trigger_apps").add(triggerApp);
					}

					triggerApp.set("message", triggerData.message);
				});
			},

			getRecipients: function () {
				return this.get("trigger_apps").map(function (triggerApp) {
					var recipient = triggerApp.get("recipient");

					if (triggerApp.get("countryCode")) {
						//sms - need to build object
						recipient = {
							countryCode: triggerApp.get("countryCode"),
							phoneNumber: recipient
						};
					}

					return recipient;
				});
			},

			getMessage: function () {
				// odd case where the view is trying to use this before we've fully ingested the server data
				if (this.get("trigger_apps") && this.get("trigger_apps").length > 0) {
					return this.get("trigger_apps").first().get("message");
				}
				return "";
			},

			aParse: function (trigger) {
				var currentTriggerApps = this.get("trigger_apps");

				if (currentTriggerApps) {
					currentTriggerApps.set(trigger.trigger_apps, {parse: true});
				} else {
					currentTriggerApps = new App.Models.AylaTriggerAppCollection(trigger.trigger_apps, { parse: true });
				}

				this.serverTriggerApps = new App.Models.AylaTriggerAppCollection(currentTriggerApps.toJSON(), { parse: true });

				return {
					property_key: trigger.property_key,
					key: trigger.key,
					value: trigger.value,
					trigger_apps: currentTriggerApps
				};
			},

			_getTriggerAppPayload: function (isUpdate) {
				var that = this;
				if (isUpdate) {
					return _.map(this.get("trigger_apps").filter(function () {
						return !that.get("key");
					}), function (trigger_app) {
						return trigger_app.toJSON();
					});
				} else {
					return this.get("trigger_apps").toJSON();
				}
			},

			_getTriggerAppsToDelete: function () {
				var newTriggerApps = this.get("trigger_apps");
				return this.serverTriggerApps.filter(function (triggerApp) {
					return !!newTriggerApps.findWhere({
						key: triggerApp.get("key")
					});
				});
			}
		}).mixin(AylaBackedMixin, {
			apiWrapperObjectName: "trigger"
		});
	});

	return App.Models.AylaTriggerModel;
});
"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/settings/views/EmptyList.view",
	"consumer/schedules/views/modal/SingleControlModalWidget.view"
], function (App, P, consumerTemplates, SalusView) {

	App.module("Consumer.Schedules.Views.Modal.Duplicate", function (Views, App, B, Mn, $, _) {
		Views.CheckboxView = Mn.LayoutView.extend({
			template: consumerTemplates["schedules/modal/thermostatCheckboxRow"],
			regions: {
				checkboxRegion: ".bb-checkbox"
			},
			initialize: function () {
				this.checkbox = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "equipmentCheckbox",
						nonKeyLabel: this.model.get("name"),
						secondaryIconClass: "",
						isChecked: false
					})
				});

				this.listenTo(this.checkbox, "clicked:checkbox", function () {
					this.trigger("clicked:checkbox");
				});
			},
			isChecked: function () {
				return this.checkbox.getIsChecked();
			},

			onRender: function () {
				this.checkboxRegion.show(this.checkbox);
			}
		}).mixin([SalusView]);

		Views.CheckboxCollection = Mn.CollectionView.extend({
			template: false,
			childView: Views.CheckboxView,
            emptyView: App.Consumer.Settings.Views.EmptyListView,
            emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "equipment.myEquipment.newGroup.empty"
			},
			childViewOptions: function () {
				return {
					mode: this.options.mode
				};
			},
			childEvents: {
				"clicked:checkbox": "checkForSelectedModels"
			},

			initialize: function () {
				_.bindAll(this, "checkForSelectedModels");

				//true if at least one item is checked
				this.oneIsChecked = false;
			},

			getSelectedModels: function () {
				return _.map(this.children.filter(function (view) {
					return view.isChecked();
				}), function (view) {
					return view.model;
				});
			},

			checkForSelectedModels: function () {
				this.oneIsChecked = this.children.some(function (view) {
					return view.isChecked();
				});

				this.trigger("save:duplication");
			}
		}).mixin([SalusView]);

		return App.Consumer.Schedules.Views.Modal.Duplicate.CheckboxCollection;
	});
});

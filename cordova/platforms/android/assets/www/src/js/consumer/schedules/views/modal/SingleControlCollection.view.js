"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/settings/views/EmptyList.view",
	"consumer/schedules/views/modal/SingleControlModalWidget.view"
], function (App, P, consumerTemplates, SalusView) {

	App.module("Consumer.Schedules.Views.Modal", function (Views, App, B, Mn, $, _) {
		Views.SingleControlCollection = Mn.CollectionView.extend({
			childView: Views.SingleControlModalWidget,
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "schedules.edit.modal.emptySingleControls"
			},
			className: "single-controls-table",
			initialize: function () {
				this.childViews = [];

				this.listenTo(this, "save:tsc", function () {
					this._saveAllSingleControls();
				});
			},
			childEvents: {
				"render": "_onAddChildView",
				"remove:tsc": "_onTscRemove"
			},
			// destroy unsaved singleControlWidget if delete is clicked
			_onTscRemove: function (childView) {
				childView.destroy();
			},
			_onAddChildView: function (childView) {
				if (childView.hasOwnProperty("isEdit")) {
					this.childViews.push(childView);
				}
			},

			_hasCheckedDuplicateTstats: function(views) {
				var checkedThermostats = [];
				_.each(views, function(view) {
					_.each(view.thermostatsRegion.currentView.getCheckedItemKeys(), function(key) {
						checkedThermostats.push(key);
					});
				});
				var uniqueCheckedTstats = _.uniq(checkedThermostats);

				return checkedThermostats.length !== uniqueCheckedTstats.length;
			},

			_saveAllSingleControls: function () {
				var that = this, promises = [];
				var views = _.filter(this.childViews, function(view) {
					return !view.isDestroyed;
				});

				if (this._hasCheckedDuplicateTstats(views)) {
					promises.push(P.reject("Cannot save, duplicate thermostat used"));
				} else {
					promises = _.map(views, function (child) {
						return child.saveTSC();
					});
				}
				return P.all(promises).then(function () {
					that.trigger("tsc:post:success");
				}).catch(function () {
					that.trigger("tsc:post:fail");
				});
			}
		}).mixin([SalusView]);

		return App.Consumer.Schedules.Views.Modal.SingleControlCollection;
	});
});
"use strict";

define([
	"app",
	"momentWrapper",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel"
], function (App, moment, constants, consumerTemplates, SalusView, CheckboxView, CheckboxModel) {
	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		// simple mode menu viewModels
		var fanModes = [
			{
				modeTextKey: "equipment.oneTouch.menus.doThis.propertyChange.fanAuto",
				modeType: constants.fanModeTypes.AUTO,
				modeIconClass: "auto-icon"
			},
			{
				modeTextKey: "equipment.oneTouch.menus.doThis.propertyChange.fanOn",
				modeType: constants.fanModeTypes.ON,
				modeIconClass: "fan-icon"
			}
		];

		var systemModes = [
			{
				modeTextKey: "equipment.thermostat.menus.modeLabels.heating",
				modeType: constants.thermostatModeTypes.HEAT,
				modeIconClass: "heating-icon"
			},
			{
				modeTextKey: "equipment.thermostat.menus.modeLabels.cooling",
				modeType: constants.thermostatModeTypes.COOL,
				modeIconClass: "cooling-icon"
			},
			{
				modeTextKey: "equipment.thermostat.menus.modeLabels.off",
				modeType: constants.thermostatModeTypes.OFF,
				modeIconClass: "off-icon"
			}
		];

		var it600Modes = [
			{ // these will have hold types
				modeTextKey: "equipment.thermostat.menus.modeLabels.heating",
				modeType: constants.thermostatModeTypes.HEAT,
				modeIconClass: "heating-icon"
			},
			{
				modeTextKey: "equipment.thermostat.menus.modeLabels.off",
				modeType: constants.thermostatModeTypes.OFF,
				modeIconClass: "off-icon"
			}
		];
        
        var holdType = [
            { // these will have hold types
				modeTextKey: "equipment.thermostat.menus.modeLabels.followSchedule",
				modeType: constants.it600HoldTypes.FOLLOW,
				modeIconClass: "on-icon"
			},
//            { // these will have hold types
//				modeTextKey: "equipment.thermostat.menus.modeLabels.permanentHold",
//				modeType: constants.it600HoldTypes.PERMHOLD,
//				modeIconClass: "permanent-hold-icon"
//			},
            { // these will have hold types
				modeTextKey: "equipment.thermostat.menus.modeLabels.off",
				modeType: constants.it600HoldTypes.OFF,
				modeIconClass: "off-icon"
			},
        ];
        
        var lockKey = [
            {
                modeTextKey: "equipment.thermostat.menus.modeLabels.lock",
				modeType: constants.lockKey.LOCK,
				modeIconClass: "lock-icon"
            },
            {
                modeTextKey: "equipment.thermostat.menus.modeLabels.unlock",
				modeType: constants.lockKey.UNLOCK,
				modeIconClass: "unlock-icon"
            }
        ];

		// menu option
		// has checkbox
		Views.ThermostatModeOptionView = Mn.ItemView.extend({
			template: false,
			className: "thermostat-mode-option",
			initialize: function () {
				var that = this;

				this.checkboxView = new CheckboxView({
					model: new CheckboxModel({
						name: "thermostatModeOption",
						isChecked: false,
						secondaryIconClass: "mode-icon" + " " + this.model.get("modeIconClass"),
						labelTextKey: this.model.get("modeTextKey")
					})
				});

				// so we can disable all others on selection
				this.listenTo(this.checkboxView, "clicked:checkbox", function () {
					that.trigger("click:checkbox", that);
				});
			},
			onRender: function () {
				this.$el.append(this.checkboxView.render().$el);
			},
			onDestroy: function () {
				this.checkboxView.destroy();
			}
		}).mixin([SalusView]);

		// collection view and handles getting rule data
		Views.ThermostatModePickerView = Mn.CollectionView.extend({
			template: false,
			childView: Views.ThermostatModeOptionView,
			className: "thermostat-mode-menu margin-b-15",
			childEvents: {
				"click:checkbox": "_switchModes"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "getRuleData", "_switchModes", "getCheckedModel");

				this.subType = this.options.menu.menuModel.get("subType");
				this.currentDevice = this.options.manager.currentDevice;

				this.collection = new B.Collection();

				if (this.subType === "fan-mode") {
					this.collection.set(fanModes);
				} else if (this.subType === "system-mode") {
					if (this.currentDevice.modelType === constants.modelTypes.THERMOSTAT) {
						this.collection.set(systemModes);
					} else if (this.currentDevice.modelType === constants.modelTypes.IT600THERMOSTAT) {
						this.collection.set(it600Modes);
					}
				} else if(this.subType === "hold-type"){
					this.collection.set(holdType);
                } else if(this.subType === "lock-key"){
                    this.collection.set(lockKey);
                }
			},
			getRuleData: function () {
				var selectedModel = this.getCheckedModel(), propName;

				if (this.subType === "system-mode") {
					if (this.currentDevice.modelType === constants.modelTypes.IT600THERMOSTAT) {
						propName = "HoldType";
					} else {
						propName = "SystemMode";
					}
				} else if (this.subType === "fan-mode") {
					propName = "FanMode";
				} else if(this.subType === "hold-type"){
                    propName = "HoldType";
                } else if(this.subType === "lock-key"){
                    propName = "LockKey";
                }

				return !selectedModel ? false : {
					type: "propChange",
					newVal: selectedModel.get("modeType"),
					propName: this.currentDevice.get(propName) ? this.currentDevice.getSetterPropNameIfPossible(propName) : "",
					device: this.currentDevice,
					EUID: this.currentDevice.get("EUID") ? this.currentDevice.getEUID() : ""
				};
			},
			_switchModes: function (selectingChildView) {
				this.children.each(function (childView) {
					if (childView.checkboxView.getIsChecked()) {
						childView.checkboxView.model.set("isChecked", false);
					}
				});

				// set it back on
				selectingChildView.checkboxView.model.set("isChecked", true);
			},
			getCheckedModel: function () {
				var view = this.children.find(function (childView) {
					return childView.checkboxView.getIsChecked();
				});

				return view ? view.model : false;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.ThermostatModePickerView;
});
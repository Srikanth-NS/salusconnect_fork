"use strict";

define([
	"app",
	"consumer/consumerTemplates"
], function (App, templates) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {

		Views.ModalDefaultHeader = Mn.ItemView.extend({
			template: templates.modalHeader,
			className: "modal-header salus-modal-header",
			templateHelpers: function () {
				var title = "";
				if (this.model && this.model.get("titleKey")) {
					title = App.translate(this.model.get("titleKey"));
				}

				return {
					title: title
				};
			}
		});
	});
});
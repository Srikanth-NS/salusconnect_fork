"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, constants, templates, SalusView) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {

		Views.ScanResultsDeviceView = Mn.ItemView.extend({
			template: templates["dashboard/header/scanResultsDevice"],
			className: "dashboard-scan-results-device row center-contents-row",
			bindings: {
				".bb-device-connectivity-icon": {
					observe: "connection_status",
					update: function ($el, status) {
						$el.removeClass("online offline");

						if (status === "Online") {
							$el.addClass("online");
						} else if (status === "Offline") {
							$el.addClass("offline");
						}
					}
				},
				".bb-device-name": {
					observe: "name"
				},
				".bb-device-msg-top": {
					observe: "connection_status",
					update: function ($el, status) {
						if (status === "Online") {
							$el.text(App.translate("equipment.status.connected"));
						} else {
							$el.text(App.translate("equipment.status.notConnected"));
						}
					}
				}
			},
			/**
			 * add stickit binding by device type
			 */
			onRender: function () {
				var modelTypes = constants.modelTypes,
					stickits = {
						observe: "",
						onGet: function (prop) {
							return prop ? prop.getProperty() : null;
						}
					};

				if (this.model.isSmartPlug()) {
					stickits.observe = "OnOff";
					stickits.update = function ($el, val) {
						if (!!val) {
							$el.text(App.translate("equipment.oneTouch.menus.when.stateOfEquipment.smartPlug.on"));
						} else {
							$el.text(App.translate("equipment.oneTouch.menus.when.stateOfEquipment.smartPlug.off"));
						}
					};
				} else if (this.model.modelType === modelTypes.DOORMONITOR || this.model.modelType === modelTypes.WINDOWMONITOR) {
					stickits.observe = ["ErrorIASZSAlarmed1", "ErrorIASZSAlarmed2"];
					stickits.onGet = function (alarmProps) {
						var alarm1 = alarmProps[0], alarm2 = alarmProps[1];

						if (alarm1 && alarm2) {
							return alarm1.getProperty() || alarm2.getProperty();
						}

						return false;
					};
					stickits.update = function ($el, val, deviceModel) {
						var nameTranslated = App.translate("equipment.names." + deviceModel.modelType), status = nameTranslated + " ";

						if (val === 1) {
							status += App.translate("equipment.oneTouch.menus.when.stateOfEquipment.doorWindow.open");
						} else if (val === 0) {
							status += App.translate("equipment.oneTouch.menus.when.stateOfEquipment.doorWindow.closed");
						} else {
							status = "";
						}

						$el.text(status);
					};
				} else if (this.model.modelType === modelTypes.WATERHEATER) {
					var waterHeaterModes = constants.waterHeaterHoldTypes;
					stickits.observe = "HoldType";
					stickits.update = function ($el, val, deviceModel) {
						var nameTranslated = App.translate("equipment.names." + deviceModel.modelType), status = nameTranslated + " ";

						if (val === waterHeaterModes.BOOST) {
							status += App.translate("equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.boost");
						} else if (val === waterHeaterModes.CONTINUOUS) {
							status += App.translate("equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.continuous");
						} else if (val === waterHeaterModes.OFF) {
							status += App.translate("equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.off");
						} else if (val === waterHeaterModes.SCHEDULE) {
							status += App.translate("equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.schedule");
						} else {
							status = "";
						}

						$el.text(status);
					};
				}

				// TODO: thermostats

				this.addBinding(null, ".bb-device-msg-bottom", stickits);
			}
		}).mixin([SalusView]);

		Views.ScanResultsCategories = Mn.CompositeView.extend({
			template: templates["dashboard/header/scanResultCategory"],
			childView: Views.ScanResultsDeviceView,
			className: "dashboard-scan-results-category col-xs-12",
			initialize: function () {
				this.manager = this.options.manager;
				this.category = this.model;
				this.collection = new B.Collection(this.model.get("category_devices"));
			},
			templateHelpers: function () {
				return {
					categoryName: App.translate(this.model.get("category_name_key"))
				};
			}
		}).mixin([SalusView]);

		Views.ScanResultsLayout = Mn.CollectionView.extend({
			className: "dashboard-scan-results row",
			childView: Views.ScanResultsCategories,
			childViewOptions: function () {
				return {
					manager: this.manager
				};
			},
			initialize: function () {
				this.manager = this.options.manager;
				this.collection = App.salusConnector.getDeviceCollectionSortedByCategory();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.ScanResultsLayout;
});
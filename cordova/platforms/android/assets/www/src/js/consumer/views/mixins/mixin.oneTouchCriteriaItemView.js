"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.OneTouchCriteriaItemMixin = function () {
			// also mixin salusView
			this.mixin([SalusView]);

			this.setDefaults({
				template: consumerTemplates["oneTouch/oneTouchCriteriaItem"],
				className: "one-touch-criteria-item",
				ui: {
					conditionOrAction: ".bb-condition-action-type",
					description: ".bb-condition-action-description",
					deleteButton: ".bb-delete-button"
				},
				events: {
					"click": "_handleClick"
				},

				_handleClick: function (event) {
					// checking if we clicked delete button first - otherwise behave like a disable click
					var $srcElement = $(event.originalEvent.target);

					if ($srcElement.hasClass("bb-delete-button")) {
						this._handleDeleteButtonPress();
					} else {
						// take away confirm from delete - works like a flag
						this.ui.deleteButton.text("");
						this.ui.deleteButton.removeClass("confirm");
					}
				},
				_handleDeleteButtonPress: function () {
					// confirm first
					if (!this.ui.deleteButton.hasClass("confirm")) {
						this.ui.deleteButton.addClass("confirm");
						this.ui.deleteButton.text(App.translate("common.labels.delete"));
					} else {
						this.trigger("remove:selection", this);
					}
				},
				/**
				 * helper function for propVals
				 * returns some context text for different devices
				 * TODO: more devices accounted for
				 * @param value - the integer value of the property we will get/set
				 * @param propName
				 * @returns {*}
				 * @private
				 */
				_getValueForDevice: function (value, prop) {
					if (!this.device) {
						return value;
					}

					var propName = prop || "";
					propName = _.isArray(propName) ? propName[0] : propName;

					var deviceModelType = this.device.modelType, propertyString;

					if (this.device.isSmartPlug()) {
						// value is 1 or 0 -> ON or OFF
						return App.translate(value ? "equipment.oneTouch.criteria.propVal.smartPlug.on" : "equipment.oneTouch.criteria.propVal.smartPlug.off");
					} else if (this.device.isThermostat()) {
						if (propName.indexOf("SystemMode") > -1) {
							if (value === constants.thermostatModeTypes.HEAT) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.heating");
							} else if (value === constants.thermostatModeTypes.COOL) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.cooling");
							} else if (value === constants.thermostatModeTypes.AUTO) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.auto");
							} else {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.off");
							}

							return propertyString;

						} else if (propName.indexOf("FanMode") > -1) {
							if (value === constants.fanModeTypes.AUTO) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.fanAuto");
							} else {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.fanOn");
							}

							return propertyString;
						} else if (propName.indexOf("SetHeating") > -1 || propName.indexOf("SetCooling") > -1 || propName.indexOf("LocalTemp") > -1) {
							// 1000 -> 100 degrees
							value = value / 100;

							return value + "" + App.translate("equipment.oneTouch.criteria.propVal.thermostat.degrees");
						} else if(propName.indexOf("RunningMode") > -1 ) {
                            switch(value){
                                case constants.runningModeTypes.HEAT:
                                    return App.translate("equipment.thermostat.menus.modeLabels.heating").toLowerCase();
                                case constants.runningModeTypes.COOL:
                                    return App.translate("equipment.thermostat.menus.modeLabels.cooling").toLowerCase();
                                case constants.runningModeTypes.OFF:
                                    return App.translate("equipment.thermostat.menus.modeLabels.off").toLowerCase();
                                default:
                                    return '';
                            }
                        } else if(propName.indexOf("SetHoldType") > -1){
                            switch(value){
                                case constants.it600HoldTypes.FOLLOW:
                                    return App.translate("equipment.oneTouch.criteria.propVal.waterHeater.schedule").toLowerCase();
                                case constants.it600HoldTypes.PERMHOLD:
                                    return App.translate("equipment.thermostat.menus.modeLabels.permanentHold").toLowerCase();
                                case constants.it600HoldTypes.OFF:
                                    return App.translate("equipment.thermostat.menus.modeLabels.off").toLowerCase();
                            }
                        } else if(propName.indexOf("HoldType") > -1){
                            switch(value){
                                case constants.it600HoldTypes.FOLLOW:
                                    return App.translate("equipment.oneTouch.criteria.propVal.waterHeater.schedule").toLowerCase();
                                case constants.it600HoldTypes.TEMPHOLD:
                                    return App.translate("equipment.thermostat.menus.modeLabels.temporaryHold").toLowerCase();
                                case constants.it600HoldTypes.PERMHOLD:
                                    return App.translate("equipment.thermostat.menus.modeLabels.permanentHold").toLowerCase();
                                case constants.it600HoldTypes.OFF:
                                    return App.translate("equipment.thermostat.menus.modeLabels.offMode").toLowerCase();
                            }
                        } else if(propName.indexOf("LockKey") > -1){
                            switch(value){
                                case constants.lockKey.LOCK:
                                    return App.translate("equipment.thermostat.menus.modeLabels.lock").toLowerCase();
                                case constants.lockKey.UNLOCK:
                                    return App.translate("equipment.thermostat.menus.modeLabels.unlock").toLowerCase();
                            }
                        } else {
//							throw new Error("Invalid property name");
						}
					} else if (deviceModelType === constants.modelTypes.CARBONMONOXIDEDETECTOR || deviceModelType === constants.modelTypes.SMOKEDETECTOR) {
						return App.translate("equipment.oneTouch.criteria.propVal.sensor.alerting");
					} else if (deviceModelType === constants.modelTypes.WINDOWMONITOR || deviceModelType === constants.modelTypes.DOORMONITOR) {
						// 0 or 1
						return App.translate(value ? "equipment.oneTouch.criteria.propVal.doorWindow.open" : "equipment.oneTouch.criteria.propVal.doorWindow.closed");
					} else if (deviceModelType === constants.modelTypes.WATERHEATER) {
						if (propName.indexOf("HoldType") > -1) {
							if (value === constants.waterHeaterHoldTypes.OFF) {
//                                propertyString = App.translate("equipment.oneTouch.criteria.propVal.waterHeater.off");
								propertyString = App.translate("equipment.thermostat.menus.modeLabels.offMode").toLowerCase();
							} else if (value === constants.waterHeaterHoldTypes.SCHEDULE) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.waterHeater.schedule");
							} else if (value === constants.waterHeaterHoldTypes.CONTINUOUS) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.waterHeater.continuous");
							} else if (value === constants.waterHeaterHoldTypes.BOOST) {
								propertyString = App.translate("equipment.oneTouch.criteria.propVal.waterHeater.boost");
							}

							return propertyString;
						} else if(propName.indexOf("RunningMode") > -1){
                            if(value ===constants.runningModeTypes.HEAT){
                                return App.translate("equipment.thermostat.menus.modeLabels.on").toLowerCase();
                            }else if(value ===constants.runningModeTypes.OFF){
                                return App.translate("equipment.thermostat.menus.modeLabels.off").toLowerCase();
                            }
                        } else if(propName.indexOf("LockKey") > -1){
                            switch(value){
                                case constants.lockKey.LOCK:
                                    return App.translate("equipment.thermostat.menus.modeLabels.lock").toLowerCase();
                                case constants.lockKey.UNLOCK:
                                    return App.translate("equipment.thermostat.menus.modeLabels.unlock").toLowerCase();
                            }
                        }
					}
                    
                    return "";
				},
				_changeOrWhenText: function () {
					if (this.currentRule) {
						if (this.currentRule.getConditions().isEmpty()) {
							this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.when"));
						} else {
							this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.orWhen"));
						}
					}
				}
			});

			this.before("initialize", function (/*options*/) {
				_.bindAll(this,
					"_handleClick",
					"_handleDeleteButtonPress",
					"_getValueForDevice",
					"_changeOrWhenText"
				);

				// ruleType is tod, dow, sms, email, propVal, propChange, etc..
				this.ruleType = this.model.get("type");

				// key off constants.oneTouchMenuTypes.*
				this.conditionOrAction = this.options.type;

				// get the device model from our model, or by looking for EUID
				this.device = this.model.get("device") || App.salusConnector.getDeviceByEUID(this.model.get("EUID"));

				if (this.model.get("type") === "buttonPress") {
					this.currentRule = App.Consumer.OneTouch.ruleMakerManager.getCurrentRule();

					if (this.currentRule) {
						this.listenTo(this.currentRule.getConditions(), "update", this._changeOrWhenText);
					}
				}
			});

			this.after("render", function () {
				if (this.options.collectionIndex === 0) { // first of type
					if (this.conditionOrAction === constants.oneTouchMenuTypes.condition) {
						this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.when"));
					} else if (this.conditionOrAction === constants.oneTouchMenuTypes.action) {
						this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.doThis"));
					} else if (this.conditionOrAction === constants.oneTouchMenuTypes.delayedAction) {
						this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.thenDoThisLater"));
					}
				} else {
					if (this.conditionOrAction === constants.oneTouchMenuTypes.condition) {
						this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.andWhen"));
					} else if (this.conditionOrAction === constants.oneTouchMenuTypes.action) {
						this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.thenDoThis"));
					} else if (this.conditionOrAction === constants.oneTouchMenuTypes.delayedAction) {
						this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.thenDoThisLater"));
					}
				}

				if (this.model.get("type") === "buttonPress") {
					this._changeOrWhenText();
				}

				if (this.model.get("type") === "unparsable") {
					this.ui.conditionOrAction.text(App.translate("equipment.oneTouch.criteria.sorry"));
				}

				if (this.options.readonly) {
					this.ui.deleteButton.detach();
					this.ui.description.addClass("extend");
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.OneTouchCriteriaItemMixin;
});
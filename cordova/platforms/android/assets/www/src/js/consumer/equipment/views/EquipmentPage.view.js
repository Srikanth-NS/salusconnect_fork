"use strict";

define([
	"app",
	"bluebird",
	"common/config",
	"common/constants",
	"common/util/equipmentUtilities",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/Breadcrumbs.view",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/SalusModal.view",
	"consumer/equipment/views/DeleteEquipmentModal.view",
	"consumer/equipment/views/EquipmentSettingsWidget.view"
], function (App, P, config, constants, equipUtils, consumerTemplates, SalusPageMixin, BreadcrumbsView, SalusLinkButtonView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.EquipmentPageView = Mn.LayoutView.extend({
			id: "bb-equipment-page",
			regions: {
				breadcrumbs: ".bb-breadcrumbs",
				trvRegion: "#bb-trv-region",
				infoRegion: "#bb-info-region",
				statusRegion: "#bb-status-region",
				scheduleRegion: "#bb-schedule-region",
				historyRegion: "#bb-history-region",
				removeEquipment: "#bb-remove-equipment",
				graphRegion: "#bb-graph-region"
			},
			className: "equipment-page container",
			template: consumerTemplates["equipment/equipmentPage"],
			initialize: function (options) {
				_.bindAll(this, "handleRemoveClick", "initiateRefreshPoll", "clearRefreshPollTimeout", "refreshPoll");

				this.dsn = options.dsn;
				this.model = App.salusConnector.getDevice(this.dsn);

				if (App.Consumer.shouldScrollTo) {
					this.scrollTo = App.Consumer.shouldScrollTo;
					App.Consumer.shouldScrollTo = "";
				}
			},
			onBeforeDestroy: function() {
				this.clearRefreshPollTimeout();
			},
            
            showBreadcrumbs: function(){
                var that = this;
                that.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
                    crumbs: [
                        {
                            textKey: "equipment.myEquipment.title",
                            href: "/equipment"
                        },
                        {
                            text: that.model.getDisplayName(),
                            active: true
                        }
                    ]
                }));
            },
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
					if (!that.model) {
						that.model = App.salusConnector.getDevice(that.dsn);

						if (!that.model) {
							// couldn't get
							App.navigate("equipment");
							return;
						}
					}

					that.model.getDevicePropertiesPromise().then(function () {
						if (!that.isDestroyed) {
							that.showBreadcrumbs();
                            
							//BUG_SCS-3279 start
                            if (that.model.get("EUID") && _.isNull(that.model.getPropertyValue("EUID")) && that.model.isOnline() && !that.model.isLeaveNetwork()) {
								that.model.setProperty("SetRefresh_d", 1);
							}
							//BUG_SCS-3279 end

							if (that.model.modelType === constants.modelTypes.IT600THERMOSTAT && that.model.attributes.PairedWCNumber.attributes.value === 101) {

								that.trvRegion.show(new Views.EquipmentSettingsWidget({
									type: constants.widgetTypes.TRV,
									model: that.model
								}));
							}

							
                            that.statusView = new Views.EquipmentSettingsWidget({
								type: constants.widgetTypes.STATUS,
								model: that.model
							});
                            
                            that.listenTo(that.statusView, "click:saveName", function() {
                                that.showBreadcrumbs();
                            });
                            
                            that.statusRegion.show(that.statusView);

							that.infoRegion.show(new Views.EquipmentSettingsWidget({
								type: constants.widgetTypes.INFORMATION,
								model: that.model
							}));

							if (that.model.supportsSchedules()) {
								that.scheduleRegion.show(new Views.EquipmentSettingsWidget({
									type: constants.widgetTypes.SCHEDULE,
									model: that.model,
									dsn: that.dsn
								}));
							}

							if (App.isDevOrInt()) {
								that.historyRegion.show(new Views.EquipmentSettingsWidget({
									type: constants.widgetTypes.HISTORY,
									model: that.model
								}));
							}

							if (that.model.modelType === constants.modelTypes.ENERGYMETER || 
                                    that.model.modelType === constants.modelTypes.SMARTPLUG || 
                                    that.model.modelType === constants.modelTypes.MINISMARTPLUG ) {
								that.graphView = new Views.EquipmentSettingsWidget({
									type: constants.widgetTypes.USAGE,
									model: that.model
								});

								// TODO: this was done as part of SCS-2683 to achieve scrolling to the graph region from the dashboard tile
								that.listenTo(that.graphView, "show:usage", function () {
									that.trigger("show:children");
								});

								that.graphRegion.show(that.graphView);
                                
							}
                             //if current gateway status is online,it can be show "remove device"
                             //Bug_CASBW-216 start
                            if(App.getCurrentGateway().get("connection_status") === "Online"){
                             //Bug_CASBW-216 end
                                var removeEquipmentFormatKey = that.model.isSharedDevice() ?
									"equipment.delete.removeSharedEquipmentFormatted" :
									"equipment.delete.removeEquipmentFormatted";

                                that.removeEquipment.show(new SalusLinkButtonView({
                                    buttonText: App.translate(removeEquipmentFormatKey, {
                                        modelTypeName: App.translate("equipment.names." + (that.model.modelType || constants.modelTypes.GENERIC))
                                    }),
                                    cssClass: "center-block" + (equipUtils.shouldPreserveCategoryCase(that.model) ? " preserve-case" : ""),
                                    clickedDelegate: that.handleRemoveClick
                                }));
                            }
							
						}
					});
				});
				
				this.initiateRefreshPoll();
			},
			handleRemoveClick: function () {
				if (this.model.isSharedDevice()) {
					//TODO SCS-1807
					this._notImplementedAlert();
				} else {
					App.modalRegion.show(new App.Consumer.Views.SalusModalView({
						contentView: new App.Consumer.Equipment.Views.DeleteEquipmentModalView({model: this.model})
					}));

					App.showModal();
				}
			},
           
			initiateRefreshPoll: function () {
				this.pollingInterval = setInterval(this.refreshPoll, config.devicePollingInformation.devicePropsPollingInterval);
			},
			clearRefreshPollTimeout: function () {
				clearInterval(this.pollingInterval);
			},
			refreshPoll: function () {
				this.model.getDeviceProperties(true);
			}
           
		}).mixin([SalusPageMixin], {
			analyticsSection: "equipment",
			analyticsPage: "equipment" // TODO: This should be a function so the equipment Id is recorded.
		});
	});

	return App.Consumer.Equipment.Views.EquipmentPageView;
});
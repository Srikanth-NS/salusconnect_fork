"use strict";

define([
	"app",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusDropdown",
	"consumer/views/mixins/mixin.validation",
	"dropdown"
], function (App, commonTemplates) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $, _) {
		Views.SalusDropdownItemViewNoi18N = Mn.ItemView.extend({
			template: commonTemplates.salusDropdownItemNoi18N,
			tagName: "li",
			triggers: {
				"click": "change:value"
			},
			onRender: function () {
				this.$el.attr("value", this.model.get("value"));
			}
		}).mixin([Views.Mixins.SalusView]);

		Views.SalusDropdownItemView = Mn.ItemView.extend({
			template: commonTemplates.salusDropdownItem,
			tagName: "li",
			triggers: {
				"click": "change:value"
			},
			onRender: function () {
				this.$el.attr("value", this.model.get("value"));
			}
		}).mixin([Views.Mixins.SalusView]);

		/**
		 * Dropdown view, no i18N
		 */
		Views.SalusDropdownViewNoi18N = Mn.CompositeView.extend({
			template: commonTemplates.salusDropdownNoi18N,
			className: "dropdown",
			ui: {
				"dropdownToggle": ".dropdown-toggle"
			},
			childView: Views.SalusDropdownItemViewNoi18N,
			childViewContainer: ".bb-dropdown-item-container"
		}).mixin([Views.Mixins.SalusDropdown]);

		/**
		 * Dropdown view
		 */
		Views.SalusDropdownView = Mn.CompositeView.extend({
			template: commonTemplates.salusDropdown,
			className: "dropdown",
			ui: {
				"dropdownToggle": ".dropdown-toggle"
			},
			childView: Views.SalusDropdownItemView,
			childViewContainer: ".bb-dropdown-item-container"
		}).mixin([Views.Mixins.SalusDropdown]);

		/**
		 * Dropdown view with validation
		 */
		Views.SalusDropdownWithValidation = Views.SalusDropdownView.extend({
			events: {
				"focusout": "validateDropdownField"
			},
			isValid: function () {
				var validation = { valid: true };

				//Can't test for truthy value as "0" is a valid value.
				var value = this.viewModel.get("value");
				if (this.options.required && (_.isUndefined(value) || _.isNull(value))) {
					validation = {
						valid: false,
						message: "common.validation.default.requiredField"
					};
				}

				return validation;
			},
			onBeforeDestroy: function () {
				this.$(".bb-dropdown-item-container").off();
			},
			validateDropdownField: function () {
				var that = this;

				setTimeout(function () {
					var $activeElement = $(document.activeElement),
							$dropdownItems = that.$(".bb-dropdown-item-container");

					if ($.contains($dropdownItems[0], $activeElement[0])) {
						$activeElement.on("focusout", function () {
							that.isValid();
							$activeElement.off();
						});
					} else {
						that.validateField();
					}

				}, 0);
			}
		}).mixin([Views.Mixins.Validation]);
	});

	return App.Consumer.Views.SalusDropdownView;
});

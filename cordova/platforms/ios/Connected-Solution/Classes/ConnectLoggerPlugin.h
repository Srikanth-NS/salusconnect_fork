//
//  ConnectLoggerPlugin.h
//  Connected-Solution
//
//

#import <Cordova/CDVPlugin.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ConnectLoggerPlugin : CDVPlugin <MFMailComposeViewControllerDelegate>

- (void) sendErrorLogs:(CDVInvokedUrlCommand *)command;
- (void) writeToLog:(CDVInvokedUrlCommand *)command;

@end

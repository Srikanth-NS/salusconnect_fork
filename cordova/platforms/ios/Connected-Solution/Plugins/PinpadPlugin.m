//
//  PinpadPlugin.m
//  Connected-Solution
//

#import "PinpadPlugin.h"

static NSUInteger PIN_LENGTH = 4;

@implementation PinpadPlugin


/******** CORDOVA PLUGIN FUNCTIONS ********/

/** 
 * havePinSet
 * checks to see if there is a pin set on the device
 * used only when we set the mode (create, authenticate)
 * of the pinpad on js end
 */
-(void) havePinSet:(CDVInvokedUrlCommand *)command
{
    [self.commandDelegate runInBackground:^{
        if ([self hasPin]) {
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES] callbackId:command.callbackId];
        } else {
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:NO] callbackId:command.callbackId];
        }
    }];
}

/**
 * setPin
 * set the pin and return result
 */
-(void) setPin:(CDVInvokedUrlCommand *) command
{
    NSArray *pin = command.arguments.firstObject;
    
    [self.commandDelegate runInBackground:^{
        if (pin.count == PIN_LENGTH) {
            [self storePin:pin];
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES] callbackId:command.callbackId];
        } else {
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Pin was not length 4"] callbackId:command.callbackId];
        }
    }];
}

/**
 * checkPin
 * check the pin against what is saved in keychain and send a bool as result
 * or error if there was no pin set
 */
-(void) checkPin:(CDVInvokedUrlCommand *) command
{
    NSArray *pin = command.arguments.firstObject;

    [self.commandDelegate runInBackground:^{
        if ([self hasPin]) {
            if ([self comparePin:pin]) {
                [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES] callbackId:command.callbackId];
            } else {
                [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:NO] callbackId:command.callbackId];
            }
        } else {
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"User did not have a pin set"] callbackId:command.callbackId];
        }
    }];
}

/**
 * removePin
 * return OK and pin was removed upon success
 * or ERROR and user no pin set on fail
 */
-(void) removePin:(CDVInvokedUrlCommand *)command
{
    [self.commandDelegate runInBackground:^{
        if ([self hasPin]) {
            [self removePin];
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Pin was removed"] callbackId:command.callbackId];
        } else {
            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"User did not have a pin set"] callbackId:command.callbackId];
        }
    }];
}


/******** LOCAL HELPERS ********/

/**
 * hasPin
 * checks if there is a pin set
 */
-(BOOL) hasPin
{
    if ([[UICKeyChainStore keyChainStore] dataForKey:@"userHasPin"]) {
        return YES;
    }
    return NO;
}

/**
 * comparePin
 * compare passed in pin (as array) to keychain stored pin
 */
-(BOOL) comparePin:(NSArray *) pin
{
    if (pin.count != PIN_LENGTH || ![self hasPin]) {
        return NO;
    }
    // convert pin (array) to string
    NSString *pinToCheck = [self stringFromArray:pin];
    
    return [pinToCheck isEqualToString:[[UICKeyChainStore keyChainStore] stringForKey:@"userPin"]];
}

/**
 * getPin
 * get the pin from keychain as a string
 */
-(NSString *) getPin
{
    return [[UICKeyChainStore keyChainStore] stringForKey:@"userPin"];
}

/**
 * storePin
 * store the pin in keychain
 * set true for userHasPin
 */
-(void) storePin:(NSArray *) pin
{
    if (pin != nil && pin.count == PIN_LENGTH) {
        // our keychain wrapper only lets us store strings or data - convert pinArray to string
        NSString *pinString = [self stringFromArray:pin];
            
        // [NSData data] is truthy
        [[UICKeyChainStore keyChainStore] setString:pinString forKey:@"userPin"];
        [[UICKeyChainStore keyChainStore] setData:[NSData data] forKey:@"userHasPin"];
    }
}

/**
 * removePin
 * remove the stored pin and set nil for userHasPin
 */
-(void) removePin
{
    [[UICKeyChainStore keyChainStore] removeItemForKey:@"userPin"];
    [[UICKeyChainStore keyChainStore] setData:nil forKey:@"userHasPin"];
}

/** 
 * stringFromArray
 * converts an array to a string
 */
-(NSString *) stringFromArray:(NSArray *) array
{
    NSString *string = @"";
    for (id item in array) {
        string = [string stringByAppendingString:[item stringValue]];
    }
    return string;
}

@end

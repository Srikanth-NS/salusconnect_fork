"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusButtonPrimary"
], function (App, consumerTemplates, SalusViewMixin, SalusBtnPrimView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.WelcomeWellView = Mn.ItemView.extend({
			id: "login-welcome-well",
			template: consumerTemplates["login/WelcomeWell"],
			ui: {
				"siloSwitch": ".bb-silo-switch"
			},
			initialize: function () {
				_.bindAll(this, "handleButtonClicked");

				this.signUpButton = new SalusBtnPrimView({
					id: "sign-up-btn",
					buttonTextKey: "login.welcome.button",
					clickedDelegate: this.handleButtonClicked
				});
			},

			onRender: function () {
				this.$("#sign-in-btn-region").append(this.signUpButton.render().$el);

				if (App.onMobile()) {
					this.$(".bb-silo-switch-container").removeClass('hidden');
					this.listenTo(App.vent, "swap:silo", this._setSwitchText);
					this._setSwitchText();
					this.ui.siloSwitch.click(function () {
						App.swapSilos();
					});
				}

				return this;
			},

			onDestroy: function () {
				this.signUpButton.destroy();
			},

			handleButtonClicked: function () {
				App.navigate("newUser");
			},

			_setSwitchText: function () {
				this.ui.siloSwitch.text(App.translate("login.login.siloMessage." + (App.getIsEU() ? "eu" : "us")));
			}
		}).mixin([SalusViewMixin]);
	});


	return App.Consumer.Login.Views.WelcomeWellView;
});

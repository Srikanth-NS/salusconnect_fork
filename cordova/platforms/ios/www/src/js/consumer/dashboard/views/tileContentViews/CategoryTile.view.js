"use strict";

define([
	"app",
	"bluebird",
	"jquery.mobile",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs"
], function (App, P, $Mobile, constants, consumerTemplates, SalusView, VendorfyCssForJs) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {
		Views.CategoryTileView = Mn.ItemView.extend({
			template: consumerTemplates["dashboard/tile/categoryTile"],
			className: "category-tile col-xs-6 col-sm-4",
			attributes: {
				"role": "button"
			},
			ui: {
				alert: ".bb-category-alert-icon",
				gradientWithIcon: ".bb-category-tile-background"
			},
			events: {
				click: "_handleClick"
			},
			onRender: function () {
				this._setIdleStyleOnTile();
			},
			/**
			 * Sets the green gradient and tile image for this tile
			 * @private
			 */
			_setIdleStyleOnTile: function () {
				VendorfyCssForJs.formatBgGradientWithImage(
					this.ui.gradientWithIcon,
					"center 50%/48% 48%",
					"no-repeat",
					this.model.get("category_icon_url"),
					"145deg",
					constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
					"0%",
					constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
					"100%"
				);
			},
			/**
			 * TODO Wire this up to actual alerts when those occur
			 * @private
			 */
			_setAlertStyleOnTile: function () {
				VendorfyCssForJs.formatBgGradientWithImage(
					this.ui.gradientWithIcon,
					"center 50%/48% 48%",
					"no-repeat",
					this.model.get("category_icon_url"),
					"145deg",
					constants.tileGradients.TILE_RED_GRADIENT_TOP,
					"0%",
					constants.tileGradients.TILE_RED_GRADIENT_BOTTOM,
					"100%"
				);
			},
			_handleClick: function (/*event*/) {
				this._goToCategoryPage(this.model.get("category_type"));
			},
			_goToCategoryPage: function (category) {
				App.navigate("equipment/categories/" + category);
			},
			templateHelpers: function () {
				return {
					categoryName: App.translate(this.model.get("category_name_key"))
				};
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.CategoryTileView;
});

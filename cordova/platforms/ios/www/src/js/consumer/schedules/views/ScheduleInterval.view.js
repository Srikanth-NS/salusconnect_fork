"use strict";

define([
    "app",
    "moment",
    "common/util/utilities",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/schedules/views/AddNewInterval.view"
], function (App, moment, utilities, constants, consumerTemplates, SalusViewMixin) {

    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

        Views.ScheduleIntervalView = Mn.LayoutView.extend({
            className: "interval-view",
            template: consumerTemplates["schedules/scheduleInterval"],
            ui: {
                time: ".bb-time",
                temperature: ".bb-temperature",
                delete: ".bb-delete-interval",
                confirmDelete: ".bb-confirm-delete-interval"
            },
            events: {
                "click .bb-edit-interval": "_editClicked",
                "click .bb-delete-interval": "_deleteClicked",
                "click .bb-confirm-delete-interval": "_confirmDeleteClicked"
            },
            bindings: {
                ".bb-time": {
                    observe: "time",
                    onGet: function (time) {
                        if (this.timeFormat24Hour) {
                            return moment(time, "HH:mm:ss").format("HH:mm");
                        } else {
                            return moment(time, "HH:mm:ss").format("hh:mm a");
                        }
                    }
                },
                ".bb-temperature": {
                    observe: "value",
                    onGet: function (value) {
                        var numberString, splitNumberString;

                        if (this.onOffMode) {
                            this.ui.temperature.addClass("mode");

                            if (!value) {
                                this.ui.temperature.addClass("lighter");
                            }
                            return value ? App.translate("common.labels.on") : App.translate("common.labels.off");
                        }

                        numberString = utilities.roundHalf(value / 100);
                        splitNumberString = numberString.split(".");

                        return (splitNumberString[1] === "0" ? splitNumberString[0] : numberString) + "\u00B0";
                    }
                }
            },
            initialize: function (options) {
                options = options || {};
                this.onOffMode = !!options.onOffMode;
                this.timeFormat24Hour = options.timeFormat24Hour;

                this.$el.addClass(this.onOffMode ? "on-off-mode" : "");
            },
            _editClicked: function () {
                this.model.isEditMode = true;
                this.trigger("enabled:editMode");
            },
            _deleteClicked: function () {
                this.ui.delete.addClass("hidden");
                this.ui.confirmDelete.removeClass("hidden");
            },
            _confirmDeleteClicked: function () {
                this.trigger("intervalChange");
                this.model.destroy();
            }


        }).mixin([SalusViewMixin]);
    });

    return App.Consumer.Schedules.Views.ScheduleIntervalView;
});

"use strict";

define([
	"app",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, commonTemplates, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.CheckboxView = Mn.ItemView.extend({
			template: commonTemplates["salusForms/salusCheckbox"],
			className: "salus-checkbox",
			events: {
				"click": "_handleClick",
                "keydown": "_handleKeyDown"
			},
			modelEvents: {
				"change:disabled": "_handleDisabledChange"
			},
			ui: {
				"checkbox": ".bb-checkbox-icon"
			},
			bindings: {
				".checkbox-icon": {
					observe: "isChecked",
					update: function ($el, val) {
						$el.toggleClass("checked", val);
					},
					onGet: function (value) {
						return value;
					}
				},
				".checkbox-input": {
					observe: "isChecked",
					update: function ($el, val) {
						$el.attr("checked", val);
					},
					onGet: function (value) {
						return value;
					}
				}
			},
			initialize: function () {
				if (this.model.get("classes")) {
					this.$el.addClass(this.model.get("classes"));
				}
			},
			showCheckBox: function () {
				this.ui.checkbox.show();
				this.model.set("disabled", false);
			},
			hideCheckBox: function () {
				this.ui.checkbox.hide();
				this.model.set("disabled", true);
			},
			onRender: function () {
				this._handleDisabledChange();
			},
			getIsChecked: function () {
				return this.model.get("isChecked");
			},
			_handleClick: function (evt) {
				evt.preventDefault();
                
				if (!this.model.get("disabled")) {
					this.model.set("isChecked", !this.model.get("isChecked"));
					this.trigger("clicked:checkbox", this.model);
				}
			},
            
            _handleKeyDown: function(evt) {
                if(evt.keyCode && evt.keyCode === 13) {
                    if (!this.model.get("disabled")) {
                        this.model.set("isChecked", !this.model.get("isChecked"));
                        this.trigger("clicked:checkbox", this.model);
                    }
                }
            },
            
			_handleDisabledChange: function () {
				var disabled = this.model.get("disabled");

				this.$("input").toggleClass("disabled", disabled);
				this.ui.checkbox.toggleClass("disabled", disabled);
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Views.CheckboxView;
});
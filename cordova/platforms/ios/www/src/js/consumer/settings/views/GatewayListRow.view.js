"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/equipment/views/GatewayForm.view"
], function (App, consumerTemplates, SalusView, RegisteredRegions ) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.GatewayListRowView = Mn.LayoutView.extend({
			template: consumerTemplates["settings/manageDevices/gatewayListRow"],
			tagName: "li",
			className: "list-table-item row",
			regions: {
				"editRegion": ".bb-edit-region"
			},
			events: {
				"click .bb-collapsed-view": "edit"
			},
			bindings: {
				".bb-gateway-list-device-name": {
					observe: "name"
				},
				".bb-device-status": {
					observe: "connection_status",
					update: function ($el, connection) {
						if (connection === "Online") {
							$el.text(App.translate("equipment.status.online"));
						} else {
							$el.text(App.translate("equipment.status.offline"));
						}
					}
				}
			},
			initialize: function () {
				_.bindAll(this, "edit");

				this.editView = new App.Consumer.Equipment.Views.GatewayForm({
					model: this.model,
					mode: "edit"
				});

				this.registerRegion("editRegion", this.editView);

				this.listenTo(this.editView, "edit", this.edit);
				this.expanded = false;
			},

			edit: function () {
				var isCollapsed = this.editView.isCollapsed();

				this.editView.collapseOrExpand(!isCollapsed);
				this.$el.toggleClass("edit-enabled", isCollapsed);
			}
		}).mixin([SalusView, RegisteredRegions], {});
	});

	return  App.Consumer.Settings.Views.GatewayListRowView;
});

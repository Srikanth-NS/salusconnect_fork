"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		// item in a menu
		Views.DayOfWeekPickerView = Mn.ItemView.extend({
			className: "day-of-week-menu",
			template: consumerTemplates["oneTouch/dayOfWeekPicker"],
			events: {
				"click .bb-day-of-week-btn": "_handleDayClick"
			},

			initialize: function (options) {
				var preselected = options.menu.preselected;
				this.model = new B.Model({
					sun:  preselected.indexOf(1) >= 0 || false,
					mon:  preselected.indexOf(2) >= 0 || false,
					tues: preselected.indexOf(3) >= 0 || false,
					wed:  preselected.indexOf(4) >= 0 || false,
					thur: preselected.indexOf(5) >= 0 || false,
					fri:  preselected.indexOf(6) >= 0 || false,
					sat:  preselected.indexOf(7) >= 0 || false
				});
			},

			_handleDayClick: function (event) {
				var $target = $(event.currentTarget);

				var dayOfWeek = $target.data("day");
                				
				this.model.set(dayOfWeek, !this.model.get(dayOfWeek));
				$target.toggleClass("selected", this.model.get(dayOfWeek));
                   
                var val = _.every(this.model.attributes, function(value) {
                    if(!value) {
                        return true;
                    }
                });
                if(val) {
                    $(".bb-finished-button .btn").attr("disabled", true);
                } else {
                    $(".bb-finished-button .btn").attr("disabled", false);
                }
                
			},
            
			getRuleData: function () {
				var dateArray = [], ruleData = { "type": "dow" };
				if (this.model.get("sun")) {
					dateArray.push(1);
				}

				if (this.model.get("mon")) {
					dateArray.push(2);
				}

				if (this.model.get("tues")) {
					dateArray.push(3);
				}

				if (this.model.get("wed")) {
					dateArray.push(4);
				}

				if (this.model.get("thur")) {
					dateArray.push(5);
				}

				if (this.model.get("fri")) {
					dateArray.push(6);
				}

				if (this.model.get("sat")) {
					dateArray.push(7);
				}

				ruleData.dayPattern = dateArray;

				return ruleData;
			}


		}).mixin([SalusView]);

	});

	return App.Consumer.OneTouch.Views.DayOfWeekPickerView;
});
"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusDropdown.view",
	"consumer/models/SalusDropdownViewModel"
], function (App, constants, consumerTemplates, SalusView, FormTextInput, CheckboxView, CheckboxModel) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		// layout
		// has sections and ui
		Views.ContactInfoView = Mn.LayoutView.extend({
			className: "contact-info-view",
			template: consumerTemplates["oneTouch/contactInfoBase"],
			regions: {
				emailRegion: ".bb-email-region",
				smsRegion: ".bb-sms-region",
				inputRegion: ".bb-input-region"
			},
			ui: {
				newContactAnchor: ".bb-new-contact-anchor",
				newContactButton: ".bb-new-contact",
				mainContent: ".bb-main-content"
			},
			events: {
				"click @ui.newContactButton": "showNewContactForm"
			},
			bindings: {
				".bb-input-region input": "inputMessage"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "getRuleData", "showNewContactForm", "hideNewContactForm");

				this.model = new B.Model({
					"inputMessage": ""
				});

				this.type = this.options.menu.menuModel.get("contactType");

				this.newContactForm = new Views.NewContactFormView({
					type: this.type
				});

				// we could have one for push notifications too
				this.emailSectionView = new Views.ContactInfoCompositeView({
					type: "email"
				});

				this.smsSectionView = new Views.ContactInfoCompositeView({
					type: "sms"
				});

				this.inputSectionView = new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "alert-input",
					inputPlaceholder: App.translate("equipment.oneTouch.menus.doThis.notifyMe.messagePlaceHolder"),
					inputType: "input",
					overlayedUnits: "",
					hideErrorTriangle: true,
					hideArrows: true
				});

				// flag
				this.showingForm = false;

				this.listenTo(this.newContactForm, "click:cancel", this.hideNewContactForm);
			},
			onRender: function () {
				this.emailRegion.show(this.emailSectionView);
				this.smsRegion.show(this.smsSectionView);
				this.inputRegion.show(this.inputSectionView);
			},
			/**
			 * return the rule data for sms or for email
			 * @returns {*}
			 */
			getRuleData: function () {
				// hijacking submit if we are showing a form
				if (this.showingForm) {
					var info = this.newContactForm.getContactInfo();

					if (info) {
						if (info.sms) {
							this.smsSectionView.addToCollection(info);
						}

						if (info.email) {
							this.emailSectionView.addToCollection(info);
						}

						this.hideNewContactForm();
					}
				} else {
					var emailChoices = this.emailSectionView.getCheckedItems(),
						smsChoices = this.smsSectionView.getCheckedItems(),
						messageText = this.model.get("inputMessage"),
						ruleData = [];

					if (emailChoices.length > 0) {
						ruleData.push({
							type: "email",
							recipients: emailChoices, // array
							message: messageText
						});
					}

					if (smsChoices.length > 0) {
						smsChoices = this._cleanAndBuildSMS(smsChoices);

						ruleData.push({
							type: "sms",
							recipients: smsChoices, // array
							message: messageText
						});
					}

					return ruleData.length > 0 ? ruleData : false;
				}
			},
			showNewContactForm: function () {
				this.ui.newContactAnchor.append(this.newContactForm.render().$el);
				this.showingForm = true;

				this.ui.mainContent.addClass("hidden");
			},
			hideNewContactForm: function () {
				this.ui.newContactAnchor.children().detach();
				this.showingForm = false;

				this.ui.mainContent.removeClass("hidden");
			},
			_cleanAndBuildSMS: function (smsChoices) {
				// map the smsChoices to another array, with the country code and digits split as an object
				return _.map(smsChoices, function (phoneNum) {
					var split = phoneNum.split(" ");

					return {
						countryCode: parseInt(split[0]),
						phoneNumber: parseInt(split[1])
					};
				});
			},
			onDestroy: function () {
				this.newContactForm.destroy();
			}
		}).mixin([SalusView]);

		// new contact info form
		Views.NewContactFormView = Mn.ItemView.extend({
			className: "new-contact-menu",
			template: consumerTemplates["oneTouch/newContactForm"],
			ui: {
				phoneDropdown: ".bb-mobile-number-dropdown",
				phoneTextbox: ".bb-mobile-number-textbox",
				emailTextbox: ".bb-email-address",
				emailConfirmTextbox: ".bb-email-address-confirm"
			},
			events: {
				"click .bb-cancel-button": "_handleCancelClick"
			},
			initialize: function (/*options*/) {
				var that = this;

				_.bindAll(this, "_handleCancelClick", "getContactInfo");

				this.type = this.options.type;

				// build country code dropdown
				var countryCodesCollection = new App.Consumer.Models.DropdownItemViewCollection();
				this.currentCountryCode = null;

				_.each(constants.countryPhoneCodes, function (value) {
					countryCodesCollection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: value,
						displayText: value
					}));
				});

				this.phoneDropdownView = new App.Consumer.Views.SalusDropdownViewNoi18N({
					collection: countryCodesCollection,
					viewModel: new App.Consumer.Models.DropdownItemViewModel({
						id: "countryCodeSelect",
						dropdownLabelKey: App.translate("equipment.oneTouch.menus.doThis.notifyMe.contactMenu.countryCode")
					})
				});

				this.listenTo(this.phoneDropdownView, "value:change", function (dropdownItem) {
					that.currentCountryCode = dropdownItem.selectedValue;
				});

				this.phoneTextbox = new FormTextInput({
					labelText: this.type === "sms" ?
						"equipment.oneTouch.menus.doThis.notifyMe.contactMenu.mobilePhone" :
						"equipment.oneTouch.menus.doThis.notifyMe.contactMenu.mobilePhoneOptional"
				});

				this.emailTextbox = new FormTextInput({
					labelText: this.type === "email" ?
						"equipment.oneTouch.menus.doThis.notifyMe.contactMenu.email" :
						"equipment.oneTouch.menus.doThis.notifyMe.contactMenu.emailOptional"
				});

				this.emailConfirmTextbox = new FormTextInput({
					labelText: "equipment.oneTouch.menus.doThis.notifyMe.contactMenu.emailConfirm"
				});
			},
			onRender: function () {
				this.ui.phoneDropdown.append(this.phoneDropdownView.render().$el);
				this.ui.phoneTextbox.append(this.phoneTextbox.render().$el);
				this.ui.emailTextbox.append(this.emailTextbox.render().$el);
				this.ui.emailConfirmTextbox.append(this.emailConfirmTextbox.render().$el);
			},
			onDestroy: function () {
				this.phoneDropdownView.destroy();
				this.phoneTextbox.destroy();
				this.emailTextbox.destroy();
				this.emailConfirmTextbox.destroy();
			},
			_handleCancelClick: function (/*event*/) {
				this.trigger("click:cancel", this);
			},
			/**
			 * get the contact info, based on the menu choice. (sms/email)
			 * if they fill out the optional field (opposite of their choice)
			 * still treat it like they are trying to add that info
			 * @returns {*}
			 */
			getContactInfo: function () {
				var contactInfo = {},
					sms = this.phoneTextbox.getValue(),
					email1 = this.emailTextbox.getValue(),
					email2 = this.emailConfirmTextbox.getValue(),
					emailValid,
					smsValid,
					optionalValid = true;

				// validate email
				if (!email1) {
					if (this.type === "email") {
						this.emailConfirmTextbox.showErrors("common.validation.email.notValid");
						return false;
					} else {
						optionalValid = false;
					}
				}

				if (email1 !== email2) {
					if (this.type === "email") {
						this.emailConfirmTextbox.showErrors("common.validation.email.notMatching");
						return false;
					} else {
						optionalValid = false;
					}
				}

				emailValid = this.type === "email" || optionalValid ? App.validate.email(email1) : {valid: true};

				if (!emailValid.valid) {
					if (this.type === "email") {
						this.emailTextbox.showErrors(emailValid.message);
						return false;
					} else {
						optionalValid = false;
					}
				}

				// validate sms
				if (!sms) {
					if (this.type === "sms") {
						this.phoneTextbox.showErrors("common.validation.mobile.notValid");
						return false;
					} else {
						optionalValid = false;
					}
				}

				if (!this.currentCountryCode) {
					if (this.type === "sms") {
						this.phoneTextbox.showErrors("common.validation.mobile.selectCountry");
						return false;
					} else {
						optionalValid = false;
					}
				}

				smsValid = this.type === "sms" || optionalValid ? App.validate.mobileNumber(sms) : {valid: true};

				if (!smsValid.valid) {
					if (this.type === "sms") {
						this.phoneTextbox.showErrors(smsValid.message);
						return false;
					} else {
						optionalValid = false;
					}
				}

				contactInfo.sms = this.type === "sms" || optionalValid ? this.currentCountryCode + " " + this._cleanSMS(sms) : "";
				contactInfo.email = this.type === "email" || optionalValid ? email1 : "";

				return contactInfo;
			},
			_cleanSMS: function (sms) {
				// remove non-numeric
				return sms.replace(/\D/g, '');
			}
		}).mixin([SalusView]);

		Views.ContactItem = Mn.ItemView.extend({
			template: false,
			className: "contact-menu-item",
			initialize: function () {
				var that = this,
					displayText = this.options.type === "email" ? this.model.get("email") : this.model.get("sms");

				this.checkboxView = new CheckboxView({
					model: new CheckboxModel({
						name: "contactCheckbox",
						isChecked: false,
						secondaryIconClass: "",
						nonKeyLabel: displayText
					})
				});

				this.listenTo(this.checkboxView, "clicked:checkbox", function () {
					that.trigger("clicked:checkbox");
				});
			},
			onRender: function () {
				this.$el.append(this.checkboxView.render().$el);
			},
			onDestroy: function () {
				this.checkboxView.destroy();
			}
		}).mixin([SalusView]);

		// this is a section in the select contact menu
		// can make multiple instances of this thing, just need to set type
		Views.ContactInfoCompositeView = Mn.CompositeView.extend({
			template: consumerTemplates["oneTouch/contactInfoComposite"],
			className: "contact-composite",
			ui: {
				type: ".bb-contact-type",
				check: ".bb-green-check"
			},
			childView: Views.ContactItem,
			childViewContainer: ".bb-contact-options-container",
			childEvents: {
				"clicked:checkbox": "_handleCheckboxClick"
			},
			childViewOptions: function (model) {
				return {
					model: model,
					type: this.type
				};
			},
			initialize: function () {
				_.bindAll(this, "_buildRecipientsModels", "addToCollection", "getCheckedItems", "_handleCheckboxClick", "_anyChecked");

				this.type = this.options.type;

				this._buildRecipientsModels();
			},
			_buildRecipientsModels: function () {
				var allRecipients = App.salusConnector.getRuleCollection().getAllRecipients();

				var mappedRecipients = _.map(allRecipients[this.type], function (recipient) {
					// make it look like a model that we'd create using the form
					if (_.isObject(recipient)) {
						return {
							email: "",
							sms: "+" + recipient.countryCode + " " + recipient.phoneNumber
						};
					} else {
						return {
							email: recipient,
							sms: ""
						};
					}
				});

				this.collection = new B.Collection(mappedRecipients);
			},
			onRender: function () {
				this.ui.check.toggleClass("green-check", this._anyChecked());

				if (this.type === "sms") {
					this.ui.type.text(App.translate("equipment.oneTouch.menus.doThis.notifyMe.smsHeader"));
				} else if (this.type === "email") {
					this.ui.type.text(App.translate("equipment.oneTouch.menus.doThis.notifyMe.emailHeader"));
				}
			},
			addToCollection: function (contactObj) {
				this.collection.add(contactObj);
			},
			_anyChecked: function () {
				if (!this.collection || this.collection.isEmpty()) {
					return false;
				} else {
					return _.some(this.children._views, function (childView) {
						return childView.checkboxView.getIsChecked();
					});
				}
			},
			getCheckedItems: function () {
				var that = this,
					array = [];

				if (!this.collection.isEmpty()) {
					_.each(this.children._views, function (childView) {
						if (childView.checkboxView.getIsChecked()) {
							array.push(childView.model.get(that.type));
						}
					});
				}

				return array;
			},
			_handleCheckboxClick: function () {
				this.ui.check.toggleClass("green-check", this._anyChecked());
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.TimeOfDayPickerView;
});
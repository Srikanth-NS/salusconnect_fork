//
//  AMLSettingTestViewController.m
//  AML IOS
//
//  Created by Yipei Wang on 1/31/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import "AMLSettingTestViewController.h"
#import "AylaTest.h"
@interface AMLSettingTestViewController ()

@end

@implementation AMLSettingTestViewController

@synthesize textView = _textView;
@synthesize statusField = _statusField;
@synthesize button = _button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(runAllInALoop){
        [_button setHidden:YES];
        [self start:nil];
    }
}

- (IBAction)start:(id)sender
{    
    bool success = true;
    [_statusField setEnabled:false];
    [_statusField setText:@"working"];
    [_textView setText:@">>Start Setting Test\n"];
    
    // read config settings - assumes default values
    success = [self checkLoadSavedSettings: ^() {
        return [[AylaSystemUtils wifiTimeout] isEqualToNumber:[NSNumber numberWithInt:DEFAULT_WIFI_TIMEOUT]] == YES;
    } exprString:[NSString stringWithFormat: @"wifiTimeout==DEFAULT_WIFI_TIMEOUT(%d)", DEFAULT_WIFI_TIMEOUT]];
    
    // change wifi time-out value to a non-default value
    NSNumber *testWiFiTimeout = [NSNumber numberWithInt:13];  // set non-default value
    [AylaSystemUtils wifiTimeout:testWiFiTimeout];
    success = success && [self checkSaveCurrentSettings:[NSString stringWithFormat: @"wifiTimeout=%@", testWiFiTimeout]];
    
    // read config values and check for non-default wifi time-out value
    success = success && [self checkLoadSavedSettings: ^() {
        return [[AylaSystemUtils wifiTimeout] isEqualToNumber:testWiFiTimeout] == YES;
    } exprString:[NSString stringWithFormat: @"wifiTimeout==%@", testWiFiTimeout]];

    // reset wifi time-out to default value
    success = success && [self checkSaveDefaultSettings];
    
    // read configs and ensure wifi time-out value is set to the default
    success = success && [self checkLoadSavedSettings: ^() {
        return [[AylaSystemUtils wifiTimeout] isEqualToNumber:[NSNumber numberWithInt:DEFAULT_WIFI_TIMEOUT]] == YES;
    } exprString:[NSString stringWithFormat: @"wifiTimeout==%d", DEFAULT_WIFI_TIMEOUT]];
    
    gblTestSectionsCompleted++;
    
    if(success == true)
        [_statusField setText:@"success"];
    else
        [_statusField setText:@"failure"];
    
    [_statusField setEnabled:true];
    [_textView setText:[NSString stringWithFormat: @"%@%@\n", _textView.text, @">>end"]];
    
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlSettingServiceTest", @"TotalTestsRunned&Running", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed, nil];
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    //the end of settting test
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AMLUserTestView"];
        [self.navigationController pushViewController:viewController animated:true];
    }
}

- (bool)checkLoadSavedSettings:     // returns success bool
(int (^)(void))checkExpr
                    exprString: (NSString *)exprString
{
    bool success = true;
    int err = AylaSystemUtils.loadSavedSettings;
    if (err == SUCCESS) {
        if (checkExpr()) {
            PFLog(@"P", @"%@:%@", @"checkExpr", exprString);
        } else {
            success = false;
            PFLog(@"F", @"%@:%@", @"checkExpr", exprString);
        }
    } else {
        success = false;
        PFLog(@"F", @"%@:%d", @"err", err);
    }
    return success;
}

- (bool)checkSaveCurrentSettings:
(NSString *)exprString
{
    bool success = true;
    int err = [AylaSystemUtils saveCurrentSettings];
    if (err == SUCCESS) {
        PFLog(@"P", @"%@:%@", @"saveExpr", exprString);
    } else {
        success = false;
        PFLog(@"F", @"%@:%@", @"saveExpr", exprString);
    }
    return success;
}

- (bool)checkSaveDefaultSettings
{
    bool success = true;
    int err = [AylaSystemUtils saveDefaultSettings];
    NSString *saveExpr = [NSString stringWithFormat: @"wifiTimeout=%d", DEFAULT_WIFI_TIMEOUT];
    if (err == SUCCESS) {
        PFLog(@"P", @"%@:%@", @"saveExpr", saveExpr);
    } else {
        success = false;
        PFLog(@"F", @"%@:%@", @"saveExpr", saveExpr);
    }
    return success;
}


- (void)viewWillDisappear:(BOOL)animated
{
    if([self.navigationController.viewControllers indexOfObject:self] == NSNotFound){
        runAllInALoop = NO;
    }
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [self setTextView:nil];
    [self setStatusField:nil];
    [self setButton:nil];
    [super viewDidUnload];
}
@end

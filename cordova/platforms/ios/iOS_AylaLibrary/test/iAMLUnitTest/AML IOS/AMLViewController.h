//
//  AMLViewController.h
//  AML IOS
//
//  Created by Daniel Myers on 7/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMLViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *testView;
@end

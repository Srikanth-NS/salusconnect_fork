//
//  main.m
//  AML IOS
//
//  Created by Daniel Myers on 7/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMLAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AMLAppDelegate class]));
  }
}

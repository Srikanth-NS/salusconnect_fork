//
//  AMLUserTestViewController.m
//  AML IOS
//
//  Created by Yipei Wang on 1/31/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import "AMLUserTestViewController.h"
#import "AylaTest.h"
@interface AMLUserTestViewController ()

@end

@implementation AMLUserTestViewController
@synthesize textView = _textView;
@synthesize statusField = _statusField;
@synthesize button = _button;


static int signup_test_cases = 3;
static TestSequencer *testSequencer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(runAllInALoop){
        [_button setHidden:YES];
        [self start:nil];
    }
}


- (void)declareTests
{
    testSequencer =
    [TestSequencer testSequencerWithTestSuite:
     [NSArray arrayWithObjects:
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLoadSavedSettings:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserLogin:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestChangeUserPassword1:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestChangeUserPassword2:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUpdateUserInfo:successBlock failure:failureBlock]; } copy],
      
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserContactCreate:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserContactGetAll:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserContactGetWithId:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserContactUpdate:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserContactDelete:successBlock failure:failureBlock]; } copy],
      
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUserLogout:successBlock failure:failureBlock]; } copy],
// WARNING: This test will attempt to create an account using the AML_TEST_USER_NAME defined macro - no effect if it already exists
//      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self signUpTest:successBlock failure:failureBlock]; } copy],
      nil]];
}

- (IBAction)start:(id)sender
{
    [_statusField setEnabled:false];
    [_statusField setText:@"working"];
    
    [_textView setText:@">>start\n"];
    
    [self declareTests];
    
    [testSequencer executeNextTest:^{[self setSuccess];} failure:^{[self setFailure];}];     // amlTestSettings

    //currently last test in this section
    gblTestSectionsCompleted++;
}

- (void)amlTestLoadSavedSettings:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /* read setting info first */
    int err = AylaSystemUtils.loadSavedSettings;
    if (err == SUCCESS) {
        PFLog(@"P");

        [testSequencer executeNextTest:successBlock failure:failureBlock];
    } else {
        PFLog(@"F");
    }
    
    [_textView setText:[NSString stringWithFormat:@"%@%@\n", _textView.text, @">>Start User Service Test"]];
}

- (void)amlTestUserLogin:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    [AylaUser login:AML_TEST_USER_NAME password:AML_TEST_PASSWORD appId:AML_TEST_APP_ID appSecret:AML_TEST_APP_SECRET
            success:^(AylaResponse *resp, AylaUser *user){
                PFLog(@"P");

                [testSequencer executeNextTest:successBlock failure:failureBlock];
            }
            failure:^(AylaError *err){
                NSError *error = err.nativeErrorInfo;
                
                PFLog(@"F", @"%@:%d, %@:%@", @"httpCode", err.httpStatusCode, @"err", error);
            }
     ];
}

- (void)amlTestChangeUserPassword1:
/*success:*/(void (^)(void))successBlock
                           failure:(void (^)(void))failureBlock
{
    [AylaUser changePassword:AML_TEST_PASSWORD newPassword:@"123456"
         success:^(AylaResponse *resp){
             PFLog(@"P");
             
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         } failure:^(AylaError *err) {
             NSDictionary *details = err.errorInfo;
             
             PFLog(@"F", @"%@:%d, %@:%@", @"httpCode", err.httpStatusCode, @"err", details);
         }
     ];
}

- (void)amlTestChangeUserPassword2:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    [AylaUser changePassword:@"123456" newPassword:AML_TEST_PASSWORD
        success:^(AylaResponse *resp){
             PFLog(@"P");

             [testSequencer executeNextTest:successBlock failure:failureBlock];
         }
         failure:^(AylaError *err) {
             NSDictionary *details = err.errorInfo;

             PFLog(@"F", @"%@:%d, %@:%@", @"httpCode", err.httpStatusCode, @"err", details);
        }
     ];
}

- (void)amlTestUpdateUserInfo:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    NSDictionary *update = [[NSDictionary alloc] initWithObjectsAndKeys:@"testNewStreet",@"street", nil];
    
    [AylaUser updateUserInfo:update
        success:^(AylaResponse *response){
            PFLog(@"P");

            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } failure:^(AylaError *err) {
            NSDictionary *details = err.errorInfo;
            
            PFLog(@"F", @"%@:%d, %@:%@", @"httpCode", err.httpStatusCode, @"err", details);
        }
     ];
}

static NSString * const contactDisplayName = @"amlTestContact";
static NSString * const contactFirstName = @"first";
static NSString * const contactEmail = @"email@aylanetworks.com";
static NSString * const contactNewEmail = @"newemail@aylanetworks.com";
static AylaContact *amlTestUserContact = nil;

- (void)amlTestUserContactCreate:
/*success:*/(void (^)(void))successBlock
                         failure:(void (^)(void))failureBlock
{
    AylaContact *contact = [AylaContact new];
    contact.displayName = contactDisplayName;
    contact.firstName = contactFirstName;
    contact.email = contactEmail;
    
    [AylaContact createContact:contact withParams:nil success:^(AylaResponse *resp, AylaContact *createdContact) {
        
        PFLog(@"P", @"%@:%@", @"contact created",createdContact.displayName);
        [testSequencer executeNextTest:successBlock failure:failureBlock];
        
    } failure:^(AylaError *err) {
        NSError *error = err.nativeErrorInfo;
        PFLog(@"F", @"%@:%ld, %@:%ld", @"httpCode", (unsigned long)err.httpStatusCode, @"errCode", (unsigned long)error.code);
    }];
}

- (void)amlTestUserContactGetAll:
/*success:*/(void (^)(void))successBlock
                         failure:(void (^)(void))failureBlock
{
    amlTestUserContact = nil;
    [AylaContact getAllWithParams:nil success:^(AylaResponse *resp, NSArray *contacts) {
        
        for(AylaContact *contact in contacts) {
            if([contact.displayName isEqualToString:contactDisplayName]) {
                amlTestUserContact = contact;
            }
        }
        
        if(amlTestUserContact) {
            PFLog(@"P", @"%@:%ld", @"getAll contact", (unsigned long)contacts.count);
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        }
        else {
            PFLog(@"F", @"%@:%@", @"contact", @"not found");
        }
        
    } failure:^(AylaError *err) {
        NSError *error = err.nativeErrorInfo;
        PFLog(@"F", @"%@:%ld, %@:%ld", @"httpCode", (unsigned long)err.httpStatusCode, @"errCode", (unsigned long)error.code);
    }];
    
}

- (void)amlTestUserContactGetWithId:
/*success:*/(void (^)(void))successBlock
                            failure:(void (^)(void))failureBlock
{
    [AylaContact getWithId:amlTestUserContact.id success:^(AylaResponse *resp, AylaContact *contact) {
        
        if([contact.displayName isEqualToString:contactDisplayName]) {
            amlTestUserContact = contact;
            PFLog(@"P", @"%@:%@", @"contact getWithId", contact.displayName);
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        }
        else {
            PFLog(@"F", @"%@:%@", @"contact", @"not found");
        }
        
    } failure:^(AylaError *err) {
        NSError *error = err.nativeErrorInfo;
        PFLog(@"F", @"%@:%ld, %@:%ld", @"httpCode", (unsigned long)err.httpStatusCode, @"errCode", (unsigned long)error.code);
    }];
}

- (void)amlTestUserContactUpdate:
/*success:*/(void (^)(void))successBlock
                         failure:(void (^)(void))failureBlock
{
    [AylaContact updateContact:amlTestUserContact
                    withParams:@{ kAylaContactParamEmail: contactNewEmail }
                       success:^(AylaResponse *resp, AylaContact *updatedContact) {
                           if([updatedContact.email isEqualToString:contactNewEmail]) {
                               amlTestUserContact = updatedContact;
                               PFLog(@"P", @"%@:%@", @"contact update", updatedContact.email);
                               [testSequencer executeNextTest:successBlock failure:failureBlock];
                           }
                           else {
                               PFLog(@"F", @"%@:%@", @"contact", @"not found");
                           }
                           
                       } failure:^(AylaError *err) {
                           NSError *error = err.nativeErrorInfo;
                           PFLog(@"F", @"%@:%ld, %@:%ld", @"httpCode", (unsigned long)err.httpStatusCode, @"errCode", (unsigned long)error.code);
                       }];
}


- (void)amlTestUserContactDelete:
/*success:*/(void (^)(void))successBlock
                         failure:(void (^)(void))failureBlock
{
    [AylaContact deleteContact:amlTestUserContact
                       success:^(AylaResponse *resp) {
                           amlTestUserContact = nil;
                           PFLog(@"P", @"%@:%@", @"contact deleted", @"nil");
                           [testSequencer executeNextTest:successBlock failure:failureBlock];
                       } failure:^(AylaError *err) {
                           NSError *error = err.nativeErrorInfo;
                           PFLog(@"F", @"%@:%ld, %@:%ld", @"httpCode", (unsigned long)err.httpStatusCode, @"errCode", (unsigned long)error.code);
                       }];
}

- (void)amlTestUserLogout:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /*user logout*/
    [AylaUser logoutWithParams:@{kAylaUserLogoutClearCache: @(YES)}
         success:^(AylaResponse *response){
             PFLog(@"P");
             
             //This's the end of user Service Test
             [testSequencer executeNextTest:successBlock failure:failureBlock];
             
         } failure:^(AylaError *err) {
             NSError *error = err.nativeErrorInfo;
             PFLog(@"F", @"%@:%d, %@:%d", @"httpCode", err.httpStatusCode, @"errCode", error.code);
         }
    ];
}

- (void)signUpTest:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    int tries = 0;
    if(tries == signup_test_cases){ successBlock(); return;}
    
    NSMutableString *mail = [[NSMutableString alloc] init];
    [mail setString: AML_TEST_USER_NAME];  // this address should has already been taken
    NSString *pwd = @"123456";
    NSString *firstName = @"firstName"; // not used in errSignUp
    NSString *lastName = @"lastName";
    NSString *country = @"country";  //not  used in errSignUp
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:mail,@"email", pwd , @"password", firstName, @"firstname", lastName, @"lastname",country, @"country", nil];
    
    if(tries == 1){  // second one
         dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:mail,@"email", firstName, @"firstname", lastName, @"lastname", nil];
    }
    else if(tries == 2){
        dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:pwd , @"password", country, @"country",nil];
    }
    else{
        dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:mail,@"email", pwd , @"password", firstName, @"firstname", lastName, @"lastname", country, @"country", nil];
    }
    //only check incorrect user signup here
    [AylaUser signUp: dict appId:AML_TEST_APP_ID appSecret:AML_TEST_APP_SECRET
        success:^(AylaResponse *resp){
            PFLog(@"F", "%@:%@, %@:%@", @"ErrTest", @"should not pass.", @"mail", mail);
            failureBlock();
        } failure:^(AylaError *err) {
            
            if( err.errorCode == 1){
                NSError *nserr = err.nativeErrorInfo;
                
                PFLog(@"F", "%@:%d, %@:%d", @"httpCode", err.httpStatusCode, @"err", nserr.code);
                failureBlock();
                
            }
            else if( err.errorCode == AML_USER_INVALID_PARAMETERS ){
            
                NSDictionary *errors = err.errorInfo;
                NSEnumerator *en = [errors keyEnumerator];
                NSString *key = [en nextObject];
                
                PFLog(@"P", "%@:%d, %@:%@", @"totalErrs", [errors count],
                      key, [errors valueForKeyPath:key]);

                //This's the end of user Service Test
                [testSequencer executeNextTest:successBlock failure:failureBlock];
            }
            
        }
    ];
}

- (void)setSuccess
{
    [self complete];
    if([_statusField.text compare:@"working"]==0)
        [_statusField setText:@"success"];
    [_statusField setEnabled:true];
}

- (void)setFailure
{
    [self abortMsg];
    if([_statusField.text compare:@"working"]==0)
        [_statusField setText:@"failure"];
    [_statusField setEnabled:true];
}

- (void)complete
{
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlUserServiceTest", @"TotalTestsRunned&Running", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed, nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlUserServiceTest", @"TotalTestsRunnedOrRunning", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed);
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    
    if(runAllInALoop){
    
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AMLDeviceTestView"];
        [self.navigationController pushViewController:viewController animated:true];
    
    }
    
}

- (void)abortMsg
{
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlUserServiceTest", @"TotalTestsRunned&Running", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort", nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlUserServiceTest", @"TotalTestsRunnedOrRunning", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort");
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    
    if(runAllInALoop){
        
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        
            UIViewController *viewController = [self.storyboard     instantiateViewControllerWithIdentifier:@"AMLDeviceTestView"];
            [self.navigationController pushViewController:viewController animated:true];

    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    if([self.navigationController.viewControllers indexOfObject:self] == NSNotFound){
        runAllInALoop = NO;
    }
    [super viewWillDisappear:YES];
}

- (void)viewDidUnload
{
    [self setTextView:nil];
    [self setStatusField:nil];
    [self setButton:nil];
    [super viewDidUnload];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

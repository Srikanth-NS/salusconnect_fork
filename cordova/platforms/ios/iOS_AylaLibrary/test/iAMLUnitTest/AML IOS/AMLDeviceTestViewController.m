//
//  AMLDeviceTestViewController.m
//  AML IOS
//
//  Created by Yipei Wang on 1/31/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import "AMLDeviceTestViewController.h"
#import "AylaTest.h"
#import "AylaSystemUtilsSupport.h"


@interface AMLDeviceTestViewController ()

@end

@implementation AMLDeviceTestViewController

@synthesize textView = _textView;
@synthesize statusField = _statusField;
@synthesize button = _button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    if(runAllInALoop){
        [_button setHidden:YES];
        [self start:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

static int currentTotalTest = 0;
static int currentTotalTestPassed = 0;
static int currentTotalTestFailed = 0;
static TestSequencer *testSequencer;



//[self amlTestSettings];
//[self amlTestAylaUserLogin];
//[self almTestGetDevice];

// Properties
//[self amlTestGetProperties]; // 2 tests
//[self amlTestGetProperty];

// Datapoints
//[self amlTestCreateDatapoint]; // 2 tests
//[self amlTestGetDatapoints];

//Property Triggers
//[self amlTestCreateTrigger]; // 3 tests
//[self amlTestGetTriggers];
//[self amlTestDeleteTrigger];

// Application Triggers
//[self amlTestCreateSmsApplicationTrigger]; // 4 tests
// [self amlTestCreateEmailApplicationTrigger];
//[self amlTestGetApplicationTriggers];
//[self amlTestDeleteApplicationTrigger];

// [self amlTestUnregisterDevice]; // 2 tests
// [self amlTestRegisterNewDevice];

//------------- End of test cases here --------------

- (void)declareTests
{
    testSequencer =
    [TestSequencer testSequencerWithTestSuite:
     [NSArray arrayWithObjects:
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLoadSavedSettings:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestAylaUserLogin:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDevices:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDeviceDetail:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetProperties:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetPropertyDetail:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateTrigger:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateDatapoint:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDatapoints:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetTriggers:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateSMSApplicationTrigger:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateEmailApplicationTrigger:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetApplicationTriggers:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestDestroyApplicationTrigger:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestDestroyPropertyTrigger:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateDeviceDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUpdateDeviceDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDeviceDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestDeleteDeviceDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateUserDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUpdateUserDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetUserDatum:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetUserDatums:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestDeleteUserDatum:successBlock failure:failureBlock]; } copy],
#ifdef AML_TEST_DEVICE_FILE_PROPERTY
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateBlob:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetBlobs:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetBlobSaveToFile:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestBlobFetched:successBlock failure:failureBlock]; } copy],
#endif
#ifdef AML_TEST_DEVICE_NOTIFICATION
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateDeviceNotification:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDeviceNotifications:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUpdateDeviceNotification:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestCreateDeviceAppNotification:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDeviceAppNotifications:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUpdateDeviceAppNotification:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestDestroyDeviceAppNotification:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestDestroyDeviceNotification:successBlock failure:failureBlock]; } copy],
#endif
#ifdef AML_TEST_SHARE_EMAIL
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestShareDevice:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestReceivedDevice:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUpdateShare:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDeviceShares:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestReceivedDevice:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUnshare:successBlock failure:failureBlock]; } copy],
#endif
#ifdef AML_TEST_RESEND_CONFIRMATION_EMAIL
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestResendConfirmation:successBlock failure:failureBlock]; } copy],
#endif
#ifdef AML_TEST_RESET_PASSWORD_EMAIL
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestResetPassword:successBlock failure:failureBlock]; } copy],
#endif
#ifdef ENABLE_DEVICE_REGISTRATION_TEST
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestUnregisterDevice:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestRegisterNewDevice:successBlock failure:failureBlock]; } copy],
#endif
      nil]];
}

- (IBAction)start:(id)sender
{
    
    [_statusField setEnabled:false];
    [_statusField setText:@"working"];
    
    [_textView setText:@">>start\n"];
    currentTotalTest = gblNumberOfTests;
    currentTotalTestPassed = gblTestsPassed;
    currentTotalTestFailed = gblTestsFailed;

    [self declareTests];
    
    [testSequencer executeNextTest:^{[self setSuccess];} failure:^{[self setFailure];}];     // amlTestSettings
}

- (void)amlTestLoadSavedSettings:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * read setting info first
     */
    int err = AylaSystemUtils.loadSavedSettings;
    [AylaSystemUtils serviceType: [NSNumber numberWithInteger:AML_DEVELOPMENT_SERVICE]];
    [AylaSystemUtils loggingLevel:AylaSystemLoggingAll];
    
    //err = !OK;
    if (err == SUCCESS) {
        PFLog(@"P", @"BeforeUserSignin");
        
        [testSequencer executeNextTest:successBlock failure:failureBlock];    // amlTestAylaUserLogin
    } else {
        PFLog(@"F", @"BeforeUserSignin");
        
        failureBlock();
    }
}

- (void)amlTestAylaUserLogin:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    /**
     * User Login First
     */
    [AylaUser login:AML_TEST_USER_NAME password:AML_TEST_PASSWORD appId:AML_TEST_APP_ID appSecret:AML_TEST_APP_SECRET
            success:^(AylaResponse *resp, AylaUser *user)
     {
         [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text, @">>start Device Test\n"]];
         PFLog(@"P");
         
         // Devices
         NSMutableDictionary *callParams = [NSMutableDictionary new];
         [callParams setObject:[NSNumber numberWithInt:5] forKey:@"count"];
         [callParams setObject:@"descending" forKey:@"direction"];
         
         NSMutableDictionary *params = [NSMutableDictionary new];
         NSDateComponents *comps = [NSDateComponents new];
         [comps setDay:24]; [comps setMonth:5]; [comps setYear:2012];
         NSDate *beginDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
         [comps setDay:31]; [comps setMonth:5]; [comps setYear:2012];
         NSDate *endDate =  [[NSCalendar currentCalendar] dateFromComponents:comps];
         [params setObject:@"date" forKey:@"rangeType"];
         [params setObject:beginDate forKey:@"rangeBegin"];
         [params setObject:endDate forKey:@"rangeEnd"];
         
         [AylaReachability determineServiceReachabilityWithBlock:^(int reachable) {
             
             [testSequencer executeNextTest:successBlock failure:failureBlock];    // amlTestGetDevices
         }];
     }
            failure:^(AylaError *err)
     {
         NSString *description= nil;
         if(err.nativeErrorInfo!=nil){
             NSError *nsError = err.nativeErrorInfo;
             description = [AylaSystemUtils shortErrorFromError:nsError];
         }
         int httpStatusCode = err.httpStatusCode;
         //saveToLog(@"%@ %@ %@ %@", @"F", @"amlTest", @"Description:", description, @"RetrieveUserLogin");
         
         PFLog(@"F", @"%@:%d %@:%d %@:%@", @"HttpStatusCode", httpStatusCode, @"errCode", err.errorCode, @"Description:", description);
         failureBlock();
     }];
}

- (void)amlTestGetDevices:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * getDevices test
     */
    [AylaDevice getDevices: nil
                   success:^(AylaResponse *resp, NSArray *devices) {
                       int count;
                       NSString *dsn;
                       NSString *name;
                       for(count=0; count < devices.count; count++){
                           AylaDevice *device = [devices objectAtIndex:count];
                           name = device.productName;
                           dsn = device.dsn;
                           
                           if ([dsn isEqualToString:gblAmlProductName]) {
                               PFLog(@"P", @"%@:%@ %@:%@", @"dsn", dsn, @"productName", name);
                              gblAmlTestDevice = device;
                           }
                       }
                       
                       if(gblAmlTestDevice == nil){
                           PFLog(@"F", @"%@:%@%@", @"Err", @"can't find device with name ", gblAmlProductName);
                           
                           failureBlock();
                           return;
                       }
                       
                       PFLog(@"I", @"%@:%d", @"count", count);
    
                       [testSequencer executeNextTest:successBlock failure:failureBlock];       // amlTestGetDeviceDetail

                   }
                   failure:^(AylaError *err) {
                       NSError *nsError = err.nativeErrorInfo;
                       NSString *description = @"None";
                       if (nsError != nil) {
                           description = [AylaSystemUtils shortErrorFromError:nsError];
                       }
                       
                       PFLog(@"F", @"%@:%d, %@:%@", @"errCode", err.httpStatusCode, @"Description:", description);
                       
                       failureBlock();
                   }
     ];
}

- (void)amlTestGetDeviceDetail:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to get device details
     */
    [gblAmlTestDevice getDeviceDetail:nil
                              success:^(AylaResponse *resp, AylaDevice *device) {
                                  NSMutableString *productName = [NSMutableString stringWithString:device.productName];
                                  
                                  PFLog(@"P", @"%@:%@", @"name", productName);
                                  [testSequencer executeNextTest:successBlock failure:failureBlock];        // amlTestGetProperties
                              }
                              failure:^(AylaError *err) {
                                  NSError *nsError = err.nativeErrorInfo;
                                  NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                                  
                                  PFLog(@"F", @"%@:%d, %@:%@", @"errCode", err.httpStatusCode, @"Description:", description);
                                  
                                  // Execute next test, even on failure
                                  [testSequencer executeNextTest:successBlock failure:failureBlock];        // amlTestGetProperties
                              }
     ];
}

- (void)amlTestGetProperties:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to get device properties
     */
    [gblAmlTestDevice getProperties:nil
                    success:^(AylaResponse *resp, NSArray *properties) {
                        NSMutableString *name;
                        int count;
                        NSString *baseType = gblAmlTestProperty.baseType;
                        NSString *valueStr;
                        for(count=0; count<properties.count; count++){
                            AylaProperty *property = [properties objectAtIndex:count];
                            name = [NSMutableString stringWithString:property.name];
                            if ([baseType isEqualToString:@"string"]) {
                                valueStr = [NSString stringWithFormat:@"%@", property.datapoint.sValue];
                            } else  {
                                valueStr = [NSString stringWithFormat:@"%@", property.datapoint.nValue];
                            }
                            
                            PFLogOnly(@"I", @"%@:%@, %@:%@", @"name", property.name, @"value", valueStr);
                            if ([name isEqualToString:gblAmlPropertyName]) {
                                PFLog(@"P", @"%@:%@, %@:%@", @"name", property.name, @"value", valueStr);
                                gblAmlTestProperty = property;
                            }
                            else if([name isEqualToString:gblFilePropertyName]) {
                                gblAmlTestFileProperty = property;
                            }
                        }
                        PFLog(@"I", @"%@:%d", @"count", count);
                        
                        if(gblAmlTestProperty == nil){
                            
                            PFLog(@"F", @"%@:%@%@, %@", @"Property", @"can't find property ", gblAmlPropertyName, @"Property not assigned");
                            
                            failureBlock();
                            return;
                        }

                        // have the property, launch tests
                        [testSequencer executeNextTest:successBlock failure:failureBlock];      // amlTestGetProperty
                    }
                    failure:^(AylaError *err) {
                        NSError *nsError = err.nativeErrorInfo;
                        NSString *description = [AylaSystemUtils shortErrorFromError:nsError];

                        PFLog(@"F", @"%@:%@", @"Description:", description);
                        failureBlock();
                    }
     ];
}

- (void)amlTestGetPropertyDetail:
            /*success:*/(void (^)(void))successBlock
            failure:(void (^)(void))failureBlock
{
        [gblAmlTestProperty getPropertyDetail: nil
          success:^(AylaResponse *resp, AylaProperty *property) {
              NSString *baseType = gblAmlTestProperty.baseType;
              NSString *valueStr;
              if ([baseType isEqualToString:@"string"]) {
                  valueStr = [NSString stringWithFormat:@"%@", property.datapoint.sValue];
              } else  {
                  valueStr = [NSString stringWithFormat:@"%@", property.datapoint.nValue];
              }
              PFLog(@"P", @"%@:%@, %@:%@", @"name", property.name, @"value", valueStr);

              [testSequencer executeNextTest:successBlock failure:failureBlock];
          }
          failure:^(AylaError *err) {
              NSError *nsError = err.nativeErrorInfo;
              NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
              
              PFLog(@"F", @"%@:%@", @"Description:", description);
              // Execute next test, even on failure
              [testSequencer executeNextTest:successBlock failure:failureBlock];
          }];
}

- (void)amlTestCreateDatapoint:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    NSNumber *nValue= nil;
    //srand(time(NULL)); int rndBool = rand() % 2 == 1; nValue = [NSNumber numberWithInt:rndBool];
    //srand(time(NULL)); int rndInt = rand(); nValue = [NSNumber numberWithInt:rndInt];
    //CGFloat aFloat = 1.0123456789; nValue = [NSNumber numberWithFloat:aFloat];
    //CGFloat aDecimal = 12345678.90; nValue = [NSNumber numberWithFloat:aDecimal];

    // Create long test string
    srand(time(NULL));
    gblTestString = @"Hello - ";
    while (gblTestString.length < TEST_STRING_LEN) {
        char ch = (char) ((rand() % 95) + 32);
        NSString *chStr = [NSString stringWithFormat:@"%c" , ch];
        gblTestString = [gblTestString stringByAppendingString:chStr];
    }
    gblTestString = [gblTestString stringByReplacingOccurrencesOfString:@"%" withString: @"%%"];     // Escape % so it is not interpreted as a format character when printed
    // gblTestString = [gblTestString stringByReplacingOccurrencesOfString:@"\\/" withString: @""];
    
    /**
     * Test to create datapoint
     */
    AylaDatapoint *tmpDatapoint = [AylaDatapoint new];
    tmpDatapoint.sValue = gblTestString;
    tmpDatapoint.nValue = nValue;
    [gblAmlTestProperty createDatapoint:tmpDatapoint
                success:^(AylaResponse *resp, AylaDatapoint *datapoint)
                 {
                     PFLog(@"P", @"%@:%@, %@:%@", @"value", gblAmlTestProperty.datapoint.sValue, @"createdAt", datapoint.createdAt);
                     [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestGetDatapoints
                 }
                failure:^(AylaError *err)
                 {
                     NSError * nsError = err.nativeErrorInfo;
                     NSString *description = @"None";
                     if (nsError != nil) {
                         description = [AylaSystemUtils shortErrorFromError:nsError];
                     }
                     
                     PFLog(@"F", @"%@:%@, %@:%d, %@:%@", @"Description:", description, @"httpStatusCode", err.httpStatusCode, @"value", tmpDatapoint.sValue);
                     failureBlock();
                 }
     ];
}

- (void)amlTestGetDatapoints:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    NSMutableDictionary *callParams = [NSMutableDictionary new];
    [callParams setObject:[NSNumber numberWithInt:5] forKey:@"count"];
    
    /**
     * Test to get Datapoints by activity
     */
    [gblAmlTestProperty getDatapointsByActivity:callParams
        success:^(AylaResponse *resp, NSArray *datapoints)
         {
             int i;
             NSString *baseType = gblAmlTestProperty.baseType;
             NSString *valueStr;
             for(i=0; i<datapoints.count; i++){
                 AylaDatapoint *datapoint = [datapoints objectAtIndex:i];
                 if ([baseType isEqualToString:@"string"]) {
                     valueStr = [NSString stringWithFormat:@"%@", datapoint.sValue];
                 } else  {
                     valueStr = [NSString stringWithFormat:@"%@", datapoint.nValue];
                 }
                 PFLogOnly(@"I", @"%@:%@, %@:%@", @"createdAt", datapoint.createdAt, @"value", valueStr);
             }
             PFLogOnly(@"I", @"%@:%d", @"i", i);
             for(i=0; i<gblAmlTestProperty.datapoints.count; i++){
                 AylaDatapoint *datapoint = [gblAmlTestProperty.datapoints objectAtIndex:i];
                 if ([baseType isEqualToString:@"string"]) {
                     valueStr = [NSString stringWithFormat:@"%@", datapoint.sValue];
                 } else  {
                     valueStr = [NSString stringWithFormat:@"%@", datapoint.nValue];
                 }
                 PFLogOnly(@"I", @"%@:%@, %@:%@", @"createdAt", datapoint.createdAt, @"value", valueStr);
             }
             PFLogOnly(@"I", @"%@:%d", @"i", i);
             
             // Check to see if the last string created matches the one we started with
             if ((datapoints.count > 0) && ([gblAmlTestProperty.baseType isEqualToString:@"string"])) {
                 AylaDatapoint *datapoint = [datapoints objectAtIndex:datapoints.count - 1];
                 if ([datapoint.sValue isEqualToString:gblTestString]) {
                     PFLog(@"P");
                 } else {
                     PFLog(@"F", @"%@!=%@", datapoint.sValue, gblTestString);
                 }
             } else {
                 PFLog(@"P");
             }
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         }
       failure:^(AylaError *err)
         {
             NSError * nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             
             PFLog(@"F", @"%@:%@", @"Description:", description);
             // Execute next test, even on failure
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         }];
}

// ------------------------------ Property Triggers ---------------------
- (void)amlTestCreateTrigger:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    AylaPropertyTrigger *propertyTrigger = [AylaPropertyTrigger new];
    
    propertyTrigger.triggerType = @"compare_absolute";
    propertyTrigger.compareType = @">=";
    propertyTrigger.value = @"1";
    //propertyTrigger.deviceNickname = @"abc";
    //propertyTrigger.propertyNickname = @"Blue_button2";
    // {"trigger":{"trigger_type":"compare_absolute", "compare_type":">=", "value":60 }}
   
    /**
     * Test to create trigger 
     */
    [gblAmlTestProperty createTrigger:propertyTrigger
                              success:^(AylaResponse *resp, AylaPropertyTrigger *propertyTriggerCreated)
     {
         //NSString *triggerType = propertyTriggerCreated.triggerType;
         NSString *compareType = propertyTriggerCreated.compareType;
         NSString *value = propertyTriggerCreated.value;
         //NSNumber *key = propertyTriggerCreated.key;
         //NSString *deviceNickname = propertyTriggerCreated.deviceNickname;
         //NSString *propertyNickname = gblAmlTest_property.propertyTrigger.propertyNickname;
         //NSString *propertyNickname = gblAmlTest_property.propertyTrigger.propertyNickname;
         //NSDate *retrievedAt = propertyTriggerCreated.retrievedAt;
         
         PFLog(@"P", @"%@:%@, %@:%@", @"compareType", compareType, @"value", value);
         [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestGetTriggers
     }
      failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         
         NSDictionary *errors = nil;
         if(err.errorInfo!=nil){
             errors = err.errorInfo;
         }
         
         if (errors != nil) {
             PFLog(@"F", @"%@:%@", @"Description:", errors);
         } else
             PFLog(@"F", @"%@:%@", @"Description:", description);
         failureBlock();
     }];
}

- (void)amlTestGetTriggers:
    /*success*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to get triggers
     */
    [gblAmlTestProperty getTriggers:nil
          success:^(AylaResponse *resp, NSArray *propertyTriggers)
         {
             int count;
             for(count=0; count < gblAmlTestProperty.propertyTriggers.count; count++){
                 AylaPropertyTrigger *propertyTrigger = [propertyTriggers objectAtIndex:count];
                 PFLogOnly(@"I", @"%@:%@, %@:%@", @"compareType", propertyTrigger.compareType, @"value", propertyTrigger.value);
                 gblAmlTestPropertyTrigger = propertyTrigger;
             }
             PFLog(@"P", @"%@:%d", @"count", count);
             
             [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestCreateSMSApplicationTrigger
        }
          failure:^(AylaError *err)
         {
             NSError * nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description:", description);
             
             failureBlock();
             
         }
     ];
}

- (void)amlTestDestroyPropertyTrigger:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    [gblAmlTestProperty destroyTrigger:gblAmlTestPropertyTrigger
        success:^(AylaResponse *resp)
         {
             PFLog(@"P");
             
             // Test Registration
             [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestUnregisterDevice
         }
        failure:^(AylaError *err)
         {
             NSError *nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description:", description);
             
             failureBlock();
         }];
}

//----------------------------- Application Triggers ----------------------
- (void)amlTestCreateSMSApplicationTrigger:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to create SMS application trigger
     */
    AylaApplicationTrigger *applicationTrigger = [AylaApplicationTrigger new];
    applicationTrigger.countryCode = gblAmlCountryCode;
    applicationTrigger.phoneNumber = gblAmlPhoneNumber;
    applicationTrigger.message = @"Testing Ayla Mobile Library";
    
    [gblAmlTestPropertyTrigger createSmsApplicationTrigger:applicationTrigger
       success:^(AylaResponse *resp, AylaApplicationTrigger *applicationTriggerCreated)
         {
             //NSString *countryCode = applicationTriggerCreated.countryCode;
             NSString *phoneNumber = applicationTriggerCreated.phoneNumber;
             NSString *message = applicationTriggerCreated.message;
             //NSDate *retrievedAt = applicationTriggerCreated.retrievedAt;
             
             PFLog(@"P", @"%@:%@, %@:%@", @"phoneNumber", phoneNumber, @"message", message);
             
             //gblAmlTest_applicationTrigger = applicationTriggerCreated;
             
             [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestCreateEmailApplicationTrigger
         }
      failure:^(AylaError *err)
         {
             NSError *nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             
             if(err.errorInfo!=nil){
                 NSDictionary *dict = err.errorInfo;
                 PFLog(@"F", @"%@:%@, %d", @"Description:", dict , err.httpStatusCode);
             }
             else{
                 PFLog(@"F", @"%@:%@, %d", @"Description:", description, err.httpStatusCode);
             }
             failureBlock();
         }];
}

- (void)amlTestCreateEmailApplicationTrigger:
        /*success:*/(void (^)(void))successBlock
        failure:(void (^)(void))failureBlock
{
    /**
     * Test to create email application trigger
     */
    AylaApplicationTrigger *applicationTrigger = [AylaApplicationTrigger new];
    
    applicationTrigger.userName = @"MDA";
    applicationTrigger.emailAddress = gblAmlEmailAddress;
    applicationTrigger.message = @"Testing Ayla Mobile Library";
    
    [gblAmlTestPropertyTrigger createEmailApplicationTrigger:applicationTrigger
       success:^(AylaResponse *resp, AylaApplicationTrigger *applicationTriggerCreated)
         {
             NSString *userName = applicationTriggerCreated.userName;
             NSString *emailAddress = applicationTriggerCreated.emailAddress;
             //NSString *message = applicationTriggerCreated.message;
             
             PFLog(@"P", @"%@:%@, %@:%@", @"userName", userName, @"emailAddress", emailAddress);
             
             gblAmlTestApplicationTrigger = applicationTriggerCreated;
             [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestGetApplicationTriggers
         }
       failure:^(AylaError *err)
         {
             NSError *nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             //NSDictionary *dict = (__bridge NSDictionary *)err->errorInfo;
             PFLog(@"F", @"%@:%@", @"Description:", description);
             
             failureBlock();
         }];
}

- (void)amlTestGetApplicationTriggers:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to get application trigger
     */
    [gblAmlTestPropertyTrigger getApplicationTriggers:nil
      success:^(AylaResponse *resp, NSArray *applicationTriggers)
         {
             int count;
             for(count=0; count < gblAmlTestPropertyTrigger.applicationTriggers.count; count++){
                 AylaApplicationTrigger *applicationTrigger = [applicationTriggers objectAtIndex:count];
                 PFLogOnly(@"I", @"%@:%@, %@:%@", @"appName", applicationTrigger.appName, @"userName", applicationTrigger.userName);
                 gblAmlTestApplicationTrigger = applicationTrigger;
             }
             PFLog(@"P", @"%@:%d", @"count", count);
             
             [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestDeleteApplicationTrigger
        }
      failure:^(AylaError *err)
         {
             NSError *nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description:", description);
             
             failureBlock();
         }];
}

- (void) amlTestDestroyApplicationTrigger:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to destroy application trigger
     */
    [gblAmlTestPropertyTrigger destroyApplicationTrigger:gblAmlTestApplicationTrigger
      success:^(AylaResponse *resp)
         {
             PFLog(@"P");
             [testSequencer executeNextTest:successBlock failure:failureBlock];     // amlTestDeleteTrigger
         }
     failure:^(AylaError *err)
         {
             NSError *nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description:", description);
             
             failureBlock();
         }];
}

//----------------------------- Device Datum ----------------------

- (void) amlTestCreateDeviceDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to create device datum
     */
    gblAmlTestDatum = [[AylaDatum alloc] initWithKey:@"aDeviceKey" andValue:@"aValue1"];
    
    [gblAmlTestDevice createDatum:gblAmlTestDatum
    success:^(AylaResponse *resp, AylaDatum *newDatum)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if ([newDatum.key isEqualToString: gblAmlTestDatum.key] && [newDatum.value isEqualToString: gblAmlTestDatum.value]) {
                 PFLog(@"P");
                 [testSequencer executeNextTest:successBlock failure:failureBlock];
             } else {
                 PFLog(@"F", @"(%@,%@)!=(%@,%@)", newDatum.key, newDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         if (err.httpStatusCode == 422) {     // allow recovery from previous failure
             PFLog(@"P", @"Datum already exists - continue");
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         } else {
             NSError *nsError = err.nativeErrorInfo;
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description", description);
             
             failureBlock();
         }
     }];
}


- (void) amlTestUpdateDeviceDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to update device datum
     */
    gblAmlTestDatum.value = @"aValue2";
    
    [gblAmlTestDevice updateDatum:gblAmlTestDatum
    success:^(AylaResponse *resp, AylaDatum *updatedDatum)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if ([updatedDatum.key isEqualToString: gblAmlTestDatum.key] && [updatedDatum.value isEqualToString: gblAmlTestDatum.value]) {
                 PFLog(@"P");
                 [testSequencer executeNextTest:successBlock failure:failureBlock];
             } else {
                 PFLog(@"F", @"(%@,%@)!=(%@,%@)", updatedDatum.key, updatedDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);
         
         failureBlock();
     }];
}

- (void) amlTestGetDeviceDatum:
/*success:*/(void (^)(void))successBlock
                     failure:(void (^)(void))failureBlock
{
    /**
     * Test to get device datum
     */
    [gblAmlTestDevice getDatumWithKey:gblAmlTestDatum.key
    success:^(AylaResponse *resp, AylaDatum *newDatum)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if ([newDatum.key isEqualToString: gblAmlTestDatum.key] && [newDatum.value isEqualToString: gblAmlTestDatum.value]) {
                 PFLog(@"P");
                 [testSequencer executeNextTest:successBlock failure:failureBlock];
             } else {
                 PFLog(@"F", @"(%@,%@)!=(%@,%@)", newDatum.key, newDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);
         
         failureBlock();
     }];
}


- (void) amlTestDeleteDeviceDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to delete device datum
     */
    [gblAmlTestDevice deleteDatum:gblAmlTestDatum
    success:^(AylaResponse *resp)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             PFLog(@"P");
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);
         
         failureBlock();
     }];
}

//----------------------------- User Datum ----------------------

- (void) amlTestCreateUserDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to create user datum
     */
    gblAmlTestDatum = [[AylaDatum alloc] initWithKey:@"aUserKey" andValue:@"aValue1"];
    
    [[AylaUser currentUser] createDatum:gblAmlTestDatum
     success:^(AylaResponse *resp, AylaDatum *newDatum)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if ([newDatum.key isEqualToString: gblAmlTestDatum.key] && [newDatum.value isEqualToString: gblAmlTestDatum.value]) {
                 PFLog(@"P");
                 [testSequencer executeNextTest:successBlock failure:failureBlock];
             } else {
                 PFLog(@"F", @"(%@,%@)!=(%@,%@)", newDatum.key, newDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
     failure:^(AylaError *err)
        {
            if (err.httpStatusCode == 422) {     // allow recovery from previous failure
                PFLog(@"P", @"Datum already exists - continue");
                [testSequencer executeNextTest:successBlock failure:failureBlock];
            } else {
                NSError *nsError = err.nativeErrorInfo;
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description", description);
                
                failureBlock();
            }
        }];
}


- (void) amlTestUpdateUserDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to update user datum
     */
    gblAmlTestDatum.value = @"aValue2";
    
    [[AylaUser currentUser] updateDatum:gblAmlTestDatum
    success:^(AylaResponse *resp, AylaDatum *updatedDatum)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if ([updatedDatum.key isEqualToString: gblAmlTestDatum.key] && [updatedDatum.value isEqualToString: gblAmlTestDatum.value]) {
                 PFLog(@"P");
                 [testSequencer executeNextTest:successBlock failure:failureBlock];
             } else {
                 PFLog(@"F", @"(%@,%@)!=(%@,%@)", updatedDatum.key, updatedDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);
         
         failureBlock();
     }];
}

- (void) amlTestGetUserDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to get user datum
     */
    [[AylaUser currentUser] getDatumWithKey:gblAmlTestDatum.key
    success:^(AylaResponse *resp, AylaDatum *newDatum)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if ([newDatum.key isEqualToString: gblAmlTestDatum.key] && [newDatum.value isEqualToString: gblAmlTestDatum.value]) {
                 PFLog(@"P");
                 [testSequencer executeNextTest:successBlock failure:failureBlock];
             } else {
                 PFLog(@"F", @"(%@,%@)!=(%@,%@)", newDatum.key, newDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);

         failureBlock();
     }];
}


- (void) amlTestGetUserDatums:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to get muliple user datums
     */
    NSDictionary *params = @{@"filters": @[@"%%UserKey"]};

    [[AylaUser currentUser] getDatumWithParams:params
    success:^(AylaResponse *resp, NSArray *newDatums)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             if (newDatums.count == 1) {
                 AylaDatum *newDatum = newDatums[0];
                 if ([newDatum.key isEqualToString: gblAmlTestDatum.key] && [newDatum.value isEqualToString: gblAmlTestDatum.value]) {
                     PFLog(@"P");
                     [testSequencer executeNextTest:successBlock failure:failureBlock];
                 } else {
                     PFLog(@"F", @"(%@,%@)!=(%@,%@)", newDatum.key, newDatum.value, gblAmlTestDatum.key, gblAmlTestDatum.value);
                     failureBlock();
                 }
             } else {
                 PFLog(@"F", @"%@(==%d)!=%d", @"newDatums.count", newDatums.count, 1);
                 failureBlock();
             }
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);
         
         failureBlock();
     }];
}

- (void) amlTestDeleteUserDatum:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test to delete user datum
     */
    [[AylaUser currentUser] deleteDatum:gblAmlTestDatum
    success:^(AylaResponse *resp)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             PFLog(@"P");
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         PFLog(@"F", @"%@:%@", @"Description", description);
         
         failureBlock();
     }];
}

//------------------------- File Property ---------------------

static AylaDatapointBlob *amlTestDatapointBlob = nil;
- (void) amlTestCreateBlob:
/*success:*/(void (^)(void))successBlock
        failure:(void (^)(void))failureBlock
{
    //read default image
    UIImage *img = [UIImage imageNamed:@"Default-568h@2x.png"];
    NSData *data = UIImageJPEGRepresentation(img, 0.6);

    [gblAmlTestFileProperty createBlob:@{kAylaBlobFileData: data}
    success:^(AylaResponse *response, AylaDatapointBlob *datapointCreated) {
        PFLog(@"P, amlTestCreateBlob");
        [testSequencer executeNextTest:successBlock failure:failureBlock];
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
        PFLog(@"F", @"%@:%@", @"Description", description);

        failureBlock();
    }];
}

- (void) amlTestGetBlobs:
/*success:*/(void (^)(void))successBlock
                   failure:(void (^)(void))failureBlock
{

    [gblAmlTestFileProperty getBlobsByActivity:nil success:^(AylaResponse *response, NSArray *retrievedDatapoints) {
        if(retrievedDatapoints.count > 0) {
            PFLog(@"P, amlTestGetBlobs");
            amlTestDatapointBlob = retrievedDatapoints[0];
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        }
        else {
            PFLog(@"F", @"httpStatusCode(%ld) is not 2XX", response.httpStatusCode);
            failureBlock();
        }
        
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
        PFLog(@"F", @"%@:%@", @"Description", description);
        
        failureBlock();
    }];
}


- (void) amlTestGetBlobSaveToFile:
/*success:*/(void (^)(void))successBlock
                 failure:(void (^)(void))failureBlock
{
    [gblAmlTestFileProperty getBlobSaveToFlie:amlTestDatapointBlob params:nil success:^(AylaResponse *response, NSString *retrieveBlobFileName) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //get image from binary file
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSData *imageData = [[NSData alloc] initWithContentsOfFile:[documentsDirectory stringByAppendingFormat:@"/%@", retrieveBlobFileName]]; // use your retrieved file name in retrieved blobs, by default it's <property name>_BlobStream
        
        UIImage *nImage = [UIImage imageWithData:imageData];

        if(nImage) {
            PFLog(@"P, amlTestGetBlobSaveToFile");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        }
        else {
            PFLog(@"F", @"httpStatusCode(%ld) is not 2XX", response.httpStatusCode);
            failureBlock();
        }
        
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
        PFLog(@"F", @"%@:%@", @"Description", description);
        
        failureBlock();
    }];
}

- (void) amlTestBlobFetched:
/*success:*/(void (^)(void))successBlock
                          failure:(void (^)(void))failureBlock
{
    
    [amlTestDatapointBlob markFetched:nil success:^(AylaResponse *response) {
        PFLog(@"P, amlTestBlobFetched");
        [testSequencer executeNextTest:successBlock failure:failureBlock];
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
        PFLog(@"F", @"%@:%@", @"Description", description);
        failureBlock();
    }];
}

//----------------------------- Shares ----------------------

#ifdef AML_TEST_SHARE_EMAIL

- (void) amlTestShareDevice:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test of shareDevice
     */
    AylaShare *share = [AylaShare new];
    share.userEmail = AML_TEST_SHARE_EMAIL;
    share.resourceId = gblAmlTestDevice.dsn;
    share.resourceName = @"device";
    share.operation = AylaShareOperationReadAndWrite;
    [[AylaUser currentUser] createShare:share success:^(AylaResponse *resp, AylaShare *share) {
        if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
            gblAmlTestShare = share;
            PFLogOnly(@"I", @"%@:%@", @"share.resourceId", gblAmlTestShare.resourceId);
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
            failureBlock();
        }
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        
        if (nsError != nil) {
            NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
            PFLog(@"F", @"%@:%@", @"Description", description);
        } else {
            PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
        }
        
        failureBlock();
    }];
}

- (void) amlTestUpdateShare:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test of updateShare
     */
    gblAmlTestShare.operation = AylaShareOperationReadOnly;
    [[AylaUser currentUser] updateShare:gblAmlTestShare success:^(AylaResponse *resp, AylaShare *updatedShare) {
        gblAmlTestShare = updatedShare;
        if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
            PFLogOnly(@"I", @"%@:%@", @"share.resourceId", gblAmlTestShare.resourceId);
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
            failureBlock();
        }
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        
        if (nsError != nil) {
            NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
            PFLog(@"F", @"%@:%@", @"Description", description);
        } else {
            PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
        }
        
        failureBlock();
    }];
}

- (void) amlTestGetDeviceShares:
/*success:*/(void (^)(void))successBlock
                    failure:(void (^)(void))failureBlock
{
    /**
     * Test of getDeviceShares
     */
    [gblAmlTestDevice getSharesWithSuccess:^(AylaResponse *resp, NSArray *deviceShares) {
        if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
            for(int i=0; i < deviceShares.count; i++){
                AylaShare *share = [deviceShares objectAtIndex:i];
                PFLogOnly(@"I", @"%@:%@", @"share.resourceId", share.resourceId);
            }
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
            failureBlock();
        }

    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        
        if (nsError != nil) {
            NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
            PFLog(@"F", @"%@:%@", @"Description", description);
        } else {
            PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
        }
        
        failureBlock();
    }];
}

- (void) amlTestReceivedDevice:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test of getReceivedShares
     */
    NSDictionary *params = nil;
    [[AylaUser currentUser] getReceivedShares:params
    success:^(AylaResponse *resp, NSArray *deviceShares) {
        if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
            for(int i=0; i < deviceShares.count; i++){
                AylaShare *share = [deviceShares objectAtIndex:i];
                PFLogOnly(@"I", @"%@:%@", @"share.resourceId", share.resourceId);
            }
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
            failureBlock();
        }
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        
        if (nsError != nil) {
            NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
            PFLog(@"F", @"%@:%@", @"Description", description);
        } else {
            PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
        }
        
        failureBlock();
    }];

}

- (void) amlTestUnshare:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test of unshare
     */
    [[AylaUser currentUser] deleteShare:gblAmlTestShare success:^(AylaResponse *resp) {
        if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
            failureBlock();
        }
    } failure:^(AylaError *err) {
        NSError *nsError = err.nativeErrorInfo;
        
        if (nsError != nil) {
            NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
            PFLog(@"F", @"%@:%@", @"Description", description);
        } else {
            PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
        }
        
        failureBlock();
    }];
}
#endif  // AML_TEST_SHARE_EMAIL

//----------------------------- resendConfirmation & resetPassword ----------------------

#ifdef AML_TEST_RESEND_CONFIRMATION_EMAIL

- (void) amlTestResendConfirmation:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test of resendConfirmation
     */
    NSDictionary *params = @{
                             AML_EMAIL_TEMPLATE_ID : @"ayla_confirmation_template_02",
                             AML_EMAIL_SUBJECT : @"Test of resendConfirmation()",
                             AML_EMAIL_BODY_HTML : @"This is a test of resendConfirmation()",
                             };
    [AylaUser resendConfirmation:AML_TEST_RESEND_CONFIRMATION_EMAIL
                           appId:AML_TEST_APP_ID
                       appSecret:AML_TEST_APP_SECRET
                       andParams:params
    success:^(AylaResponse *resp)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             PFLog(@"P");
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;

         if (nsError != nil) {
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description", description);
         } else {
             PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
         }
         
         failureBlock();
     }];
}
#endif

#ifdef AML_TEST_RESET_PASSWORD_EMAIL

- (void) amlTestResetPassword:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Test of resetPassword
     */
    NSDictionary *params = @{
                             AML_EMAIL_TEMPLATE_ID : @"ayla_passwd_reset_template_02",
                             AML_EMAIL_SUBJECT : @"Test of resetPassword()",
                             AML_EMAIL_BODY_HTML : @"This is a test of resetPassword()",
                             };
    [AylaUser resetPassword:AML_TEST_RESET_PASSWORD_EMAIL
                      appId:AML_TEST_APP_ID
                  appSecret:AML_TEST_APP_SECRET
                  andParams:params
    success:^(AylaResponse *resp)
     {
         if (resp.httpStatusCode >= 200 && resp.httpStatusCode < 300) {
             PFLog(@"P");
             [testSequencer executeNextTest:successBlock failure:failureBlock];
         } else {
             PFLog(@"F", @"httpStatusCode(%d) is not 2XX", resp.httpStatusCode);
             failureBlock();
         }
     }
    failure:^(AylaError *err)
     {
         NSError *nsError = err.nativeErrorInfo;
         
         if (nsError != nil) {
             NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
             PFLog(@"F", @"%@:%@", @"Description", description);
         } else {
             PFLog(@"F", @"%@:%d", @"httpStatusCode", err.httpStatusCode);
         }
         
         failureBlock();
     }];
}
#endif

//----------------------------- Registration ----------------------
- (void)amlTestUnregisterDevice:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    
    /**
     * Test to unregister device
     */
    [gblAmlTestDevice unregisterDevice:nil
               success:^(AylaResponse *resp) {
                   
                   PFLog(@"P");
                   
                   [testSequencer executeNextTest:successBlock failure:failureBlock];       // amlTestRegisterNewDevice
               }
               failure:^(AylaError *err) {
                   NSError * nsError = err.nativeErrorInfo;
                   NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                   PFLog(@"F", @"%@:%@", @"Description:", description);
                   
                   failureBlock();
               }];
}

- (void)amlTestRegisterNewDevice:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    sleep(3);
    /**
     * test to register device
     */
    [AylaDevice registerNewDevice:nil
     //[AylaDevice registerNewDevice:gblAmlTestDevice.dsn
              success:^(AylaResponse *resp, AylaDevice *device) {
                  NSMutableString *productName = [NSMutableString stringWithString:device.productName];
                  PFLog(@"P", @"%@:%@", @"productName", productName);
                  gblAmlTestDevice = device;
                  [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
              }
              failure:^(AylaError *err) {
                  NSError *nsError = err.nativeErrorInfo;
                  if (nsError != nil) {
                      NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                      PFLog(@"F", @"%@:%@", @"Description:", description);
                  } else {
                      PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
                  }
                  
                  failureBlock();
              }
     ];
}

static AylaDeviceNotification *gblCreatedDeviceNotification = nil;
- (void)amlTestCreateDeviceNotification:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    gblCreatedDeviceNotification = nil;
    AylaDeviceNotification *deviceNotification = [AylaDeviceNotification new];
    deviceNotification.notificationType = aylaDeviceNotificationTypeOnConnectionLost;
    deviceNotification.threshold = 3600;
    deviceNotification.deviceNickname = @"a nick";
    [gblAmlTestDevice createNotification:deviceNotification
        success:^(AylaResponse *response, AylaDeviceNotification *createdDeviceNotification) {
            gblCreatedDeviceNotification = createdDeviceNotification;
            PFLog(@"P", @"%@:%@", @"createdNotification", createdDeviceNotification.notificationType );
            [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
        } failure:^(AylaError *err) {
            NSError *nsError = err.nativeErrorInfo;
            if (nsError != nil) {
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description:", description);
            } else {
                PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
            }
            failureBlock();
        }];
}

- (void)amlTestGetDeviceNotifications:
                        /*success:*/(void (^)(void))successBlock
                        failure:(void (^)(void))failureBlock
{
    [gblAmlTestDevice getNotifications:nil
                 success:^(AylaResponse *response, NSArray *deviceNotifications) {
                     AylaDeviceNotification *deviceNotification = [deviceNotifications objectAtIndex:0];
                     PFLog(@"P", @"%@:%@", @"getDeviceNotifications", @(deviceNotifications.count));
                     [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
                 } failure:^(AylaError *err) {
                     NSError *nsError = err.nativeErrorInfo;
                     if (nsError != nil) {
                         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                         PFLog(@"F", @"%@:%@", @"Description:", description);
                     } else {
                         PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
                     }
                     failureBlock();
                 }];
}

- (void)amlTestUpdateDeviceNotification:
                        /*success:*/(void (^)(void))successBlock
                        failure:(void (^)(void))failureBlock
{
    if(gblCreatedDeviceNotification){
        gblCreatedDeviceNotification.threshold = 7200;
        gblCreatedDeviceNotification.deviceNickname = @"changed";
        [gblAmlTestDevice updateNotification:gblCreatedDeviceNotification
                                                 success:^(AylaResponse *response, AylaDeviceNotification *updatedDeviceNotification) {
                                                    //gblCreatedDeviceNotification= updatedDeviceNotification;
                                                     PFLog(@"P", @"%@:%@", @"updateDeviceNotifications", @(gblCreatedDeviceNotification.threshold));
                                                     [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
                                                 } failure:^(AylaError *err) {
                                                     NSError *nsError = err.nativeErrorInfo;
                                                     if (nsError != nil) {
                                                         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                                                         PFLog(@"F", @"%@:%@", @"Description:", description);
                                                     } else {
                                                         PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
                                                     }
                                                     failureBlock();
                                                 }];
    }
    else {
        [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
    }
}

static AylaAppNotification *gblCreatedDeviceAppNotification = nil;
- (void)amlTestCreateDeviceAppNotification:
        /*success:*/(void (^)(void))successBlock
        failure:(void (^)(void))failureBlock
{
    if(gblCreatedDeviceNotification){
        AylaAppNotification *deviceAppNotification = [AylaAppNotification new];
        deviceAppNotification.appType = aylaAppNotificationTypeEmail;

        deviceAppNotification.notificationAppParameters.email = @"1@aylanetworks.com";
        deviceAppNotification.notificationAppParameters.emailSubject = @"email subject";
        deviceAppNotification.notificationAppParameters.emailTemplateId = @"email template1";
        deviceAppNotification.notificationAppParameters.emailBodyHtml = @"body";
        deviceAppNotification.notificationAppParameters.username = @"user name";
        deviceAppNotification.notificationAppParameters.message = @"message";
        deviceAppNotification.nickname = @"nick 1";
        [gblCreatedDeviceNotification createApp:deviceAppNotification
        success:^(AylaResponse *response, AylaAppNotification *createdDeviceAppNotification) {
            gblCreatedDeviceAppNotification = createdDeviceAppNotification;
            PFLog(@"P", @"%@:%@", @"createDeviceAppNotifications", gblCreatedDeviceAppNotification.appType);
            [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
        } failure:^(AylaError *err) {
            
            NSError *nsError = err.nativeErrorInfo;
            if (nsError != nil) {
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description:", description);
            } else {
                PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
            }
            failureBlock();
        }];
    }
    else {
        [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
    }
}

- (void)amlTestGetDeviceAppNotifications:
                                    /*success:*/(void (^)(void))successBlock
                                   failure:(void (^)(void))failureBlock
{
    if(gblCreatedDeviceNotification){
        [gblCreatedDeviceNotification getApps:nil
        success:^(AylaResponse *response, NSMutableArray *deviceAppNotifications) {
            PFLog(@"P", @"%@:%@", @"getDeviceAppNotifications", @(deviceAppNotifications.count));
            gblCreatedDeviceAppNotification = deviceAppNotifications[0];
            [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
        } failure:^(AylaError *err) {
            NSError *nsError = err.nativeErrorInfo;
            if (nsError != nil) {
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description:", description);
            } else {
                PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
            }
            failureBlock();
        }];
    }
    else {
        [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
    }
}

- (void)amlTestUpdateDeviceAppNotification:
        /*success:*/(void (^)(void))successBlock
        failure:(void (^)(void))failureBlock
{
    if(gblCreatedDeviceAppNotification) {
        gblCreatedDeviceAppNotification.notificationAppParameters.emailSubject = @"test2";
        
        [gblCreatedDeviceNotification updateApp:gblCreatedDeviceAppNotification
        success:^(AylaResponse *response, AylaAppNotification *updatedDeviceAppNotification) {
            
            gblCreatedDeviceAppNotification = updatedDeviceAppNotification;
            PFLog(@"P", @"%@:%@", @"updateDeviceAppNotifications", updatedDeviceAppNotification.notificationAppParameters.emailSubject);
            [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
            
        } failure:^(AylaError *err) {
            NSError *nsError = err.nativeErrorInfo;
            if (nsError != nil) {
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description:", description);
            } else {
                PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
            }
            failureBlock();
        }];
    }
    else {
        [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
    }
}

- (void)amlTestDestroyDeviceAppNotification:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    if(gblCreatedDeviceAppNotification) {

        [gblCreatedDeviceNotification destroyApp:gblCreatedDeviceAppNotification
          success:^(AylaResponse *response) {
            gblCreatedDeviceAppNotification = nil;
            PFLog(@"P", @"%@:%@", @"destroyDeviceAppNotifications", @"none");
            [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
        } failure:^(AylaError *err) {
            NSError *nsError = err.nativeErrorInfo;
            if (nsError != nil) {
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description:", description);
            } else {
                PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
            }
            failureBlock();
        }];
    }
    else {
        [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
    }
}

- (void)amlTestDestroyDeviceNotification:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    if(gblCreatedDeviceNotification) {
        
        [gblAmlTestDevice destroyNotification:gblCreatedDeviceNotification
        success:^(AylaResponse *response) {
            PFLog(@"P", @"%@:%@", @"destroyDeviceNotifications", @"none");
            [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
        } failure:^(AylaError *err) {
            NSError *nsError = err.nativeErrorInfo;
            if (nsError != nil) {
                NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
                PFLog(@"F", @"%@:%@", @"Description:", description);
            } else {
                PFLog(@"F", @"%@:%d", @"httpStatusCode:", err.httpStatusCode);
            }
            failureBlock();
        }];
    }
    else {
        [testSequencer executeNextTest:successBlock failure:failureBlock];       // END OF DEVICE UNIT TEST?
    }
}

- (void)setSuccess
{
    [self complete];
    if([_statusField.text compare:@"working"]==0)
        [_statusField setText:@"success"];
    [_statusField setEnabled:true];
}

- (void)setFailure
{
    [self abortMsg];
    if([_statusField.text compare:@"working"]==0)
        [_statusField setText:@"failure"];
    [_statusField setEnabled:true];
}

- (void)complete
{
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlDeviceServiceTest", @"TotalTestsExecuted&Running", gblNumberOfTests-currentTotalTest, @"Passed", gblTestsPassed-currentTotalTestPassed, @"Failed", gblTestsFailed-currentTotalTestFailed, nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlDeviceServiceTest", @"TotalTestsExecutedOrRunning", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed);
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AMLDeviceLanModeView"];
        [self.navigationController pushViewController:viewController animated:true];
    }
}

- (void)abortMsg
{
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlDeviceServiceTest", @"TotalTestsExecuted&Running", gblNumberOfTests-currentTotalTest, @"Failed", gblTestsFailed-currentTotalTestFailed, @"Passed", gblTestsPassed-currentTotalTestPassed, @"Abort", nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlDeviceServiceTest", @"TotalTestsExecutedOrRunning", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort");
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AMLDeviceLanModeView"];
        [self.navigationController pushViewController:viewController animated:true];
    }
}


- (void)viewDidUnload
{
    [self setTextView:nil];
    [self setStatusField:nil];
    [self setButton:nil];
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if([self.navigationController.viewControllers indexOfObject:self] == NSNotFound){
        runAllInALoop = NO;
    }
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

//
//  AMLViewController.m
//  AML IOS
//
//  Created by Daniel Myers on 7/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//
//  Unit Tests for Ayla Mobile Libraries
//
#import "AMLViewController.h"
#import "AylaTest.h"
#import "AylaSystemUtilsSupport.h"

//
// Simple unit test application for Ayla Mobile Library
//
// Provides example code for all supported calls
// Indicates tests passed and failed on completion or 30 seconds, which ever occurs first
// Designed to run with a registered Standard Demo Kit
//


@implementation AMLViewController
@synthesize testView = _testView;

- (void)viewWillAppear:(BOOL)animated
{
    if(outputMsgs!=nil){
        self.testView.text = outputMsgs;
    }
    runAllInALoop = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [AylaSystemUtils loadSavedSettings];
    [AylaSystemUtils setDeviceSsidRegex:AML_DEVICE_SSID_REGEX];
    [AylaSystemUtils saveCurrentSettings];
}


- (void)viewDidAppear:(BOOL)animated
{
    BOOL isChanged = NO;
    if([AML_TEST_USER_NAME isEqualToString:@"me@mycompany.com"]){
        _testView.text = [NSString stringWithFormat:@"%@\n%@", _testView.text, @"Please change value of AML_TEST_USER_NAME to your user name."];
        isChanged = YES;
    }
    if([AML_TEST_PASSWORD isEqualToString:@"myPassword"]){
        _testView.text = [NSString stringWithFormat:@"%@\n%@", _testView.text, @"Please change value of AML_TEST_PASSWORD to your password."];
        isChanged = YES;
    }
    if([gblAmlProductName isEqualToString:@"AC000W000XXXXXX"]){
        _testView.text = [NSString stringWithFormat:@"%@\n%@", _testView.text, @"Please change value of gblAmlProductName to your device."];
        isChanged = YES;
    }}


- (IBAction)testAll:(id)sender
{
    runAllInALoop = YES;
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AMLSettingTestView"];
    [self.navigationController pushViewController:viewController animated:true];
}

- (void)viewDidUnload
{
    [self setTestView:nil];
  [super viewDidUnload];
  // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
  } else {
    return YES;
  }
}

@end

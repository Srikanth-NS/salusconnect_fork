//
//  AylaDeviceSupport.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 3/20/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AylaDevice.h"
@class AFHTTPRequestOperation;
@class AylaLanCommandEntity;
@interface AylaDevice(Support)
@property (nonatomic, copy)     NSNumber *key;                       //Device key number
@property (nonatomic, strong)   AylaLanModeConfig* lanModeConfig;    //Lan mode support
@property (nonatomic, copy)     NSMutableArray *features;
@property (nonatomic, strong, readwrite) NSString *connectionStatus;

- (id)   initDeviceWithDictionary:(NSDictionary *)dictionary;
- (void) updateWithDictionary:(NSDictionary *)deviceDictionary;
+ (instancetype) deviceFromDeviceDictionary:(NSDictionary *)deviceDictionary;

+ (void) getNewDeviceConnected:(NSString *)dsn
                    setupToken:(NSString *) setupToken
                       success:(void (^)(AylaResponse *response, NSDictionary *responce))successBlock
                       failure:(void (^)(AFHTTPRequestOperation *operation, AylaError *err))failureBlock;

- (BOOL)          isLanModeEnabled;
- (BOOL)          isLanModeActive;
+ (void)          deleteLanModeSession; //currently just for testing
+ (void)          extendLanModeSession:(int)method haveDataToSend:(BOOL)haveDataToSend;
- (AylaProperty*) findProperty:(NSString *)propertyName;
- (BOOL)          readLanModeConfigFromCache;
- (void)          getLanModeConfig;

- (BOOL)          initPropertiesFromCache;
+ (Class)         deviceClassFromDeviceDictionary:(NSDictionary *)dictionary;
+ (Class)         deviceClassFromClassName:(NSString *)className;
- (NSInteger)     lanModeWillSendCmdEntity:(AylaLanCommandEntity *)entity;
- (NSInteger)     lanModeUpdateWithPropertyName:(NSString *)propertyName andValue:(NSString *)value;
- (NSInteger)     lanModeUpdateWithCmdId:(NSInteger)cmdId status:(NSInteger)status propertyName:(NSString *)propertyName andValue:(id)value;
- (NSString *)    lanModeToDeviceUpdateWithCmdId:(__unused int)cmdId property:(AylaProperty *)property valueString:(NSString *)valueString;
- (NSString *)    lanModeToDeviceCmdWithCmdId:(int)cmdId requestType:(NSString *)type sourceLink:(NSString *)sourceLink uri:(NSString *)uri data:(NSString *)data;

/*
 * This api will be called by AylaLanMode when device taged/untagged as current LME device.
 * Note this api doesn't guarantee a lan session could be succesfully eastablished.
 */
- (void)          didEnableLanMode;
- (void)          didDisableLanMode;

- (AylaDevice *)  lanModeEdptFromDsn:(NSString *)dsn;
- (NSString *)    lanModePropertyNameFromEdptPropertyName:(NSString *)name;
@end


@interface AylaProperty(Support)
@property (nonatomic, copy) NSNumber *key;

+ (NSOperation *) getProperties:(AylaDevice *)device callParams:(NSDictionary *)callParams
               success:(void (^)(AylaResponse *response, NSArray *properties))successBlock
               failure:(void (^)(AylaError *err))failureBlock;

- (id) initDevicePropertiesWithDictionary:(NSDictionary *)propertiesDictionary;
- (void) updateDatapointFromProperty;
+ (void) setLastDsn:(NSString *) dsn;
- (void) lanModeEnable:(AylaDevice *)device;
- (void) lanModeEnable:(AylaDevice *)device property:(AylaProperty *)property;
+ (void) lanModeEnable:(AylaDevice *)device properties:(NSArray *)properties;
@end

@interface AylaDatapoint(Support)

+ (NSOperation *) createDatapoint:(AylaProperty *)property datapoint:(AylaDatapoint *)datapoint
                 success:(void (^)(AylaResponse *response, AylaDatapoint *datapointCreated))successBlock
                 failure:(void (^)(AylaError *err))failureBlock;

+ (NSOperation *) getDatapointsByActivity:(AylaProperty *)property callParams:(NSDictionary *)callParams
                         success:(void (^)(AylaResponse *response, NSArray *datapoints))successBlock
                         failure:(void (^)(AylaError *err))failureBlock;
@end

@interface AylaDatapointBlob(Support)

@property (nonatomic, strong) NSString *fileUrl;
@property (nonatomic, strong) NSString *location;

+ (NSOperation *) getBlobsByActvity:(AylaProperty *)property callParams:callParams
                   success:(void (^)(AylaResponse *response, NSArray *retrievedDatapoint))successBlock
                   failure:(void (^)(AylaError *err))failureBlock;

+ (void)getBlobSaveToFileWithDatapoint:(AylaDatapointBlob *)datapoint property:(AylaProperty *)property
                                params:(NSDictionary *)callParams
                               success:(void (^)(AylaResponse *response, NSString *retrievedBlobName))successBlock
                               failure:(void (^)(AylaError *err))failureBlock;

/*
 * @note: Only only upload once for each datapoint.
 */
+ (void)uploadBlobWithDatapoint:(AylaDatapointBlob *)datapoint params:(NSDictionary *)callParams
                        success:(void (^)(AylaResponse *response, AylaDatapointBlob *datapoint))successBlock
                        failure:(void (^)(AylaError *err))failureBlock;

+ (void) createBlobWithProperty:(AylaProperty *)property params:(NSDictionary *)callParams
                       success:(void (^)(AylaResponse *response, AylaDatapointBlob *retrievedBlobs))successBlock
                       failure:(void (^)(AylaError *err))failureBlock;

+ (NSOperation *) getBlobLocation:(AylaDatapointBlob *)datapoint
                         success:(void (^)(AylaResponse *response, AylaDatapointBlob *datapoint))successBlock
                         failure:(void (^)(AylaError *err))failureBlock;

- (NSOperation *) markFinished:(NSDictionary *)callParams
                      success:(void (^)(AylaResponse *response))successBlock
                      failure:(void (^)(AylaError *err))failureBlock;

@end

extern NSString * const kAylaDeviceClassNameGateway;
extern NSString * const kAylaDeviceClassNameNode;
extern NSString * const kAylaDeviceClassName;
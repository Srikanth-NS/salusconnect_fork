//
//  AylaLanMode.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 2/8/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaNetworks.h"
#import "AylaLanMode.h"
#import "AylaHttpServer.h"
#import "AylaLanCommandEntity.h"
#import "AylaNotify.h"
#import "AylaTimer.h"
#import "AylaEncryption.h"
#import "AylaReachabilitySupport.h"
#import "AylaDeviceSupport.h"
#import "AylaDiscovery.h"

@implementation AylaLanMode

//static NSString *serverIpAddress = nil;
//static File serverRootDir = nil;
static AylaTimer *sessionTimer = nil;
static int seqNo = 0;
static AylaHttpServer *httpServer = nil;

static AylaDevice *device = nil; 	// currently selected lan mode device
static NSMutableDictionary *devices = nil; // lan mode devices registered to this user

static NSMutableArray *commandsQueue = nil;

static int nextCommandOutstandingId = 0;
static NSMutableDictionary *commandsOutstanding = nil;

static enum lanModeSession sessionState = DOWN;

static BOOL inSetupMode = NO;

+ (NSMutableDictionary *)devices
{
    if(devices == nil){
        devices = [[NSMutableDictionary alloc] init];
    }
    return devices;
}

+ (AylaTimer *)sessionTimer
{
    return sessionTimer;
}
+ (void)setSessionTimer:(AylaTimer*)_timer
{
    sessionTimer = _timer;
}
+ (AylaDevice *)device
{
    return device;
}
+ (void)setDevice:(AylaDevice *)_device
{
    _device? [_device didEnableLanMode]: [device didDisableLanMode];
    device = _device;
}
+ (int)sessionState{
    return (int)sessionState;
}
+ (void)setSessionState:(enum lanModeSession)_sessionState
{
    sessionState = _sessionState;
}
+ (int)seqNo
{
    return seqNo;
}
+ (void)addSeqNo
{
    seqNo = [AylaLanMode nextSequenceNumber:seqNo];
}
+ (int)nextCommandOutstandingId
{
    int cur = nextCommandOutstandingId;
    nextCommandOutstandingId = [AylaLanMode nextSequenceNumber:cur];
    return cur;
}

const static int MAX_U16 = 65535;
+ (int)nextSequenceNumber:(int)cur
{
    if(++cur > MAX_U16){
        cur = 0;
    }
    return cur;
}

+ (BOOL) inSetupMode {
    return inSetupMode;
}

+ (void) inSetupMode:(BOOL)_inSetupMode {
    inSetupMode = _inSetupMode;
}




//----------------  Used for session extension/keep-alive with the module

static int sessionInterval = 25-3;
+ (void)initSessionTimer
{
    void (^sessionHandle)(NSTimer *timer) = ^(NSTimer *timer){
        if(device == nil){ //stop
            [timer invalidate];
            return;
        }
        if([commandsQueue count]!= 0){
            saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"more command?", true, @"sessionTimer");
            [AylaDevice extendLanModeSession:PUT_LOCAL_REGISTRATION haveDataToSend:true];
        }
        else{
           saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"more command?", false, @"sessionTimer");
           [AylaDevice extendLanModeSession:PUT_LOCAL_REGISTRATION haveDataToSend:false];
        }
    };
    
    if(sessionTimer != nil){
        [sessionTimer stop];
        sessionTimer = nil;
    }
    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"interval", sessionInterval, @"sessionTimer");
    sessionTimer = [[AylaTimer alloc] initWithIntervalAndHandle:sessionInterval tickHandle:sessionHandle];
}

// --------------------------------------- begin LAN MODE enablement & disablement ------------------------
+ (int)enableWithNotifyHandle:
    /* notifyHandle*/ (void(^)(NSDictionary*)) notifyHandle
    ReachabilityHandle: (void(^)(NSDictionary*)) reachabilityHandle
{
    [super lanModeState:ENABLED];
    
    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"currentState", [self lanModeState], @"enable");
 
    [AylaNotify register:notifyHandle];
    //start sesstion timer
    [self initSessionTimer];
    //--------------
    [AylaReachability register:reachabilityHandle];
    return [self resume];
}

+ (int)enable
{
    [super lanModeState:ENABLED];    
    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"currentState", [self lanModeState], @"enable");

    [AylaNotify register:nil];
    [self initSessionTimer];
    [AylaReachability register:nil];
    
    return [self resume];
}


+ (int)resume
{
    //Refresh user access token if necessary
    [AylaUser refreshAccessTokenOnExpiry];
    
    if([super lanModeState] == DISABLED){
         saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaLanMode", @"currentState", [super lanModeState], @"resume");
        return AML_ERROR_FAIL;
    }
    
    //start HTTP_SERVER
    //[DDLog addLogger:[DDTTYLogger sharedInstance]];
	
	// Create server using AylaHTTPServer class
    if(httpServer == nil)
        httpServer = [[AylaHttpServer alloc] initWithPort:[[self serverPortNumber] intValue]];
    
	// Serve files from embedded Web folder
	NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"local_lan"];
	[httpServer setDocumentRoot:webPath];	
	// Start the server (and check for problems)
	NSError *error;
	
    if([httpServer isRunning]) {
        [self lanModeState:RUNNING];
    }
    else if([httpServer start:&error]){
		saveToLog(@"%@, %@, Started HTTPServer, port %hu", @"I", @"AylaLanMode", [httpServer listeningPort]);
	    [self lanModeState:RUNNING];
    }
	else{
		saveToLog(@"%@, %@, Error starting HTTP Server: %@", @"E", @"AylaLanMode", error);
        [self lanModeState:FAILED];
        return AML_ERROR_FAIL;
    }
    
    if([self lanModeState] == RUNNING){
        [AylaReachability determineReachability];
    
        //set session timer
        [sessionTimer stop];
        [sessionTimer setInterval:sessionInterval];
        [sessionTimer start];
        saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"currentState", [self lanModeState], @"resume");
    }
    else{
        return AML_ERROR_FAIL;
    }
    return AML_ERROR_OK;
}


+ (int)disable
{
    if([AylaSystemUtils lanModeState] == RUNNING){
        [AylaLanMode pause];
        [AylaLanMode setDevice:nil];
    }
    [AylaSystemUtils lanModeState:DISABLED];
    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"currentState", [AylaSystemUtils lanModeState], @"disable");
    return AML_ERROR_OK;
}

+ (int)pause
{    
    if([AylaSystemUtils lanModeState] == DISABLED)
        return AML_ERROR_OK;
    
    [sessionTimer stop];
    if(httpServer!=nil){
        [httpServer stop];
        httpServer = nil;
    }
    [AylaDiscovery cancelDiscovery];
    [AylaSystemUtils lanModeState:STOPPED];
    
    if([AylaLanMode device]!= nil ){
        [[AylaLanMode device] lanModeDisable];
    }
    usleep(400000);

    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"currentState", [AylaSystemUtils lanModeState], @"pause");
    return AML_ERROR_OK;
}

+ (void)sendNotifyToLanModeDevice:(int)cmdId baseType:(int)baseType jsonString:(NSString *)jsonString
                            block:(void(^)(NSDictionary *))block
{
    [AylaLanMode enQueue:cmdId baseType:baseType jsonString:jsonString];
    if(block != nil){
        [AylaLanMode putCommandsOutstanding:cmdId block:block];
    }
    if([AylaLanMode commandsQueueCount]==1){
        saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaLanMode", @"sendNotify", true, @"sendNotifyToLanModeDevice");
        [AylaDevice extendLanModeSession:PUT_LOCAL_REGISTRATION haveDataToSend:true];
    }
}

+ (void)notifyAcknowledge
{
    [AylaNotify setNotifyOutstandingCounter:[AylaNotify notifyOutstandingCounter]-1];
}

//-------------- register new handler

+ (void)registerReachabilityHandle:(void(^)(NSDictionary *))handle
{
    [AylaReachability register:handle];
}
+ (void)registerNotifyHandle:(void(^)(NSDictionary *))handle
{
    [AylaNotify register:handle];
}

//------------------------------------------------------------

+ (void)enQueue:(int)cmdId baseType:(int)baseType jsonString:(NSString *)jsonString
{
    if (commandsQueue == nil) {
        commandsQueue = [[NSMutableArray alloc] init];
    }
    
    AylaLanCommandEntity *command = [[AylaLanCommandEntity alloc] initWithParams:cmdId jsonString:jsonString type:baseType];
    [commandsQueue addObject:command];
}

+ (AylaLanCommandEntity *)nextInQueue
{
    if([commandsQueue count] == 0) return nil;
    id command = [commandsQueue objectAtIndex:0];
    return command;
}

+ (void)deQueue
{
    if([commandsQueue count] == 0) return;
    id command = [commandsQueue objectAtIndex:0];
    if(command != nil ){
        [commandsQueue removeObjectAtIndex:0];
    }
}

+ (void)clearQueue
{
    [commandsQueue removeAllObjects];
}


+ (int)commandsQueueCount
{
    return (int)[commandsQueue count];
}

+ (void (^)(NSDictionary *))getCommandsOutstanding:(NSString *)cmdId
{
    if([commandsOutstanding valueForKeyPath:cmdId]!=NULL){
        return [commandsOutstanding valueForKeyPath:cmdId];
    }
    return nil;
}

+ (void)putCommandsOutstanding:(int)cmdId block:(void (^)(NSDictionary *))block
{    
    if(commandsOutstanding == nil){
        commandsOutstanding = [[NSMutableDictionary alloc] init];
    }
    [commandsOutstanding setObject:[block copy] forKey:[NSString stringWithFormat:@"%d", cmdId]];
}


+ (void)removeCommandsOutstanding:(int)cmdId
{    
    //if([commandsOutstanding objectForKey:[NSString stringWithFormat:@"%d", cmdId]] != nil){
    [commandsOutstanding removeObjectForKey:[NSString stringWithFormat:@"%d", cmdId]];
    // }
}

+ (void)clearCommandOutstanding
{
    [commandsOutstanding removeAllObjects];
}


//helpful method
+ (NSString *)buildToDeviceCommand:
    (NSString *)method cmdId:(int)cmdId
    resourse:(NSString *)resource data:(NSString *)data
    uri:(NSString *) uri
{
    return [NSString stringWithFormat:@"{\"cmd\": {\"cmd_id\":%@, \"method\":\"%@\", \"resource\": \"%@\", \"data\":\"%@\",\"uri\":\"%@\"}}",
                [NSNumber numberWithInt:cmdId],
                method,
                resource,
                data == nil? @"none":data,
                uri
            ];
}

+ (void)cleanCurrentEncSession
{
    [AylaEncryption cleanCurrentSession];
}
@end

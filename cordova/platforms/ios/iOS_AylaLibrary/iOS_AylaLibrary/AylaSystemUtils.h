//
//  AylaSystemUtils.h
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 6/26/12.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

typedef enum {
    AylaSystemLoggingNone = 0x00,
    AylaSystemLoggingError = 0x01,
    AylaSystemLoggingWarning = 0x03,
    AylaSystemLoggingInfo = 0x07,
    AylaSystemLoggingAll = 0xff,
} AylaSystemLoggingLevel;

typedef NS_ENUM(NSUInteger, AylaServiceLocation) {
    AylaServiceLocationUS = 0,
    AylaServiceLocationCN
};

@interface AylaSystemUtils : NSObject 

/**
 * The Wi-Fi time-out value in seconds
 */
+ (NSNumber *)wifiTimeout;
+ (void) wifiTimeout:(NSNumber *)wifiTimeout;

/**
 * Maximum number of objects returned per request.
 */
+ (NSNumber *)maxCount;
+ (void) maxCount:(NSNumber *)maxCount;

/**
 * Directive to use development, staging or production Ayla cloud services.
 */
+ (NSNumber *)serviceType;
+ (void) serviceType:(NSNumber *)serviceType;

/**
 * Directive to use development, staging or production Ayla cloud services.
 */
+ (AylaServiceLocation) serviceLocation;
+ (void) serviceLocationWithAppId:(NSString *)appId;

/**
 * Set level of logs to be displayed
 */
+ (AylaSystemLoggingLevel)loggingLevel;
+ (void)loggingLevel:(AylaSystemLoggingLevel)level;

/**
 * Current Lan Mode State. Normally Set it to ENABLED before LAN Mode method [AylaLanMode enable] is called.
 */
+ (enum AML_LAN_MODE_STATE) lanModeState;
+ (void) lanModeState: (enum AML_LAN_MODE_STATE) lanModeState;

/**
 * Port number of Http Server.
 */
+ (NSNumber *) serverPortNumber;
+ (void) serverPortNumber:(int)serverPortNumber;

/**
 * Set timeout value for service reachability check. 
 */
+ (NSNumber *) serviceReachableTimeout;

/**
 * Regular Expression for device SSID. Used to check connected device during setup task. 
 */
+ (NSString *)deviceSsidRegex;
+ (void) setDeviceSsidRegex: (NSString *)_deviceSsidRegex;

/**
 * Set timeout-value for service reachability check.
 */
+ (void) serviceReachableTimeout:(NSNumber *) serviceReachableTimeout;

/**
 * Set retries-value for setup confirmation check.
 */
+ (NSNumber *) newDeviceToServiceConnectionRetries;
+ (void) newDeviceToServiceConnectionRetries: (NSNumber *)newDeviceToServiceConnectionRetries;

/**
 * Set to use slow connection mechanism or not
 */
+ (NSNumber *) slowConnection;
+ (void) slowConnection: (NSNumber *)slowConnection;

/**
 * Send app id to library
 */
+ (NSString *) appId;
+ (void) appId:(NSString *)appId;

/**
 * Current iAML version
 */
+ (NSString *) version;

/**
 * Save current configuration settings to nonvolatile memory.
 */
+ (int) saveCurrentSettings;

/**
 * Read the configuration settings from nonvolatile memory and apply to current settings.
 */
+ (int) loadSavedSettings;

/**
 * Set current configuration settings to default values and save them to nonvolatile memory.
 */
+ (int) saveDefaultSettings;

/**
 * Log Support
 */
+ (NSString *) getLogFilePath;
+ (NSString *) getSupportMailAddress;
+ (NSString *) getLogMailSubjectWithAppId:(NSString *)appId;

/* Not available
 + (NSNumber *)refreshInterval;
 + (void) refreshInterval:(NSNumber *)refreshInterval;
 */
@end


//---------------------------------- Logging --------------------
@interface FileLogger : NSObject {
    NSFileHandle *logFile;
}

@property (nonatomic) NSString *pLogFilePath;
@property (nonatomic) NSNumber *fileLoggingEnabled;

+ (FileLogger *)sharedInstance;
- (void)log:(NSString *)format, ...;

@end

#define saveToLog(fmt, ...) [[FileLogger sharedInstance] log:fmt, ##__VA_ARGS__]


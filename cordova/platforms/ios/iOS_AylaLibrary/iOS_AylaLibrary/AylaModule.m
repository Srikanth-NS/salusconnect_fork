//
//  AylaModule.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 1/23/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaLogService.h"
#import "AylaSystemUtilsSupport.h"
#import "AylaSetupSupport.h"
#import "AylaDeviceSupport.h"
#import "AylaHost.h"
#import "AylaReachabilitySupport.h"
#import "AylaLanModeSupport.h"
#import "AylaSecuritySupport.h"
#import "AylaErrorSupport.h"
#import "AylaCacheSupport.h"

@implementation AylaModule

@synthesize deviceService = _deviceService;
@synthesize lastConnectMtime = _lastConnectMtime;
@synthesize mtime = _mtime;
@synthesize version = _version;
@synthesize apiVersion = _apiVersion;
@synthesize build = _build;

static NSDictionary *results = nil;
static int responseCode = 0;
static int subTaskFailed = 0;

+ (int)responseCode
{
    return responseCode;
}

+ (NSDictionary *)results
{
    return results;
}

+ (int)subTaskFailed
{
    return subTaskFailed;
}

- (id)initModuleWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self){
        self.dsn = [dictionary valueForKeyPath:@"dsn"];
        self.mac = [dictionary valueForKeyPath:@"mac"];
        self.model = [dictionary valueForKeyPath:@"model"];
        _deviceService = [dictionary valueForKeyPath:@"device_service"];
        _lastConnectMtime = [dictionary valueForKeyPath:@"last_connect_mtime"];
        _mtime = [dictionary valueForKeyPath:@"mtime"];
        _version = [dictionary valueForKeyPath:@"version"];
        _apiVersion = [dictionary valueForKeyPath:@"api_version"];
        _build = [dictionary valueForKeyPath:@"build"];
        self.features = [dictionary valueForKeyPath:@"features"] == [NSNull null]? nil: [dictionary valueForKeyPath:@"features"];
    }
    return self;
}

- (void)setNewDeviceTime:(NSNumber*) newTime
                 success:(void (^)(AylaResponse *response))successBlock
                 failure:(void (^)(AylaError *err))failureBlock
{
    NSDictionary *time =[NSDictionary dictionaryWithObjectsAndKeys:
                         newTime, @"time", nil];
    
    NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"PUT" path:@"time.json" parameters:time];
    [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
                 success:^(AFHTTPRequestOperation *operation, id responseObject){
                     saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaSetup", @"Success", operation.response.statusCode, @"AylaModule.setNewDeviceTime");
                     AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                     successBlock(resp);
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error){
                     saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaSetup", @"Failed", operation.response.statusCode, @"AylaModule.setNewDeviceTime");
                     AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = error;
                     subTaskFailed = AML_SET_NEW_DEVICE_TIME;
                     NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                     NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                     err.errorInfo = description;
                     failureBlock(err);
                 }
    ];
    [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
}

- (void)getNewDeviceDetail:
                /*success:*/(void (^)(AylaResponse *response, AylaModule *device))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
    NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"GET" path:@"status.json" parameters:nil];
    [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
             success:^(AFHTTPRequestOperation *operation, id responseObject){
                 saveToLog(@"%@, %@, %@:%d, %@", @"I", @"DeviceSetup", @"Success", operation.response.statusCode, @"getNewDeviceDetail.getPath");
                 AylaModule *newDevice = [[AylaModule alloc] initModuleWithDictionary:responseObject];
                 AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                 successBlock(resp, newDevice);
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error){
                 saveToLog(@"%@, %@, %@, %@", @"E", @"DeviceSetup", @"Failed", @"getNewDeviceDetail.getPath");
                 AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = error;
                 subTaskFailed = AML_GET_NEW_DEVICE_DETAIL;
                 NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                 NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                 err.errorInfo = description;
                 failureBlock(err);
             }
    ];
    [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
}

+ (void)getNewDeviceScanForAPs:
            /*success:*/(void (^)(AylaResponse *response, NSMutableArray *apList)) successBlock
            failure:(void (^)(AylaError *err)) failureBlock
{
    if( [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_CONNECT_TO_NEW_DEVICE &&
       [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_GET_DEVICE_SCAN_FOR_APS)
    {
        saveToLog(@"%@, %@, %@:%@,%@:%d, %@", @"E", @"AylaModule", @"Failed", @"TaskOutOfOrder", @"lastMethodCompleted", [AylaSetup lastMethodCompleted],  @"getNewDeviceScanForAPs");
        NSNumber *methodCompleted = [[NSNumber alloc] initWithInt:[AylaSetup lastMethodCompleted]];
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys: methodCompleted ,@"taskOutOfOrder", nil];
        AylaError *err = [AylaError new]; err.errorCode = AML_TASK_ORDER_ERROR; err.httpStatusCode = 0;
        err.errorInfo = description; err.nativeErrorInfo = nil;
        failureBlock(err);
        return;
    }
    
    if([AylaSetup hostNewDeviceSsid] == nil){
        AylaError *err = [AylaError new];
        err.errorCode = AML_NO_DEVICE_CONNECTED;
        err.httpStatusCode = 0;
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:@"Invalid new device SSID.", @"error", nil];
        err.errorInfo = description;
        err.nativeErrorInfo = nil;
        failureBlock(err);
        return;
    }
    
    [self setNewDeviceScanForAPs:
     /*success*/^(AylaResponse *response){
         saveToLog(@"%@, %@, %@, %@", @"I", @"AylaModule", @"Success", @"setNewDeviceScanForAPs.postPath");
         sleep(2);
         NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"GET" path:@"wifi_scan_results.json" parameters:nil];
         [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
         AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      
                      saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaModule", @"Success",operation.response.statusCode, @"getNewDeviceScanForAPs.getPath");
                      NSDictionary * dict = [responseObject objectForKey:@"wifi_scan"];
                      NSArray *result = [dict objectForKey:@"results"];
                      NSMutableArray * apList = [NSMutableArray array];
                      for(NSDictionary *candidate in result){
                          AylaModuleScanResults *ap = [[AylaModuleScanResults alloc] initModuleScanResultWithDictionary:candidate];
                          [apList addObject:ap];
                      }
                      [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_GET_DEVICE_SCAN_FOR_APS];
                      AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                      successBlock(resp, apList);
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaModule", @"Failed",operation.response.statusCode,  @"getNewDeviceScanForAPs.getPath");
                      AylaError *err = [AylaError new]; err.errorCode = 1;  err.httpStatusCode = operation.response.statusCode;
                      err.nativeErrorInfo = error;
                      subTaskFailed = AML_GET_NEW_DEVICE_SCAN_FOR_APS;
                      NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                      NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                      err.errorInfo = description;
                      failureBlock(err);
                  }
          ];
         [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
     } failure:^(AylaError *err) {
         failureBlock(err);
     }];
    
}

+ (void)setNewDeviceScanForAPs:
            /*success:*/(void (^)(AylaResponse *response)) successBlock
            failure:(void (^)(AylaError *err)) failureBlock
{
    NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"POST" path:@"wifi_scan.json" parameters:nil];
    [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
                                         
             success:^(AFHTTPRequestOperation *operation, id responseObject){
                 saveToLog(@"%@, %@, %@:%d, %@", @"I", @"DeviceModule", @"Success",operation.response.statusCode, @"setNewDeviceScanForAPs.postPath");
                 AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                 successBlock(resp);
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error){
                 saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceModule", @"Failed",operation.response.statusCode, @"setNewDeviceScanForAPs.postPath");
                 AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode;
                 err.nativeErrorInfo = error;
                 subTaskFailed = AML_SET_NEW_DEVICE_SCAN_FOR_APS;
                 NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                 NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                 err.errorInfo = description;
                 failureBlock(err);
             }
    ];
    [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
}



+ (void)disconnectNewDevice:(NSDictionary *)params
                    success:(void (^)(AylaResponse *response))successBlock
                    failure:(void (^)(AylaError *err))failureBlock
{
    NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"PUT" path:@"wifi_stop_ap.json" parameters:nil];
    [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
             success:^(AFHTTPRequestOperation *operation, id responseObject){
                 saveToLog(@"%@, %@, %@:%d, %@", @"I", @"DeviceModule", @"Success",operation.response.statusCode, @"disconnectNewDevice");
                 AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                 successBlock(resp);
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error){
                 saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceModule", @"Failed",operation.response.statusCode, @"disconnectNewDevice");
                 AylaError *err = [AylaError new]; err.errorCode = 1;
                 err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = nil; err.errorInfo = nil;
                 failureBlock(err);
             }
    ];
    [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
}


+ (void)connectNewDevcieToServiceContinued:(NSDictionary *)callParams
                                   success:(void (^)(AylaResponse *response))successBlock
                                   failure:(void (^)(AylaError *err))failureBlock
{
    int pollInterval = DEFAULT_SETUP_STATUS_POLLING_INTERVAL;
    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"DeviceSetup", @"Success", @"", @"connectNewDeviceToService");
    __block int errorTries = 2;
    void (^__block __weak __failureBlock)(AylaError *);
    void (^__block __weak __successBlock)(AylaResponse *response, AylaWiFiStatus *);
    void (^__block _failureBlock)(AylaError *);
    void (^__block _successBlock)(AylaResponse *response, AylaWiFiStatus *);
    __successBlock = _successBlock = ^(AylaResponse *response, AylaWiFiStatus *wifiStatus) {
        sleep(pollInterval);
        [AylaSetup getNewDeviceWiFiStatus:^(AylaResponse *response, AylaWiFiStatus *wifiStatus) {
            errorTries = 2;
            NSArray *connectionHistory = wifiStatus.connectHistory;
            if ([connectionHistory count]>0) {
                AylaWiFiConnectHistory *latestHistory = [connectionHistory objectAtIndex:0];
                if ([latestHistory.error intValue] == 0) { // no error
                    [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
                    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"DeviceSetup", @"Success", @"0", @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                    successBlock(response);
                }
                else if ([latestHistory.error intValue] == 6 ||
                         [latestHistory.error intValue] == 3) { // incorrect key
                    saveToLog(@"%@, %@, %@:%@, %@", @"E", @"DeviceSetup", @"Failed", @"Incorrect Key", @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                    AylaError *err = [AylaError new]; err.errorCode = AML_SETUP_DEVICE_ERROR; err.httpStatusCode = 400; err.nativeErrorInfo = nil;
                    subTaskFailed = AML_GET_NEW_DEVICE_WIFI_STATUS;
                    NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                    NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", latestHistory.error, @"error", latestHistory.msg, @"msg", nil];
                    err.errorInfo = description;
                    failureBlock(err);
                }
                else if ([latestHistory.error intValue] == 20) {
                    saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"I", @"DeviceSetup", @"StillInProgress",  [latestHistory.error integerValue], @"oneMoreTires", @"null" , @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                    sleep(pollInterval);
                    [AylaSetup getNewDeviceWiFiStatus: __successBlock
                                              failure: _failureBlock];
                }
                else {
                    saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"E", @"DeviceSetup", @"errCode", [latestHistory.error integerValue], @"msg", latestHistory.msg, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                    AylaError *err = [AylaError new]; err.errorCode = AML_SETUP_DEVICE_ERROR; err.httpStatusCode = 400; err.nativeErrorInfo = nil;
                    subTaskFailed = AML_GET_NEW_DEVICE_WIFI_STATUS;
                    NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                    NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", latestHistory.error, @"error", latestHistory.msg, @"msg", nil];
                    err.errorInfo = description;
                    failureBlock(err);
                }
            } else {
                saveToLog(@"%@, %@, %@:%@, %@", @"E", @"DeviceSetup", @"Failed", @"No WiFi history received from module.", @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                AylaError *err = [AylaError new]; err.errorCode = AML_SETUP_DEVICE_ERROR; err.httpStatusCode = 404; err.nativeErrorInfo = nil;
                subTaskFailed = AML_GET_NEW_DEVICE_WIFI_STATUS;
                NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                err.errorInfo = description;
                failureBlock(err);
            }
        } failure:^(AylaError *err) {
            if (errorTries-- > 0){
                if ([AylaHost isNewDeviceConnected]) {
                    sleep(DEFAULT_SETUP_STATUS_POLLING_INTERVAL);
                    saveToLog(@"%@, %@, %@:%d, %@:%d, %@", @"I", @"DeviceSetup", @"FailureBlockOneMoreTry", err.errorCode, @"errorTries", errorTries, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                    [AylaSetup getNewDeviceWiFiStatus: __successBlock
                                              failure: _failureBlock];
                }
                else {
                    saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"Failed", err.errorCode, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                    //now set last method completed to AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE to guarrantee confirm connection method could be called after this failure
                    [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
                    failureBlock(err);
                }
            }
            else {
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"Failed", err.errorCode, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                //now set last method completed to AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE to guarrantee confirm connection method could be called after this failure
                [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
                failureBlock(err);
            }
        }];
    };
    
    __failureBlock = _failureBlock = ^(AylaError *err) {
        
        if (errorTries-- > 0){
            if ([AylaHost isNewDeviceConnected]) {
                sleep(pollInterval);
                saveToLog(@"%@, %@, %@:%d, %@:%d, %@", @"I", @"DeviceSetup", @"FailureBlockOneMoreTry", err.errorCode, @"errorTries", errorTries, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                [AylaSetup getNewDeviceWiFiStatus:_successBlock
                                          failure:__failureBlock];
            }
            else {
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"Failed", err.errorCode, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
                //now set last method completed to AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE to guarrantee confirm connection method could be called after this failure
                [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
                failureBlock(err);
            }
        }
        else {
            saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"Failed", err.errorCode, @"connectNewDeviceToService.getNewDeviceWiFiStatus");
            //now set last method completed to AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE to guarrantee confirm connection method could be called after this failure
            [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
            failureBlock(err);
        }
    };
    
    /**
     * Check if new device supports feature "ap-sta"
     */
    
    BOOL isApStaSupported = NO;
    
    /**
     * Key "Features" is added since device ver 1.4
     */
    if([AylaSetup newDevice].features != nil){
        for(NSString *feature in [AylaSetup newDevice].features) {
            if([feature isEqualToString:@"ap-sta"]) {
                isApStaSupported = YES;
                break;
            }
        }
    }
    
    if(isApStaSupported) {
        sleep(pollInterval);
        // start to poll network status
        [AylaSetup getNewDeviceWiFiStatus:_successBlock failure:^(AylaError *err) {
            if([AylaHost isNewDeviceConnected]) {
                [AylaSetup getNewDeviceWiFiStatus:_successBlock failure:_failureBlock];
            }
            else {
                [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
                failureBlock(err);
            }
        }];
    }
    else { // new ap-sta setup is not supported by current device.
        if(callParams) {
            
            AFHTTPRequestOperation *operation = [callParams objectForKey:@"operation"];
            NSDictionary *responseObject = [callParams objectForKey:@"response"];
            
            if(operation.response.statusCode == 200){
                AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                err.errorCode = AML_SETUP_CONNECTION_ERROR;
                NSString *description = [responseObject valueForKeyPath:@"msg"];
                NSDictionary *error = [[NSDictionary alloc] initWithObjectsAndKeys:description, @"msg", [responseObject valueForKeyPath:@"error"], @"error", nil];
                err.errorInfo = error;
                err.nativeErrorInfo = nil;
                subTaskFailed = AML_CONNECT_NEW_DEVICE_TO_SERVICE;
                saveToLog(@"%s, %s, %s:%s, %s", @"E", @"AylaSetup", @"Failed", description, @"AylaModule.setDeviceConnectToNetwork_handler");
                failureBlock(err);
            }
            else{
                [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
                [AylaSetup setConnectedMode:AML_CONNECTION_UNKNOWN];  //
                saveToLog(@"%@, %@, %@:%@, %@", @"I", @"AylaSetup", @"Success",@"OK", @"connectNewDeviceToService.postPath");
                AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                successBlock(resp);
            }
        }
        else { //encrypted
            [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE];
            [AylaSetup setConnectedMode:AML_CONNECTION_UNKNOWN];  //
            saveToLog(@"%@, %@, %@:%@, %@", @"I", @"AylaSetup", @"Success",@"OK", @"connectNewDeviceToService.postPath");
            AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = AML_ERROR_ASYNC_OK;
            successBlock(resp);
        }
        
    }


}


+ (void)connectNewDeviceToService:(NSString *)ssid
                         password:(NSString *)password
                         optionalParams:(NSDictionary *)callParams
                         isHidden:(Boolean)isHidden
                         success:(void (^)(AylaResponse *))successBlock
                         failure:(void (^)(AylaError *))failureBlock
{
    //currently step getNewDeviceScanForAPs can be skipped here
    if( [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_CONNECT_TO_NEW_DEVICE &&
       [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_GET_DEVICE_SCAN_FOR_APS )
    {
        saveToLog(@"%@, %@, %@:%@,%@:%d, %@", @"E", @"AylaModule", @"Failed", @"TaskOutOfOrder", @"lastMethodCompleted", [AylaSetup lastMethodCompleted],  @"connectNewDeviceToService");
        NSNumber *methodCompleted = [[NSNumber alloc] initWithInt:[AylaSetup lastMethodCompleted]];
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys: methodCompleted ,@"taskOutOfOrder", nil];
        AylaError *err = [AylaError new]; err.errorCode = AML_TASK_ORDER_ERROR;
        err.errorInfo = description; err.nativeErrorInfo = nil;
        err.httpStatusCode = 0;
        failureBlock(err);
        return;
    }
    
    if([AylaSetup hostNewDeviceSsid] == nil){
        AylaError *err = [AylaError new]; err.httpStatusCode = 0;
        err.errorCode = AML_NO_DEVICE_CONNECTED;
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:@"Invalid new device SSID.", @"error", nil];
        err.errorInfo = description;
        err.nativeErrorInfo = nil;
        failureBlock(err);
        return;
    }
    
    NSString *setupToken = [AylaSystemUtils randomToken: DEFAULT_SETUP_TOKEN_LEN];
    [AylaSetup setSetupToken:setupToken];
    NSString *path;
    
    NSString *ssidHtml = [AylaSystemUtils uriEscapedStringFromString:ssid];
    NSString *passwordHtml = [AylaSystemUtils uriEscapedStringFromString:password];

    if(password==nil || [password isEqualToString:@""])
        path = [[NSString alloc] initWithFormat:@"wifi_connect.json?ssid=%@&setup_token=%@",
                ssidHtml,
                setupToken];
    else
        path = [[NSString alloc] initWithFormat:@"wifi_connect.json?ssid=%@&key=%@&setup_token=%@",
                ssidHtml,
                passwordHtml,
                setupToken];
    // check optinal params
    if( callParams &&
       [callParams objectForKey:AML_SETUP_LOCATION_LONGTITUDE] &&
       [callParams objectForKey:AML_SETUP_LOCATION_LATITUDE]) {
        
        if([[callParams objectForKey:AML_SETUP_LOCATION_LATITUDE] isKindOfClass:[NSNumber class]] &&
           [[callParams objectForKey:AML_SETUP_LOCATION_LONGTITUDE] isKindOfClass:[NSNumber class]]) {
         
            double longtitude = [(NSNumber *)[callParams objectForKey:AML_SETUP_LOCATION_LONGTITUDE] doubleValue];
            double latitude = [(NSNumber *)[callParams objectForKey:AML_SETUP_LOCATION_LATITUDE] doubleValue];
            
            NSString *locInfo = [NSString stringWithFormat:@"location=%f,%f", latitude, longtitude];
            path = [NSString stringWithFormat:@"%@&%@", path, locInfo];
        }
        else {
            AylaError *err = [AylaError new]; err.httpStatusCode = 0;
            err.errorCode = AML_NO_DEVICE_CONNECTED;
            NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:@"Invalid location information.", @"error", nil];
            err.errorInfo = description;
            err.nativeErrorInfo = nil;
            failureBlock(err);
            return;
        }
    }
    
    
    if([AylaSetup securityType] != AylaSetupSecurityTypeNone) {
         int cmdId = [AylaLanMode nextCommandOutstandingId];
         __block int getResp = NO;
         NSString *jsonString = [AylaLanMode buildToDeviceCommand:@"POST" cmdId:cmdId resourse:path
                                                            data:nil
                                                              uri:@"local_lan/connect_status"];
         [AylaLanMode sendNotifyToLanModeDevice:cmdId baseType:AYLA_LAN_COMMAND jsonString:jsonString block:^(NSDictionary *resp) {
             getResp = YES;
             if([resp objectForKey:@"status"] && [[resp objectForKey:@"status"] isEqualToString:@"success"]){
                 [AylaModule connectNewDevcieToServiceContinued:nil
                                                        success:successBlock failure:failureBlock];
                 
             }
             else {
                 NSDictionary *respData = [resp objectForKey:@"error"];
                 AylaError *error = [AylaError new];
                 error.errorCode = AML_SETUP_DEVICE_ERROR;
                 error.nativeErrorInfo = nil;
                 subTaskFailed = AML_CONNECT_NEW_DEVICE_TO_SERVICE;
                 NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                 error.httpStatusCode = ((NSNumber *)[respData objectForKey:@"httpStatusCode"]).integerValue;
                 if([respData objectForKey:@"data"]){
                     NSDictionary *dict = [respData objectForKey:@"data"];
                     saveToLog(@"%@, %@, %@:%@, %@", @"E", @"DeviceSetup", @"Error msg", [dict objectForKey:@"msg"], @"connectNewDeviceToService");
                     NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", [dict objectForKey:@"error"], @"error", [dict objectForKey:@"msg"], @"msg", nil];
                     error.errorInfo = description;
                 }
                 else {
                     saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"httpStatusCode", error.httpStatusCode, @"connectNewDeviceToService");
                     error.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", @"Securing setup: failure response from module.", @"msg", [NSNumber numberWithInt:0], @"error", nil];
                 }
                 failureBlock(error);
             }
         }];
        double delayInSeconds = DEFAULT_SETUP_WIFI_HTTP_TIMEOUT;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if(!getResp) {
                [AylaLanMode clearQueue];
                AylaError *error = [AylaError new]; error.errorCode = AML_ERROR_REQUEST_TIME_OUT;
                error.nativeErrorInfo = nil;
                subTaskFailed = AML_CONNECT_NEW_DEVICE_TO_SERVICE;
                NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                error.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", @"request timed out.", @"msg", [NSNumber numberWithInt:AML_ERROR_REQUEST_TIME_OUT], @"error", nil];
                failureBlock(error);
            }
        });
    }
    else {
    
    NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"POST" path:path parameters:nil];
    [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
                 success:^(AFHTTPRequestOperation *operation, id responseObject){
                     [AylaModule connectNewDevcieToServiceContinued:[NSDictionary dictionaryWithObjectsAndKeys:operation, @"operation", responseObject, @"response", nil]
                                                            success:successBlock failure:failureBlock];
                 
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error){
                     saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"Failed", error.code, @"connectNewDeviceToService.postPath");
                     AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = error;
                     subTaskFailed = AML_CONNECT_NEW_DEVICE_TO_SERVICE;
                     NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                     NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                     err.errorInfo = description;
                     failureBlock(err);
                 }
     ];
    [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
        
    }
}

+ (void)confirmNewDeviceToServiceConnection:
                /*success:*/(void (^)(AylaResponse *response, NSDictionary *result))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
    if([AylaSetup lastMethodCompleted] != AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE &&
       [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_GET_NEW_DEVICE_WIFI_STATUS)
    {
        saveToLog(@"%@, %@, %@:%@,%@:%d, %@", @"E", @"AylaModule", @"Failed", @"TaskOutOfOrder", @"lastMethodCompleted", [AylaSetup lastMethodCompleted],  @"confirmNewDeviceToServiceConnection");
        NSNumber *methodCompleted = [[NSNumber alloc] initWithInt:[AylaSetup lastMethodCompleted]];
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys: methodCompleted ,@"taskOutOfOrder", nil];
        AylaError *err = [AylaError new]; err.errorCode = AML_TASK_ORDER_ERROR; err.httpStatusCode = 0;
        err.errorInfo = description; err.nativeErrorInfo = nil;
        failureBlock(err);
        return;
    }
    
    void (^_successBlock)(AylaResponse *, NSDictionary *) = ^(AylaResponse *resp, NSDictionary *responce) {
        
        NSDictionary *info = [responce objectForKey:@"device"];
        
        [AylaSetup setLanIp:[info valueForKeyPath:@"lan_ip"]];
        [AylaSetup setConnectedMode:AML_CONNECTED_TO_SERVICE];
        
        AylaDevice *device = [AylaDevice new];
        [device setSetupToken: [AylaSetup setupToken]];
        [device setRegistrationType:[info valueForKeyPath:@"registration_type"]];
        [device setLanIp:[AylaSetup lanIp]];
        [device setDsn: [[AylaSetup newDevice] dsn]];
        
        NSString *connectedAt = [info valueForKeyPath:@"connected_at"];
        [device setConnectedAt:connectedAt];
        saveToLog(@"%@, %@, %@:%@, %@:%@, %@", @"I", @"AylaSetup", @"connectedAt", connectedAt , @"lanIp", [AylaSetup lanIp],  @"AylaModule.confirmNewDeviceToServiceConnection.getPath");
        
        // Save New Device to cache
        [AylaCache save:AML_CACHE_SETUP withObject:[NSArray arrayWithObjects:device, nil]];
        
        NSDictionary *result = [[NSDictionary alloc] initWithObjectsAndKeys:@"success", @"success", device, @"device", nil];
        
        [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONFIRM_NEW_DEVICE_TO_SERVICE_CONNECTION];
        [AylaReachability setConnectivity:AML_REACHABILITY_REACHABLE];
        
        if(![AylaSetup inExit]){
            successBlock(resp, result);
        }
    }; 
    
    void (^__block __weak __failureBlock)(AFHTTPRequestOperation *, AylaError *);
    void (^__block _failureBlock)(AFHTTPRequestOperation *, AylaError *);
    __failureBlock = _failureBlock = ^(AFHTTPRequestOperation *operation, AylaError *err){
        if([AylaSetup inExit]){
            return;
        }
        if(err.errorCode == 1){
            NSError * nserr = err.nativeErrorInfo;
            
            int tries = AylaSetup.newDeviceToServiceConnectionRetries;
            int noInternetTries = AylaSetup.newDeviceToServiceNoInternetConnectionRetries;
            
            if(tries < [[AylaSystemUtils newDeviceToServiceConnectionRetries] integerValue] &&
               noInternetTries < DEFAULT_NEW_DEVICE_TO_SERVICE_NO_INTERNET_CONNECTION_RETRIES &&
               (nserr.code == NSURLErrorNotConnectedToInternet||
                nserr.code == NSURLErrorCannotFindHost||
                nserr.code == NSURLErrorCannotConnectToHost||
                operation.response.statusCode == 404)){
                   sleep(2);
                   
                   if(nserr.code == NSURLErrorNotConnectedToInternet){
                       [AylaSetup setNewDeviceToServiceNoInternetConnectionRetries:noInternetTries+1];
                   }
                   else{
                       if (nserr.code == NSURLErrorCannotConnectToHost) {
                           [AylaSetup setNewDeviceToServiceConnectionRetries:tries+2];
                       }
                       else
                           [AylaSetup setNewDeviceToServiceConnectionRetries:tries+1];
                   }
                   
                   saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d, %@", @"I", @"AylaSetup",
                             @"error", nserr.code, @"tries", tries, @"noInternetTries", noInternetTries,
                             @"AylaModule.confirmNewDeviceToServiceConnection.failureBlock.Retry");
                   [AylaDevice getNewDeviceConnected:AylaSetup.newDevice.dsn setupToken:AylaSetup.setupToken
                                             success:_successBlock
                                             failure:__failureBlock];
               }
            else{
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaSetup", @"nativeError", nserr.code, @"AylaModule.confirmNewDeviceToServiceConnection");
                failureBlock(err);
            }            
        }
        else{
            saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaSetup", @"AylaError", err.errorCode, @"AylaModule.confirmNewDeviceToServiceConnection");
            failureBlock(err);
        }
    };
    
    void (^__block confirmBlock)(void) = ^(void){
        double delayInSeconds = DEFAULT_SETUP_STATUS_POLLING_INTERVAL;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [AylaDevice getNewDeviceConnected:AylaSetup.newDevice.dsn
                                   setupToken:AylaSetup.setupToken
                                      success:_successBlock
                                      failure:_failureBlock];
        });
    };
    
    /**
     * Disconnect new device
     */
    if ([AylaHost isNewDeviceConnected]) {
        [AylaModule disconnectNewDevice:nil success:^(AylaResponse *response){
            
            [[AylaSetup newDevice] lanModeDisable];
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [AylaDevice getNewDeviceConnected:AylaSetup.newDevice.dsn
                                       setupToken:AylaSetup.setupToken
                                          success:_successBlock
                                          failure:_failureBlock];
                [AylaSecurity refreshSessionKeyPair];
            });
        } failure:^(AylaError *err) {
            if(err.httpStatusCode == 403) { // Disconnect request is refused by module
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"DeviceSetup", @"Failed", err.errorCode, @"connectNewDeviceToService.disconnectNewDevice");                subTaskFailed = AML_DISCONNECT_NEW_DEVICE;
                NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                err.errorInfo = description;
                failureBlock(err);
            }
            else {
                if ([AylaHost isNewDeviceConnected]) {
                    AylaError *err = [AylaError new]; err.errorCode = AML_AYLA_ERROR_FAIL; err.httpStatusCode = 0; err.nativeErrorInfo = nil;
                    subTaskFailed = AML_DISCONNECT_NEW_DEVICE;
                    NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                    NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                    err.errorInfo = description;
                    failureBlock(err);
                }
                else {
                    [[AylaSetup newDevice] lanModeDisable];
                    confirmBlock();
                }
            }
        }];
    }
    else {
        confirmBlock();
    }
       
}

+ (void)getNewDeviceWiFiStatus:
        /*success:*/(void (^)(AylaResponse *response, AylaWiFiStatus *result))successBlock
        failure:(void (^)(AylaError *err))failureBlock
{    
    if( [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_CONNECT_TO_NEW_DEVICE &&
       [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_CONNECT_NEW_DEVICE_TO_SERVICE &&
       [AylaSetup lastMethodCompleted] != AML_SETUP_TASK_GET_DEVICE_SCAN_FOR_APS )
    {
        saveToLog(@"%@, %@, %@:%@,%@:%d, %@", @"E", @"AylaModule", @"Failed", @"TaskOutOfOrder", @"lastMethodCompleted", [AylaSetup lastMethodCompleted],  @"getNewDeviceWiFiStatus");
        NSNumber *methodCompleted = [[NSNumber alloc] initWithInt:[AylaSetup lastMethodCompleted]];
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys: methodCompleted ,@"taskOutOfOrder", nil];
        AylaError *err = [AylaError new]; err.errorCode = AML_TASK_ORDER_ERROR; err.httpStatusCode = 0;
        err.errorInfo = description; err.nativeErrorInfo = nil;
        failureBlock(err);
        return;
    }
    
    if([AylaSetup hostNewDeviceSsid] == nil){
        AylaError *err = [AylaError new];
        err.errorCode = AML_NO_DEVICE_CONNECTED;
        err.httpStatusCode = 0;
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:@"Invalid new device SSID.", @"error", nil];
        err.errorInfo = description;
        err.nativeErrorInfo = nil;
        
        failureBlock(err);
        return;
    }
    
    NSMutableURLRequest *request = [[AylaApiClient sharedNewDeviceInstance] requestWithMethod:@"GET" path:@"wifi_status.json" parameters:nil];
    [request setTimeoutInterval:DEFAULT_SETUP_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedNewDeviceInstance] HTTPRequestOperationWithRequest:request
                 success:^(AFHTTPRequestOperation *operation, id responseObject){
                     NSDictionary *dict = [responseObject valueForKey:@"wifi_status"];
                     AylaWiFiStatus *wifiStatus = [[AylaWiFiStatus alloc] initWiFiStatusWithDictionary:dict];
                     saveToLog(@"%@, %@, %@:%@, %@:%d, %@", @"I", @"AylaSetup", @"mac", wifiStatus.mac, @"wifiConnectHistories", wifiStatus.connectHistory.count, @"getNewDeviceWiFiStatus");
                     // Currently do not update lastMethodCompleted here
                     // [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_GET_NEW_DEVICE_WIFI_STATUS];
                     [AylaModule appendToLogService:dict];
                     AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                     successBlock(resp, wifiStatus);
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error){
                     saveToLog(@"%@, %@, %@, %@", @"E", @"DeviceSetup", @"Failed", @"getNewDeviceWiFiStatus.getPath");
                     AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode;
                     err.nativeErrorInfo = error;
                     subTaskFailed = AML_GET_NEW_DEVICE_WIFI_STATUS;
                     NSNumber *taskFailed = [[NSNumber alloc] initWithInt:subTaskFailed];
                     NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:taskFailed, @"subTaskFailed", nil];
                     err.errorInfo = description;
                     failureBlock(err);
                 }
    ];
    [[AylaApiClient sharedNewDeviceInstance] enqueueHTTPRequestOperation:operation];
}


+ (void)appendToLogService:(NSDictionary *)wifiStatusDict
{
    //Skip invalid wifi status
    if(!wifiStatusDict) {
        return;
    }
    
    NSMutableDictionary *aLog = [NSMutableDictionary new];
    [aLog setObject:[wifiStatusDict objectForKey:@"dsn"] forKey:@"dsn"];
    [aLog setObject:@"warning" forKey:@"level"];
    [aLog setObject:@"AylaSetup.AylaModule.getNewDeviceWiFiStatus" forKey:@"mod"];
    
    NSUInteger timeInSeconds= [[NSDate date] timeIntervalSince1970];
    NSNumber *time  = [NSNumber numberWithInteger: timeInSeconds];
    [aLog setObject:time forKey:@"time"];
    
    NSError *error;
    NSData *logInJson = [NSJSONSerialization dataWithJSONObject:wifiStatusDict
                                                       options:0
                                                         error:&error];
    NSString *logString = nil;
    if (!logInJson) {
        saveToLog(@"%@, %@, %@, %@", @"E", @"DeviceSetup", @"FailedToGenerateLogInJson", @"appendToLogService");
        logString = @"Failed to generate a valid Json String - appendToLogService.";
    }
    else
        logString = [[NSString alloc] initWithData:logInJson encoding:NSUTF8StringEncoding];
    
    [aLog setObject:logString forKey:@"text" ];
    [AylaLogService sendLogServiceMessage:aLog withDelay:YES];
}


@end

//
//  AylaDevice.m
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 5/30/12.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaDeviceSupport.h"
#import "AylaSystemUtilsSupport.h"
#import "AylaLanModeSupport.h"
#import "AylaNotify.h"
#import "AylaTimer.h"
#import "AylaReachabilitySupport.h"
#import "AylaScheduleSupport.h"
#import "AylaRegistration.h"
#import "AylaResponse.h"
#import "AylaError.h"
#import "AylaErrorSupport.h"
#import "AylaCacheSupport.h"
#import "AylaSecuritySupport.h"
#import "AylaDeviceNotificationSupport.h"
#import "AylaGrantSupport.h"
#import "AylaDeviceGateway.h"
#import "AylaDeviceGatewaySupport.h"
#import "AylaDeviceNode.h"
#import "NSObject+AylaNetworks.h"

@implementation AylaLanModeConfig : NSObject
@synthesize lanipKey = _lanipKey;
@synthesize lanipKeyId = _lanipKeyId;
@synthesize keepAlive = _keepAlive;
@synthesize status = _status;

- (id)initAylaLanModeConfigWithDictionary:(NSDictionary *)dictionary
{    
    self = [super init];
    if(self){
        _lanipKey= [dictionary valueForKeyPath:@"lanip_key"];
        _lanipKeyId = [dictionary valueForKeyPath:@"lanip_key_id"];
        _keepAlive = [dictionary valueForKeyPath:@"keep_alive"];
        _status = [dictionary valueForKeyPath:@"status"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_lanipKey forKey:@"lanipKey"];
    [encoder encodeObject:_lanipKeyId forKey:@"lanipKeyId"];
    [encoder encodeObject:_keepAlive forKey:@"keepAlive"];
    [encoder encodeObject:_status forKey:@"status"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    _lanipKey = [decoder decodeObjectForKey:@"lanipKey"];
    _lanipKeyId = [decoder decodeObjectForKey:@"lanipKeyId"];
    _keepAlive = [decoder decodeObjectForKey:@"keepAlive"];
    _status = [decoder decodeObjectForKey:@"status"];
    return self;
}
@end;


@interface AylaDevice ()
@property (nonatomic, retain) AylaLanModeConfig *lanModeConfig;
@property (nonatomic, copy) NSMutableArray *features;
@property (nonatomic, copy) NSNumber *key;         //Device key number
@property (nonatomic, strong, readwrite) NSString *connectionStatus;
@end

@implementation AylaDevice

// API Client Instance method
// Device methods
@synthesize productName = _productName;
@synthesize model = _model;
@synthesize dsn = _dsn;
@synthesize oemModel = _oemModel;
@synthesize connectedAt = _connectedAt;
@synthesize key= _key;
@synthesize mac = _mac;
@synthesize lanIp = _lanIp;
@synthesize features = _features;

@synthesize hasProperties = _hasProperties;
@synthesize ip = _ip;
@synthesize swVersion = _swVersion;
@synthesize productClass = _productClass;
@synthesize lanEnabled = _lanEnabled;

@synthesize connectionStatus = _connectionStatus;
@synthesize templateId = _templateId;
@synthesize lat = _lat;
@synthesize lng = _lng;
@synthesize userId = _userId;
@synthesize moduleUpdatedAt = _moduleUpdatedAt;

@synthesize retrievedAt = _retrievedAt;
@synthesize properties = _properties;
@synthesize property = _property;
@synthesize lanModeConfig = _lanModeConfig;

@synthesize schedules = _schedules;
@synthesize schedule = _schedule;

@synthesize registrationType = _registrationType;
@synthesize registrationToken = _registrationToken;
@synthesize setupToken = _setupToken;

- (NSString *)description
{
  return [NSString stringWithFormat:@"\n" 
          "productName: %@\n"
          "model: %@\n"
          "dsn: %@\n"
          "oemModel: %@\n"
          "connectedAt: %@\n"
          "mac: %@\n"
          "lanIp: %@\n"
          "retrievedAt: %@\n"
          , _productName, _model, _dsn, _oemModel, _connectedAt, _mac, _lanIp,
          _retrievedAt];
}

// ---------------------- Retrieve Devices ---------------------------

static NSString * const kAylaDeviceType = @"device_type";
static NSString * const kAylaGatewayType = @"gateway_type";
static NSString * const kAylaNodeType = @"node_type";

+ (NSOperation *)getDevices: (NSDictionary *)callParams
                success:(void (^)(AylaResponse *response, NSArray *devices))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
    NSString *path = [NSString stringWithFormat:@"%@", @"devices.json"];
    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"path", path, @"getDevices");
  
    if([AylaReachability isInternetReachable]){
        
        // [AylaReachability determineServiceReachabilityWithBlock:^(int reachable){
        NSMutableArray *devices = [AylaCache get:AML_CACHE_DEVICE];
        
        if( [AylaCache cachingEnabled:AML_CACHE_DEVICE] &&
            [AylaSystemUtils slowConnection].boolValue &&
            devices!=nil && [devices count]>0){
            // Read from cache
            [AylaDevice setupDevices:devices];
            AylaResponse *resp = [AylaResponse new];
            resp.httpStatusCode = AML_ERROR_ASYNC_OK_NON_AUTH_INFO;
            successBlock(resp, devices);
        }
        else{
            if([AylaReachability getConnectivity] == AML_REACHABILITY_REACHABLE){
               return [[AylaApiClient sharedDeviceServiceInstance] getPath:path
                      parameters:nil
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             int i = 0;
                             NSMutableArray *devices = [NSMutableArray array];
                             NSMutableArray *gateways = [NSMutableArray array];
                             for (NSDictionary *deviceDictionary in responseObject) {
                                 AylaDevice *device = [AylaDevice deviceFromDeviceDictionary:deviceDictionary];
                                 saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"key", device.key, @"getDevices.getPath");
                                 [devices addObject:device];
                                 if([device isKindOfClass:[AylaDeviceGateway class]]) {
                                     [gateways addObject:device];
                                 }
                                 i++;
                             }
                             saveToLog(@"%@, %@, %@:%d, %@", @"I", @"Devices", @"i", i, @"getDevices.getPath");
                             
                             //nodes update for gateways
                             for(AylaDeviceGateway *gateway in gateways) {
                                 [gateway updateNodesFromGlobalDeviceList:devices];
                             }
                             
                             // Save to cache
                             [AylaCache save:AML_CACHE_DEVICE withObject:devices];
                             
                             AylaResponse *resp = [AylaResponse new];
                             resp.httpStatusCode = operation.response.statusCode;
                             successBlock(resp, devices);
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Devices", @"NSError.code", error.code, @"getDevices.getPath", [AylaSystemUtils shortErrorFromError:error]);
                             
                             AylaError *err = [AylaError new];
                             err.errorCode = AML_ERROR_FAIL; err.nativeErrorInfo = error;
                             err.httpStatusCode = operation.response.statusCode; err.errorInfo = nil;
                             failureBlock(err);
                         }];
            }
            else{
                // When service is not reachable
                saveToLog(@"%@, %@, %@:%d, %@", @"I", @"Devices", @"serviceReachability", [AylaReachability getConnectivity], @"getDevices.getPath");
                [AylaReachability determineServiceReachabilityWithBlock:nil];

                if([AylaCache cachingEnabled:AML_CACHE_DEVICE]) {
                    saveToLog(@"%@, %@, %@:%ld, %@", @"I", @"Devices", @"203", devices.count, @"getDevices.getPath");
                    AylaResponse *resp = [AylaResponse new];
                    resp.httpStatusCode = AML_ERROR_ASYNC_OK_NON_AUTH_INFO;
                    successBlock(resp, devices?:[NSMutableArray array]);
                }
                else {
                    AylaError *err = [AylaError new];
                    err.errorCode = AML_ERROR_NO_CONNECTIVITY; err.nativeErrorInfo = nil;
                    err.httpStatusCode = 0; err.errorInfo = nil;
                    failureBlock(err);
                }
            }
        }
    }
    else{
        saveToLog(@"%@, %@, %@:%d, %@", @"E", @"Devices", @"internetReachability", [AylaReachability isInternetReachable], @"getDevices.getPath");
        AylaError *err = [AylaError new];
        err.errorCode = AML_ERROR_NO_CONNECTIVITY; err.nativeErrorInfo = nil;
        err.httpStatusCode = 0; err.errorInfo = nil;
        failureBlock(err);
    }
    return nil;
}

/*
 * Setup devices(wifi/gatetway/node) based on device list
 */
+ (void)setupDevices:(NSArray *)devices {
    for(AylaDevice *device in devices) {
        if([device isKindOfClass:[AylaDeviceGateway class]]) {
            AylaDeviceGateway *gateway = (AylaDeviceGateway *)device;
            [gateway updateNodesFromGlobalDeviceList:devices];
        }
    }
}

+ (instancetype)deviceFromDeviceDictionary:(NSDictionary *)deviceDictionary
{
    NSDictionary *deviceAttributes = [deviceDictionary objectForKey:@"device"];
    NSString *deviceType = [deviceAttributes objectForKey:kAylaDeviceType];
    Class deviceClass = [AylaDevice deviceClassFromDeviceType:deviceType andDeviceDictionary:deviceDictionary];
    return [[deviceClass alloc] initDeviceWithDictionary:deviceDictionary];
}

+ (Class) deviceClassFromDeviceDictionary:(NSDictionary *)dictionary
{
    return [AylaDevice class];
}

- (id)initDeviceWithDictionary:(NSDictionary *)deviceDictionary
{
  self = [super init];
  if (self) {
    if ([deviceDictionary objectForKey:@"device"]) {
        [self updateWithDictionary:deviceDictionary];
    } else {
      saveToLog(@"%@, %@, %@:%@, %@", @"E", @"Devices", @"device", @"nil", @"getDevices.initDevicesWithDictionary");
    }
  }
  return self;
}

- (void)updateWithDictionary:(NSDictionary *)deviceDictionary
{
    NSDictionary * device = [deviceDictionary objectForKey:@"device"];
    self.productName = ([device valueForKeyPath:@"product_name"] != [NSNull null]) ? [device valueForKeyPath:@"product_name"] : @"";
    self.model = ([device valueForKeyPath:@"model"] != [NSNull null]) ? [device valueForKeyPath:@"model"] : @"";
    self.dsn = ([device valueForKeyPath:@"dsn"] != [NSNull null]) ? [device valueForKeyPath:@"dsn"] : @"";
    self.oemModel = ([device valueForKeyPath:@"oem_model"] != [NSNull null]) ? [device valueForKeyPath:@"oem_model"] : @"";
    self.deviceType = ([device valueForKeyPath:kAylaDeviceType] != [NSNull null])? [device valueForKeyPath:kAylaDeviceType]: nil;
    self.connectedAt = ([device valueForKeyPath:@"connected_at"] != [NSNull null]) ? [device valueForKeyPath:@"connected_at"] : @"";
    self.key = [device valueForKeyPath:@"key"];
    self.mac = ([device valueForKeyPath:@"mac"] != [NSNull null]) ? [device valueForKeyPath:@"mac"] : @"";
    self.lanIp = ([device valueForKeyPath:@"lan_ip"] != [NSNull null]) ? [device valueForKeyPath:@"lan_ip"] : @"";
    self.retrievedAt = [NSDate date];
    self.features = ([device valueForKeyPath:@"features"] != [NSNull null]) ? [device valueForKeyPath:@"features"] : nil;
    
    self.ip = ([device valueForKeyPath:@"ip"] != [NSNull null]) ? [device valueForKeyPath:@"ip"] : @"";
    self.hasProperties = ([device valueForKeyPath:@"has_properties"] != [NSNull null]) ? [device valueForKeyPath:@"has_properties"] : nil;
    self.productClass = ([device valueForKeyPath:@"product_class"] != [NSNull null]) ? [device valueForKeyPath:@"product_class"] : @"";
    self.ssid = ([device valueForKeyPath:@"ssid"] != [NSNull null]) ? [device valueForKeyPath:@"ssid"] : @"";
    self.swVersion = ([device valueForKeyPath:@"sw_version"] != [NSNull null]) ? [device valueForKeyPath:@"sw_version"] : @"";
    self.lanEnabled = ([device valueForKeyPath:@"lan_enabled"] != [NSNull null]) ? [device valueForKeyPath:@"lan_enabled"] : nil;
    
    _connectionStatus = [device valueForKey:@"connection_status"] && ([device valueForKey:@"connection_status"] != [NSNull null])? [device valueForKey:@"connection_status"]:@"";
    _templateId = [device valueForKey:@"template_id"] && ([device valueForKey:@"template_id"] != [NSNull null])? [device valueForKey:@"template_id"]:nil;
    _lat = [device valueForKey:@"lat"] && ([device valueForKey:@"lat"] != [NSNull null])? [device valueForKey:@"lat"]:@"";
    _lng = [device valueForKey:@"lng"] && ([device valueForKey:@"lng"] != [NSNull null])? [device valueForKey:@"lng"]:@"";
    _userId = [device valueForKey:@"user_id"] && ([device valueForKey:@"user_id"] !=[NSNull null])? [device valueForKey:@"user_id"]:nil;
    _moduleUpdatedAt = [device valueForKey:@"module_updated_at"] && ([device valueForKey:@"module_updated_at"] != [NSNull null])?[device valueForKey:@"module_updated_at"]: @"";
    
    if([device objectForKey:@"grant"]) {
        self.grant = [[AylaGrant alloc] initWithDictionary:device[@"grant"]];
    }
}


// --------------------------- Retrive Device ----------------------------
//
// Used to update/refresh a device object from the Ayla Cloud Service
//

- (NSOperation *)getDeviceDetail:(NSDictionary *)callParams
        success:(void (^)(AylaResponse *reponse, AylaDevice *deviceUpdated))successBlock
        failure:(void (^)(AylaError *err))failureBlock
{
  NSString *path = [NSString stringWithFormat:@"%@%@%@", @"devices/", self.key, @".json"];
  saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"path", path, @"getDeviceDetail");
  return [[AylaApiClient sharedDeviceServiceInstance] getPath:path
     parameters:nil
        success:^(AFHTTPRequestOperation *operation, id responseObject) {
          AylaDevice *updatedDevice = [[AylaDevice alloc] initDeviceWithDictionary:responseObject];
          saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"key", updatedDevice.key, @"getDeviceDetail.getPath");
          
          AylaResponse *resp = [AylaResponse new];
          resp.httpStatusCode = AML_ERROR_ASYNC_OK;
          successBlock(resp, updatedDevice);
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Devices", @"NSError.code", error.code, @"getDeviceDetail.getPath", [AylaSystemUtils shortErrorFromError:error]);
          AylaError *err = [AylaError new]; err.errorCode = AML_ERROR_FAIL; err.nativeErrorInfo = error;
          err.httpStatusCode = operation.response.statusCode; err.errorInfo = nil;
          failureBlock(err);
        }];
}


- (NSOperation *)update:(NSDictionary *)callParams
        success:(void (^)(AylaResponse *response, AylaDevice *deviceUpdated))successBlock
        failure:(void (^)(AylaError *err))failureBlock
{
    if (gblAuthToken == nil) {
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN;
        err.nativeErrorInfo = nil; err.errorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    
    if (callParams==nil || [callParams objectForKey:@"product_name"] == nil) {
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.nativeErrorInfo = nil; err.errorInfo = nil; err.httpStatusCode = 0;
        NSDictionary *resp = [[NSDictionary alloc] initWithObjectsAndKeys:@"only support to change product_name", @"base", nil];
        err.errorInfo = resp;
        failureBlock(err);
        return nil;
    }
    
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:[[callParams objectForKey:@"product_name"] copy], @"product_name", nil];
    NSDictionary *sendParam = [[NSDictionary alloc] initWithObjectsAndKeys:params, @"device", nil];
    
    NSString *path = [NSString stringWithFormat:@"%@%@%@", @"devices/", self.key, @".json"];
    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"path", path, @"update");
    return [[AylaApiClient sharedDeviceServiceInstance] putPath:path
              parameters:sendParam
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"key", self.key, @"update.putPath");
                     [self setProductName:[params objectForKey:@"product_name"]];
                     NSMutableArray *devices = [AylaCache get:AML_CACHE_DEVICE];

                     if (devices != nil) {
                         for (AylaDevice *dev in devices) {
                             if([dev.dsn isEqualToString:self.dsn]){
                                 dev.productName = self.productName;
                                 break;
                             }
                         }
                         [AylaCache save:AML_CACHE_DEVICE withObject:devices];
                     }
                     
                     AylaResponse *response = [AylaResponse new];
                     response.httpStatusCode = operation.response.statusCode;
                     successBlock(response, self);
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     
                     saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Devices", @"NSError.code", error.code, @"getDeviceDetail.getPath", [AylaSystemUtils shortErrorFromError:error]);
                     
                     AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
                     err.httpStatusCode = operation.response.statusCode; err.errorInfo = nil;
                     failureBlock(err);
                 }];
}



- (NSOperation *)getProperties:(NSDictionary *)callParams
    success:(void (^)(AylaResponse *response, NSArray *properties))successBlock
    failure:(void (^)(AylaError *err))failureBlock
{
  return [AylaProperty getProperties:self callParams:callParams
       success:^(AylaResponse *response, NSArray *properties) {
         if(_properties == nil)
            _properties = [[NSMutableDictionary alloc] init];
         for(AylaProperty *property in properties){
            [_properties setValue:property forKey:property.name];
         }
         successBlock(response, properties);
       }
       failure:^(AylaError *err) {
         failureBlock(err);
       }
   ];
}

// --------------------------- Register/Unregister Pass Through ------------------------

+ (void)registerNewDevice:(AylaDevice *)device
      success:(void (^)(AylaResponse *response, AylaDevice *registeredDevice))successBlock
      failure:(void (^)(AylaError *err))failureBlock
{
  [AylaRegistration registerNewDevice:device
    success:^(AylaResponse *response, AylaDevice *registeredDevice) {
      [AylaCache clearAll];
      successBlock(response, registeredDevice);
    }
    failure:^(AylaError *err) {
      failureBlock(err);
    }
  ];
}
  
- (NSOperation *)unregisterDevice:(NSDictionary *)callParams
      success:(void (^)(AylaResponse *response))successBlock
      failure:(void (^)(AylaError *))failureBlock
{
  return [AylaRegistration unregisterDevice:self callParams:callParams
    success:^(AylaResponse *response) {
      successBlock(response);
    }
    failure:^(AylaError *err) {
      failureBlock(err);
    }
  ];
}

//------------------------------------ Reset -------------------------------------

- (NSOperation *)factoryReset:(NSDictionary *)callParams
                      success:(void (^)(AylaResponse *response))successBlock
                      failure:(void (^)(AylaError *err))failureBlock
{
    if(!self.key) {
        NSDictionary *errors = @{@"device": @"is invalid."};
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:errors];
        failureBlock(error);
        return nil;
    }
    NSString *path = [NSString stringWithFormat:@"%@%@%@", @"devices/", self.key, @"/cmds/factory_reset.json"];
    return
    [[AylaApiClient sharedDeviceServiceInstance] putPath:path
        parameters:nil
        success:^(AFHTTPRequestOperation *operation, id responseObject) {
            saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"dsn", self.dsn, @"factoryReset");
            AylaResponse *response = [AylaResponse new];
            [AylaCache clear:AML_CACHE_PROPERTY withParams:@{kAylaCacheParamDeviceDsn: self.dsn}];
            response.httpStatusCode = operation.response.statusCode;
            successBlock(response);
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"Devices", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"factoryReset");
            failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
        }];
}

//---------------------------------Schedules------------------------------------

- (void)createSchedule:(AylaSchedule *)schedule
               success:(void (^)(AylaResponse *response, AylaSchedule *createdSchedule))successBlock
               failure:(void (^)(AylaError *err))failureBlock
{
    [schedule createWithDevice:self Name:schedule.name andActions:schedule.scheduleActions
        success:^(AylaResponse *response, AylaSchedule *newSchedule) {
            successBlock(response, newSchedule);
        } failure:^(AylaError *err) {
            failureBlock(err);
        }];
}

- (NSOperation *)getAllSchedules:(NSDictionary *)callParams
                success:(void (^)(AylaResponse *response, NSArray *schedules))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
    if(_schedule == nil) {
        _schedule = [[AylaSchedule alloc] init];
    }
    
    return [_schedule getAll:self success:^(AylaResponse *response, NSArray *schedules) {
            successBlock(response, schedules);
        } failure:^(AylaError *err) {
            failureBlock(err);
        }];
}

- (NSOperation *)getScheduleByName:(NSString *)scheduleName
                  success:(void (^)(AylaResponse *response, AylaSchedule *schedule))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    if(_schedule == nil) {
        _schedule = [[AylaSchedule alloc] init];
    }
    
    return [_schedule getByName:scheduleName device:self success:^(AylaResponse *response, AylaSchedule *schedule) {
            successBlock(response, schedule);
        } failure:^(AylaError *err) {
            failureBlock(err);
        }];

}

- (NSOperation *)updateSchedule:(AylaSchedule *)schedule
                  success:(void (^)(AylaResponse *response, AylaSchedule *schedule))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    if(_schedule == nil) {
        _schedule = [[AylaSchedule alloc] init];
    }
    return [schedule update:self success:^(AylaResponse *response, AylaSchedule *updatedSchedule) {
            successBlock(response, updatedSchedule);
        } failure:^(AylaError *err) {
            failureBlock(err);
        }];
}

- (NSOperation *)clearSchedule:(AylaSchedule *)schedule
              success:(void (^)(AylaResponse *response, AylaSchedule *schedule))successBlock
              failure:(void (^)(AylaError *err))failureBlock
{
    return [schedule clear:nil success:^(AylaResponse *response, AylaSchedule *clearedSchedule) {
            successBlock(response, clearedSchedule);
        } failure:^(AylaError *err) {
            failureBlock(err);
        }];
}


//------------------------------- Device Notification ----------------------------------------

- (NSOperation *)getNotifications:(NSDictionary *)params
                        success:(void (^)(AylaResponse *response, NSMutableArray *deviceNotifications))successBlock
                        failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDeviceNotification getNotificationsWithDevice:self params:params success:successBlock failure:failureBlock];
}

- (NSOperation *)createNotification:(AylaDeviceNotification *)deviceNotification
                                  success:(void (^)(AylaResponse *response, AylaDeviceNotification *createdDeviceNotification))successBlock
                                  failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDeviceNotification createNotification:deviceNotification withDevice:self success:successBlock failure:failureBlock];
}

- (NSOperation *)updateNotification:(AylaDeviceNotification *)deviceNotification
                                  success:(void (^)(AylaResponse *response, AylaDeviceNotification *updatedDeviceNotification))successBlock
                                  failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDeviceNotification updateNotification:deviceNotification success:successBlock failure:failureBlock];
}

- (NSOperation *)destroyNotification:(AylaDeviceNotification *)deviceNotification
                                   success:(void (^)(AylaResponse *response))successBlock
                                   failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDeviceNotification destroyNotification:deviceNotification withDevice:self success:successBlock failure:failureBlock];
}

//------------------------------- Time Zone Support ----------------------------------------

- (NSOperation *) getTimeZoneLocation:(NSDictionary *)callParams
                     success:(void (^)(AylaResponse *response, AylaTimeZone *timeZone))successBlock
                     failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaTimeZone getTimeZoneWithDevice:self
            success:^(AylaResponse *response, AylaTimeZone *devTimeZone) {
                successBlock(response, devTimeZone);
            } failure:^(AylaError *err) {
                failureBlock(err);
            }];
}


- (NSOperation *) updateTimeZoneLocation:(AylaTimeZone *)timeZone
                        success:(void (^)(AylaResponse *response, AylaTimeZone *updatedTimeZone))successBlock
                        failure:(void (^)(AylaError *err))failureBlock
{
    return [timeZone updateTimeZoneWithDevice:self
        success:^(AylaResponse *response, AylaTimeZone *updatedTimeZone) {
            successBlock(response, updatedTimeZone);
        } failure:^(AylaError *err) {
            failureBlock(err);
        }];
}



//--------------------------------- Datum -----------------------------------
- (NSOperation *) createDatum:(AylaDatum *)datum
                      success:(void (^)(AylaResponse *response, AylaDatum *newDatum))successBlock
                      failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDatum createWithObject:self andDatum:datum success:successBlock failure:failureBlock];
}

- (NSOperation *)getDatumWithKey:(NSString *)key
                         success:(void (^)(AylaResponse *response, AylaDatum *datum))successBlock
                         failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum getWithObject:self andKey:key success:successBlock failure:failureBlock];
}

- (NSOperation *) updateDatum:(AylaDatum *)datum
                      success:(void (^)(AylaResponse *response, AylaDatum *updatedDatum))successBlock
                      failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDatum updateWithObject:self andDatum:datum success:successBlock failure:failureBlock];
}

- (NSOperation *)deleteDatum:(AylaDatum *)datum
                     success:(void (^)(AylaResponse *response))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum deleteWithObject:self andDatum:datum success:successBlock failure:failureBlock];
}


//--------------------------------- Share -----------------------------------

static const NSString *shareAttrResourceId = @"resource_id";
static const NSString *shareAttrResourceName = @"resource_name";
- (NSOperation *)createShare:(AylaShare *)share
                     success:(void (^)(AylaResponse *resp, AylaShare *share))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaShare create:share object:self success:successBlock failure:failureBlock];
}

- (NSOperation *)getSharesWithSuccess:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                   failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare get:self callParams:nil success:successBlock failure:failureBlock];
}

+ (NSOperation *)getAllSharesWithSuccess:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                                 failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare get:nil callParams:@{kAylaShareParamResourceName: kAylaShareResourceNameDevice} success:successBlock failure:failureBlock];
}

- (NSOperation *)getReceivedSharesWithSuccess:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                                      failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare getReceives:self callParams:nil success:successBlock failure:failureBlock];
}

+ (NSOperation *)getAllReceivedSharesWithSuccess:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                                 failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare getReceives:nil callParams:@{kAylaShareParamResourceName: kAylaShareResourceNameDevice} success:successBlock failure:failureBlock];
}

- (NSOperation *)getShareWithId:(NSString *)id
                    success:(void (^)(AylaResponse *resp, AylaShare *share))successBlock
                    failure:(void (^)(AylaError *error))failureBlock;
{
    return [AylaShare getWithId:id success:successBlock failure:failureBlock];
}

- (NSOperation *)updateShare:(AylaShare *)share
                     success:(void (^)(AylaResponse *resp, AylaShare *updatedShare))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaShare update:share success:successBlock failure:failureBlock];
}

- (NSOperation *)deleteShare:(AylaShare *)share
                     success:(void (^)(AylaResponse *response)) successBlock
                     failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare delete:share success:successBlock failure:failureBlock];
}


//--------------------------------- Grant -----------------------------------
- (BOOL)amOwner
{
    return self.grant? YES: NO;
}

//---------------------------------NewDeviceConnected-----------------------------------

+ (void)getNewDeviceConnected: (NSString *)dsn
            setupToken:(NSString *) setupToken
            success:(void (^)(AylaResponse *resp, NSDictionary *response))successBlock
            failure:(void (^)(AFHTTPRequestOperation *operation, AylaError *err))failureBlock
{    
    static AylaApiClient *tmpClientInstance;
    tmpClientInstance = DEFAULT_SECURE_SETUP == NO? [AylaApiClient sharedNonSecureDeviceServiceInstance]:[AylaApiClient sharedDeviceServiceInstance];
    NSString *path = [[NSString alloc] initWithFormat:@"devices/connected.json?dsn=%@&setup_token=%@", dsn, setupToken];    
    [tmpClientInstance getPath:path parameters:nil
           success:^(AFHTTPRequestOperation *operation, id responseObject){
               AylaResponse *response = [AylaResponse new]; response.httpStatusCode = operation.response.statusCode;
               successBlock(response, responseObject);
           }
           failure:^(AFHTTPRequestOperation *operation, NSError *error){
               saveToLog(@"%@, %@, %@:%d, %@", @"E", @"confirmConnection", @"Failed", error.code , @"confirmNewDeviceToServiceConnection.getPath");
               AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode;
               err.nativeErrorInfo = error;
               NSNumber *errNum = [[NSNumber alloc] initWithInt:AML_GET_NEW_DEVICE_CONNECTED];
               NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:errNum, @"subTaskFailed", nil];
               err.errorInfo = description;
               failureBlock( operation, err);
           }
     ];
}

//-----------------------------------------------------------------------------------
//------------------------------------ Lan Mode -------------------------------------

- (void)lanModeEnable
{
    //initialize LanMode
    [AylaLanMode inSetupMode:NO];
    
    saveToLog(@"%@, %@, %@, %@", @"I", @"AylaDevice", @"entry", @"lanModeEnable");
    if([AylaSystemUtils lanModeState]!= DISABLED){
        // disable current device
        if([AylaLanMode device] != nil &&
           [[AylaLanMode device] properties]!=nil ){
            if(self != [AylaLanMode device]){
                [[AylaLanMode device] lanModeDisable];
            }
        }
        [[AylaLanMode devices] setValue:self forKey:self.dsn];
        [AylaLanMode setDevice:[[AylaLanMode devices] valueForKey:self.dsn] ];
        [AylaProperty setLastDsn: nil];
        
        [AylaReachability getDeviceIpAddressWithHostName:[[AylaLanMode device] dsn] resultBlock:^(NSString *devIp, NSString *devHost) {
            
            if(devIp !=nil && [devHost isEqualToString:[[AylaLanMode device] dsn]]){
                [[AylaLanMode device] setLanIp:devIp];
                [[AylaLanMode device] updateDevicesCacheLanIp:devIp];
            }else if(![devHost isEqualToString:[[AylaLanMode device] dsn]]){
                return;
            }
            
            [AylaReachability setDeviceReachability:AML_REACHABILITY_UNKNOWN];
            saveToLog(@"%@, %@, %@, %@, %@", @"I", @"AylaDevice", @"discoveredLanIp", devIp , @"lanModeEnable");
            self.lanModeConfig = nil;
            [self getLanModeConfig];
        }];
    }
    else {
        saveToLog(@"%@, %@, %@, %@", @"I", @"AylaDevice", @"currentState:DISABLED", @"lanModeEnable");
    }
}


- (void)lanModeDisable
{
    // Currently only one LME device supported each time
    if([AylaLanMode device]!=nil){
        [AylaDevice cancelAllOutstandingRequests];
        [AylaLanMode clearQueue];
        [AylaLanMode clearCommandOutstanding];
        
        saveToLog(@"%@, %@, %@, %@, %@", @"I", @"AylaDevice", @"deviceDsn", [[AylaLanMode device] dsn] , @"lanModeDisable");
        [[AylaLanMode sessionTimer] stop];
        [AylaLanMode registerNotifyHandle:nil];
        [AylaLanMode registerReachabilityHandle:nil];
        [AylaNotify setNotifyOutstandingCounter:0];
        [AylaLanMode cleanCurrentEncSession];
        
        [AylaReachability setDeviceReachability:AML_REACHABILITY_LAN_MODE_DISABLED];
        //[AylaLanMode clearCommandOutstanding];
        
        if(![AylaLanMode inSetupMode]) {
            NSArray *arr = [[[AylaLanMode device] properties] allValues];
            NSMutableArray *propArr = [[NSMutableArray alloc] initWithArray:arr];
            if([arr count]>0){
                [AylaCache save:AML_CACHE_PROPERTY withIdentifier:[[AylaLanMode device] dsn] andObject:propArr];
            }
        }
        
        [[AylaLanMode device] setLanModeConfig:nil];
        [AylaLanMode setSessionState:DOWN];
        [AylaLanMode setDevice:nil];
        [AylaLanMode inSetupMode:NO];
        usleep(100000);
    }
}


//------------------------ @Override methods to support Lan Mode --------------------------

- (BOOL)isLanModeActive
{
    if([AylaReachability getDeviceReachability] == AML_REACHABILITY_REACHABLE &&
       [AylaLanMode device] &&
       [[[AylaLanMode device] dsn] isEqualToString:self.dsn] &&
       [[AylaLanMode device] properties] &&
       [AylaLanMode sessionState] == UP
       ) {
        return YES;
    }
    return NO;
}


- (BOOL)initPropertiesFromCache
{
    // Currently only use stored properties info to set default, would be deprecated later
    NSMutableArray *arr = [AylaCache get:AML_CACHE_PROPERTY withIdentifier:self.dsn];
    
    // the properties have already been cached read it, otherwise go back to original steps
    if(arr!=nil){
        if(!self.properties) {
            self.properties = [NSMutableDictionary new];
        }
        
        for(AylaProperty *property in arr){
            [self.properties setValue:property forKey:[property.name copy]];
            [property updateDatapointFromProperty];
        }
        return YES;
    }
    return NO;
}

- (AylaDevice *)lanModeEdptFromDsn:(NSString *)dsn
{
    return [dsn isEqualToString:self.dsn]? self: nil;
}

- (NSString *)lanModePropertyNameFromEdptPropertyName:(NSString *)name
{
    return name;
}

//-----------------------------------------------------------------------------------------

+ (void)cancelAllOutstandingRequests
{
    if([[AylaLanMode device] lanIp])
        [[[AylaApiClient sharedConnectedDeviceInstance:[[AylaLanMode device] lanIp]] operationQueue] cancelAllOperations];
}

- (void)updateDevicesCacheLanIp:(NSString *)discoveredLanIp
{
    NSMutableArray *devices = [AylaCache get:AML_CACHE_DEVICE];

    if (devices!=nil && [devices count]>0 ){
        for(AylaDevice *dev in devices){
            if([dev.dsn isEqualToString:self.dsn]){
                [dev setLanIp:discoveredLanIp];
            }
        }
        [AylaCache save:AML_CACHE_DEVICE withObject:devices];
    }
}


+ (void)deleteLanModeSession
{
    if ([AylaLanMode device]!=nil) {    
        saveToLog(@"%@, %@, %@:%@, %@", @"I", @"AylaDevice", @"device", [[AylaLanMode device] dsn] , @"deleteLanModeSession");
        [[AylaLanMode sessionTimer] stop];
        [AylaLanMode registerNotifyHandle:NULL];
        [AylaLanMode registerReachabilityHandle:NULL];
        
        int cmdId = [AylaLanMode nextCommandOutstandingId];
        NSString *source = @"local_reg.json";
        NSString *cmd = [AylaLanMode buildToDeviceCommand:@"DELETE" cmdId:cmdId resourse:source data:nil uri:@"local_lan"];
        [AylaLanMode sendNotifyToLanModeDevice:cmdId baseType:AYLA_LAN_COMMAND
                jsonString:cmd block:^(NSDictionary *dict) {
                    //return values all error
                    //now no return from device
                }];
    }
}

- (BOOL)isLanModeEnabled
{
    if(self.lanModeConfig != nil && [self.lanModeConfig.status isEqualToString:@"enable"] && [self.lanModeConfig.lanipKeyId intValue]!= -1){
        //saveToLog(@"%@, %@, %@, %d, %@", @"I", @"AylaDevice", @"isLanModeEnabled?", true , @"isLanModeEnabled");
        return true;
    }
    else{
        saveToLog(@"%@, %@, %@, %d, %@", @"I", @"AylaDevice", @"isLanModeEnabled?", false , @"isLanModeEnabled");
        return false;
    }
}


//------------------ Get lan mode key info from the device service --------------

- (BOOL)readLanModeConfigFromCache
{
    NSMutableArray *singleConfig = [AylaCache get:AML_CACHE_LAN_CONFIG withIdentifier:[[AylaLanMode device] dsn]];
    AylaLanModeConfig *config = singleConfig == nil? nil:[singleConfig objectAtIndex:0];
    [[AylaLanMode device] setLanModeConfig:config];
    return config? YES: NO;
}

- (void)getLanModeConfig
{
    saveToLog(@"%@, %@, %@, %@", @"I", @"AylaDevice", @"entry", @"getLanModeConfig");
    NSString *path = [NSString stringWithFormat:@"%@%@%@", @"devices/", self.key, @"/lan.json"];
    //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Devices", @"path", path, @"getLanModeConfig");
    
    //if device is reachable and lanIpKey is available
    NSMutableArray *singleConfig = [AylaCache get:AML_CACHE_LAN_CONFIG withIdentifier:[[AylaLanMode device] dsn]];
    AylaLanModeConfig *config = singleConfig == nil? nil:[singleConfig objectAtIndex:0];
    if(config && !config.status) {
        //invalid config found
        config = nil;
    }
    
    if([AylaReachability isWiFiEnabled] && config!=nil){
        //load from cache..
        [[AylaLanMode device] setLanModeConfig:config];
        bool haveDataToSend = false;
        [AylaDevice startLanModeSession:[AylaLanMode device] method:POST_LOCAL_REGISTRATION haveDataToSend:haveDataToSend];
    }
    else{
        //[[AylaLanMode device] setLanModeConfig:nil];
        NSMutableURLRequest *request = [[AylaApiClient sharedDeviceServiceInstance] requestWithMethod:@"GET" path:path parameters:nil];
        [request setTimeoutInterval:10];
        
        AFHTTPRequestOperation *operation = [[AylaApiClient sharedDeviceServiceInstance] HTTPRequestOperationWithRequest:request
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 //AylaDevice *updatedDevice = [[AylaDevice alloc] initDeviceWithDictionary:responseObject];
                  saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Device", @"lanIp", _lanIp, @"getLanModeConfig");
            
                 NSDictionary *lanInfo = [responseObject valueForKeyPath:@"lanip"];
                 
                 //Check empty data from server
                 if(!lanInfo) {
                    [[AylaLanMode device] setLanModeConfig:nil];
                    [AylaDevice lanModeSessionFailed:AML_REACHABILITY_UNREACHABLE];
                    return;
                 }
                 
                 _lanModeConfig = [[AylaLanModeConfig alloc] initAylaLanModeConfigWithDictionary:lanInfo];
                 
                 if([AylaLanMode device]!=nil){
                    [[AylaLanMode device] setLanModeConfig:_lanModeConfig];                     
                     int ka = [[[[AylaLanMode device] lanModeConfig] keepAlive] intValue];
                     if(ka <= AML_LAN_MODE_TIMEOUT_SAFETY){
                         ka += AML_LAN_MODE_TIMEOUT_SAFETY;
                     }
                     [[AylaLanMode sessionTimer] setInterval: ka - AML_LAN_MODE_TIMEOUT_SAFETY];
                 }
                 [AylaReachability determineReachability];
                 
                 //save to cache
                 NSMutableArray *arr = [NSMutableArray array];
                 [arr addObject:_lanModeConfig];
                 [AylaCache save:AML_CACHE_LAN_CONFIG withIdentifier:[[AylaLanMode device] dsn] andObject:arr];
                 
                 bool haveDataToSend = false;
                 [AylaDevice startLanModeSession:[AylaLanMode device] method:POST_LOCAL_REGISTRATION haveDataToSend:haveDataToSend];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Devices", @"NSError.code", error.code, @"getLanModeConfig", [AylaSystemUtils shortErrorFromError:error]);
                 if([AylaLanMode device] != nil){
                     [[AylaLanMode device] setLanModeConfig:nil];
                 }
                 if(error.code == NSURLErrorNotConnectedToInternet)
                     [AylaReachability setConnectivity:AML_REACHABILITY_UNREACHABLE];
                 [AylaDevice lanModeSessionFailed:AML_REACHABILITY_UNREACHABLE];
             }];
        [[AylaApiClient sharedDeviceServiceInstance] enqueueHTTPRequestOperation:operation];
    }
}

//--------------------------------------------------
// Start a new session or extend the existing one if it's already up
// called via the lan mode sessionTimer
+ (void)startLanModeSession:(AylaDevice *)device method:(int)method haveDataToSend:(BOOL)haveDataToSend
{
    //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"AylaDevice", @"entry", [[AylaLanMode device] dsn] , @"startLanModeSession");
    [[AylaLanMode sessionTimer] stop];
    //[AylaLanMode setSessionState:LOCAL_REGISTRATION];
    if([AylaLanMode device] == nil){
        if (device != nil){
            [AylaLanMode setDevice:device];
        }
        else{
            saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaDevice", @"device", @"null", @"startLanModeSession");
        }
    }
    if([AylaLanMode lanModeState]!=RUNNING){
        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaDevice", @"currentState", @"!RUNNING", @"startLanModeSession");
        [AylaLanMode setSessionState:DOWN];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *returnNotify =
                [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:device.dsn status:412 description:nil values:nil];
            [AylaNotify returnNotify:returnNotify];
        });
        [[AylaLanMode sessionTimer] start];
        return;
    }
    if([AylaLanMode device] == nil || (![AylaLanMode inSetupMode] && ![[AylaLanMode device] isLanModeEnabled])){
        [AylaReachability setDeviceReachability: AML_REACHABILITY_LAN_MODE_DISABLED];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *returnNotify =
                [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:device.dsn status:404 description:nil values:nil];
            [AylaNotify returnNotify:returnNotify];
        });
        [[AylaLanMode sessionTimer] start];
        return;
    }
    
    if(method == PUT_LOCAL_REGISTRATION && [AylaLanMode sessionState] == DOWN){
        method = POST_LOCAL_REGISTRATION;
    }
 
    NSString *path = [NSString stringWithFormat:@"local_reg.json"];
    //{"local_reg":{"ip":"192.168.0.2","port":10275,"uri":"local_reg","notify":1}}'
    NSString *ip = [AylaSystemUtils getIPAddress];
    
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            ip,@"ip",
                            [AylaSystemUtils serverPortNumber],@"port",
                            @"local_lan",@"uri",
                            [NSNumber numberWithInt:haveDataToSend], @"notify", nil];
    NSDictionary *send = [[NSDictionary alloc] initWithObjectsAndKeys:params, @"local_reg",nil];
    
    NSMutableURLRequest *request;
    if (method == POST_LOCAL_REGISTRATION) {
        request = [[AylaApiClient sharedConnectedDeviceInstance:device.lanIp] requestWithMethod:@"POST" path:path parameters:send];
    }
    else if (method == PUT_LOCAL_REGISTRATION) {
        request = [[AylaApiClient sharedConnectedDeviceInstance:device.lanIp] requestWithMethod:@"PUT" path:path parameters:send];
    }
    else{
        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaDevice", @"err", @"invalid method", @"startLanModeSession");
        [AylaLanMode setSessionState:DOWN];
        [[AylaLanMode sessionTimer] start];
        return;
    }

    [request setTimeoutInterval:DEFAULT_LOCAL_WIFI_HTTP_TIMEOUT];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedConnectedDeviceInstance:device.lanIp] HTTPRequestOperationWithRequest:request
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             //saveToLog(@"%@, %@, %@, %@", @"I", @"AylaDevice", @"success", @"local_reg");
             if(operation.response.statusCode == 202){
             
                 if(![AylaLanMode inSetupMode] && ( [[[AylaLanMode device] lanModeConfig] lanipKeyId] == nil ||
                    [[[[AylaLanMode device] lanModeConfig] lanipKeyId] intValue] == -1)){
                     //key cann't match
                     [AylaDevice lanModeSessionFailed:[AylaReachability getDeviceReachability]];
                     return;
                 }
                 [AylaReachability setDeviceReachability:AML_REACHABILITY_REACHABLE];
                 
                 if (method == POST_LOCAL_REGISTRATION) {
                     //[AylaLanMode setSessionState:KEY_EXCHANGE];
                 }
                 else{
                     [AylaLanMode setSessionState:UP];
                 }
                 [[AylaLanMode sessionTimer] start];
                 saveToLog(@"%@, %@, %@, %@", @"I", @"AylaDevice", @"success", @"local_reg");
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) { 
             // check delay issues
             if (![device.lanIp isEqualToString: [[AylaLanMode device] lanIp]]) {
                 // old return skip
                 [[AylaLanMode sessionTimer] start];
                 return;
             }
             switch (operation.response.statusCode) {
                 case 400: //400: Forbidden - Bad Request (JSON parse failed)
                     [AylaReachability setDeviceReachability:AML_REACHABILITY_REACHABLE];
                     [AylaLanMode setSessionState:DOWN];
                     break;
                 case 403: //403: Forbidden - lan_ip on a different network
                     [AylaReachability setDeviceReachability:AML_REACHABILITY_UNREACHABLE];
                     [AylaLanMode setSessionState:DOWN];
                     break;
                 case 404: //404: Not Found - Lan Mode is not supported by this module
                     [AylaReachability setDeviceReachability:AML_REACHABILITY_UNREACHABLE];
                     [AylaLanMode setSessionState:DOWN];
                     break;
                 case 412:
                     if([AylaLanMode inSetupMode]) {
                         [AylaSecurity startKeyExchange:NULL];
                     }
                     else {
                         [AylaReachability setDeviceReachability:AML_REACHABILITY_UNREACHABLE];
                         [AylaLanMode setSessionState:DOWN];
                     }
                     break;
                 case 503: //503: Service Unavailable - Insufficient resources or maximum number of sessions exceeded
                     [AylaReachability setDeviceReachability:AML_REACHABILITY_UNREACHABLE];
                     [AylaLanMode setSessionState:DOWN];
                     break;
                 default:
                     [AylaReachability setDeviceReachability:AML_REACHABILITY_UNREACHABLE];
                     [AylaLanMode setSessionState:DOWN];
                     break;
             }
             
             if (error.code ==  NSURLErrorNotConnectedToInternet) {
                 [AylaReachability setConnectivity:AML_REACHABILITY_UNREACHABLE];
             }
             
             [[AylaLanMode sessionTimer] start];
             //new properties update is required
             [AylaReachability setIsDeviceReachabilityExpired:true];
             [AylaDevice lanModeSessionFailed:[AylaReachability getDeviceReachability]];
             saveToLog(@"%@, %@, %@:%d, %@:%d, %@", @"W", @"->AylaDevice", @"errCode", 0, @"httpCode", operation.response.statusCode,  @"local_reg");;
         }];
    
    [[AylaApiClient sharedConnectedDeviceInstance:device.lanIp] enqueueHTTPRequestOperation:operation];
}


+ (void)extendLanModeSession:(int)method haveDataToSend:(BOOL)haveDataToSend
{
    if([AylaLanMode device] != nil){
        [AylaDevice startLanModeSession:[AylaLanMode device] method:method haveDataToSend:haveDataToSend];
    }
}


+ (void)lanModeSessionFailed:(int)reachability
{
    [AylaLanMode setSessionState:DOWN];
    [AylaReachability setDeviceReachability:reachability];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *returnNotify =
            [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:[AylaLanMode device].dsn status:400 description:nil values:nil];
        [AylaNotify returnNotify:returnNotify];
    });
}

- (NSInteger)lanModeWillSendCmdEntity:(AylaLanCommandEntity *)entity
{
    if(entity && entity.baseType == AYLA_LAN_PROPERTY){
        // send AYLA_LAN_PROPERTY to device
        dispatch_async(dispatch_get_main_queue(), ^{
            void (^block)(NSDictionary *) =  [AylaLanMode getCommandsOutstanding:[NSString stringWithFormat:@"%d", entity.cmdId]];
            [AylaLanMode removeCommandsOutstanding:entity.cmdId];
            if(block != nil){
                NSDictionary *re = [[NSDictionary alloc] initWithObjectsAndKeys:@"success", @"status",nil];
                block(re);
            }
        });
    }
    return 200;
}

- (NSInteger)updateWithPropertyName:(NSString *)propertyName andValue:(NSString *)value
{
    AylaProperty *property = [self findProperty:propertyName];
    if(!property) {
        saveToLog(@"%@, %@, %@:%@, %@, %@", @"E", @"AylaDevice", @"propertyName", propertyName, @"not found", @"updateWithPropertyName");
        return 404;
    }
    [property setValue:value];
    [property setDataUpdatedAt:[[AylaSystemUtils timeFmt] stringFromDate:[NSDate date]]];
    [property updateDatapointFromProperty];
    [property lanModeEnable:self];
    return 200;
}

- (NSInteger)lanModeUpdateWithPropertyName:(NSString *)propertyName andValue:(NSString *)value
{
    NSInteger propertyUpdateStatus = [self updateWithPropertyName:propertyName andValue:value];
    switch (propertyUpdateStatus) {
        case 404:
            return propertyUpdateStatus;
            break;
        default:
            break;
    }
    
    saveToLog(@"%@, %@, %@, %@, %@", @"I", @"lanUpdate", @"statusFromDevice", @"update received", @"updateWithPropertyName");
    dispatch_sync(dispatch_get_main_queue(), ^{
        [AylaNotify setNotifyOutstandingCounter: [AylaNotify notifyOutstandingCounter]+1];
        NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_PROPERTY dsn:[AylaLanMode device].dsn status:200 description:nil values:[NSArray arrayWithObject:propertyName]];
        [AylaNotify returnNotify:returnNotify];
    });
    
    return 200;
}

- (NSInteger)lanModeUpdateWithCmdId:(NSInteger)cmdId status:(NSInteger)status propertyName:(NSString *)propertyName andValue:(id)value
{
    NSInteger propertyUpdateStatus = [self updateWithPropertyName:propertyName andValue:value];
    switch (propertyUpdateStatus) {
        case 404:
            return propertyUpdateStatus;
            break;
        default:
            break;
    }
    
    void(^block)(NSDictionary *) =
    [AylaLanMode getCommandsOutstanding:[NSString stringWithFormat:@"%ld", (long)cmdId]];
    if(block == nil){
        saveToLog(@"%@, %@, %@:%@, %ld, %@", @"I", @"lanUpdate", @"noReturnBlockForCommandResp-Discard", propertyName, (long)status, @"lanModeupdateWithCmdId");
    }
    
    BOOL blockNeedsToBeCalled = YES;
    NSDictionary *resp = @{};
    if(status < 200 || status > 299){
        NSDictionary *errorDict = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInteger:status] , @"httpStatusCode", nil];
        NSDictionary *error = [[NSDictionary alloc] initWithObjectsAndKeys:errorDict, @"error", nil];
        saveToLog(@"%@, %@, %@:%ld, %@", @"E", @"lanUpdate", @"statusFromDevice", (long)status, @"lanModeupdateWithCmdId");
        resp = error;
    }
    else{
        NSDictionary *success = [[NSDictionary alloc] initWithObjectsAndKeys: @"success" , @"status", nil];
        saveToLog(@"%@, %@, %@:%ld, %@", @"I", @"lanUpdate", @"statusFromDevice", (long)status, @"lanModeupdateWithCmdId");
        resp = success;
    }
    
    [AylaLanMode removeCommandsOutstanding:(int)cmdId];
    
    if(block != nil && blockNeedsToBeCalled){
        dispatch_async(dispatch_get_main_queue(), ^{
            block(resp);
        });
    }
    
    return 200;
}

- (NSString *)lanModeToDeviceUpdateWithCmdId:(__unused int)cmdId property:(AylaProperty *)property valueString:(NSString *)valueString
{
    NSString *jsonString = nil;
    if([property.baseType isEqualToString:@"string"]){
        jsonString = [NSString stringWithFormat:@"{\"property\": {\"name\": \"%@\",\"value\":\"%@\",\"base_type\":\"%@\"}}", property.name, [AylaSystemUtils jsonEscapedStringFromString:valueString], property.baseType];
    }
    else {
        jsonString = [NSString stringWithFormat:@"{\"property\": {\"name\": \"%@\",\"value\":%@,\"base_type\":\"%@\"}}", property.name, valueString, property.baseType];
    }
    return jsonString;
}

- (NSString *)lanModeToDeviceCmdWithCmdId:(int)cmdId requestType:(NSString *)type sourceLink:(NSString *)sourceLink uri:(NSString *)uri data:(NSString *)data
{
    return [AylaLanMode buildToDeviceCommand:type cmdId:cmdId resourse:sourceLink data:data uri:uri];
}

- (AylaProperty*)findProperty:(NSString *)propertyName
{
    AylaProperty *property = [self.properties valueForKeyPath:propertyName];
    return property;
}

- (void)didEnableLanMode
{
    //Nothing to be set in AylaDevice
}

- (void)didDisableLanMode
{
    //Nothing to be set in AylaDevice
}

//--------------------caching helper methods----------------------

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_productName forKey:@"productName"];
    [encoder encodeObject:_model forKey:@"model"];
    [encoder encodeObject:_dsn forKey:@"dsn"];
    [encoder encodeObject:_oemModel forKey:@"oemModel"];
    [encoder encodeObject:_deviceType forKey:kAylaDeviceType];
    [encoder encodeObject:_connectedAt forKey:@"connectedAt"];
    [encoder encodeObject:_mac forKey:@"mac"];
    [encoder encodeObject:_lanIp forKey:@"lanIp"];
    [encoder encodeObject:_ip forKey:@"ip"];
    //[encoder encodeObject:_hasProperties forKey:@"hasProperties"];
    [encoder encodeObject:_productClass  forKey:@"productClass"];
    [encoder encodeObject:_ssid forKey:@"ssid"];
    [encoder encodeObject:_swVersion forKey:@"swVersion"];
    //[encoder encodeObject:_lanEnabled forKey:@"lanEnabled"];
    [encoder encodeObject:_features forKey:@"features"];
    
    [encoder encodeObject:_key forKey:@"key"];
    [encoder encodeObject:_registrationType forKey:@"registrationType"];
    [encoder encodeObject:_setupToken forKey:@"setupToken"];
    //[encoder encodeObject:_properties forKey:@"properties"];
    //[encoder encodeObject:_lanModeConfig forKey:@"lanModeConfig"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    _productName = [decoder decodeObjectForKey:@"productName"];
    _model = [decoder decodeObjectForKey:@"model"];
    _dsn = [decoder decodeObjectForKey:@"dsn"];
    _oemModel = [decoder decodeObjectForKey:@"oemModel"];
    _deviceType = [decoder decodeObjectForKey:kAylaDeviceType];
    _connectedAt = [decoder decodeObjectForKey:@"connectedAt"];
    _mac = [decoder decodeObjectForKey:@"mac"];
    _lanIp= [decoder decodeObjectForKey:@"lanIp"];
    _key= [decoder decodeObjectForKey:@"key"];
    _registrationType = [decoder decodeObjectForKey:@"registrationType"];
    _setupToken = [decoder decodeObjectForKey:@"setupToken"];
    _ip = [decoder decodeObjectForKey:@"ip"];
    _productClass = [decoder decodeObjectForKey:@"productClass"];
    _ssid = [decoder decodeObjectForKey:@"ssid"];
    _swVersion = [decoder decodeObjectForKey:@"swVersion"];
    _features = [decoder decodeObjectForKey:@"features"];
    
    //_hasProperties = [decoder decodeObjectForKey:@"hasProperties"];
    //_lanEnabled = [decoder decodeObjectForKey:@"lanEnabled"];
    
    //_properties = [decoder decodeObjectForKey:@"properties"];
    //_lanModeConfig = [decoder decodeObjectForKey:@"lanModeConfig"];
    return self;
}

//------------------------------helpful methods---------------------------------

+ (Class)deviceClassFromDeviceType:(NSString *)deviceType andDeviceDictionary:(NSDictionary *)deviceDictionary
{
    static Class AylaDeviceClassGateway;
    static Class AylaDeviceClassNode;
    static Class AylaDeviceClass;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        AylaDeviceClass = [AylaDevice deviceClassFromClassName:kAylaDeviceClassName];
        AylaDeviceClassGateway = [AylaDevice deviceClassFromClassName:kAylaDeviceClassNameGateway];
        AylaDeviceClassNode = [AylaDevice deviceClassFromClassName:kAylaDeviceClassNameNode];
    });
    
    if(!deviceType ||
       [deviceType isEqualToString:kAylaDeviceTypeWifi]) {
        return AylaDeviceClass;
    }
    else if (AylaDeviceClassGateway &&
             [deviceType isEqualToString:kAylaDeviceTypeGateway]) {
        return [AylaDeviceGateway deviceClassFromDeviceDictionary:deviceDictionary];
    }
    else if(AylaDeviceClassNode &&
            [deviceType isEqualToString:kAylaDeviceTypeNode]) {
        return [AylaDeviceNode deviceClassFromDeviceDictionary:deviceDictionary];
    }
    return AylaDeviceClass;
}

+ (Class)deviceClassFromClassName:(NSString *)className
{
    return [AylaSystemUtils classFromClassName:className];
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        AylaDevice *_copy = copy;
        _copy.productName = [_productName copy];
        _copy.model = [_model copy];
        _copy.dsn = [_dsn copy];
        _copy.oemModel = [_oemModel copy];
        _copy.deviceType = [_deviceType copy];
        _copy.connectedAt = [_connectedAt copy];
        _copy.mac = [_mac copy];
        _copy.lanIp = [_lanIp copy];
        _copy.key = [_key copy];
        _copy.registrationType = [_registrationType copy];
        _copy.setupToken = [_setupToken copy];
        _copy.ip = [_ip copy];
        _copy.productClass = [_productClass copy];
        _copy.ssid = [_ssid copy];
        _copy.swVersion = [_swVersion copy];
        _copy.properties = [_properties copy];
        _copy.property = [_property copy];
        _copy.features = [_features copy];
    }
    return copy;
}


@end // AylaDevice
//===================================== Properties =================================


@interface AylaProperty ()
@property (nonatomic, copy) NSNumber *key;
@property (nonatomic, copy) NSNumber *deviceKey;
- (id) initDevicePropertiesWithDictionary:(NSDictionary *)propertiesDictionary;
@end

@implementation AylaProperty
// Device Property Methods
@synthesize baseType = _baseType;
@synthesize value = _value;
@synthesize dataUpdatedAt = _dataUpdatedAt;
@synthesize deviceKey = _deviceKey;
@synthesize name = _name;
@synthesize key = _key;
@synthesize direction = _direction;
@synthesize retrievedAt = _retrievedAt;
@synthesize displayName = _displayName;

@synthesize datapoint = _datapoint;
@synthesize datapoints = _datapoints;

@synthesize propertyTrigger = _propertyTrigger;
@synthesize propertyTriggers = _propertyTriggers;

@synthesize metadata = _metadata;

//@override
- (void)setDatapoints:(NSMutableArray *)datapoints
{
    _datapoints = datapoints;
}
- (void)setDatapoint:(AylaDatapoint *)datapoint
{
    _datapoint = datapoint;
}


- (NSString *)description
{
  return [NSString stringWithFormat:@"\n" 
          "baseType: %@\n"
          "datapoint.sValue: %@\n"
          "datapoint.nValue: %@\n"
          "name: %@\n"
          "direction: %@\n" 
          "retrievedAt: %@\n"
          , _baseType, _datapoint.sValue, _datapoint.nValue, _name, _direction,  _retrievedAt]; 
}


static NSString *lastDsn = nil;
+ (void)setLastDsn:(NSString *)dsn
{
    lastDsn = dsn;
}
+ (NSArray *)getProperyArrayWithNames:(NSArray *)names
{
    if(!names) return [NSArray new];
    NSMutableArray *array = [NSMutableArray new];
    for(NSString *name in names) {
        if([AylaLanMode.device.properties objectForKey:name])
          [array addObject:[AylaLanMode.device.properties objectForKey:name]];
    }
    return array;
}
// ---------------------- Retrieve Properties ---------------------------
+ (NSOperation *)getProperties:(AylaDevice *)device callParams:(NSDictionary *)callParams
                success:(void (^)(AylaResponse *response, NSArray *properties))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
    static BOOL cache = true;
    static BOOL first = true;
    
    if(device.dsn == nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS; err.nativeErrorInfo = nil;
            err.httpStatusCode = 0; err.errorInfo = nil;
            failureBlock(err);
        });
        return nil;
    }
    
    if(lastDsn == nil){
        lastDsn = [NSString stringWithString:device.dsn];
        first = true;
    }
    else if([lastDsn isEqualToString:device.dsn]){
        first = false;
    }
    else{
        //new start;
        first = true;
        lastDsn = [device.dsn copy];
    }
    
    //[AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
    if(first && [AylaLanMode device]!=nil &&
       (!device.properties || device.properties.count == 0))
    {
        [device initPropertiesFromCache];
    }
    first = false;
    
    //not applicable for new lan mode
    if([AylaNotify notifyOutstandingCounter] > 0 &&
       device.properties &&
       [device isLanModeActive]) {
  
        [AylaNotify setNotifyOutstandingCounter:[AylaNotify notifyOutstandingCounter]-1];
        NSMutableDictionary *properties = device.properties;
        
        NSArray *respArr = nil;
        if(callParams && [callParams objectForKey:@"names"]) {
            NSMutableArray *array = [NSMutableArray new];
            for(NSString *name in [callParams objectForKey:@"names"]) {
                if([properties objectForKey:name])
                    [array addObject:[properties objectForKey:name]];
            }
            respArr = array;
        }
        else {
            respArr = [properties allValues];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            AylaResponse *resp = [AylaResponse new];
            resp.httpStatusCode = AML_ERROR_ASYNC_OK;
            successBlock(resp, respArr);
        });
    }
    else{
        //version 3.x.x, check if all requested properties have been registerd at lan mode
        BOOL isPropertiesCached = YES;
        if(device.properties){
            NSArray *nameArray = [callParams objectForKey:@"names"];
            
            if(nameArray && nameArray.count == 0) {
                AylaResponse *resp = [AylaResponse new];
                resp.httpStatusCode = 200;
                successBlock(resp, [NSArray new]);
                return nil;
            }
            else if(nameArray && nameArray.count >0) {
                int found = 0;
                for(NSString *name in nameArray) {
                    if([device.properties objectForKey:name]) {
                        found++;
                    }
                }
                if(found != [nameArray count]) {
                    isPropertiesCached = NO;
                    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Properties", @"isCached", @"NO", @"getProperties");
                }
            }
        }
        
    //[AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
            if([device isLanModeActive] &&
               isPropertiesCached){
                
                //update new properties from module
                bool getAllProperties =false;
                NSArray *lanProperties = nil;
                if(callParams ==nil || [callParams valueForKey:@"names"] == nil){
                    getAllProperties = true;
                    lanProperties = [device.properties allKeys];
                }
                else{
                    lanProperties = [callParams valueForKey:@"names"];
                }
                
                __block long count;
                count = [lanProperties count];
                long oneTimeCount = count;
                for(NSString *pName in lanProperties){
                    
                    if(getAllProperties || [device.properties objectForKey:pName]!=nil ){
                        
                        //AylaDevice *device = [AylaLanMode device];
                        AylaProperty *prop = [device.properties objectForKey:pName];
                        if([AML_LANMODE_IGNORE_BASETYPES rangeOfString:[prop baseType]].location != NSNotFound){
                            count--;
                            continue;
                        }
                        int cmdId = [AylaLanMode nextCommandOutstandingId];
                        NSString *source = [NSString stringWithFormat:@"property.json?name=%@", pName];
                        NSString *cmd = [device lanModeToDeviceCmdWithCmdId:cmdId requestType:@"GET" sourceLink:source uri:@"local_lan/property/datapoint.json" data:nil];
                        [AylaLanMode sendNotifyToLanModeDevice:cmdId baseType:AYLA_LAN_COMMAND
                                jsonString:cmd block:^(NSDictionary *dict) {
                                    
                                    if([dict objectForKey:@"error"]!=nil){
                                        // Error happens
                                        if(count >= 0){
                                            NSDictionary *error = [dict objectForKey:@"error"];
                                            AylaError *err = [AylaError new]; err.errorCode = [[error objectForKey:@"httpStatusCode"] intValue]; err.nativeErrorInfo = nil;
                                            err.httpStatusCode = [[error objectForKey:@"httpStatusCode"] intValue] ; err.errorInfo = nil;
                                            count = -1;
                                            failureBlock(err);
                                        }
                                    }
                                    else{
                                        if(--count == 0){
                                            NSMutableArray *array = [NSMutableArray new];
                                            for(NSString *name in lanProperties) {
                                                [array addObject:[device.properties objectForKey:name]];
                                            }
                                            //NSMutableDictionary *properties = [[AylaLanMode device] properties];
                                            //NSArray *respArr = [properties allValues];
                                            AylaResponse *resp = [AylaResponse new];
                                            resp.httpStatusCode = AML_ERROR_ASYNC_OK;
                                            successBlock(resp, array);
                                        }
                                    }
                                }];
                    }else{
                        count-- ;
                        oneTimeCount--;
                    }
                }
                if(oneTimeCount==0){
                    AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS; err.nativeErrorInfo = nil;
                    err.httpStatusCode = 0; err.errorInfo = nil;
                    failureBlock(err);
                }
                
            }
            else if([AylaReachability getConnectivity]== AML_REACHABILITY_REACHABLE){
                cache = true;
                NSString *path = [NSString stringWithFormat:@"%@%@%@",  @"devices/", device.key, @"/properties.json"];
                
                if(callParams && [callParams objectForKey:@"names"]) {
                    NSArray *nameArray = [callParams objectForKey:@"names"];
                    if(nameArray.count > 0){
                        NSMutableString *names = [NSMutableString new];
                        bool first = YES;
                        NSString *firFormat = @"?names[]=%@";
                        NSString *nextFormat = @"&names[]=%@";
                        for(NSString *pname in nameArray) {
                            [names appendString:[NSString stringWithFormat:first?firFormat:nextFormat, pname]];
                            first = NO;
                        }
                        path = [NSString stringWithFormat:@"%@%@", path, names];
                    }
                }
                
                saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Properties", @"path", path, @"retrieveDeviceProperties");
                return [[AylaApiClient sharedDeviceServiceInstance] getPath:path
                      parameters:nil
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             int i = 0;
                             NSMutableArray *properties = [NSMutableArray array];
                             for (NSDictionary *propertiesDictionary in responseObject) {
                                 AylaProperty *property = [[AylaProperty alloc] initDevicePropertiesWithDictionary:propertiesDictionary];
                                 property.owner = device.dsn;
                                 [properties addObject:property];
                                 ++i;
                                 //saveToLog(@"%@, %@, %@:%@, %@:%@, %@", @"I", @"Properties", @"propertyKey", property.key, @"name", property.name, @"retrieveDeviceProperties.getPath");
                             }
                             saveToLog(@"%@, %@, %@%d, %@", @"I", @"Properties", @"i:", i, @"retrieveDeviceProperties.getPath");
                             [AylaProperty lanModeEnable:device properties:properties];
                             AylaResponse *resp = [AylaResponse new];
                             resp.httpStatusCode = operation.response.statusCode;
                             successBlock(resp, properties);
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Properties", @"NSError.code", error.code, @"RetrieveDeviceProperteis.getPath", [AylaSystemUtils shortErrorFromError:error]);
                             AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
                             err.httpStatusCode = operation.response.statusCode; err.errorInfo = nil;
                             failureBlock(err);
                         }];
            }
            else{
                if([[AylaLanMode device] properties]!=nil && [[[AylaLanMode device] properties] count]!=0 &&
                   [AylaLanMode.device.dsn isEqualToString:device.dsn] &&
                   [AylaCache cachingEnabled:AML_CACHE_PROPERTY]){
                    NSMutableDictionary *properties = [[AylaLanMode device] properties];
                    NSArray *respArr = [properties allValues];
                    
                    AylaResponse *resp = [AylaResponse new];
                    resp.httpStatusCode = AML_ERROR_ASYNC_OK_NON_AUTH_INFO;
                    successBlock(resp, respArr);
                }
                else{
                    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Properties", @"Fail", @"no connectivity", @"getProperties");
                    [AylaReachability determineReachability];
                    AylaError *err = [AylaError new]; err.errorCode = AML_ERROR_NO_CONNECTIVITY; err.nativeErrorInfo = nil;
                    err.httpStatusCode = 0; err.errorInfo = nil;
                    failureBlock(err);
                }
            }
       // }];
    }
    return nil;
}

- (id)initDevicePropertiesWithDictionary:(NSDictionary *)propertiesDictionary
{
  self = [super init];
  if (self)
  {
    if([propertiesDictionary objectForKey:@"property"]) {
        propertiesDictionary = propertiesDictionary[@"property"];
    }
    NSDictionary *property = propertiesDictionary;
    if (property) {
      self.baseType = ([property valueForKeyPath:@"base_type"] != [NSNull null]) ? [property valueForKeyPath:@"base_type"] : @"";
      self.value = ([property valueForKeyPath:@"value"] != [NSNull null])? [NSString stringWithFormat:@"%@", [property valueForKeyPath:@"value"]]: @"" ;
      //self.dataUpdatedAt = ([property valueForKeyPath:@"data_updated_at"] != [NSNull null]) ? [property valueForKeyPath:@"data_updated_at"] : @"";      
      self.deviceKey = [property valueForKeyPath:@"device_key"];
      self.name = ([property valueForKeyPath:@"name"] != [NSNull null]) ? [property valueForKeyPath:@"name"] : @"";
      self.key = [property valueForKeyPath:@"key"];
      self.direction = ([property valueForKeyPath:@"direction"] != [NSNull null]) ? [property valueForKeyPath:@"direction"] : @"";
      self.displayName = [property valueForKeyPath:@"display_name"]? [property valueForKeyPath:@"display_name"] : nil;
        
      AylaDatapoint *datapoint = [AylaDatapoint new];
      datapoint.sValue = [property valueForKeyPath:@"value"];
      datapoint.nValue = [property valueForKeyPath:@"value"];
      datapoint.createdAt = ([property valueForKeyPath:@"data_updated_at"] != [NSNull null]) ? [property valueForKeyPath:@"data_updated_at"] : @"";
      _datapoint = datapoint;
      //self.retrievedAt = [NSDate date];
        
      //metadata
      _metadata = [[property valueForKeyPath:@"metadata"] mutableCopy];
        
    } else {
      saveToLog(@"%@, %@, %@:%@, %@", @"E", @"Properties", @"property", @"nil", @"retrieveDeviceProperties.initDevicePropertiesWithDictionary");
    }
  }
  return self;
}

// ----------------------- Datapoint Helper Methods --------------------------
- (void)updateDatapointFromProperty
{    
    if(_datapoint == nil){
        _datapoint = [[AylaDatapoint alloc] init];
    }
    
    NSNumberFormatter *form = [[NSNumberFormatter alloc] init];
    [form setNumberStyle:NSNumberFormatterDecimalStyle];
    
    if(_value != nil){
        _datapoint.value = [NSString stringWithString:_value];
        _datapoint.sValue = [NSString stringWithString:_value];
        _datapoint.nValue = [form numberFromString:_value];
    }
    
    if(_dataUpdatedAt!=nil){
        _datapoint.createdAt = [NSString stringWithString:_dataUpdatedAt];
        //_datapoint.retrievedAt = [NSString stringWithString:_dataUpdatedAt];
    }
}


//------------------------- Lan Mode Support ---------------------------

- (void)lanModeEnable:(AylaDevice*)device
{
    [self lanModeEnable:device property:self];
}

/*
+(void) lanModeEnable:(AylaProperty *)property{
    if([AylaSystemUtils currentState] == DISABLED){
        if([AylaLanMode device]!=nil){
            [[AylaLanMode device] setProperty:property];
        }
        else{
            saveToLog(@"%@, %@, %@:%@, %@", @"E", @"Property", @"lanMode", @"no device is enabled", @"lanModeEnable");
        }
    }
}
*/

+ (void)lanModeEnable:(AylaDevice *)device properties:(NSMutableArray *)properties
{
    if([AylaSystemUtils lanModeState]!= DISABLED){
        if([AylaLanMode device]!=nil){
            
            if([[[AylaLanMode device] dsn] isEqualToString:device.dsn]){
                
                AylaDevice *lanModeDevice = [AylaLanMode device];
                if(![lanModeDevice properties]) {
                    lanModeDevice.properties = [NSMutableDictionary new];
                }
                
                for(AylaProperty *property in properties){
                    [lanModeDevice.properties setObject:property forKey:[property.name copy]];
                }
                
                if(lanModeDevice.properties.count > 0)
                    [AylaCache save:AML_CACHE_PROPERTY withIdentifier:lanModeDevice.dsn andObject: [NSMutableArray arrayWithArray:[lanModeDevice.properties allValues]]];
            }
            else{
                AylaDevice *dev = device;
                if([[AylaLanMode devices] objectForKey:device.dsn] ) {
                    dev = [[AylaLanMode devices] objectForKey:device.dsn];
                }
                
                if(![dev properties]) {
                    dev.properties = [NSMutableDictionary new];
                }
                for(AylaProperty *property in properties){
                    [[dev properties] setValue:property forKey:property.name];
                }
                if(dev.properties.count>0)
                    [AylaCache save:AML_CACHE_PROPERTY withIdentifier:dev.dsn andObject: [NSMutableArray arrayWithArray:[dev.properties allValues]]];
            }
        }
        else{
            saveToLog(@"%@, %@, %@:%@, %@", @"W", @"AylaProperty", @"AylaLanMode.device", @"null", @"lanModeEnable_properties");
        }
    }
}

- (void)lanModeEnable:(AylaDevice*)device property:(AylaProperty *)property
{
    if ([AylaSystemUtils lanModeState]!= DISABLED) {
        if ([AylaLanMode device]!=nil) {
            if ([[[AylaLanMode device] dsn] isEqualToString:device.dsn]) {
                [[AylaLanMode device] setProperty:property];
                [[[AylaLanMode device] properties] setValue:property forKey:property.name];
            }
            else {
                AylaDevice *dev = device;
                if([[AylaLanMode devices] objectForKey:device.dsn]) {
                    dev = [[AylaLanMode devices] objectForKey:device.dsn];
                }
                [dev setProperty:property];
                [[dev properties] setValue:property forKey:property.name];
            }
        }
        else {
            saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaProperty", @"AylaLanMode.device", @"null", @"lanModeEnable_property");
        }
    }
}





// --------------------------- Retrive Property ----------------------------
- (NSOperation *)getPropertyDetail:(NSDictionary *)callParams
               success:(void (^)(AylaResponse *response, AylaProperty *propertyUpdated))successBlock
               failure:(void (^)(AylaError *err))failureBlock
{
  NSString *path = [NSString stringWithFormat:@"%@%@%@", @"properties/", self.key, @".json"];
  saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Properties", @"path", path, @"retrieveDeviceProperty");
  return [[AylaApiClient sharedDeviceServiceInstance] getPath:path
                                            parameters:nil
       success:^(AFHTTPRequestOperation *operation, id responseObject) {
         AylaProperty *property = [[AylaProperty alloc] initDevicePropertiesWithDictionary:responseObject];
         saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Properties", @"propertyKey", property.key, @"retrieveDeviceProperty.getPath");
         
         AylaResponse *response = [AylaResponse new];
         response.httpStatusCode = operation.response.statusCode;
         successBlock(response, property);
       }
       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Properties", @"NSError.code", error.code, @"retrieveDeviceProperty.getPath", [AylaSystemUtils shortErrorFromError:error]);
         AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
         err.httpStatusCode = operation.response.statusCode; err.errorInfo = nil;
         failureBlock(err);
       }];
}


- (NSOperation *)createDatapoint:(AylaDatapoint *)datapoint
              success:(void (^)(AylaResponse *response, AylaDatapoint *datapoint))successBlock
              failure:(void (^)(AylaError *err))failureBlock;
{
  return [AylaDatapoint createDatapoint:self datapoint:(AylaDatapoint *)datapoint
        success:^(AylaResponse *response, AylaDatapoint *datapoint2) {
          _datapoint = datapoint2;
          successBlock(response, datapoint2);
        }
        failure:^(AylaError *err2) {
          failureBlock(err2);
        }];
}

- (void)createBlob:(NSDictionary *)callParams
         success:(void (^)(AylaResponse *response, AylaDatapointBlob *datapointCreated))successBlock
         failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaDatapointBlob createBlobWithProperty:self params:callParams success:successBlock failure:failureBlock];
}

- (NSOperation *)getDatapointsByActivity:(NSDictionary *)callParams
              success:(void (^)(AylaResponse *response, NSArray *datapoints))successBlock
              failure:(void (^)(AylaError *err))failureBlock;
{
  return [AylaDatapoint getDatapointsByActivity: self callParams:callParams
        success:^(AylaResponse *response, NSArray *datapoints2) {
            _datapoints = [NSMutableArray arrayWithArray: datapoints2 ];
          successBlock(response, datapoints2);
        }
        failure:^(AylaError *err2) {
          failureBlock(err2);
        }];
}

- (NSOperation *)getBlobsByActivity:(NSDictionary *)callParams
                    success:(void (^)(AylaResponse *response, NSArray *retrievedDatapoints))successBlock
                    failure:(void (^)(AylaError *err))failureBlock
{
   return [AylaDatapointBlob getBlobsByActvity:self callParams:callParams success:successBlock failure:failureBlock];
}

static NSString * const kAylaBlobPropertyName = @"property_name";
- (void)getBlobSaveToFlie:(AylaDatapointBlob *)datapoint params:(NSDictionary *)callParams
                   success:(void (^)(AylaResponse *response, NSString *retrieveBlobFileName))successBlock
                   failure:(void (^)(AylaError *err))failureBlock
{
    saveToLog(@"%@, %@, %@%d, %@", @"I", @"Datapoints", @"datapointProp", self.name, @"getBlobSaveToFlie");
    [AylaDatapointBlob getBlobLocation:datapoint success:^(AylaResponse *response, AylaDatapointBlob *retrievedDatapoint) {
        [AylaDatapointBlob getBlobSaveToFileWithDatapoint:retrievedDatapoint property:self params:callParams success:successBlock failure:failureBlock];
    } failure:failureBlock];
}

- (NSOperation *)createTrigger:(AylaPropertyTrigger *)propertyTrigger
            success:(void (^)(AylaResponse *response, AylaPropertyTrigger *propertyTriggerCreated))successBlock
            failure:(void (^)(AylaError *err))failureBlock
{
  return [AylaPropertyTrigger createTrigger: self propertyTrigger:propertyTrigger
     success:^(AylaResponse *response, AylaPropertyTrigger *propertyTriggerCreated) {
       _propertyTrigger= propertyTriggerCreated;
       successBlock(response, propertyTriggerCreated);
     }
     failure:^(AylaError *err) {
       failureBlock(err);
     }];
}

- (NSOperation *)getTriggers:(NSDictionary *)callParams
               success:(void (^)(AylaResponse *response, NSArray *propertyTriggers))successBlock
               failure:(void (^)(AylaError *err))failureBlock
{
  return [AylaPropertyTrigger getTriggers: self callParams:callParams
    success:^(AylaResponse *response, NSMutableArray *propertyTriggers){
       _propertyTriggers= propertyTriggers;
       successBlock(response, propertyTriggers);
     }
     failure:^(AylaError *err){
       failureBlock (err);
     }];
}

- (NSOperation *)updateTrigger:(AylaPropertyTrigger *)propertyTrigger
                        success:(void (^)(AylaResponse *response, AylaPropertyTrigger *propertyTrigger))successBlock
                        failure:(void (^)(AylaError *err))failureBlock
{
  return [AylaPropertyTrigger updateTrigger:self propertyTrigger:propertyTrigger
    success:^(AylaResponse *response, AylaPropertyTrigger *propertyTrigger) {
      successBlock(response, propertyTrigger);
    }
    failure:^(AylaError *err) {
      failureBlock(err);
    }];
}

- (NSOperation *)destroyTrigger:(AylaPropertyTrigger *)propertyTrigger
                success:(void (^)(AylaResponse *response))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
  return [AylaPropertyTrigger destroyTrigger:propertyTrigger
      success:^(AylaResponse *response){
        successBlock(response);
       }
      failure:^(AylaError *err){
        failureBlock (err);
      }];
}



//--------------------cache helper methods

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_baseType forKey:@"baseType"];
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_owner forKey:@"owner"];
    [encoder encodeObject:_direction forKey:@"direction"];
    //[encoder encodeObject:_retrievedAt forKey:@"retrievedAt"];
    [encoder encodeObject:_key forKey:@"key"];
    [encoder encodeObject:_value forKey:@"value"];
    [encoder encodeObject:_dataUpdatedAt forKey:@"dataUpdatedAt"];
    [encoder encodeObject:_displayName?_displayName:[NSNull null] forKey:@"displayName"];
    [encoder encodeObject:_metadata forKey:@"metadata"];
}

- (id)initWithCoder:(NSCoder *)decoder
{    
    _baseType = [decoder decodeObjectForKey:@"baseType"];
    _name = [decoder decodeObjectForKey:@"name"];
    _owner = [decoder decodeObjectForKey:@"owner"];
    _direction = [decoder decodeObjectForKey:@"direction"];
    //_retrievedAt = [decoder decodeObjectForKey:@"retrievedAt"];
    _key = [decoder decodeObjectForKey:@"key"];
    _value = [decoder decodeObjectForKey:@"value"];
    _dataUpdatedAt= [decoder decodeObjectForKey:@"dataUpdatedAt"];
    _displayName = [decoder decodeObjectForKey:@"displayName"]!= [NSNull null] ?[decoder decodeObjectForKey:@"displayName"]:nil;
    _metadata = [[decoder decodeObjectForKey:@"metadata"] nilIfNull];
    return self;
}

//-------------------- helper methods ------------------

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        AylaProperty *_copy = copy;
        _copy.baseType = [_baseType copy];
        _copy.name = [_name copy];
        _copy.owner = [_owner copy];
        _copy.direction = [_direction copy];
        //_copy.retrievedAt = [_retrievedAt copy];
        _copy.key = [_key copy];
        _copy.value = [_value copy];
        _copy.displayName = [_displayName copy];
        _copy.dataUpdatedAt = [_dataUpdatedAt copy];
        _copy.datapoint = [_datapoint copy];
        _copy.datapoints = [_datapoints copy];
        _copy.metadata = [_metadata mutableCopy];
    }
    return copy;
}

@end  // AylaProperty

//==================================== AylaDatapoint ===========================

@interface AylaDatapoint ()
- (id)initPropertyDatapointWithDictionary:(NSDictionary *)datapointDictionary;
@end

@implementation AylaDatapoint

@synthesize createdAt = _createdAt;
@synthesize value = _value;
@synthesize nValue = _nValue;
@synthesize sValue = _sValue;
@synthesize retrievedAt = _retrievedAt;


- (NSString *)description
{
  return [NSString stringWithFormat:@"\n" 
          "createdAt: %@\n"
          "retrievedAt: %@\n"
          "sValue: %@\n"
          "nValue: %@\n"
          , _createdAt, _retrievedAt, _sValue, _nValue]; 
}
//--------------------------- Set Property Data Point --------------------------------------

+ (NSOperation *)createDatapoint:(AylaProperty *)thisProperty datapoint:(AylaDatapoint *)datapoint
                      success:(void (^)(AylaResponse *response, AylaDatapoint *datapoint))successBlock
                      failure:(void (^)(AylaError *err))failureBlock
{
  NSString *valueStr;
  NSString *baseType = thisProperty.baseType;
  
  AylaError *paramError = [AylaError new]; paramError.errorCode = AML_USER_INVALID_PARAMETERS; paramError.nativeErrorInfo = nil;
  paramError.httpStatusCode = 0; paramError.errorInfo = nil;
  NSDictionary *respDict;
    if(baseType == nil){
        respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"can't be blank", @"baseType", nil];
        paramError.errorInfo = respDict;
    }else if ([baseType isEqualToString:@"integer"]) {
        if (datapoint.nValue == nil){
            respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"can't be blank", @"nValue", nil];
            paramError.errorInfo = respDict;
        }
        else if (strcmp([datapoint.nValue objCType], @encode(long)) != 0 &&
            strcmp([datapoint.nValue objCType], @encode(int)) != 0){
            respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"is invalid", @"nValue", nil];
            paramError.errorInfo = respDict;
        } else
            valueStr = [NSString stringWithFormat:@"%@", datapoint.nValue];
    } else if ([baseType isEqualToString:@"string"]) {
        if (datapoint.sValue == nil){
            respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"can't be blank", @"sValue", nil];
            paramError.errorInfo = respDict;
        }
        else {
            valueStr = [NSString stringWithFormat:@"%@", datapoint.sValue];
        }
    } else if ([baseType isEqualToString:@"boolean"]) {               // checks TBD
        if (datapoint.nValue == nil){
            respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"can't be blank", @"nValue", nil];
            paramError.errorInfo = respDict;
        }
        else
            valueStr = [NSString stringWithFormat:@"%d", datapoint.nValue.boolValue];
    } else if ([baseType isEqualToString:@"decimal"]) {
        if (datapoint.nValue == nil){
            respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"can't be blank", @"nValue", nil];
            paramError.errorInfo = respDict;
        }
        else
            valueStr = [NSString stringWithFormat:@"%@", datapoint.nValue]; // formating TBD
    } else if ([baseType isEqualToString:@"float"]) {
        if (datapoint.nValue == nil){
            respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"can't be blank", @"nValue", nil];
            paramError.errorInfo = respDict;
        }
        else
            valueStr = [NSString stringWithFormat:@"%@", datapoint.nValue];
    } else {
        respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"is not unknown", @"baseType", nil];
        paramError.errorInfo = respDict;
        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"Datapoints", @"baseType", baseType, @"createDatapoint: unknown base type");
    }
    
    //now both property type float and stream are not supported yet
  if(baseType!=nil && [AML_LANMODE_IGNORE_BASETYPES rangeOfString:baseType].location != NSNotFound){
     respDict = nil;
     respDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"is not supported", @"baseType", nil];
     paramError.errorInfo = respDict;
  }
    
  if(paramError.errorInfo!=nil){
    failureBlock(paramError);
    return nil;
  }
  
  AylaDevice *endpoint = [[AylaLanMode device] lanModeEdptFromDsn:thisProperty.owner];
  if (endpoint && [endpoint isLanModeActive]) {
      
      NSString *propertyName = [endpoint lanModePropertyNameFromEdptPropertyName:thisProperty.name];
      AylaProperty *lanProp = [[AylaLanMode device] findProperty:propertyName];
      if(lanProp!=nil){
          [lanProp lanModeEnable:[AylaLanMode device]];
      }
      else{
          saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Datapoints", @"property", @"cannot find", @"createDatapoint_lanMode");
          AylaError *err = [AylaError new]; err.errorCode = AML_ERROR_NOT_FOUND; err.nativeErrorInfo = nil;
          err.httpStatusCode = AML_ERROR_NOT_FOUND; err.errorInfo = nil;
          failureBlock(err);
          return nil;
      }
      datapoint.createdAt = [[AylaSystemUtils timeFmt] stringFromDate:[NSDate date]];
      //datapoint.retrievedAt = [NSString stringWithString:datapoint.createdAt];
      datapoint.value = [NSString stringWithString:valueStr];
      [datapoint lanModeEnable: nil];
      int cmdId = [AylaLanMode nextCommandOutstandingId];
      NSString *jsonString = [endpoint lanModeToDeviceUpdateWithCmdId:cmdId property:thisProperty valueString:valueStr];

      // Currently do not set timer
      // [datapoint performSelector:@selector(createDatapointEnd:) withObject:[NSNumber numberWithInt:cmdId] afterDelay:3];
      
      __block AylaDatapoint *bPoint = [datapoint copy];      
      [AylaLanMode sendNotifyToLanModeDevice:cmdId baseType:AYLA_LAN_PROPERTY jsonString:jsonString block:^(NSDictionary *resp) {
          if([resp objectForKey:@"error"]!=nil){
              NSDictionary *error = [resp objectForKey:@"error"];
              AylaError *err = [AylaError new]; err.errorCode = [[error objectForKey:@"httpStatusCode"] intValue]; err.nativeErrorInfo = nil;
              err.httpStatusCode = [[error objectForKey:@"httpStatusCode"] intValue] ; err.errorInfo = error;
              failureBlock(err);
          }
          else{
              AylaResponse *response = [AylaResponse new];
              response.httpStatusCode = AML_ERROR_ASYNC_OK;
              successBlock(response, bPoint);
          }
      }];
    }
  else{
      NSDictionary *parameters =[NSDictionary dictionaryWithObjectsAndKeys:
                                 valueStr, @"value", nil ];
      NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:
                             parameters, @"datapoint", nil];
      NSString *path = [NSString stringWithFormat:@"%@%@%@", @"properties/", thisProperty.key, @"/datapoints.json"];
      saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Datapoints", @"path", path, @"createDatapoint");
      return [[AylaApiClient sharedDeviceServiceInstance] postPath:path
               parameters:params
                  success:^(AFHTTPRequestOperation *operation, id datapointDict) {
                    AylaDatapoint *datapoint = [[AylaDatapoint alloc] initPropertyDatapointWithDictionary:datapointDict];
                    saveToLog(@"%@, %@, %@, %@", @"I", @"Datapoints", @"none", @"createDatapoint.postPath");
                    AylaResponse *response = [AylaResponse new];
                    response.httpStatusCode = operation.response.statusCode;
                    successBlock(response, datapoint);
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Datapoints", @"NSError.code", error.code, @"createDatapoint.postPath", [AylaSystemUtils shortErrorFromError:error]);
                    AylaError *err = [AylaError new]; err.nativeErrorInfo = error;
                    err.httpStatusCode = operation.response.statusCode;
                    NSDictionary *resp = nil;
                    if(operation.responseString!=nil){
                        NSError *jerr;
                        id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                        resp = responseJSON;
                        err.errorCode = AML_USER_INVALID_PARAMETERS;
                        err.errorInfo = resp;
                    }
                    else{
                        err.errorCode = 1;
                        err.errorInfo = nil ;
                    }
                    failureBlock(err);
                  }];
  }
  return nil;
}


- (void)createDatapointEnd:(NSNumber *)cmdId
{
    void (^block)(NSDictionary *) = [AylaLanMode getCommandsOutstanding: [cmdId stringValue]];
    if(block != nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            [AylaLanMode removeCommandsOutstanding:[cmdId intValue]];
            NSDictionary *blockResp = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:0], @"error", nil];
            if(block!=nil)
                block(blockResp);
        });
    }
}

- (void)lanModeEnable:(AylaProperty *)property
{
    if([AylaSystemUtils lanModeState] != DISABLED){
        AylaLanMode.device.property.value = [NSString stringWithString: self.value];
        AylaLanMode.device.property.dataUpdatedAt = [NSString stringWithString: self.createdAt];
        [[[AylaLanMode device] property] updateDatapointFromProperty];
        if(AylaLanMode.device.property.datapoints == nil){
            AylaLanMode.device.property.datapoints = [NSMutableArray array];
        }
        NSMutableArray *dps = AylaLanMode.device.property.datapoints;
        [dps addObject:self];
        AylaLanMode.device.property.datapoint = self;
    }
}

- (id)initPropertyDatapointWithDictionary:(NSDictionary *)datapointDictionary
{
  self = [super init];
  if (self) {
    NSArray *datapoint = [datapointDictionary objectForKey:@"datapoint"];
    if (datapoint) {
      self.createdAt = ([datapoint valueForKeyPath:@"created_at"] != [NSNull null]) ? [datapoint valueForKeyPath:@"created_at"] : @"";
      //self.value = [datapoint valueForKeyPath:@"value"];
      self.nValue = [datapoint valueForKeyPath:@"value"];
      self.sValue = [datapoint valueForKeyPath:@"value"];
      //self.retrievedAt = [NSDate date];
    } else {
      saveToLog(@"%@, %@, %@:%@, %@", @"E", @"Datapoints", @"datapoint", @"nil", @"createDatapoint.initSetPropertyDatapointWithDictionary");
    }
  }
  return self;
}

//---------------------------- Get Property Data Points ----------------------------------

+ (NSOperation *)getDatapointsByActivity:(AylaProperty *)property callParams:callParams
                success:(void (^)(AylaResponse *response, NSArray *datapoints))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
  // Limit count to maxCount
  NSNumber *count = [callParams valueForKeyPath:@"count"];
  NSNumber *maxCount = [AylaSystemUtils maxCount];
  count = ([count compare:maxCount] == NSOrderedAscending) ? count : maxCount;

    AylaDevice *endpoint = [[AylaLanMode device] lanModeEdptFromDsn:property.owner];
    if (endpoint
        && [endpoint isLanModeActive]
        && [count intValue] == 1)
    {
        AylaProperty *lanProp = [endpoint findProperty:property.name];
        if (lanProp != nil) {
            
            int cmdId = [AylaLanMode nextCommandOutstandingId];
            NSString *source = [NSString stringWithFormat:@"property.json?name=%@", property.name];
            NSString *cmd = [endpoint lanModeToDeviceCmdWithCmdId:cmdId requestType:@"GET" sourceLink:source uri:@"local_lan/property/datapoint.json" data:nil];

            [AylaLanMode sendNotifyToLanModeDevice:cmdId baseType:AYLA_LAN_COMMAND
                                        jsonString:cmd block:^(NSDictionary *dict) {
                                            AylaResponse *response = [AylaResponse new];
                                            response.httpStatusCode = AML_ERROR_ASYNC_OK;
                                            successBlock(response, [[NSArray alloc] initWithObjects: lanProp.datapoint , nil]);
                                        }];
            }
        else {
            AylaError *err = [AylaError new]; err.errorCode = AML_ERROR_NOT_FOUND; err.nativeErrorInfo = nil;
            err.httpStatusCode = AML_ERROR_NOT_FOUND; err.errorInfo = nil;
            failureBlock(err);
        }
    }
    else {
        // properties/44/datapoints.json?limit=5
        NSString *path = [NSString stringWithFormat:@"%@%@%@%@%@", @"properties/", property.key, @"/datapoints.json", @"?limit=", count];
        saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Datapoints", @"path", path, @"retrievePropertyDatapoints");
        return [[AylaApiClient sharedDeviceServiceInstance] getPath:path
              parameters:nil
                 success:^(AFHTTPRequestOperation *operation, id datapointsDict) {
                     int i = 0;
                     NSMutableArray *datapoints = [NSMutableArray array];
                     for (NSDictionary *datapointDictionary in datapointsDict) {
                         AylaDatapoint *datapoint = [[AylaDatapoint alloc] initPropertyDatapointWithDictionary:datapointDictionary];
                         [datapoints addObject:datapoint];
                         i++;
                     }
                     saveToLog(@"%@, %@, %@%d, %@", @"I", @"Datapoints", @"i:", i, @"RetrieveDatapoints.getPath");
                     AylaResponse *response = [AylaResponse new];
                     response.httpStatusCode = operation.response.statusCode;
                     successBlock(response, datapoints);
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Datapoints", @"NSError.code", error.code, @"RetrieveDatapoints.getPath", [AylaSystemUtils shortErrorFromError:error]);
                     
                     AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
                     err.httpStatusCode = operation.response.statusCode; err.errorInfo = nil;
                     failureBlock(err);
                 }];
    }
    return nil;
}

//------------------------------helper methods---------------------------------
- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        AylaDatapoint *_copy = copy;
        _copy.createdAt = [_createdAt copy];
        _copy.value = [_value copy];
        _copy.nValue = [_nValue copy];
        _copy.sValue = [_sValue copy];
        //_copy.retrievedAt = [_retrievedAt copy];
    }
    return copy;
}

@end


@interface AylaDatapointBlob ()

@property (nonatomic, strong) NSString *fileUrl;
@property (nonatomic, strong) NSString *location;

@end

@implementation AylaDatapointBlob

static NSString * const kAylaPropertyBaseTypeStream = @"stream"; //going to be deprecated
static NSString * const kAylaPropertyBaseTypeFile = @"file";

- (instancetype)initPropertyDatapointWithDictionary:(NSDictionary *)datapointDictionary
{
    self = [super initPropertyDatapointWithDictionary:datapointDictionary];
    if(self) {
        NSDictionary *attributes = [datapointDictionary objectForKey:@"datapoint"];
        self.location = [attributes objectForKey:@"location"]?:self.sValue;
        self.fileUrl = [attributes objectForKey:@"file"];
        self.closed = [[attributes objectForKey:@"closed"] boolValue];
        
        //get url from location
        if(self.location) {
            NSRange range = [self.location rangeOfString:@"devices/"];
            if(range.location != NSNotFound) {
                NSRange rangeOfJson = [self.location rangeOfString:@".json"];
                self.url = [NSString stringWithFormat:rangeOfJson.location == NSNotFound? @"%@.json": @"%@", [self.location substringFromIndex:range.location]];
            }
        }
    }
    return self;
}

+ (NSOperation *)getBlobsByActvity:(AylaProperty *)property callParams:callParams
                           success:(void (^)(AylaResponse *response, NSArray *retrievedDatapoints))successBlock
                           failure:(void (^)(AylaError *err))failureBlock
{
    NSNumber *count = [callParams valueForKeyPath:@"count"]?:@(1); //retrieve one datapoint by default
    NSNumber *maxCount = [AylaSystemUtils maxCount];
    count = ([count compare:maxCount] == NSOrderedAscending)? count : maxCount;
    
    NSString *path = [NSString stringWithFormat:@"%@%@%@%@%@", @"properties/", property.key, @"/datapoints.json", @"?limit=", count];
    saveToLog(@"%@, %@, %@:%@, %@: %@", @"I", @"Datapoints", @"path", path, @"getBlobs_count", count);
    
    return
    [[AylaApiClient sharedDeviceServiceInstance] getPath:path
                                              parameters:nil
                                                 success:^(AFHTTPRequestOperation *operation, id datapointsDict) {
                                                     NSMutableArray *datapoints = [NSMutableArray array];
                                                     for (NSDictionary *datapointDictionary in datapointsDict) {
                                                         AylaDatapointBlob *datapoint = [[AylaDatapointBlob alloc] initPropertyDatapointWithDictionary:datapointDictionary];
                                                         [datapoints addObject:datapoint];
                                                     }
                                                     
                                                     AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                                                     successBlock(resp, datapoints);
                                                 }
                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                     saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Datapoints", @"NSError.code", error.code, @"RetrieveDatapoints.getPath", [AylaSystemUtils shortErrorFromError:error]);
                                                     AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
                                                     err.httpStatusCode = operation.response.statusCode;
                                                     err.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInt: AML_GET_BLOBS_GET], @"subtaskfailed", nil];
                                                     failureBlock(err);
                                                 }];
}

+ (void)createBlobWithProperty:(AylaProperty *)property params:(NSDictionary *)callParams
                       success:(void (^)(AylaResponse *response, AylaDatapointBlob *retrievedBlobs))successBlock
                       failure:(void (^)(AylaError *err))failureBlock
{
    [AylaDatapointBlob createBlobDatapointWithProperty:property params:callParams success:^(AylaResponse *response, AylaDatapointBlob *datapoint) {
        [AylaDatapointBlob uploadBlobWithDatapoint:datapoint params:callParams success:^(AylaResponse *response, AylaDatapointBlob *datapoint) {
            [datapoint markFinished:nil success:^(AylaResponse *response) {
                //mark finished compelted, update close value to be true.
                datapoint.closed = YES;
                successBlock(response, datapoint);
            } failure:failureBlock];
        } failure:failureBlock];
    } failure:failureBlock];
}

+ (NSOperation *)createBlobDatapointWithProperty:(AylaProperty *)property params:(NSDictionary *)callParams
                                         success:(void (^)(AylaResponse *response, AylaDatapointBlob *datapoint))successBlock
                                         failure:(void (^)(AylaError *err))failureBlock
{
    if(!([property.baseType isEqualToString:kAylaPropertyBaseTypeFile] ||
         [property.baseType isEqualToString:kAylaPropertyBaseTypeStream])) {
        AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{@"base_type": @"should be stream"}];
        failureBlock(err);
        return nil;
    }
    
    if(!property.key) {
        AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{@"property": @"is invalid"}];
        failureBlock(err);
        return nil;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@%@", @"properties/", property.key, @"/datapoints.json"];
    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Datapoints", @"path", path, @"createDatapoint");
    return
    [[AylaApiClient sharedDeviceServiceInstance] postPath:path
                                               parameters:nil
                                                  success:^(AFHTTPRequestOperation *operation, id datapointDict) {
                                                      AylaDatapointBlob *datapoint = [[AylaDatapointBlob alloc] initPropertyDatapointWithDictionary:datapointDict];
                                                      saveToLog(@"%@, %@, %@, %@", @"I", @"Datapoints", @"none", @"createDatapoint.postPath");
                                                      AylaResponse *response = [AylaResponse new];
                                                      response.httpStatusCode = operation.response.statusCode;
                                                      successBlock(response, datapoint);
                                                  }
                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                      saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Datapoints", @"NSError.code", error.code, @"createDatapoint.postPath", [AylaSystemUtils shortErrorFromError:error]);
                                                      AylaError *err = [AylaError new]; err.nativeErrorInfo = error;
                                                      err.httpStatusCode = operation.response.statusCode;
                                                      err.errorInfo = operation.responseObject;
                                                      failureBlock(err);
                                                  }];
}


+ (NSOperation *)getBlobLocation:(AylaDatapointBlob *)datapoint
                            success:(void (^)(AylaResponse *response, AylaDatapointBlob *datapoint))successBlock
                            failure:(void (^)(AylaError *err))failureBlock
{
    if(!datapoint.url) {
        AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{@"url": @"is invalid"}];
        failureBlock(err);
        return nil;
    }
    
    return
    [[AylaApiClient sharedDeviceServiceInstance] getPath:datapoint.url parameters:nil
    success:^(AFHTTPRequestOperation *operation, id responseObject) {
        AylaDatapointBlob *datapoint = [[AylaDatapointBlob alloc] initPropertyDatapointWithDictionary:responseObject];
        saveToLog(@"%@, %@, %@, %@", @"I", @"Datapoints", @"none", @"updateBlobLocation");
        AylaResponse *response = [AylaResponse new];
        response.httpStatusCode = operation.response.statusCode;
        successBlock(response, datapoint);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"Datapoints", @"NSError.code", error.code, @"updateBlobLocation", [AylaSystemUtils shortErrorFromError:error]);
        AylaError *err = [AylaError new]; err.nativeErrorInfo = error;
        err.httpStatusCode = operation.response.statusCode;
        err.errorInfo = operation.responseObject;
        failureBlock(err);
    }];
}

+ (void)getBlobSaveToFileWithDatapoint:(AylaDatapointBlob *)datapoint property:(AylaProperty *)property
                  params:(NSDictionary *)callParams
                  success:(void (^)(AylaResponse *response, NSString *retrievedBlobName))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    NSString *url = datapoint.fileUrl;
    NSString *localPath = [callParams objectForKey:kAylaBlobFileLocalPath];
    NSString *propertyName = property.name;
    NSString *suffixName = [callParams objectForKey:kAylaBlobFileSuffixName];
    
    if(!url) {
        saveToLog(@"%@, %@, %@: %@, %@", @"E", @"Datapoints", @"Error", @"No URL found.", @"getBlobSaveToFile");
        AylaError *err = [AylaError new];
        err.errorCode = AML_AYLA_ERROR_FAIL;
        err.errorInfo = nil; err.nativeErrorInfo = nil;
        failureBlock(err);
        return;
    }

    NSString *fileName = [NSString stringWithFormat:@"%@_%@", propertyName?:@"Blob" ,suffixName?:@"BlobFile"];
    NSString *fileAbosultePath = [NSString stringWithFormat:@"%@%@/%@", [AylaSystemUtils rootDocumentsDirectory], localPath?:@"", fileName];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSError *error;
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"GET" URLString:url parameters:nil error:&error];
    if(error) {
        AylaError *aylaErr = [AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:0 nativeError:error andErrorInfo:nil];
        failureBlock(aylaErr);
        return;
    }
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        saveToLog(@"%@, %@, %@%@, %@", @"I", @"Datapoints", @"success", @"", @"getBlobSaveToFile");
        AylaResponse *response = [AylaResponse new];
        response.httpStatusCode = operation.response.statusCode;
        successBlock(response, fileName);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
        err.httpStatusCode = operation.response.statusCode;
        err.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInt: AML_GET_BLOBS_SAVE_TO_FILE], @"subtaskfailed", nil];
        failureBlock(err);
    }];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:fileAbosultePath append:NO];
    [[AylaApiClient sharedDeviceServiceInstance] enqueueHTTPRequestOperation:operation];
}

+ (void)uploadBlobWithDatapoint:(AylaDatapointBlob *)datapoint params:(NSDictionary *)callParams
                        success:(void (^)(AylaResponse *response, AylaDatapointBlob *datapoint))successBlock
                        failure:(void (^)(AylaError *err))failureBlock
{
    if(!datapoint.fileUrl) {
        AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{@"url": @"is invalid"}];
        failureBlock(err);
        return;
    }
    
    NSData *fileData = callParams[kAylaBlobFileData];
    NSURL *filePath = callParams[kAylaBlobFileUrl];
    NSNumber *fileSize = nil; NSInputStream *inputStream = nil;
    
    if(filePath) {
        NSError *fileError;
        [filePath getResourceValue:&fileSize
                            forKey:NSURLFileSizeKey
                             error:&fileError];
        
        inputStream = [NSInputStream inputStreamWithURL:filePath];
        if(fileError || !inputStream) {
                saveToLog(@"%@, %@, %@:%@, %@", @"E", @"Datapoints", @"filePath", @"is invalid", @"updateBlobWithDatapoint");
                AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{kAylaBlobFileUrl: @"is invalid"}];
                failureBlock(err);
                return;
        }
    }
    else if(fileData) {
        fileSize = @(fileData.length);
    }
    else {
        AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{@"file": @"not found"}];
        failureBlock(err);
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    NSError *error ;
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"PUT" URLString:datapoint.fileUrl parameters:nil error:&error];
    if(error) {
        AylaError *aylaErr = [AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:0 nativeError:error andErrorInfo:nil];
        failureBlock(aylaErr);
        return;
    }
    
    [request setValue:@"application/octet-stream" forHTTPHeaderField:@"Content-Type"];
  
    if(inputStream){
        [request setHTTPBodyStream:inputStream];
    }
    else if(fileData){
        [request setHTTPBody:fileData];
    }
    
    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
    success:^(AFHTTPRequestOperation *operation, id responseObject) {
        saveToLog(@"%@, %@, %@%@, %@", @"I", @"Datapoints", @"success", @"", @"uploadBlobWithDatapoint");
        AylaResponse *response = [AylaResponse new];
        response.httpStatusCode = operation.response.statusCode;
        successBlock(response, datapoint);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        saveToLog(@"%@, %@, %@%@, %@", @"E", @"Datapoints", @"failed", error.description, @"uploadBlobWithDatapoint");
        AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
        err.httpStatusCode = operation.response.statusCode;
        err.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInt: AML_GET_BLOBS_SAVE_TO_FILE], @"subtaskfailed", nil];
        failureBlock(err);
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        if(totalBytesExpectedToWrite == totalBytesWritten)
            saveToLog(@"%@, %@, %@: %@, %@", @"I", @"Datapoints", @"upload", @"completed", @"uploadBlobWithDatapoint");
    }];
    
    [[AylaApiClient sharedDeviceServiceInstance] enqueueHTTPRequestOperation:operation];
}

static NSString * const kAylaDatapointMarkFinishTag = @"type";
static NSString * const kAylaDatapointMarkFinishTagFetched = @"fetched";
static NSString * const kAylaDatapointMarkFinishTagUploaded = @"uploaded";
- (NSOperation *)markFetched:(NSDictionary *)callParams
            success:(void (^)(AylaResponse *response))successBlock
            failure:(void (^)(AylaError *err))failureBlock
{
    return [self markFinished:@{kAylaDatapointMarkFinishTag: kAylaDatapointMarkFinishTagFetched} success:successBlock failure:failureBlock];
}

- (NSOperation *)markFinished:(NSDictionary *)callParams
               success:(void (^)(AylaResponse *response))successBlock
               failure:(void (^)(AylaError *err))failureBlock
{
    if(!self.url) {
        AylaError *err = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{@"url": @"is invalid"}];
        failureBlock(err);
        return nil;
    }
    
    NSDictionary *values = nil; NSDictionary *params = nil;
    if([callParams[kAylaDatapointMarkFinishTag] isEqualToString:kAylaDatapointMarkFinishTagFetched]) {
        values = @{kAylaDatapointMarkFinishTagFetched : @"True"};
    }
    if(values) {
        params = @{@"datapoint": values};
    }
    
    return
    [[AylaApiClient sharedDeviceServiceInstance] putPath:self.url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        saveToLog(@"%@, %@, %@: %@, %@", @"I", @"Datapoints", @"success", @"", @"markFinished");
        AylaResponse *response = [AylaResponse new];
        response.httpStatusCode = operation.response.statusCode;
        successBlock(response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        saveToLog(@"%@, %@, %@: %@, %@", @"I", @"Datapoints", @"failed", error.description, @"markFinished");
        AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
        err.httpStatusCode = operation.response.statusCode;
        err.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInt: AML_BLOBS_MARK_FINISH], @"subtaskfailed", nil];
        failureBlock(err);
    }];
}

- (NSString *)description
{
    NSString *description = [NSString stringWithFormat:@"<%@: %#x, url: %@, closed: %d>",
                             NSStringFromClass([self class]), (unsigned int) self, _url, _closed];
    return description;
}

@end

NSString * const kAylaDeviceTypeWifi = @"Wifi";
NSString * const kAylaDeviceTypeGateway = @"Gateway";
NSString * const kAylaDeviceTypeNode = @"Node";

NSString * const kAylaDeviceClassNameGateway = @"AylaDeviceGateway";
NSString * const kAylaDeviceClassNameNode = @"AylaDeviceNode";
NSString * const kAylaDeviceClassName = @"AylaDevice";
NSString * const kAylaDeviceProductName = @"product_name";

NSString * const kAylaBlobFileLocalPath = @"file_local_path";
NSString * const kAylaBlobFileSuffixName = @"file_suffix_name";
NSString * const kAylaBlobFileData = @"file_data";
NSString * const kAylaBlobFileUrl = @"file_url";

//
//  AylaEncryption.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 2/13/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AylaEncryption : NSObject


+ (int) generateSessionkeys:(NSDictionary *)param sRnd1:(NSString*)_sRnd1 nTime1:(NSNumber *)_nTime1 sRnd2:(NSString*)_sRnd2 nTime2:(NSNumber*)_nTime2;

+ (NSData *)    hmacForKeyAndData:(NSData*)key data:(NSData*)data;

+ (NSData *)    lanModeEncryptInStream:(NSString *)plainText;
+ (NSString *)  lanModeDecryptInStream:(NSData *)cipherData;

+ (NSData *)    base64Decode:(NSString *)string;

+ (NSData *)    dataFromHexString:(NSString *)hexString;
+ (NSString *)  dataToHexString:(NSData *)data;

+ (NSString *)  encryptEncapsulateSign:(int)seqNo jsonString:(NSString *)jsonString baseType:(int) baseType sign:(NSData *)sign;

+ (void)        cleanCurrentSession;

//-----------TokenGeneration-----------------------------
+ (NSString *)   randomToken:(int)len;

+ (NSNumber *)  version;
+ (void)        setVersion:(NSNumber *)_version;
+ (NSNumber *)  proto1;
+ (void)        setProto1:(NSNumber *)_proto;
+ (NSNumber *)  keyId1;
+ (void)        setKeyId1:(NSNumber *)_keyId;

+ (NSData*)     devSignKey;
+ (NSData*)     appSignKey;

@end

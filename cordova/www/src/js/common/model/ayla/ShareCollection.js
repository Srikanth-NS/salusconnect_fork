"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/ayla/Share.model",
	"common/util/utilities"
], function (App, P, AylaConfig, AylaBackedMixin, ShareModel, utils) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.ShareCollection = B.Collection.extend({
			model: ShareModel,

			load: function () {
				return this.fetch().then(this._getResolvedPromise);
			},

			refresh: function () {
				var promise = this.load();
				App.salusConnector.setDataLoadPromise("shares", promise);

				return promise;
			},

			isLoaded: function () {
				return !!this.length; //todo
			},

			getSharedUsersEmails: function () {
				return _.uniq(this.pluck("user_email"));
			},

			removeAllSharesTo: function (email) {
				var userShares = this.where({ user_email: email}),
						promises = [];

				_.each(userShares, function (share) {
					promises.push(share.unregister());
				});

				return P.all(promises);
			},

			removeShares : function (sharesList) {
				var promises = [];

				_.each(sharesList, function (share) {
					promises.push(share.unregister());
				});

				return P.all(promises);
			},

			addDeviceToAllShares: function (device) {
				var that = this;
				var payload = {}, shareArray = [],
						emailArray = this.getSharedUsersEmails();

				_.each(emailArray, function (email) {
					shareArray.push(that._buildSharePayload(device, email));
				});

				payload.shares = shareArray;

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.share.batchShare, payload , "POST");
			},

			/**
			 * batchShare entire device collection with a users email
			 */
			batchShareEntireDeviceCollection: function (email, excludedIds) {
				var that = this;
				var payload = {}, shareArray = [],
						deviceCollection = App.salusConnector.getDeviceCollection().filter(function (device) {
							return !_.contains(excludedIds, device.get("key"));
						});

				_.each(deviceCollection, function (device) {
					shareArray.push(that._buildSharePayload(device, email));
				});

				payload.shares = shareArray;

				var url = utils.appendEmailTemplateParameters(AylaConfig.endpoints.share.batchShare,
					"resource_share", App.translate("email.resource_share.emailSubject"));
				return App.salusConnector.makeAjaxCall(url, payload , "POST");
			},

			/**
			 * batchShare specified devices with a users email
			 */
			batchShareDevicesByDSN: function (email, includedIds, excludedIds) {
				var that = this;
				var sharesForUser = [], postPayload = {}, postShareArray = [];

				// get DSNs of device shares for current user
				App.salusConnector.getShareCollection().each(function (share) {
					if (share.get("user_email") === email) {
						sharesForUser.push(share.get("resource_id"));
					}
				});

				var devicesToShare = App.salusConnector.getFullDeviceCollection().filter(function (device) {
					// device should be shared if it's included, not excluded, and not already shared with the user
					return _.contains(includedIds, device.get("dsn")) &&
							!_.contains(excludedIds, device.get("dsn")) &&
							!_.contains(sharesForUser, device.get("dsn"));
				});

				// setup post params
				_.each(devicesToShare, function (device) {
					postShareArray.push(that._buildSharePayload(device, email));
				});

				postPayload.shares = postShareArray;

				var url = utils.appendEmailTemplateParameters(AylaConfig.endpoints.share.batchShare,
						"resource_share", App.translate("email.resource_share.emailSubject"));

				return App.salusConnector.makeAjaxCall(url, postPayload , "POST");
			},

			/**
			 * share a device to a bunch of emails
			 * opposite of the function above
			 * @param device - device model
			 * @param emailList - array of emails
			 */
			batchShareDeviceToEmails: function (device, emailList) {
				// if device is not deviceModel or no emails
				if (!(device && device instanceof B.Model) || !emailList || (_.isArray(emailList) && emailList.length === 0)) {
					return P.reject("Missing parameters");
				}

				var that = this,
						payload = {
							shares: []
						};

				var url = utils.appendEmailTemplateParameters(AylaConfig.endpoints.share.batchShare,
						"resource_share", App.translate("email.resource_share.emailSubject"));

				_.each(emailList, function (email) {
					payload.shares.push(that._buildSharePayload(device, email));
				});

				return App.salusConnector.makeAjaxCall(url, payload, "POST", "json").then(function (data) {
					that.set(that.aParse(data.shares));
					that.trigger("sync");
				});
			},

			_buildSharePayload: function (device, email) {
				return {
					"resource_id": device.get("dsn"),
					"user_email": email,
					"resource_name": "device"
				};
			},

			aParse: function (data) {
				var out = [];

				_.each(data, function (entry) {
					var share = new App.Models.ShareModel(entry.share);
					out.push(share);
				});

				return out;
			}
		}).mixin([AylaBackedMixin], {
			fetchUrl: AylaConfig.endpoints.share.list,
			apiWrapperObjectName: "share"
		});
	});

	return App.Models.ShareCollection;
});
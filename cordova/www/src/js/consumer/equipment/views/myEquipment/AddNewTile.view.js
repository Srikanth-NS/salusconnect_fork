"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs"
], function (App, config, constants, P, consumerTemplates, SalusView) {
	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {

		var sizeClasses = {
			regular: "col-xs-6 col-sm-4",	// groups, categories, ...
			small: "col-xs-3 col-sm-2"		// ungrouped device
		};

		var textClasses = {
			regular: "regular-text",
			small: "small-text"
		};

		/**
		 * tile with dashed border
		 */
		Views.AddNewTileView = Mn.ItemView.extend({
			template: consumerTemplates["equipment/myEquipment/addNewTile"],
			attributes: {
				role: "button"
			},
			ui: {
				text: ".bb-add-new-tile-text",
				background: ".bb-add-new-tile-background"
			},
			events: {
				click: "_handleClick"
			},
			initialize: function (options) {
				_.bindAll(this, "_handleClick");

				this.textKey = options.i18nTextKey || "";
				this.sizeClass = options.size || "";
				this.classes = options.classes || "";
				this.clickDestination = options.clickDestination || "";

				if (this.sizeClass !== "regular" && this.sizeClass !== "small") {
					this.sizeClass = "regular";
				}
			},
			onRender: function () {
				// make $el say something like col-xs-6 col-sm-4 group-tile
				this.$el.addClass(this.classes).addClass(sizeClasses[this.sizeClass]);

				// small size needs to have it's bg-position moved down
				if (this.sizeClass === "small") {
					this.ui.background.addClass(this.sizeClass);
				}

				// text display and formatting styles
				this.ui.text.addClass(textClasses[this.sizeClass]);
				this.ui.text.text(App.translate(this.textKey));

				// sometimes we need to make the border on the $el instead (thermostat categories - has defined px size)
				if (this.options.swapBorderEls) {
					this.ui.background.css("border", 0);
					this.$el.css("border", "1px dashed $GRY5");
				}
			},
			_handleClick: function () {
				if (_.isString(this.clickDestination) && this.clickDestination !== "") {
					App.navigate(this.clickDestination);
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.AddNewTileView;
});
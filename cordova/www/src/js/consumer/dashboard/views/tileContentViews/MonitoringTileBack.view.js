"use strict";

define([
	"app",
	"common/util/equipmentUtilities",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/views/mixins/mixin.batteryLevel"
], function (App, equipUtils, consumerTemplates, TileContentViewMixin, BatteryLevel) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {
		Views.MonitoringTileBackView = Mn.ItemView.extend({
			className: "monitoring-tile",
			template: consumerTemplates["dashboard/tile/monitoringTileBack"],
			bindings: {
				".bb-monitoring-icon": {
					observe: "ErrorIASZSTrouble",
					onGet: function (trouble) {
						return trouble ? trouble.getProperty() : trouble;
					},
					update: function ($el, val) {
						// when ErrorIASZSTrouble is 1
						$el.toggleClass("not-monitoring", !!val);
					}
				}
			}
		}).mixin([TileContentViewMixin, BatteryLevel]);
	});

	return App.Consumer.Dashboard.Views.WindowMonitorTile;
});

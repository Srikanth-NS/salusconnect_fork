"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusRadio.view",
	"consumer/models/SalusRadioViewModel"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {
		Views.PinCustomContentView = Mn.ItemView.extend({
			className: "pin-custom-content row",
			template: consumerTemplates["setupWizard/provisioning/pinEquipmentCustomContent"],
			ui: {
				pinAnchor: ".bb-pin-anchor"
			},
			initialize: function () {
                this.pinView = new Views.PinEquipmentRadio({
					isPin: true,
					model: this.model
				});

                this.unpinView = new Views.PinEquipmentRadio({
					isPin: false,
					model: this.model
				});
				
				
			},
			onRender: function () {
				this.ui.pinAnchor.append(this.pinView.render().$el).append(this.unpinView.render().$el);
			},
			onDestroy: function () {
				this.pinView.destroy();
				this.unpinView.destroy();
			}
		}).mixin([SalusView]);

		/**
		 * PinEquipmentRadio
		 * pin or unpin
		 */
		Views.PinEquipmentRadio = Mn.ItemView.extend({
			template: consumerTemplates["setupWizard/provisioning/pinEquipmentRadio"],
			className: "pin-equipment-radio col-sm-12",
			ui: {
				pinImage: ".bb-pin-image",
				pinRadio: ".bb-pin-radio"
			},
			events: {
				"change input[name=pinToDashRadio]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange");

				this.isPin = this.options.isPin;

				var textKey = this.isPin ? "equipment.oneTouch.create.pin.pinText" : "equipment.oneTouch.create.pin.unpinText",
						isChecked = this.isPin ? this.model.get("isInDashboard") : !this.model.get("isInDashboard");
                
                this.model.shouldPin = true;
                
				this.radioView = new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						name: "pinToDashRadio",
						value: true,
						isChecked: isChecked,
						labelTextKey: textKey
					})
				});
			},
			onRender: function () {
				if (this.isPin) { // on left - workaround for mobile screens fitting both on the same level
					this.$el.addClass("col-xs-12");
					this.ui.pinImage.addClass("pin");
				} else {
					this.$el.addClass("col-xs-12");
				}

				this.ui.pinRadio.append(this.radioView.render().$el);
			},
			handleRadioChange: function () {
				// this only sets a flag on the device, it will be pinned or not pinned on Complete Click
				this.model.shouldPin = this.isPin;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Provisioning.Views.PinCustomContentView;
});
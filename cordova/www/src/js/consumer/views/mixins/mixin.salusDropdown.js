"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.salusView"
], function (App) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn,  $, _) {
		Mixins.SalusDropdown = function () {
			this.mixin([Mixins.SalusView]);

			this.setDefaults({
				childEvents: function () {
					return {
						"change:value": function (childView) {
							var selectedVal = childView.model.get("value");
                            var defaultVal = this.viewModel ? this.viewModel.get("labelTextKey") : "";

							// close the dropdown
							this.ui.dropdownToggle.dropdown("toggle");

							this.viewModel.set({
								dropdownLabelText: childView.model.get("text") || "",
								dropdownLabelKey: childView.model.get("displayText") || childView.model.get("textKey"),
								value: selectedVal
							});

							if (this.model && this.boundAttribute) {
								this.model.set(this.boundAttribute, selectedVal);
							}

							this.trigger("value:change", {'selectedValue': selectedVal});
							this.ui.dropdownToggle.focus();
                            
                            if (this.$("span.pull-left").text() !== App.translate(defaultVal)) {
                                this.$("label").addClass("show");
                                this.$("span.pull-left").addClass("label-value");
                            } else {
                                this.$("label").removeClass("show");
                                this.$("span.pull-left").removeClass("label-value");
                            }
						}
					};
				},
				templateHelpers: function () {
					return this.viewModel ? this.viewModel.attributes : {};
				}
			});

			this.before("initialize", function () {
				this.viewModel = this.options.viewModel;
				if (this.viewModel) {
					this.orignalDropdownLabelKey = this.viewModel.get("dropdownLabelKey");
					this.orignalDropdownLabelText = this.viewModel.get("dropdownLabelText");
				}

				this.boundAttribute = this.options.boundAttribute;
				this.listenTo(this.model, "change:" + this.boundAttribute, this.render);
				this.listenTo(this.viewModel, "change:dropdownLabelKey", this.render);
				this.listenTo(this.viewModel, "change:dropdownLabelText", this.render);
			});

			this.after("render", function () {
				// have the correct child be selected on render
				//The boundAttribute can have a null value so we need to check attributes for the key
				//instead of asking model.has(this.boundAttribute) which will return false on null values.
				if (this.model && _.has(this.model.attributes, this.boundAttribute)) {
					var value = this.model.get(this.boundAttribute);

					var selectedItem = this.collection.filter(function (item) {
						return value === item.get("value");
					});

					if (selectedItem.length > 0) {
						this.viewModel.set({
							dropdownLabelKey: selectedItem[0].get("textKey") || selectedItem[0].get("displayText"),
							dropdownLabelText: selectedItem[0].get("text"),
							value: value
						});
					} else if (this.orignalDropdownLabelKey || this.orignalDropdownLabelText) {
						//Set drop down to initial state "Select item..." text.
						this.viewModel.set({
							dropdownLabelKey: this.orignalDropdownLabelKey,
							dropdownLabelText: this.orignalDropdownLabelText,
							value: value
						});
					}
				}
                
                var defaultVal = this.viewModel ? this.viewModel.get("labelTextKey") : "";
                
                if (this.$("span.pull-left").text() !== App.translate(defaultVal)) {
                    this.$("label").addClass("show");
                    this.$("span.pull-left").addClass("label-value");
                } else {
                    this.$("label").removeClass("show");
                    this.$("span.pull-left").removeClass("label-value");
                }
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusDropdown;
});
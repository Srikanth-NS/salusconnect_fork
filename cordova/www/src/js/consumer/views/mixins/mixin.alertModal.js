"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.salusView"
], function (App, SalusViewMixin) {
	App.module("Consumer.Views.Mixins", function (Mixins) { //, App, B, Mn,  $, _
		Mixins.SalusAlertModalMixin = function () {
			this.mixin([SalusViewMixin]);

			this.addToObj({
				ui: {
					leftButton: ".left.modal-button",
					rightButton: ".right.modal-button",
					icon: ".icon",
					spinnerSection: ".bb-spinner-section"
				},
				regions: {
					rightButtonRegion: "#bb-right-btn-area",
					leftButtonRegion: "#bb-left-btn-area"
				}
			});

			this.setDefaults({
				showModal: function () {
					this.$el.modal('show');

					// Toggle arid-hidden to true so we can't tab outside the modal
					$("#bb-app-connected-solution").attr("aria-hidden", true);

					// Set the modal tab index to 0 so it joins the page's tab order
					this.$el.attr("tabindex", "0");
					this.$("#bb-primary-label").focus();
				},
				hideModal: function () {
					this.$el.modal('hide');

					// Set the modal tab index to -1 so it leaves the page's tab order
					this.$el.attr("tabindex", "-1");

					// Toggle arid-hidden to false to allow tabbing on the main app element
					$("#bb-app-connected-solution").attr("aria-hidden", false);
				},
				showInternalSpinner: function () {
					this.ui.spinnerSection.show();
				},

				hideInternalSpinner: function () {
					this.ui.spinnerSection.hide();
				},
				hideButtons: function () {
					this.$(".bb-button-row").hide();
					this.$("hr").hide();
				},
				hideXButton: function () {
					this.$("#bb-icon-large-close").addClass("vis-hidden");
				}
			});

			this.before("initialize", function () {
				this.leftButton = this.model.get("leftButton");
				this.rightButton = this.model.get("rightButton");

				if (this.options.staticBackdrop) {
					this.$el.attr("data-backdrop", "static");
					this.$el.attr("data-keyboard", false);
				}
			});

			this.after("render", function () {
				this.hideInternalSpinner();

				if (this.model.get("primaryLabelText")) {
					this.addBinding(null, "#bb-primary-label", "primaryLabelText");
				}
				if (this.model.get("primaryLabelText")) {
					this.addBinding(null, "#bb-secondary-label", "secondaryLabelText");
				}

				// show the button regions if they exist
				if (this.leftButton) {
					this.leftButtonRegion.show(this.leftButton);
				}
				if (this.rightButton) {
					this.rightButtonRegion.show(this.rightButton);
				}

				// depending on which regions are shown, position the buttons
				if (this.leftButton && this.rightButton) {
					this.leftButtonRegion.$el.addClass("col-xs-8 col-xs-push-2 col-sm-4");
					this.rightButtonRegion.$el.addClass("col-xs-8 col-xs-push-2 col-sm-4");
				} else if (this.leftButton) {
                    // Bug_CASBW-22
					this.leftButtonRegion.$el.addClass("col-xs-10 col-sm-8 col-xs-push-1 col-sm-push-2");
					this.ui.rightButton.addClass("hidden");
				} else if (this.rightButton) {
					this.ui.leftButton.addClass("hidden");
                    // Bug_CASBW-22
					this.rightButtonRegion.$el.addClass("col-xs-10 col-sm-8 col-xs-push-1 col-sm-push-2");
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusAlertModalMixin;
});
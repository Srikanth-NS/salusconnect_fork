"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Models", function (Models, App, B) {
		Models.DropdownViewModel = B.Model.extend({
			defaults: {
				id: null,
				dropdownLabelText: "",
				dropdownLabelKey: "",
                labelTextKey: ""
			}
		});

		Models.DropdownItemViewModel = B.Model.extend({
			defaults: {
				text: null,
				textKey: null,  //textKey is a replacement for displayText
				displayText: "", //deprecated by textKey:  will update everywere next
				value: ""
			}
		});

		Models.DropdownItemViewCollection = B.Collection.extend({
			model: Models.DropdownItemViewModel
		});
	});

	return App.Consumer.Models;
});
"use strict";

define([
	"app",
	"consumer/newUser/views/NewUserForm",
	"consumer/newUser/views/NewUserLayout"
], function (App) {

	return App.Consumer.NewUser.Views;
});
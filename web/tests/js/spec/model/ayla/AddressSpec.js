/*jslint jasmine: true */
define([
	"underscore",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SalusConnector",
	"common/model/ayla/Address"
], function (_, SpecHelper, AylaConfig, SaluConnector, AylaAddress) {
	'use strict';

	describe("Ayla Device Address ", function () {
		describe("when creating", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect calling fetch() to load data properly", function () {
				var addrObj = {
					"city": "san francisco",
					"country": "usa",
					"state": "CA",
					"street": "1 market",
					"zip": 94105
				};

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.address)({id: 56}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							{
								"addr": addrObj
							}
						);
					});

				var address = new AylaAddress({deviceId: 56});
				expect(address).propEquals("deviceId", 56);
				//note: construction calls fetch which should be synchronous, so this should be present
				expect(address).toMatchObjectPropertiesExcept(addrObj, ["deviceId"]);
			});

			it("expect calling save() to push data properly", function (done) {
				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.address)({id: 56}),
					function (proto, url, verb, data, success, fail, raw) {
						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							{
								"addr": {}
							}
						);
					});

				var address = new AylaAddress({deviceId: 56});
				var newAddr = {
					"city": "a",
					"country": "b",
					"state": "c",
					"street": "d",
					"zip": 1532
				};

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.address)({id: 56}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("PUT");
						expect(data).toBeTruthy();

						var pData = JSON.parse(data);
						expect(pData).toBeTruthy();
						expect(pData.addr).toBeTruthy();
						expect(_.keys(pData).length).toBe(1);

						var addr = pData.addr;
						expect(addr).toMatchObjectProperties(newAddr);
						done();
					});

				address.set(newAddr);
				address.persist();
			});
		});
	});
});
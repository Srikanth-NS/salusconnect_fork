/*jslint jasmine: true */
define([
	"underscore",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SalusConnector",
	"common/model/ayla/GroupCollection"
], function (_, SpecHelper, AylaConfig, SalusConnector, GroupCollection) {
	'use strict';

	describe("Ayla Group Collection ", function () {
		describe("when loading", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("should call load() to load all groups", function (done) {
				var group = {
						key: 52,
						name: "living room",
						device_count: 0
					},
					groups = [
						{
							"group": group
						}
					];

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.groups.list,
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(groups);
					});

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.groups.load)({id: 52}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						group.devices = [
							{"key": 34}
						];

						group.device_count = 1;

						success(
							{
								group: group
							}
						);
					});

				var groupCollection = new GroupCollection();

				groupCollection.load().then(function (result) {
					expect(groupCollection.length).toEqual(1);
					expect(groupCollection.first()).toMatchObjectPropertiesExcept(group, ["devices"]);
					expect(groupCollection.first().get('devices')).toEqual([34]);
					done();
				});
			});
		});
	});
});
/*jslint jasmine: true */
define([
	"app"
], function (app) {
	'use strict';

	describe("App Spec", function () {
		it("Expect App to not be null", function () {
			expect(app).not.toBeNull();
		});
	});
});

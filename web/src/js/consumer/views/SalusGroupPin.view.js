"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.GroupPin = Mn.ItemView.extend({
			template: consumerTemplates["equipment/myEquipment/salusGroupPin"],
      templateHelpers: function() {
        return {
          isPinned: this.getOption("isPinned") === true
        }
      },
			className: "salus-group-pin",
      ui: {
        pin: ".bb-pin-icon"
      },
			events: {
        "click .bb-pin-icon": "togglePin"
      },
      togglePin: function() {
        this.ui.pin.toggleClass("unpin-icon");
      },
      hasChanged: function() {
        return this.getOption("isPinned") !== this.getPinStatus();
      },
      getPinStatus: function() {
        return this.ui.pin.hasClass("unpin-icon") === false;
      }
		}).mixin([SalusViewMixin]);
	});
});

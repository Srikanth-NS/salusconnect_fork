"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/settings/views/EmptyList.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel"
], function (App, constants, consumerTemplates, SalusView, EmptyListView, CheckboxView, CheckboxViewModel) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * ShareContactListItem
		 * childView of SharesCustomContentView
		 * has a checkboxView with the share user's email
		 * TODO: Shares will be disabled for now
		 */
		Views.ShareContactListItem = Mn.ItemView.extend({
			template: false,
			className: "share-user-item col-xs-12 col-sm-5 col-sm-offset-1",
			initialize: function () {
				this.checkboxView = new CheckboxView({
					model: new CheckboxViewModel({
						id: "share-user-checkbox",
						nonKeyLabel: this.model.get("name"),
						secondaryIconClass: "",
						isChecked: false
					})
				});
			},
			onRender: function () {
				this.$el.append(this.checkboxView.render().$el);
			},
			onDestroy: function () {
				this.checkboxView.destroy();
			}
		}).mixin([SalusView]);

		/**
		 * SharesCustomContentView
		 * manages showing all share users the primary user has created
		 * TODO: Shares
		 */
		Views.SharesCustomContentView = Mn.CompositeView.extend({
			className: "share-custom-content row",
			template: consumerTemplates["setupWizard/provisioning/sharesCustomContent"],
			childView: Views.ShareContactListItem,
			childViewContainer: ".bb-shares-list",
			emptyView: EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				className: "col-xs-12 text-center",
				i18nTextKey: "setupWizard.provisioning.customize.noShares"
			},
			initialize: function () {
				this.collection = new B.Collection();

				// refresh
				this.dependencies = App.salusConnector.getShareCollection().refresh();
			},
			onRender: function () {
				var that = this;

				this.dependencies.then(function () {
					that.collection.add(App.salusConnector.getSharedUsersEmails().map(function (share) {
						return {name: share};
					}));
				});
			},
			getCheckedItems: function () {
				var array = [];

				if (!this.collection || this.collection.isEmpty()) {
					return array;
				}

				_.each(this.children._views, function (childView) {
					if (childView.checkboxView.getIsChecked()) {
						array.push(childView.model.get("name"));
					}
				});

				return array;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Provisioning.Views.SharesCustomContentView;
});
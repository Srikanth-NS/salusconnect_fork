"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, config, consumerTemplates, SalusView) {

	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn) {
		Views.WelcomePageTitle = Mn.ItemView.extend({
			template: consumerTemplates["setupWizard/welcomePageTitle"]
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Views.WelcomePageTitle;
});
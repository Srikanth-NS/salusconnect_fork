"use strict";

define([
	"app",
	"consumer/consumer.controller",
	"consumer/dashboard/views/DashboardLayout",
	"consumer/dashboard/views/Navigation.view"
], function (App, ConsumerController) {

	App.module("Consumer.Dashboard", function (Dashboard) {
		Dashboard.Controller = {
			"show": function () {
				App.Consumer.dashboardManager.loadData();

				var dashboardView = new App.Consumer.Dashboard.Views.DashboardPage({
					manager: App.Consumer.dashboardManager
				});

				ConsumerController.showLayout(dashboardView, true);
			},
			"nav": function () {
				var navigationView = new App.Consumer.Dashboard.Views.NavigationPage();

				ConsumerController.showLayout(navigationView, true);
			}
		};
	});

	return App.Consumer.Dashboard.Controller;
});
"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage"
], function (App, consumerTemplates, SalusPageMixin) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {
		Views.NavigationPage = Mn.LayoutView.extend({
			template: consumerTemplates["dashboard/navPage"],
			templateHelpers: function () {
				//Temporary until we have design for a better location for the build number
				return {
					build: window.build || "local"
				};
			},
			ui: {
				sendLogButton: ".bb-send-log-btn"
			},
			events: {
				"click @ui.sendLogButton": "_sendLogClicked",
			},
			onRender: function () {
				// only show send log button when we're on mobile
				if (window.cordova) {
					this.ui.sendLogButton.removeClass("hidden");
				}
			},
			_sendLogClicked: function () {
				if (window.cordova) {
					window.cordova.exec(
						// Register the callback handler
						null,
						// Register the errorHandler
						null,
						// Define what class to route messages to
						'ConnectLoggerPlugin',
						// Execute this method on the above class
						"sendErrorLogs",
						["sendLogs", null]
					);
				}
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "dev",
			analyticsPage: "navigation"
		});
	});

	return App.Consumer.Dashboard.Views.NavigationPage;
});

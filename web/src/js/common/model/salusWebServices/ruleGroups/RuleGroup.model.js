"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/salusWebServices/rules/RuleDetails.model"
], function (App, P, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.RuleGroupModel = B.Model.extend({
			defaults: {
				name: null,
				key: null,
				icon: null,
				rules: null, //array of {key: ruleKey}
				dsn: null,
				selected: false // local only
			},

			initialize: function (data, options) {
				if (options && options.dsn) {
					this.set("dsn", options.dsn);
				}
			},

			load: function (isLowPriority) {
				var that = this,
					url = _.template(AylaConfig.endpoints.ruleGroups.fetch)(this.toJSON());

				return App.salusConnector.makeAjaxCall(url, null, "GET", "json", null, {isLowPriority: isLowPriority}).then(function (data) {
					that.set(that.aParse(data));
				});
			},

			_buildPayload: function () {
				var payload = {
					name: null,
					icon: null,
					rules: null
				};

				if (this.get("key")) {
					payload.key = this.get("key");
				}

				payload.name = this.get("name");
				payload.icon = this.get("icon");
				payload.rules = this.get("rules");

				return payload;
			},

			add: function () {
				var that = this,
						url = _.template(AylaConfig.endpoints.ruleGroups.create)(this.toJSON()),
						rules = this.getDirtyRules();

				return App.salusConnector.makeAjaxCall(url, this._buildPayload(), "POST", "json").then(function (data) {
					that.set(that.aParse(data));

					// save all rules to get status condition
                    return App.salusConnector.getRuleGroupCollection().refresh().then(function() {
                        return P.all(_.map(rules, function (rule) {
                            return rule.update();
                        }));
					});
				});
			},

			update: function () {
				var that = this,
						url = _.template(AylaConfig.endpoints.ruleGroups.create)(this.toJSON()),
						rules = this.getDirtyRules();

				return App.salusConnector.makeAjaxCall(url, this._buildPayload(), "PUT", "json").then(function (data) {
					that.set(that.aParse(data));

					// save all rules to get status condition
					return P.all(_.map(rules, function (rule) {
						return rule.update();
					}));
				});
			},

			unregister: function () {
				var that = this,
					url = _.template(AylaConfig.endpoints.ruleGroups.delete)(this.toJSON());

				return App.salusConnector.makeAjaxCall(url, null, "DELETE", "text").then(function () {
					that.destroy();
				});
			},

			hasRule: function (rule) {
				var key;

				if (!rule) {
					return false;
				}

				if (rule instanceof B.Model) {
					key = rule.get("key");
				} else if (_.isString(rule)) {
					key = rule;
				}

				return !!_.findWhere(this.get("rules"), {key: key});
			},

			removeRule: function (rule) {
				if (this.hasRule(rule)) {
					var idx = _.findIndex(this.get("rules"), {key: rule.get("key")});

					// we know it's a valid idx
					this.get("rules").splice(idx, 1);

					//mark as dirty so the rule itself also saves
					rule.markAsDirty();

					return this.update();
				} else {
					return P.reject("Couldn't remove a rule that wasn't there");
				}
			},

			addRule: function (rule) {
				if (!this.hasRule(rule) && rule instanceof B.Model) {
					this.get("rules").push({key: rule.get("key")});

					//mark as dirty so the rule itself also saves
					rule.markAsDirty();

					return this.update();
				} else {
					return P.reject("We don't like your rule");
				}
			},

			aParse: function (data) {
				return {
					name: data.name,
					key: data.key,
					icon: data.icon,
					rules: data.rules
				};
			},

			getDirtyRules: function () {
				var dirtyRules = App.salusConnector.getRuleCollection().filter(function (rule) {
					return rule.isDirty();
				});

				// mark them all as clean since we have retrieved them
				_.each(dirtyRules, function (rule) {
					rule.markAsClean();
				});

				return dirtyRules;
			}
		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "rulesGroup_list"
		});
	});

	return App.Models.RuleModel;
});

